<!-- footer start -->
<footer class="mt-5">
    <div class="container mt-4">
        <div class="row align-items-center">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="logo-footer pb-3">
                    <img src="<?php echo MEDIA_PATH . $footer->footerLogo; ?>" class="img-fluid" alt="">
                </div>
                <ul class="nav social">
                    <?php if ($footer->facebook) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger" href="<?php echo $footer->facebook; ?>"><i class="fac fa fa-facebook "></i></a></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->twitter) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger" href="<?php echo $footer->twitter; ?>"><i class="fat fa fa-twitter "></i></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->youtube) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger n" href="<?php echo $footer->youtube; ?>"><i class="fau fa  fa-youtube fa-2x"></i></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->instagram) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger n" href="<?php echo $footer->instagram; ?>"><img src="<?php echo MEDIA_PATH; ?>img/Instagram Icon Top.png" class="img-fluid" alt=""></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->linkedin) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger n" href="<?php echo $footer->linkedin; ?>"><img src="<?php echo MEDIA_PATH; ?>img/Linkedin Icon Top.png" class="img-fluid" alt=""></a>
                        </li>
                    <?php } ?>
                </ul>
                <p class=" text-s text-dark mt-5"><?php echo str_replace("%YEAR%", date("Y"), $footer->copyright); ?></p>

            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="what-we-offer  ">
                    <div class="list ml-5 ">
                        <ul class="text-s">
                            <?php foreach ($footerMenus as $footerMenu) { ?>
                                <li><a class="font-weight-bold h5 text-dark " href="<?php echo!empty($footerMenu->footerMenuCategoryId) || $footerMenu->footerMenuCategoryId != "0" ? $footerMenu->footerMenuHref : base_url() . $footerMenu->footerMenuHref; ?>"><?php echo $footerMenu->footerMenuName; ?></a></li>
                            <?php } ?>
                            <li><span><?php echo $footer->emailText; ?></span> <?php echo $footer->email; ?></li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-12 col-12">
                <div class="form-footer p-4">
                    <form id="newsletter_form" action="<?php echo base_url('api/add_newsletter'); ?>" method="post" onsubmit="return false;">
                        <h5 class="text-center ff-bold"><?php echo $footer->newsLetterText; ?></h5>
                        <div class="form-group">
                            <input class="form-control1" type="text" name="subscriberName" placeholder="<?php echo $footer->newsLetterNamePlaceHolderText; ?>">
                        </div>
                        <div class="form-group">
                            <input class="form-control1" type="email" name="subscriberEmail" placeholder="<?php echo $footer->newsLetterEmailPlaceHolderText; ?>">
                        </div>
<!--                        <div class="form-group">
                            <input class="form-control1" type="text" name="subscriberPostcode" placeholder="<?php // echo $footer->newsLetterPostcodePlaceHolderText; ?>">
                        </div>-->
                        <button class="btn btn2 btn-danger btn-block" type="submit"><?php echo $footer->newsLetterButtonText; ?>
                        </button>
                        <button style="display: none;" type="reset">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container-fluid   bg-dan ">
            
    </div> -->
</footer>