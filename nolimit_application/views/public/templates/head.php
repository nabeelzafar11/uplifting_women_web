<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="<?php echo MEDIA_PATH . $header->favicon; ?>">
        <meta name="google-site-verification" content="Ne6X-T4vX91gsJdM3GOd2qL6uF8tQ6ZPvekHZ4RuVFI" />
        <title><?php echo $header->seoTitle; ?></title>
        <meta name="title" content="<?php echo $header->seoTitle; ?>">
        <meta name="description" content="<?php echo $header->seoDescription; ?>">
        <meta name="keywords" content="<?php echo $header->seoKeywords; ?>">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>newtheme/css/mobiscroll.jquery.min.css">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>newtheme/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>newtheme/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>newtheme/css/mdtimepicker.css">

        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>newtheme/css/wickedpicker.min.css">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>newtheme/css/mobiscroll.jquery.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH; ?>newtheme/css/jquery-ui.multidatespicker.css">
        <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH; ?>newtheme/css/style.css">
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

        <link href="<?php echo ASSETS_PATH; ?>newtheme/css/xdialog.min.css" rel="stylesheet"/>
        <script>
            var base_url = "<?php echo base_url(); ?>";
            var ASSETS_PATH = "<?php echo ASSETS_PATH; ?>";
        </script>
        <script type="text/javascript" src="https://w.sharethis.com/button/buttons.js"></script>
    </head>
