<body>
    <div id="myLoader" style="display: none;">
    </div>
    <!-- header start html -->
    <header id="header" class="">
        <div class="top-bar ">
            <div class="container">
                <ul class="nav">
                    <?php if ($footer->facebook) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger" href="<?php echo $footer->facebook; ?>"><i class="fac fa fa-facebook "></i></a></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->twitter) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger" href="<?php echo $footer->twitter; ?>"><i class="fat fa fa-twitter "></i></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->youtube) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger n" href="<?php echo $footer->youtube; ?>"><i class="fau fa  fa-youtube fa-2x"></i></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->instagram) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger n" href="<?php echo $footer->instagram; ?>"><img src="<?php echo MEDIA_PATH; ?>img/Instagram Icon Top.png" class="img-fluid" alt=""></a>
                        </li>
                    <?php } ?>
                    <?php if ($footer->linkedin) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-danger n" href="<?php echo $footer->linkedin; ?>"><img src="<?php echo MEDIA_PATH; ?>img/Linkedin Icon Top.png" class="img-fluid" alt=""></a>
                        </li>
                    <?php } ?>
                    <li class="nav-item index">
                        <a class="nav-link text-danger" href="<?php echo base_url(); ?>"><span class="home "><img src="<?php echo MEDIA_PATH; ?>img/Home Icon Top.png" class="img-fluid" alt=""></span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- navbar scond -->
        <!-- Grey with black text -->
        <nav class="navbar navbar-expand-md bg-light navbar-light ">
            <div class="container">
                <!-- Brand -->
                <a class="navbar-brand" href="<?php echo base_url();?>">
                    <span class="logo-img">
                        <img src="<?php echo MEDIA_PATH . $header->headerLogo; ?>" class="img-fluid" alt="">
                    </span>
                </a>
                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <!-- <span class="navbar-toggler-icon"></span> -->
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav nav-justified ff-mbold" style="min-width: 100%">
                        <?php foreach ($headerMenus as $hm) { ?>
                            <li class="nav-item">
                                <?php
                                //if  $hm->footerMenuCategoryId == 0 then its not a dynamic page
                                ?>
                                <a class="nav-link" href="<?php echo $hm->footerMenuCategoryId == 0 ? base_url() .  $hm->footerMenuHref : $hm->footerMenuHref; ?>"><?php echo $hm->footerMenuName; ?></a>
                            </li>
                        <?php } ?>
                        <?php if (!empty($admin) && $admin->typeId == 3) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url("profile");?>">User Profile</a>
                            </li>
                        <?php } ?>
                        <li class="nav-item">
                            <?php if (empty($admin)) { ?>
                                <a class="nav-link btn btn-danger" href="<?php echo base_url("signin"); ?>">Sign in</a>
                            <?php } ?>
                            <?php if (!empty($admin) && $admin->typeId < 3) { ?>
                                <a class="nav-link btn btn-danger" href="<?php echo base_url("admin/dashboard"); ?>">Admin Portal</a>
                            <?php } ?>
                            <?php if (!empty($admin) && $admin->typeId == 3) { ?>
                                <a class="nav-link btn btn-danger" href="<?php echo base_url("logout"); ?>">Sign Out</a>
                            <?php } ?>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <?php
    if ($this->session->flashdata('notification')) {
        $notification = $this->session->flashdata('notification');
    ?>
        <div class="ff-bold alert alert-<?php echo $notification["status"] ?>">
            <p><?php echo $notification["message"]; ?></p>
        </div>
    <?php } ?>