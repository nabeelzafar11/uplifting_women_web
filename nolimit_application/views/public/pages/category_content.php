<section class="sign-in">
	<div class="container">
		<div class="main-tital">
			<div class="flex-parent ">
				<div class="flex-child-edge"></div>
				<div class="flex-child-text">
					<h1 class="text-center text-uppercase  ff-exbold"><?php echo $currentCategory->categoryName; ?></h1>
				</div>
				<div class="flex-child-edge"></div>
			</div>
		</div>
	</div>
</section>
<!-- main title -->
<section class="banner-about">
	<div class="banner-about">
		<img src="<?php echo ASSETS_PATH ?>uploaded_media/<?php echo $currentCategory->categoryImage; ?>" class="img-fluid dynamic-image " alt="">
	</div>
</section>
<section class="woman bg-white height500px">
	<div class="container text-center">
		<div class="wraper py-5">
			<div class="row align-self-start">
				<?php if ($currentCategory->categoryShortDescription) { ?>
					<?php echo html_entity_decode($currentCategory->categoryShortDescription); ?>
					<div class="col-lg-9 col-md-12 text-left">
						<?php echo html_entity_decode($currentCategory->categoryDescription); ?>
					</div>
				<?php } else { ?>
					<div class="col-lg-12 col-md-12 text-left">
						<?php echo html_entity_decode($currentCategory->categoryDescription); ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>