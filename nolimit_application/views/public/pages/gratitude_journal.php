
<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="gallery">
                <h1 class="text-center text-uppercase ff-blod font-weight-bold">GRATITUDE JOURNAL</h1>
            </div>
        </div>
    </div>
</section>
<section class="comment-block py-5">
    <div class="container">
        <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 bg-white">
                <h2 class="text-center ff-bold gard"> <span class=""><?php echo!empty($header->gratitude_mobile) ? $header->gratitude_mobile : ""; ?></span></h2> 
            </div>
        </div>
        <div class="row my-5">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="bg-white p-4">
                    <p readonly id="feild_h1_heading" class="notes_gratitude twxt-clr  " style="    height: 250px;overflow-y: auto;" placeholder="Things I am greateful for today"><?php echo!empty($default_type1->header) ? "<span class='gratitude_h'>" . $default_type1->header . "</span><br />" : "<br />"; ?><?php echo!empty($default_type1->message) ? $default_type1->message : ""; ?></p>
                </div>

                <input type="readonly" readonly class="feild_h1_date" id="birthday" name="birthday" value="<?php echo!empty($default_type1->date) ? date_format(date_create($default_type1->date), "m/d/Y") : ""; ?>" />
                <span class="float-right"><a href="#" id="edit-icon-h1"  data-toggle="modal" data-target="#h1modal"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Edit Icon.png" alt=""></a></span>

            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="bg-white p-4 ">
                    <p readonly id="feild_h2_heading" class=" notes_gratitude   " style="    height: 250px;overflow-y: auto;" placeholder="Things I am greateful for today"><?php echo!empty($default_type2->header) ? "<span class='gratitude_h'>" . $default_type2->header . "</span><br />" : "<br />"; ?><?php echo!empty($default_type2->message) ? $default_type2->message : ""; ?></p>
                </div>


                <input type="readonly"  readonly class="feild_h2_date" id="birthday" name="birthday" value="<?php echo!empty($default_type2->date) ? date_format(date_create($default_type2->date), "m/d/Y") : ""; ?>">
                <span class="float-right"><a href="#" id="edit-icon-h2"  data-toggle="modal" data-target="#h2modal"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Edit Icon.png" alt=""></a></span>

            </div>
            <div class="col-lg-4 col-md-6 col-12 ">
                <div class="bg-white p-4 ">
                    <p readonly id="feild_h3_heading" class="twxt-clr notes_gratitude   " style="    height: 250px;overflow-y: auto;" placeholder="Things I am greateful for today"><?php echo!empty($default_type3->header) ? "<span class='gratitude_h'>" . $default_type3->header . "</span><br />" : "<br />"; ?><?php echo!empty($default_type3->message) ? $default_type3->message : ""; ?></p>
                </div>

                <input type="readonly"  readonly class="feild_h3_date"   id="birthday" name="birthday" value="<?php echo!empty($default_type3->date) ? date_format(date_create($default_type3->date), "m/d/Y") : ""; ?>">
                <span class="float-right"><a href="#" id="edit-icon-h3"  data-toggle="modal" data-target="#h3modal"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Edit Icon.png" alt=""></a></span>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <!-- Modal -->
    <!-- Modal -->
    <div class="modal fade" id="h1modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">

                <div class="modal-body  ">
                    <span class=" font-weight-bold">DATE :</span> 
                    <input class="float-right" value="<?php echo date_format(date_create($default_type1->date), 'm-d-Y'); ?>" id="h1_date" name="birthday">
                    <div class="form-group">
                        <textarea class="form-control text-white" rows="2" placeholder="Things I am greateful for today" id="h1_heading"></textarea>
                    </div>

                    <div class="form-group">

                        <textarea class="form-control text-white" rows="4" placeholder="please start writing here" id="h1_message"></textarea>
                    </div>
                </div>


                <div class="modal-footer text-center">
                    <a class="btn btn-danger" id="modal-1-submit-button" href="#">Save</a>
                    <button class="btn btn-danger" id="modal-1-delete-button"  data-target="#h1Confirmationmodal">Delete</button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="h2modal" tabindex="-1" role="dialog" aria-labelledby="h2modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">

                <div class="modal-body  ">
                    <span class=" font-weight-bold">DATE :<span> 

                            <input class="float-right" value="<?php echo date_format(date_create($default_type2->date), 'm/d/Y'); ?>" id="h2_date" name="birthday">
                            <span > 
                            </span>
                        </span>
                        <div class="form-group">

                            <textarea class="form-control text-white" rows="2" placeholder="Things I am greateful for today" id="h2_heading"></textarea>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control text-white" rows="4" placeholder="please start writing here" id="h2_message"></textarea>
                        </div>
                </div>


                <div class="modal-footer text-center">
                    <a class="btn btn-danger" id="modal-2-submit-button" href="#">Save</a>
                    <a class="btn btn-danger" id="modal-2-delete-button"  data-target="#h2Confirmationmodal">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="h3modal" tabindex="-1" role="dialog" aria-labelledby="h3modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">

                <div class="modal-body  ">
                    <span class=" font-weight-bold">DATE :<span> 
                            <input class="float-right" value="<?php echo date_format(date_create($default_type3->date), 'm/d/Y'); ?>" id="h3_date" name="birthday">
                            <span > 
                            </span>
                        </span>
                        <div class="form-group">
                            <textarea class="form-control text-white" rows="2" placeholder="Things I am greateful for today" id="h3_heading"></textarea>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control text-white" rows="4" placeholder="please start writing here" id="h3_message"></textarea>
                        </div>
                </div>


                <div class="modal-footer text-center">
                    <a class="btn btn-danger" id="modal-3-submit-button"  data-target="#h3Confirmationmodal" href="#">Save</a>
                    <a class="btn btn-danger" id="modal-3-delete-button">Delete</a>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    
    
      <div class="modal fade" id="h1Confirmationmodal" tabindex="-1" role="dialog" aria-labelledby="deletethiseventmodal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="text-massage ">
                    <h6 class="ff-bold text-center"></h6>
                    <p>Are you sure, you want to delete this event?</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-dismiss="modal" id="deletethisevent1">Yes</a>
                    <a class="btn btn-danger" data-dismiss="modal">Cancel </a>
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal"></button>
                       <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    
      <div class="modal fade" id="h2Confirmationmodal" tabindex="-1" role="dialog" aria-labelledby="deletethiseventmodal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="text-massage ">
                    <h6 class="ff-bold text-center"></h6>
                    <p>Are you sure, you want to delete this event?</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-dismiss="modal" id="deletethisevent2">Yes</a>
                    <a class="btn btn-danger" data-dismiss="modal">Cancel </a>
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal"></button>
                       <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    
      <div class="modal fade" id="h3Confirmationmodal" tabindex="-1" role="dialog" aria-labelledby="deletethiseventmodal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="text-massage ">
                    <h6 class="ff-bold text-center"></h6>
                    <p>Are you sure, you want to delete this event?</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-dismiss="modal" id="deletethisevent3">Yes</a>
                    <a class="btn btn-danger" data-dismiss="modal">Cancel </a>
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal"></button>
                       <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .ui-datepicker-current-day a {
        border: 1.5px solid #fd5592 !important;
        border-radius: 10px !important;
        padding: 6px;
    }
    .ui-datepicker-calendar td {
        border: 1px solid transparent;
        padding: 8px;
        color: black !important;
    }
    #ui-datepicker-div {
        background: white;
        padding: 15px;
    }
    .ui-state-default {
        color: black;
    }

    .chosen-dates a {
        padding: 7px;
    }
</style>
<script>
    var h1_title = "<?php echo $default_type1->header; ?>";
    var h1_message = "<?php echo $default_type1->message; ?>";
    var h1_date = "<?php echo $default_type1->date; ?>";
    var h2_title = "<?php echo $default_type2->header; ?>";
    var h2_message = "<?php echo $default_type2->message; ?>";
    var h2_date = "<?php echo $default_type2->date; ?>";
    var h3_title = "<?php echo $default_type3->header; ?>";
    var h3_message = "<?php echo $default_type3->message; ?>";
    var h3_date = "<?php echo $default_type3->date; ?>";
    var currentGratitudeLocation = "<?php echo base_url("gratitude_journal");?>";
</script>
<script src="<?php echo ASSETS_PATH . "js/dateformat.js" ?>"></script>
<script src="<?php echo ASSETS_PATH . "js/custom/public/gratitude_journal.js" ?>"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>