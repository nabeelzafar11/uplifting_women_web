<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">Sign UP</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<!-- main title -->
<!-- login section -->
<section class="login-form">
    <div class="woraper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <form id="signup_form" name="signup_form" method="post" action="<?php echo base_url("api/register"); ?>" onsubmit="return false;">
                        <div class="form-signup p-4">
                            <div class="alert"></div>
                            <div class="form-group">
                                <label for="name" class="ff-bold">First Name</label>
                                <input type="text" placeholder="First Name" class="form-control-1" name="firstName" id="f-name">
                            </div>
                            <div class="form-group">
                                <label for="name" class="ff-bold">Last Name</label>
                                <input type="text" placeholder="Last Name" class="form-control-1" id="l-name" name="lastName">
                            </div>
                            <div class="form-group">
                                <label for="email" class="ff-bold">Email</label>
                                <input type="email" placeholder="Email" class="form-control-1" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="pwd" class="ff-bold">Password</label>
                                <input type="password" placeholder="Password" class="form-control-1" id="password" name="password">
                            </div>
                            <div class="form-group">
                                <label for="pwd" class="ff-bold">Retype Password</label>
                                <input type="password" placeholder="Retype Password" class="form-control-1" id="pwd1" name="confirmPassword">
                            </div>
                        </div>
                        <div class="button text-center">
                            <input type="submit" class="btn btn-light ff-bold btn-block mb-3" value="Sign Up" />
                        </div>
                    </form>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<!-- login secton -->
<script src="<?php echo ASSETS_PATH . "js/custom/public/signup.js" ?>"></script>