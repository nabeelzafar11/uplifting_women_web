<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">Add Item</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<!-- login section -->
<section class="login-form">
    <div class="woraper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <div class="card  box">
                        <div class="card-header bg-modal-a " style="height: 70px;">
                            <div class="d-flex justify-content-center align-items-center m-auto">

                            </div>
                            <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
                        </div>
                        <div class="card-body bg-modal-a">
                            <ul>
                                <?php
                                if (!empty($list)) {
                                    foreach ($list as $l) {
                                ?>
                                        <li class="bg-modle-li-1 removeItem-<?php echo $l->id; ?>"><a data-id="<?php echo $l->id; ?>" data-message="<?php echo $l->message; ?>" class="text-dark item additemselflove"><?php echo $l->message; ?><span class="float-right "><img src="<?php echo ASSETS_PATH ?>newtheme/img/add-icon.png" class="img-fluid" style=" margin-top: -4px; margin-right: -60px;" alt=""></span></a></li>
                                <?php
                                    }
                                }
                                ?>
                                <li class="bg-modle-li-1 customitemselflove"><a class="text-dark custom">Custom<span class="float-right "><img src="<?php echo ASSETS_PATH ?>newtheme/img/add-icon.png" class="img-fluid" style=" margin-top: -4px; margin-right: -60px;" alt=""></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div data-backdrop="static" data-keyboard="false" class="modal fade modal-open" id="event" tabindex="-1" role="dialog" aria-labelledby="event" aria-hidden="true">
            <div class="modal-dialog  " role="document">
                <div class="modal-content ">
                    <div class="modal-header ">
                        <h5 class="modal-title text-title" id="exampleModalScrollableTitle">Add Event</h5>
                    </div>
                    <div class="modal-body    ">
                        <div class="container">
                            <div class="row align-items-end">
                                <div class="col-12">
                                    <form class="form-inline" action="/action_page.php">
                                        <div class="form-group">
                                            <label for="email">Title :</label>
                                            <input id="titleplaceholder" readonly="" type="email" placeholder="" class="bor-1 form-control-1 ml-4">
                                        </div>
                                    </form>
                                    <div class='datepicker'>
                                        <div class="datepicker-header"></div>
                                    </div>

                                    <div class="modal-footer bg-white">
                                        <a class="btn btn-danger addeventbutton">Add Event</a>
                                        <a class="btn btn-danger canceleventmodal" data-toggle="modal" data-target="#date" href="#">Cancel</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div data-backdrop="static" data-keyboard="false" class="modal fade modal-open" id="custom" tabindex="-1" role="dialog" aria-labelledby="custom" aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content ">
                    <div class="modal-header ">
                        <h5 class="modal-title text-title" id="exampleModalScrollableTitle">Add Custom Event</h5>
                    </div>
                    <div class="modal-body  bg-white h-75">
                        <div class="container">
                            <div class="row align-items-end">
                                <div class="col-12">
                                    <form class="form-inline" action="javascript:void(0);">
                                        <div class="form-group">
                                            <label for="email">Title :</label>
                                            <input type="text" placeholder="Please enter your custom title" class="bor-1 form-control-1 ml-4" id="customtitle">
                                        </div>
                                    </form>
                                    <div class='datepicker'>
                                        <div class="datepicker-header"></div>
                                    </div>
                                    <div class="modal-footer bg-white">
                                        <a class="btn btn-danger mt-3 addeventbutton">Add Event</a>
                                        <a class="btn btn-danger cancelcustom">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div data-backdrop="static" data-keyboard="false" class="modal fade modal-open" id="timemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content ">
                    <div class="modal-header ">
                        <h5 class="modal-title text-clr" id="exampleModalScrollableTitle">Choose A Time</h5>
                    </div>
                    <div class="modal-body  bg-white ">
                        <div class="card-body modeltime" style="display: grid;">
                        </div>
                    </div>
                    <div class="modal-footer bg-white">
                        <a class="btn btn-danger" id="saveevents">Save</a>
                        <a class="btn btn-danger canceltimemodal" data-toggle="modal" data-target="#date" href="#">Cancel</a>

                    </div>
                </div>
            </div>
        </div>
</section>
<script>
    var current_month_id = "<?php echo $curr_month; ?>";
    var current_year = "<?php echo date("Y"); ?>";
    var formattedDate = new Date(current_month_id + "/01/" + current_year);

</script>
<script src="<?php echo ASSETS_PATH . "js/custom/public/add_self_love.js" ?>"></script>