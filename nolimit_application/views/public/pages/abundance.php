<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">Abundance</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<section class="login-form">
    <div class="woraper">
        <div class="container">
            <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <div class="card  box">
                        <div class="card-header">
                            <div class="d-flex justify-content-center align-items-center m-auto">
                                <a href="javascript:void(0);"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Add Btn.png" class="img-fluid" alt=""></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul id="abundanceNewList">
                                <?php foreach ($add_new_items_list as $new_list) { ?>
                                    <li class="bg-modle-li-1" id="remove-newlist<?php echo $new_list->id ?>"><a href="#" data-id="<?php echo $new_list->id ?>" data-message="<?php echo $new_list->message ?>" class="text-dark abundanceNewListItem"><?php echo $new_list->message ?><span class="float-right "><img src="<?php echo ASSETS_PATH ?>newtheme/img/add-icon.png" class="img-fluid" style=" margin-top: -4px; margin-right: -60px;" alt=""></span></a></li>
                                <?php } ?>
                                <li class="bg-modle-li-1"><a href="#" data-toggle="modal" data-target="#custom" class="text-dark">Custom<span class="float-right "><img src="<?php echo ASSETS_PATH ?>newtheme/img/add-icon.png" class="img-fluid" style=" margin-top: -4px; margin-right: -60px;" alt=""></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>
</section>
<!-- data-backdrop="static" -->
<!-- data-keyboard="false" -->
<div class="modal fade " id="Abundance1" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-scrollable" role="document">
        <div class="modal-content bg-modal-a">
            <div class="modal-header border-0">
                <h5 class="modal-title  text-dark" id="exampleModalScrollableTitle">Add Item</h5>
            </div>
            <div class="modal-body   bg-modal-a ">
                <div class="card-body">
                    <ul id="abundanceNewList">
                        <?php foreach ($add_new_items_list as $new_list) { ?>
                            <li class="bg-modle-li-1" id="remove-newlist<?php echo $new_list->id ?>"><a href="#" data-id="<?php echo $new_list->id ?>" data-message="<?php echo $new_list->message ?>" class="text-dark abundanceNewListItem"><?php echo $new_list->message ?><span class="float-right "><img src="<?php echo ASSETS_PATH ?>newtheme/img/add-icon.png" class="img-fluid" style=" margin-top: -4px; margin-right: -60px;" alt=""></span></a></li>
                        <?php } ?>
                        <li class="bg-modle-li-1"><a href="#" data-toggle="modal" data-target="#custom" class="text-dark">Custom<span class="float-right "><img src="<?php echo ASSETS_PATH ?>newtheme/img/add-icon.png" class="img-fluid" style=" margin-top: -4px; margin-right: -60px;" alt=""></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content text-center">
            <div class="text-massage ">
                <input type="hidden" value="" id="abundanceID" />
                <h6 class="ff-bold text-center">Message</h6>
                <p>Are you sure, you want to delete this event</p>
            </div>
            <div class="modal-footer bg-white">
                <a onclick="delete_abundance_item()" href="javascript:void(0);" class="btn btn-danger" href="#">Yes</a>
                <a class="btn btn-danger" data-dismiss="modal" href="#">No </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="custom" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content text-center">
            <div class="text-massage p-5">
                <h6 class="ff-bold text-center">Add Custom Event</h6>
                <div class="form-inline d-flex">
                    <label>Title</label>
                    <input type="text" class="form-control1" id="pwd">
                </div>
            </div>
            <div class="modal-footer bg-white">
                <a onclick="add_abundance_item()" href="javascript:void(0);" class="btn btn-danger" href="#">Create</a>
                <a class="btn btn-danger" data-dismiss="modal" href="#">Cancel</a>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo ASSETS_PATH . "js/custom/public/abundance.js" ?>"></script>