<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">Forgot Password</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<!-- login section -->
<section class="login-form">
    <div class="woraper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <form id="reset_password" action="<?php echo base_url("api/forgot_password"); ?>" method='post'  onsubmit="return false;">
                        <div class="alert"></div>
                        <div class="form-forgot p-4">

                            <div class="form-group">
                                <label for="email" class="ff-bold">Please Enter your Email address and click on the submit button. We will send a reset password email link on the provided email address.</label>
                                <input type="email" name='email' placeholder="Email address" class="form-control" id="email">
                            </div>
                        </div>
                        <div class="button text-center">
                            <input type="submit" class="btn btn-light btn-block mb-3" value="Submit" />
                        </div>
                    </form>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo ASSETS_PATH . "js/custom/public/reset_password.js" ?>"></script>