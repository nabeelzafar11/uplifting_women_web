<section class=" ">
    <div class="slider">
        <div id="carouselExampleCaptions1" class="carousel slide" data-ride="carousel">

            <div class="">
                <?php foreach ($homeSliders as $homeSlider) { ?>
                    <div class="row">
                        <img src="<?php echo MEDIA_PATH . $homeSlider->homeSliderImage; ?>" class="d-block w-100" alt="...">
                        <div class="col-sm-5 col-md-4 carousel-caption d-sm-block  text-dark ">
                            <h1 class="ff-exbold display-4"><?php echo $homeSlider->homeSliderTitleOne; ?></h1>
                            <p class="ff-mbold"><?php echo htmlspecialchars_decode(stripslashes($homeSlider->homeSliderTitleTwo)); ?></p>
                        </div>
                    </div>

                <?php } ?>
            </div>
            <?php if (sizeof($homeSliders) > 1) { ?>
                <a class="carousel-control-prev " href="#carouselExampleCaptions1" role="button" data-slide="prev"><span class="carousel-control   ff-bold"><i class="fa  fa-long-arrow-left fa-2x"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleCaptions1" role="button" data-slide="next"><span class="carousel-control   ff-bold"><i class="fa  fa-long-arrow-right fa-2x"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            <?php } ?>
        </div>
    </div>
</section>
<section class="woman bg-white">
    <div class="container text-center">
        <div class="wraper ">
            <div class="row">
                <div class="col-lg-4">
                    <?php echo htmlspecialchars_decode(stripslashes($homeData->homeDataAboutTitle)); ?>
                </div>
                <div class="col-lg-8">
                    <p class="text-s"><?php echo htmlspecialchars_decode(stripslashes($homeData->homeDataAboutDescription)); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<?php if (!empty($thirdsection)) {
    ?> 
    <section class="">
        <div class="owl-slider">
            <div class="container ">
                <div id="owl-one" class="owl-carousel  py-5">
                    <?php foreach ($thirdsection as $t) { ?>
                        <div class="card text-center">
                            <div class="text-card">
                                <p class="text-ss testimonialComment"><?php echo $t->testimonialComment; ?></p>
                            </div>
                            <p class="text-ss"><?php echo $t->testimonialName; ?></p>

                            <?php if (!empty($t->testimonialOccupation) && trim($t->testimonialOccupation) != "#") { ?>
                                <button class="btn  default ff-bold  text-uppercase"><a href="<?php echo $t->testimonialOccupation; ?>">Read More</a></button>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<section class="woman-bg">
    <div class="container text-center">
        <div class="wraper ">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo htmlspecialchars_decode(stripslashes($homeData->home_fourth_section_title)); ?>
                    <?php if (!empty($homeData->home_fourth_section_href) && trim($homeData->home_fourth_section_href) != "#") { ?>
                        <button class="btn  btn-light ff-bold mt-4  "><a href="<?php echo $homeData->home_fourth_section_href; ?>">Learn More</a></button>
                    <?php } ?>
                </div>
                <mboldv>
            </div>
        </div>
</section>
<!-- testimoanl slider -->
<?php if (!empty($testimonials)) { ?>
    <section class="testi-slider py-5">
        <div class="container-fluid">
            <div class="test-wraper py-4">
                <div class="testiomal-heading text-center pb-4">
                    <h1 class="ff-exbold"><?php echo $homeData->homeDataTestimonialTitle; ?></h1>
                    <p class="ff-mbold"><?php echo $homeData->homeDataTestimonialSubTitle; ?></p>
                </div>
            </div>
            <div class="slider-2">

                <div id="carouselExampleCaptions" class="carousel slide">
                    <ol class="carousel-indicators">
                        <?php for ($i = 0; $i < sizeof($testimonials); $i++) { ?>
                            <li data-target="#carouselExampleCaptions" data-slide-to="<?php echo $i; ?>" <?php $i == 0 ? 'class="active"' : ""; ?>></li>
                        <?php } ?>
                    </ol>
                    <div class="col-lg-12">
                        <div class="carousel-inner">
                            <?php
                            $i = 0;
                            foreach ($testimonials as $t) {
                                ?>
                                <div class="carousel-item <?php echo $i == 0 ? 'active' : ""; ?>">
                                    <div class="row align-items-center">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <p class="text-left testimonialComment text-s"><?php echo $t->testimonialComment; ?></p>
                                            <p class="text-left text-s">- <?php echo $t->testimonialName; ?></hp>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
<?php } ?>
<style>
    .woman-bg {
        /*background-image: url("<?php echo MEDIA_PATH . $homeData->home_fourth_section_image; ?>");*/
        background-color: white;
    }
</style>