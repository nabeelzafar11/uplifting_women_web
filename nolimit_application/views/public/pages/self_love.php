
<section class="blucklist wraper-blucklist-j">
    <div class="container text-center">
        <div class="wraper-blucklist  ">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12 col-12 py-3">
                    <form method="post" id="delete_event" action="<?php echo base_url("api/user/delete_user_selflovecare_list"); ?>" onsubmit="return false;">
                        <input type="hidden" name="id" value="" id="delete">
                    </form>
                    <div class=" p-2">
                        <div>
                            <?php if ($curr_month > 1) { ?>
                                <p style='float:left'><a href='<?php echo base_url("self_love") ?>?id=<?php echo encode($curr_month - 1); ?>'><i class="arrow-left arrow left"></i></a></p>
                            <?php } ?>
                            <?php if ($curr_month < 12) { ?>
                                <p style='float:right'><a href='<?php echo base_url("self_love") ?>?id=<?php echo encode($curr_month + 1); ?>'><i class="arrow-right arrow right"></i></a></p>
                            <?php } ?>
                        </div>
                        <div class="bg-white ff-bold text-dark h1 font-weight-bold text-uppercase"><?php echo!empty($selflovecare_dashboard->web_header) ? $selflovecare_dashboard->web_header : ""; ?></div>
                    </div>
                    <div class=" p-2">
                        <div class="p-1 bg-white ff-bold p text-dark"><?php echo!empty($selflovecare_dashboard->web_message) ? $selflovecare_dashboard->web_message : ""; ?></div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="card h-100">
                        <div class="card-header">
                            <div class="d-flex justify-content-center align-items-center m-auto">
                                <a href="<?php echo base_url("self-love-self-care-list/add_item") ?>?id=<?php echo encode($curr_month); ?>"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Add Btn.png" class="img-fluid" alt=""></a>	
                            </div>
                            <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
                        </div>
                        <div class="card-body">
                            <?php foreach ($user_list as $u) { ?>
                                <div class=" bg-modle-li-2 d-flex justify-content-between my-4 alert-white   fade show removeItem-<?php echo $u->id ?>" role="alert">
                                    <span class="ff-mbold text-clr"><?php echo $u->message; ?></span>
                                    <button  type="button" data-toggle="modal" data-target="#exampleModalScrollable" data-id="<?php echo $u->id; ?>"  data-message="<?php echo $u->message; ?>" class="close item removeItem btn-1" aria-label="Close" data-id="<?php echo $u->id ?>">
                                        <span aria-hidden="true"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Remove Circle.png" class="  img-fluid" alt=""></span>
                                    </button>
                                </div> 
                                <?php
                            }
                            if (empty($user_list)) {
                                ?>
                                <div class="alert alert-white alert-dismissible fade show" role="alert">
                                    <h5 class="ff-mbold text-clr"></h5>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                    </div>
                </div>

            </div>	
        </div>
    </div>
</section> 
<div data-backdrop="static" data-keyboard="false" class="modal fade modal-open" id="event" tabindex="-1" role="dialog" aria-labelledby="event" aria-hidden="true">
    <div class="modal-dialog  " role="document">
        <div class="modal-content ">
            <div class="modal-header ">
                <h5 class="modal-title text-title" id="exampleModalScrollableTitle">Edit Event</h5>
            </div>
            <div class="modal-body    ">
                <div class="container">
                    <div class="row align-items-end">
                        <div class="col-12">
                            <form class="form-inline" action="/action_page.php">
                                <div class="form-group">
                                    <label for="email">Title :</label>
                                    <input id="titleplaceholder" readonly="" type="email" placeholder="" class="form-control ml-4">
                                </div>
                            </form>
                            <div class='datepicker'>
                                <div class="datepicker-header"></div>
                            </div>

                            <div class="modal-footer bg-white">
                                <a class="btn btn-danger addeventbutton">Edit Event</a>
                                <a class="btn btn-danger canceleventmodal" data-toggle="modal"  data-target="#date" href="#">Cancel</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-open" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content text-center">
            <div class="text-massage ">
                <h6 class="ff-bold text-center"></h6>
            </div>
            <div class="modal-footer bg-white">
                <a  class="btn btn-danger edit-this-event  additemselflove" href="#">Edit This Event</a>
                <a class="btn btn-danger delete-this-event" href="#">Delete This Event</a>
                <a class="btn btn-danger" data-dismiss="modal" href="#">Cancel</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-open" id="exampleModalScrollable2" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content text-center">
            <div class="text-massage ">
                <h6 class="ff-bold text-center"></h6>
                <p>Are you sure, you want to delete this event?</p>
            </div>
            <div class="modal-footer bg-white">
                <a onclick="delete_event()" class="btn btn-danger" href="javascript:void(0);">Yes</a>
                <a class="btn btn-danger" data-dismiss="modal" href="#">No  </a>
            </div>
        </div>
    </div>
</div>
<div data-backdrop="static" data-keyboard="false" class="modal fade modal-open" id="timemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content ">
            <div class="modal-header ">
                <h5 class="modal-title text-clr" id="exampleModalScrollableTitle">Choose A Time</h5>
            </div>
            <div class="modal-body  bg-white ">
                <div class="card-body modeltime" style="display: grid;">
                </div>
            </div>
            <div class="modal-footer bg-white">
                <a class="btn btn-danger" id="saveevents">Save</a>
                <a class="btn btn-danger canceltimemodal" data-toggle="modal"  data-target="#date" href="#">Cancel</a>

            </div>
        </div>
    </div>
</div>
<style>
    .wraper-blucklist-j
    {
        background-image: url('<?php echo ASSETS_PATH . "uploaded_media/" . $selflovecare_dashboard->imageWeb; ?>') !important;
    }
    .arrow {
        border: solid #66c8c9;
        border-width: 0 4px 4px 0;
        display: inline-block;
        padding: 5px;
        margin: 15px;
    }

    .arrow-right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }

    .arrow-left  {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }
</style>
<script>
    var current_month_id = "<?php echo $curr_month; ?>";
    var current_year = "<?php echo date("Y"); ?>";
    var formattedDate = new Date(current_month_id + "/01/" + current_year);
</script>
<script src="<?php echo ASSETS_PATH . "js/custom/public/self_love.js" ?>"></script>