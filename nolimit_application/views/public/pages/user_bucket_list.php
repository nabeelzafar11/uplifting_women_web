<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="gallery">
                <h1 class="text-center text-uppercase ff-blod font-weight-bold">Shared bucket list</h1>
                <h5>Bucket list shared by: <?php echo $user_email;?> </h5>
            </div>
        </div>
    </div>
</section>
<!-- data-backdrop="static" -->
<!-- data-keyboard="false" -->
<section class="blucklist">
    <div class="container text-center">
        <div class="wraper-blucklist  ">
            <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="card h-100 box">
                        <div class="card-header">
                            <div class="d-flex justify-content-center align-items-center m-auto">
                                <a href="#"></a>
                            </div>
                        </div>
                        <ul class="card-body" id="abundanceList">
                            <?php
                            $message_detail = "";
                            foreach ($users_list as $list) {
                                $message_detail .= $list->message . "<br/>";
                                ?>
                                <li  id="remove-list<?php echo $list->id ?>" class="bg-modle-li-2 alert-white   fade show d-flex justify-content-between"  >
                                    <span><?php echo $list->message; ?></span>
                                </li>
<?php } ?>
                        </ul>
                        <!--                        <div class="sharethis-inline-share-buttons"></div>-->
                        <span id="button_1"></span>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo ASSETS_PATH . "js/custom/public/bucket_list.js" ?>"></script>