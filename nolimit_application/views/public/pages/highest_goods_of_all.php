<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="gallery">
                <h1 class="text-center text-uppercase ff-blod font-weight-bold">highest good of all</h1>
            </div>
        </div>
    </div>
</section>
<section class="blucklist2">
    <div class="container text-center">
        <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
        <div class="wraper-blucklist ">
            <div class="row">
                <div class="col-lg-7 py-5">
                    <div class="bg-white hig-p">
                        <p class="ff-reg p-4 text-left "><?php echo!empty($header->highestgoodofall_mobile) ? $header->highestgoodofall_mobile : ""; ?></p>
                    </div>
                    <button type="button" class="btn my-btn rounded-pill float-left ff-bold mt-3" data-toggle="modal" data-target="#exampleModalScrollable">
                        Add item
                    </button>


                </div>
                <div class="col-lg-5 py-5">
                    <div class="bg-white p-3  ">
                        <ul id="goodList">
                            <?php foreach ($records as $record) { ?>
                                <li style="text-align: start;" id="remove-<?php echo $record->id ?>">
                                    <a href="#" class="goodListItem" data-id="<?php echo $record->id ?>" data-message="<?php echo $record->message ?>" data-toggle="modal" data-target="#itemModel">
                                        <div class="notes not2" style="height: auto;display: flex;color:black">&#x2B24; &nbsp;<?php echo $record->message ?></div>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (count($records) == 0) { ?>
                                 <li style="text-align: start;" id="remove-001">
                                    <a href="#" class="goodListItem">
                                        <div class="notes not2" style="height: auto;display: flex;color:black">&#x2B24; &nbsp;Value all life equally</div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Button trigger modal -->

    <!-- Modal -->

    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-body  ">
                    <div class="form-group">
                        <textarea class="form-control " rows="4" placeholder="please start writing here" id="comment"></textarea>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <a class="btn btn-danger" id="modal-add-item-button" href="#">Save</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="itemModel" tabindex="-1" role="dialog" aria-labelledby="itemModelTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content text-center">
                <div class="text-massage ">
                    <input type="hidden" value="" id="id" />
                    <h6 class="ff-bold text-center"></h6>
                    <p>Are you sure, you want to delete this event?</p>
                </div>
                <div class="modal-footer bg-white">
                    <a onclick="delete_list_item()" class="btn btn-danger" href="javascript:void(0);">Yes</a>
                    <a class="btn btn-danger" data-dismiss="modal" href="#">No </a>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
var recordsCount = <?php echo sizeof($records); ?>;
</script>
<script src="<?php echo ASSETS_PATH . "js/custom/public/highest_goods_of_all.js" ?>"></script>