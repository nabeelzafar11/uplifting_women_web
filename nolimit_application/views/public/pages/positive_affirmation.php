<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="gallery">
                 <h1 class="text-center text-uppercase ff-blod font-weight-bold">positive affirmation</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<section class="woman bg-white">
    <div class="container text-center">
        <div class="wraper ">
            <div class="row">
                <div class="col-lg-12">
                    <h5 class="ff-mbold"><?php echo !empty($header->positiveaffirmation_mobile) ? $header->positiveaffirmation_mobile : ""; ?></h5>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
function getURL($id)
{
    if ($id === "2") {
        return "abundance";
    } elseif ($id === "3") {
        return "health-and-beauty";
    } elseif ($id === "4") {
        return "love";
    } elseif ($id === "5") {
        return "my-personal-affirmation";
    }
}
?>
<section class="gallery1 ">
    <div class="gallery-wraper py-5 ">
        <div class="container   ">
            <div class="row  ">
                <?php foreach ($dashboard as $dash) { ?>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mt-5">
                        <a href="positive_affirmation/<?php echo getURL($dash->id); ?>" class="text-dark">
                            <div class="card gallery-card  ">
                                <img src="<?php echo ASSETS_PATH ?>uploaded_media/<?php echo $dash->web_image ?>" class="img-fluid" alt="">
                                <div class="card-title">
                                    <h3 class="mt-3 ff-mbold"><?php echo $dash->name ?></h3>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo ASSETS_PATH . "js/custom/public/positive_affirmation.js" ?>"></script>