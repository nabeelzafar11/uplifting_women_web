<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="gallery">
                <h1 class="text-center text-uppercase ff-blod font-weight-bold">bucket list</h1>
            </div>
        </div>
    </div>
</section>
<!-- data-backdrop="static" -->
<!-- data-keyboard="false" -->
<section class="blucklist">
    <div class="container text-center">
        <div class="wraper-blucklist  ">
            <div class='col-sm-12 alert' id='alert' style='display:none;'></div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="card h-100 box">
                        <div class="card-header">
                            <div class="d-flex justify-content-center align-items-center m-auto">
                                <a href="#" data-toggle="modal" data-target="#exampleModalScrollableStart"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Add Btn.png" class="img-fluid" alt=""></a>
                            </div>
                        </div>
                        <ul class="card-body" id="abundanceList">
                            <?php
                            $message_detail = "";
                            foreach ($users_list as $list) {
                                $message_detail .= $list->message . "<br/>";
                                ?>
                                <li  id="remove-list<?php echo $list->id ?>" class="bg-modle-li-2 alert-white   fade show d-flex justify-content-between"  >
                                    <span><?php echo $list->message; ?></span>
                                    <button data-id="<?php echo $list->id ?>" type="button" data-toggle="modal" data-target="#exampleModalScrollable2" class="close abundanceListItem" aria-label="Close">
                                        <span aria-hidden="true"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Remove Circle.png" class="img-fluid" alt=""></span>
                                    </button>
                                </li>
                            <?php }
                            ?>
                            <?php if (sizeof($users_list) > 0) { ?>
                                <li id='shareButton'><span id="button_1" style="float:left;"></span></li>
                            <?php } ?>
                        </ul>
                        <!--                        <div class="sharethis-inline-share-buttons"></div>-->
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalScrollableStart" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header  ">
                <h4 class="modal-title   " id="exampleModalScrollableTitle">Bucket List Ideas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body h-75">
                <div class="modle">
                    <?php
                    foreach ($add_new_items_list as $new_list) {
                        ?>
                        <label id="remove-newlist<?php echo $new_list->id ?>" class="container1 mr-4"><?php echo $new_list->message ?>
                            <input type="checkbox" name="newListItem" class='classadmin' value="<?php echo $new_list->id; ?>" data-message="<?php echo $new_list->message; ?>">
                            <span class="checkmark  "></span>
                        </label>
                    <?php } ?>
                    <label class="container1 mr-4">Custom
                        <input type="checkbox" class='classcustom' name="newListItem" value="customListItem">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <div class="modal-footer">

                <a onclick="on_click_add_bucklist_item()" href="javascript:void(0);" class="btn btn-danger" style="background-color: #66C8C9;">ADD</a>
            </div>
        </div>
    </div>
</div>
<!-- popup -->
<div class="modal fade" id="exampleModalScrollable2" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content text-center">
            <input type="hidden" value="" id="abundanceID" />
            <div class="text-massage ">
                <h6 class="ff-bold text-center">Message</h6>
                <p>Are you sure, you want to delete this bucket list item?</p>
            </div>
            <div class="modal-footer bg-white">
                <a onclick="delete_bucklist_item()" href="javascript:void(0);" class="btn btn-danger">Yes</a>
                <a class="btn btn-danger" data-dismiss="modal" href="#">No </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="custom" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content text-center">
            <div class="text-massage p-5">
                <h6 class="ff-bold text-center">Add Custom Event</h6>
                <div class="form-inline d-flex">
                    <label>Title</label>
                    <input type="text" class="form-control1" id="pwd">
                </div>
            </div>
            <div class="modal-footer bg-white">
                <a onclick="add_bucklist_item()" style="background-color: #66C8C9;" href="javascript:void(0);" class="btn btn-danger" href="#">Create</a>
                <a class="btn btn-danger" style="background-color: #66C8C9;" data-dismiss="modal" href="#">Cancel</a>
            </div>
        </div>
    </div>
</div>
</section>
<script src="<?php echo ASSETS_PATH . "js/custom/public/bucket_list.js" ?>"></script>
<script>
                    stWidget.addEntry({
                        "service": "sharethis",
                        "element": document.getElementById('button_1'),
                        "url": "<?php echo $bucket_list_share_url; ?>",
                        "title": "My Bucket List",
                        "type": "large",
                        "text": "My Bucket List",
                        "image": "https://uplifting-women.com/assets/newtheme/img/BG=2.png",
                        "summary": "You can view my bucket list from the shared link!"
                    });
</script>
