
<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">User Profile</h1>
            </div>
        </div>
    </div>
    <hr>
    <div class="col-lg-12 d-flex justify-content-around">
        <a id="personal_tab" style="cursor:pointer; color:#FC5C9B; text-decoration: underline;" class="pt-3 ml-2 ff-bold mt-5">Personal Details</a>
        <a id="password_tab" style="cursor:pointer" class="pt-3 ml-2 ff-bold mt-5">Change Password</a>
    </div>
</section>
<!-- login section -->
<section class="login-form">
    <div class="woraper">
        <div id="personal_container" class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <form id="personal_form" name="personal_form" method="post" action="<?php echo base_url("api/user/personal-settings"); ?>" onsubmit="return false;">
                        <div style="text-align: center;">
                            <span class="logo-img" style="display: block;">
                                <img id="imagePreview" src="<?php echo $current_user->image ? $current_user->image : ASSETS_PATH . "listeo_updated/images/user.png"; ?>" class="img-fluid" style="cursor: pointer; border-radius: 50%; float: none; min-height: 120px; width:120px; max-width: 100%;" />
                                <input type="hidden" id="image" name="image" value="<?php echo $current_user->image; ?>" />
                                <input type="file" id="imageUpload" onchange="getBase64(this);" style="display:none" />
                            </span>
                        </div>
                        <div class="form-signup p-4">
                            <div class="alert"></div>
                            <div class="form-group">
                                <label for="firstName" class="ff-bold">First Name</label>
                                <input placeholder="First Name" name="firstName" type="text" class="form-control-1" value="<?php echo $current_user->firstName; ?>" id="firstName">
                            </div>
                            <div class="form-group">
                                <label for="lastName" class="ff-bold">Last Name</label>
                                <input placeholder="Last Name" name="lastName" type="text" class="form-control-1" value="<?php echo $current_user->lastName; ?>" id="lastName">
                            </div>
                            <div class="form-group">
                                <label for="email" class="ff-bold">Email</label>
                                <input readonly name="email" type="email" class="form-control-1" value="<?php echo $current_user->email; ?>" id="email">
                            </div>
                            <?php if (!empty($current_user->google_event_access_token)) { ?>
                                <div class="form-group">
                                    <label for="email" class="ff-bold">Sync Events with Google Calendar</label>
                                    <label class="switchboard">
                                        <input name="google_event_active" id="google_event_active" type="checkbox" <?php echo $current_user->google_event_active == 1 ? 'checked' : ''; ?>>
                                        <span class="sliderpushing roundslider"></span>
                                    </label>
                                </div> 
                            <?php } else {
                                $login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode(base_url("profile")) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=offline&prompt=consent';
                                ?>
                                <div class="form-group syncButton">
                                    <a href='<?php echo $login_url; ?>' onclick="location.href='<?php echo $login_url; ?>';"><label for="email" class="ff-bold syncText">Please click here to Sync Your Events with Google Calendar</label></a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="button text-center">
                            <input type="submit" class="btn btn-light btn-block mb-3" value="Save" />
                        </div>
                    </form>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
        <div id="password_container" class="container" style="display: none;">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div style="text-align: center;">
                        <span class="logo-img" style="display: block">
                            <img src="<?php echo $current_user->image ? $current_user->image : ASSETS_PATH . "listeo_updated/images/user.png"; ?>" class="img-fluid" style="border-radius: 50%; float: none; min-height: 120px; width:120px; max-width: 100%;" />
                        </span>
                    </div>
                    <form id="password_form" name="password_form" method="post" action="<?php echo base_url("api/user/reset-password"); ?>" onsubmit="return false;">
                        <div class="form-signup p-4">
                            <div class="alert"></div>
                            <div class="form-group">
                                <label for="currentPassword" class="ff-bold">Current Password</label>
                                <input type="password" placeholder="Current Password" class="form-control-1" id="currentPassword" name="currentPassword">
                            </div>
                            <div class="form-group">
                                <label for="password" class="ff-bold">Password</label>
                                <input type="password" placeholder="Password" class="form-control-1" id="password" name="password">
                            </div>
                            <div class="form-group">
                                <label for="confirmPassword" class="ff-bold">Retype Password</label>
                                <input type="password" placeholder="Retype Password" class="form-control-1" id="confirmPassword" name="confirmPassword">
                            </div>
                        </div>
                        <div class="button text-center">
                            <input type="submit" class="btn btn-light btn-block mb-3" value="Save" />
                        </div>
                    </form>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo ASSETS_PATH . "js/custom/public/user_profile.js" ?>"></script>