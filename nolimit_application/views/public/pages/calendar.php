<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="gallery">
                <h1 class="text-center text-uppercase ff-blod font-weight-bold">calender 2020</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<section class="calcander">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <form method="post" id="delete_event" action="<?php echo base_url("api/user/delete-single-event"); ?>">
                    <input type="hidden" name="eventId" value="" id="eventId">
                </form>
                <form method="post" id="delete_all_future_events" action="<?php echo base_url("api/user/delete-all-future-event"); ?>">
                    <input type="hidden" name="dateandtime" value="" id="dateandtime">
                </form>
                <div class="card card-bg">
                    <div class="d-block justify-content-center align-items-center m-auto p-2">
                        <h4 class="justify-content ff-bold">Monthly Affirmation</h4>
                        <h5 class="text-clr ff-lt h5-align-center affirmation_text"><?php echo!empty($calendardailyaffirmation->mobile_message) ? htmlspecialchars_decode(stripslashes($calendardailyaffirmation->mobile_message)) : ""; ?></h5>
                    </div>
                </div>
                <div class='datepicker'>
                    <div class="datepicker-header"></div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card  card3">
                    <div class="card-header">
                        <div class="d-flex justify-content-center align-items-center m-auto">
                            <a id="addHref" href="<?php echo base_url("self_love"); ?>"><img src="<?php echo ASSETS_PATH ?>newtheme/img/Add Btn.png" class="img-fluid" alt=""></a>  
                        </div>
                    </div>
                    <div class="card-body d-inline-block justify-content-between">
                        <h5 class="ff-bold" id="datecomplete"><?php echo $datecompelte; ?></h5>
                        <ul class="eventsfordate">
                        </ul> 
                    </div>

                </div>
            </div>

        </div>  
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-footer">
                    <a class="btn btn-danger" data-toggle='modal' data-target='#deletethiseventmodal'>Delete This Event Only</a>
                    <a class="btn btn-danger" data-toggle='modal' data-target='#deleteallfutureevents'>Delete All Future Events </a>
                    <a class="btn btn-danger" data-dismiss="modal">Cancel </a>
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal"></button>
                       <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deletethiseventmodal" tabindex="-1" role="dialog" aria-labelledby="deletethiseventmodal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="text-massage ">
                    <h6 class="ff-bold text-center"></h6>
                    <p>Are you sure, you want to delete this event?</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-dismiss="modal" id="deletethisevent">Yes</a>
                    <a class="btn btn-danger" data-dismiss="modal">Cancel </a>
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal"></button>
                       <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteallfutureevents" tabindex="-1" role="dialog" aria-labelledby="deleteallfutureevents" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="text-massage ">
                    <h6 class="ff-bold text-center"></h6>
                    <p>Are you sure, you want to delete all future events?</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-dismiss="modal" id="deleteallfutureeventsButton">Yes</a>
                    <a class="btn btn-danger" data-dismiss="modal">Cancel </a>
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal"></button>
                       <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalScrollable1" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header r bg-model">
                    <h5 class="modal-title text-white font-weight-bold ff-bold justify-content-center" id="exampleModalScrollableTitle">Add Event</h5>

                </div>
                <div class="modal-body">


                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-3">
                                <h5 class="mt-4 ff-mbold   ">Title</h5>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control1" placeholder=" "  id="name">
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-3">
                                <h5 class="mt-4 ff-mbold   ">Date</h5>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control1" placeholder=" "  id="date">
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-3">
                                <h5 class="mt-4 ff-mbold text-uppercase  ">Time</h5>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control1" placeholder=" "  id="time">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-dismiss="modal" href="#">Close</a>
                    <a class="btn btn-danger" data-dismiss="modal" href="#">Save</a>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<script>
    var minDatesForCalendar = '<?php echo date("Y"); ?>-01-01';
<?php if (date("m") == 12) { ?>
        var maxDatesForCalendar = '<?php echo date("Y") + 1; ?>-12-31';
<?php } else {
    ?>
        var maxDatesForCalendar = '<?php echo date("Y"); ?>-12-31';
    <?php }
?>
    var allMessage = <?php echo json_encode($calendardailyaffirmation_all_years); ?>;
    var availableDates = <?php echo json_encode($dates); ?>;
    var encodedMonths = <?php echo json_encode($encoded_month); ?>;
    var addHrefURL = "<?php echo base_url("self_love"); ?>";
</script>
<script src="<?php echo ASSETS_PATH . "js/custom/public/calendar.js" ?>"></script>