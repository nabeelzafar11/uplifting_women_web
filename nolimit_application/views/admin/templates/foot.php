
<!-- Scripts
================================================== -->
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/mmenu.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/chosen.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/slick.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/counterup.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/tooltips.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/custom.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


<!-- Time dropper js-->
<script src="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/time-dropper/timedropper.js"></script>
<!-- Date dropper js-->
<script src="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/date-dropper/datedropper.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listinghub/assets/js/custom.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/jquery.validate.min.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/jquery.form.min.js"></script>
<script src="<?php echo ASSETS_PATH; ?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/custom/public/general.js"></script>
<script></script>
</body>
</html>