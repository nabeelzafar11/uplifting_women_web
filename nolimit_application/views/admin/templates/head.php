<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title><?php echo $header->siteTitle;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <style type="text/css">
        /* league gothic */
        /*Regular*/
        @font-face {
            font-family: leaguegothic;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/LeagueGothic-Regular.otf);
        }
        /* Condensed */
        @font-face {
            font-family: leaguegothicbold;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/LeagueGothic-CondensedRegular.otf);
        }


        /* Cooper hewitt */

        @font-face {
            font-family: cooperhewittbold;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Bold.otf);
        }

        @font-face {
            font-family: cooperhewittbook;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Book.otf);
        }

        @font-face {
            font-family: cooperhewittheavy;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Heavy.otf);
        }

        @font-face {
            font-family: cooperhewittsemi;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Semibold.otf);
        }

        @font-face {
            font-family: cooperhewittlight;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Light.otf);
        }


        @font-face {
            font-family: cooperhewittthin;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Thin.otf);
        }





    </style>

    <link rel="icon" type="image/png" href="<?php echo MEDIA_PATH . $header->favicon; ?>">
    <meta name="title" content="<?php echo $header->seoTitle; ?>">
    <meta name="description" content="<?php echo $header->seoDescription; ?>">
    <meta name="keywords" content="<?php echo $header->seoKeywords; ?>">
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>listeo_updated/css/style.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>listeo_updated/css/colors/main.css" id="colors">

    <!-- Time Dropper -->
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/time-dropper/timedropper.css" rel="stylesheet">

    <!-- Date Dropper -->
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/date-dropper/datedropper.css" rel="stylesheet">

    <!-- Icons -->
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/themify-icons/css/themify-icons.css" rel="stylesheet">
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/line-icons/css/line-font.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <!-- Custom style -->
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/css/responsiveness.css" rel="stylesheet">
    <link  type="text/css" rel="stylesheet" id="jssDefault" href="<?php echo ASSETS_PATH; ?>listinghub/assets/css/colors/main.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/custom.css">

    <script>
        var siteURL = '<?php echo API_PATH; ?>';
        var mediaPath = '<?php echo MEDIA_PATH; ?>';
        var apiPath = '<?php echo API_PATH; ?>';
        var loginRequest = <?php echo ($this->input->get('request') == 'login') ? 1 : 0 ; ?>;
    </script>
</head>
