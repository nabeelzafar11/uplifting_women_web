<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Footer</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="footer_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_footer'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Footer</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $footer->footerLogo; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload footer Logo
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="footerLogo" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="footerLogo"
                                               value="<?php echo $footer->footerLogo; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="contactUsText">Contact Us Text</label>
                                        <input placeholder="Contact Us Text" class="form-control" type="text"
                                               id="Site contactUsText"
                                               value="<?php echo $footer->contactUsText; ?>"
                                               name="contactUsText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneText">Phone One Text</label>
                                        <input placeholder="Phone One Text" class="form-control" type="text"
                                               id="phoneText" value="<?php echo $footer->phoneText; ?>"
                                               name="phoneText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Phone One</label>
                                        <input placeholder="Phone One" class="form-control" type="text"
                                               id="phone" value="<?php echo $footer->phone; ?>"
                                               name="phone"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneTwoText">Phone Two Text</label>
                                        <input placeholder="Phone Two Text" class="form-control" type="text"
                                               id="phoneTwoText" value="<?php echo $footer->phoneTwoText; ?>"
                                               name="phoneTwoText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneTwo">Phone Two</label>
                                        <input placeholder="Phone Two" class="form-control" type="text"
                                               id="phoneTwo" value="<?php echo $footer->phoneTwo; ?>"
                                               name="phoneTwo"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="emailText">Email Text</label>
                                        <input placeholder="Email Text" class="form-control" type="text"
                                               id="emailText" value="<?php echo $footer->emailText; ?>"
                                               name="emailText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input placeholder="Email" class="form-control" type="text"
                                               id="email" value="<?php echo $footer->email; ?>"
                                               name="email"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="abnText">ABN Text</label>
                                        <input placeholder="ABN Text" class="form-control" type="text"
                                               id="abnText" value="<?php echo $footer->abnText; ?>"
                                               name="abnText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="abn">ABN</label>
                                        <input placeholder="ABN" class="form-control" type="text"
                                               id="abn" value="<?php echo $footer->abn; ?>"
                                               name="abn"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="footerText">
                                            Footer About
                                        </label>
                                        <textarea name="footerText" id="footerText" rows="5"
                                                  placeholder="Footer About"><?php echo $footer->footerText; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="copyright">Copy Right Text (use "%YEAR%" for year variable)</label>
                                        <input placeholder="Copy Right Text (use "%YEAR%" for year variable)" class="form-control" type="text"
                                               id="copyright" value="<?php echo $footer->copyright; ?>"
                                               name="copyright"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="facebook">Facebook Link</label>
                                        <input placeholder="Facebook Link" class="form-control" type="text"
                                               id="facebook" value="<?php echo $footer->facebook; ?>"
                                               name="facebook"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="twitter">Twitter Link</label>
                                        <input placeholder="Twitter Link" class="form-control" type="text"
                                               id="twitter" value="<?php echo $footer->twitter; ?>"
                                               name="twitter"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="google">Google Link</label>
                                        <input placeholder="Google Link" class="form-control" type="text"
                                               id="google" value="<?php echo $footer->google; ?>"
                                               name="google"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="linkedin">LinkedIn Link</label>
                                        <input placeholder="LinkedIn Link" class="form-control" type="text"
                                               id="linkedin" value="<?php echo $footer->linkedin; ?>"
                                               name="linkedin"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="youtube">Youtube Link</label>
                                        <input placeholder="Youtube Link" class="form-control" type="text"
                                               id="youtube" value="<?php echo $footer->youtube; ?>"
                                               name="youtube"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="skype">Skype Link</label>
                                        <input placeholder="Skype Link" class="form-control" type="text"
                                               id="skype" value="<?php echo $footer->skype; ?>"
                                               name="skype"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="vimeo">Vimeo Link</label>
                                        <input placeholder="Vimeo Link" class="form-control" type="text"
                                               id="vimeo" value="<?php echo $footer->vimeo; ?>"
                                               name="vimeo"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="instagram">Instagram Link</label>
                                        <input placeholder="Instagram Link" class="form-control" type="text"
                                               id="instagram" value="<?php echo $footer->instagram; ?>"
                                               name="instagram"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="pinterest">Pinterest Link</label>
                                        <input placeholder="Pinterest Link" class="form-control" type="text"
                                               id="pinterest" value="<?php echo $footer->pinterest; ?>"
                                               name="pinterest"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="rss">RSS Link</label>
                                        <input placeholder="RSS Link" class="form-control" type="text"
                                               id="rss" value="<?php echo $footer->rss; ?>"
                                               name="rss"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="behance">Behance Link</label>
                                        <input placeholder="Behance Link" class="form-control" type="text"
                                               id="behance" value="<?php echo $footer->behance; ?>"
                                               name="behance"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="github">Github Link</label>
                                        <input placeholder="Github Link" class="form-control" type="text"
                                               id="github" value="<?php echo $footer->github; ?>"
                                               name="github"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="newsLetterText">Subscribe Title</label>
                                        <input placeholder="Subscribe Title" class="form-control" type="text"
                                               id="newsLetterText" value="<?php echo $footer->newsLetterText; ?>"
                                               name="newsLetterText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="newsLetterNamePlaceHolderText">Subscribe Name Placeholder</label>
                                        <input placeholder="Subscribe Name Placeholder" class="form-control" type="text"
                                               id="newsLetterNamePlaceHolderText" value="<?php echo $footer->newsLetterNamePlaceHolderText; ?>"
                                               name="newsLetterNamePlaceHolderText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="newsLetterEmailPlaceHolderText">Subscribe Email Placeholder</label>
                                        <input placeholder="Subscribe Email Placeholder" class="form-control" type="text"
                                               id="newsLetterEmailPlaceHolderText" value="<?php echo $footer->newsLetterEmailPlaceHolderText; ?>"
                                               name="newsLetterEmailPlaceHolderText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="newsLetterPostcodePlaceHolderText">Subscribe Postcode Placeholder</label>
                                        <input placeholder="Subscribe Postcode Placeholder" class="form-control" type="text"
                                               id="newsLetterPostcodePlaceHolderText" value="<?php echo $footer->newsLetterPostcodePlaceHolderText; ?>"
                                               name="newsLetterPostcodePlaceHolderText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="newsLetterButtonText">Subscribe Button Text</label>
                                        <input placeholder="Subscribe Button Text" class="form-control" type="text"
                                               id="newsLetterButtonText" value="<?php echo $footer->newsLetterButtonText; ?>"
                                               name="newsLetterButtonText"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/footer.js"></script>




