<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Self Love/Self Care Listings</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2" role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper">
            <!-- small list wrapper -->
            <ul id="sortable">
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/1'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>January</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/2'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>February</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/3'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>March</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/4'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>April</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/5'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>May</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/6'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>June</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/7'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>July</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/8'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>August</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/9'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>September</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/10'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>October</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/11'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>November</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/selflovecare-list/12'); ?>">
                        <div class="small-listing-box white-bg ">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4>December</h4>
                            </div>
                            <div class="small-list-action">
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div> <!-- ending of small list wrapper -->
    </div>
</div>