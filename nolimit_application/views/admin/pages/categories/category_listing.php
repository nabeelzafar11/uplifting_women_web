<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Category Listings
                </h2>
                <?php if ($parentCategoryNames) { ?>
                    <b><?php echo $parentCategoryNames; ?></b>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($categories as $category) { ?>
                    <li data-id="<?php echo $category->categoryId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <?php if ($category->childCount) { ?>
                                    <a href="<?php echo base_url('admin/category_listings/sub_category/' . $category->categoryId); ?>">
                                        <h4><?php echo $category->categoryName; ?>
                                            <span>(<b><?php echo $category->childCount; ?></b>)</span></h4>
                                    </a>
                                <?php } else { ?>
                                    <h4><?php echo $category->categoryName; ?></h4>
                                <?php } ?>
                            </div>
                            <div class="small-list-action">
                                <label class="switch">
                                    <input type="checkbox" <?php if ($category->categoryIsActive) { ?> checked="checked" <?php } ?>
                                           onchange="trigger_is_active(this, <?php echo $category->categoryId; ?>, '<?php echo base_url('api/update_category_status'); ?>')">
                                    <span class="slider round"></span>
                                </label>
                                <?php // if ($category->categoryPageType == 3 || $category->categoryPageType == 6) { ?>
<!--                                    <a href="<?php echo base_url('admin/category_listings/category_page/' . $category->categoryId); ?>"
                                       class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                       title="Edit Page"><i class="ti-file"></i></a>-->
                                <?php // } ?>
                                <a href="<?php echo base_url('admin/category_listings/edit_category/' . $category->categoryId); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);"
                                   onclick="delete_item(this, <?php echo $category->categoryId; ?>, '<?php echo base_url('api/delete_category'); ?>');"
                                   class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i
                                            class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($categories) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    var sortUrl = '<?php echo base_url('api/sort_category'); ?>';
</script>





