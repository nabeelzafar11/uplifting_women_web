<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Category Listings / Category Page Sections
                </h2>
                <b><?php echo $category->categoryName; ?></b>
                <a href="<?php echo base_url('admin/category_listings/add_new_category_page_section/'.$category->categoryId); ?>"
                   class="button border with-icon" style="float: right;">
                    Add New Category Page Section
                    <i class="sl sl-icon-plus"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($pageSections as $pageSection) { ?>
                    <li data-id="<?php echo $pageSection->pageSectionId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4><?php echo $pageSection->pageSectionTitle; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <a href="<?php echo base_url('admin/category_listings/category_page_section/' . $pageSection->pageSectionId); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Manage Section"><i class="ti-plus"></i></a>
                                <a href="<?php echo base_url('admin/category_listings/edit_category_page_section/' . $pageSection->pageSectionId); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);"
                                   onclick="delete_item(this, <?php echo $pageSection->pageSectionId; ?>, '<?php echo base_url('api/delete_category_page_section'); ?>');"
                                   class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i
                                            class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($pageSections) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    var sortUrl = '<?php echo base_url('api/sort_category_page_sections'); ?>';
</script>





