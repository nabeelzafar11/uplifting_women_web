<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Categories Listing / Edit Category</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="category_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_category'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="categoryId" value="<?php echo $category->categoryId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Category</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                      
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $category->categoryImage; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Banner Image
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="categoryImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="categoryImage"
                                               value="<?php echo $category->categoryImage; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryName">Page Name</label>
                                        <input placeholder="Category Name" class="form-control" type="text"
                                               id="categoryName"
                                               name="categoryName" value="<?php echo $category->categoryName; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="display:none;">
                                    <div class="form-group">
                                        <label for="menuHref">Parent Category</label>
                                        <select data-placeholder="Select Text Position" id="categoryParentId"
                                                name="categoryParentId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option
                                                <?php if ($category->categoryParentId == 0) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="0">No Parent Category
                                            </option>
                                            <?php foreach ($categories as $category1) { ?>
                                                <option
                                                    <?php if ($category->categoryParentId == $category1->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="display:none;">
                                    <div class="form-group">
                                        <label for="categoryPageType">Category Page Type</label>
                                        <select data-placeholder="Select Text Position" id="categoryPageType"
                                                name="categoryPageType"
                                                class="form-control chosen-select" tabindex="2">
                                            <option
                                                <?php if ($category->categoryPageType == 0) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="0">No Page
                                            </option>
                                            <option
                                                <?php if ($category->categoryPageType == 1) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="1">Blogs
                                            </option>
<!--                                            <option-->
<!--                                                --><?php //if ($category->categoryPageType == 2) { ?>
<!--                                                    selected="selected"-->
<!--                                                --><?php //} ?>
<!--                                                    value="2">Board of Directors-->
<!--                                            </option>-->
                                            <option
                                                <?php if ($category->categoryPageType == 9) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="9">Contact Us
                                            </option>
                                            <option
                                                <?php if ($category->categoryPageType == 3) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="3">Content
                                            </option>
<!--                                            <option-->
<!--                                                --><?php //if ($category->categoryPageType == 4) { ?>
<!--                                                    selected="selected"-->
<!--                                                --><?php //} ?>
<!--                                                    value="4">Media Release-->
<!--                                            </option>-->
<!--                                            <option-->
<!--                                                --><?php //if ($category->categoryPageType == 5) { ?>
<!--                                                    selected="selected"-->
<!--                                                --><?php //} ?>
<!--                                                    value="5">News-->
<!--                                            </option>-->
                                            <option
                                                <?php if ($category->categoryPageType == 7) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="7">Search
                                            </option>
                                            <option
                                                <?php if ($category->categoryPageType == 6) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="6">Sections
                                            </option>
                                            <option
                                                <?php if ($category->categoryPageType == 8) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="8">Site Map
                                            </option>
                                            <option
                                                <?php if ($category->categoryPageType == 10) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="10">Career
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryShortDescription">
                                             Short Description
                                        </label>
                                        <textarea name="categoryShortDescription" id="categoryShortDescription" rows="5"
                                                  placeholder="Short Description"><?php echo $category->categoryShortDescription; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryDescription">
                                             Description
                                        </label>
                                        <textarea name="categoryDescription" id="categoryDescription" rows="5"
                                                  placeholder="Category Description"><?php echo $category->categoryDescription; ?>
                                        </textarea>
                                    </div>
                                </div>
                             
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list-box margin-top-0" style="display:none;">
                    <h4 class="gray">Interested In</h4>
                    <div class="dashboard-list-box-static">
                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="categoryInterestedOneCategoryId">Interested One Category</label>
                                        <select data-placeholder="Select Text Position"
                                                id="categoryInterestedOneCategoryId"
                                                name="categoryInterestedOneCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option
                                                <?php if ($category->categoryInterestedOneCategoryId == 0) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="0">No Interested Category
                                            </option>
                                            <?php foreach ($categories as $category1) { ?>
                                                <option
                                                    <?php if ($category->categoryInterestedOneCategoryId == $category1->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="categoryInterestedTwoCategoryId">Interested Two Category</label>
                                        <select data-placeholder="Select Text Position"
                                                id="categoryInterestedTwoCategoryId"
                                                name="categoryInterestedTwoCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option
                                                <?php if ($category->categoryInterestedTwoCategoryId == 0) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="0">No Interested Category
                                            </option>
                                            <?php foreach ($categories as $category1) { ?>
                                                <option
                                                    <?php if ($category->categoryInterestedTwoCategoryId == $category1->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="categoryInterestedThreeCategoryId">Interested Three Category</label>
                                        <select data-placeholder="Select Text Position"
                                                id="categoryInterestedThreeCategoryId"
                                                name="categoryInterestedThreeCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option
                                                <?php if ($category->categoryInterestedThreeCategoryId == 0) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                    value="0">No Interested Category
                                            </option>
                                            <?php foreach ($categories as $category1) { ?>
                                                <option
                                                    <?php if ($category->categoryInterestedThreeCategoryId == $category1->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Seo Settings</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryPageTitle">Page Title</label>
                                        <input placeholder="Page Title" class="form-control" type="text"
                                               id="categoryPageTitle"
                                               value="<?php echo $category->categoryPageTitle; ?>"
                                               name="categoryPageTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categorySeoTitle">Seo Title</label>
                                        <input placeholder="Seo Title" class="form-control" type="text"
                                               id="categorySeoTitle" value="<?php echo $category->categorySeoTitle; ?>"
                                               name="categorySeoTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categorySeoKeywords">Seo Keywords</label>
                                        <input placeholder="Seo Keywords" class="form-control" type="text"
                                               id="categorySeoKeywords"
                                               value="<?php echo $category->categorySeoKeywords; ?>"
                                               name="categorySeoKeywords"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categorySeoDescription">
                                            Seo Description
                                        </label>
                                        <textarea name="categorySeoDescription" id="categorySeoDescription" rows="5"
                                                  placeholder="Seo Description"><?php echo $category->categorySeoDescription; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/edit_category.js"></script>




