<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Category Listings / Category Page Sections / Add New Category Page Section Category
                </h2>
                <b><?php echo $category->categoryName; ?></b>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>
    <div class="row">
        <div class="col-sm-6">
            <form id="category_page_section_category_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_category_page_section_category'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="pageSectionCategoryPageSectionId" value="<?php echo $pageSection->pageSectionId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Category Page Section Category</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="pageSectionCategoryCategoryId">Select Category</label>
                                        <select data-placeholder="Select Text Position" id="pageSectionCategoryCategoryId"
                                                name="pageSectionCategoryCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category1) { ?>
                                                <option value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>
    <!-- Titlebar -->
    <div id="titlebar" style="margin-top: 40px;">
        <div class="row">
            <div class="col-md-12">
                <h2>Category Listings / Category Page Sections / Category Page Section Categories
                </h2>
                <b><?php echo $category->categoryName; ?></b>
            </div>
        </div>
    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($pageSectionCategories as $pageSectionCategory) { ?>
                    <li data-id="<?php echo $pageSectionCategory->pageSectionCategoryId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <h4><?php echo $pageSectionCategory->categoryName; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <a href="javascript:void(0);"
                                   onclick="delete_item(this, <?php echo $pageSectionCategory->pageSectionCategoryId; ?>, '<?php echo base_url('api/delete_category_page_section_category'); ?>');"
                                   class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i
                                            class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($pageSectionCategories) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    var sortUrl = '<?php echo base_url('api/sort_category_page_section_categories'); ?>';
</script>
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_category_page_section_category.js"></script>





