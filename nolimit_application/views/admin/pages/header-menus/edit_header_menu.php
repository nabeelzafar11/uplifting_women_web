<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Header Menu Listings / Edit Menu</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="edit_footer_menu_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_header_menu'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="menuId" value="<?php echo $footerMenu->footerMenuId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Menu</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="categoryName">Menu Name</label>
                                        <input placeholder="Menu Name" class="form-control" type="text" id="menuName"
                                               name="menuName" value="<?php echo $footerMenu->footerMenuName; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuButtonText">Menu Href Link (Linking to some-other application)</label>
                                        <input placeholder="Leave empty if want to link to internal site" class="form-control" type="text"
                                               id="footerMenuHref" value="<?php echo $footerMenu->footerMenuHref; ?>"
                                               name="footerMenuHref"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuCategoryId">Menu Href</label>
                                        <select data-placeholder="Select Text Position" id="menuCategoryId"
                                                name="menuCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if($footerMenu->footerMenuCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#edit_footer_menu_form").validate({
            rules: {
                'menuId': {
                    required: true
                },
                'menuName': {
                    required: true
                },
                'menuCategoryId': {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
</script>




