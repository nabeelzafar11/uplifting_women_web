<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Blog Category Listings / Add New Blog Category</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="blog_category_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_blog_category'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Blog Category</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogCategoryName">Category Name</label>
                                        <input placeholder="Category Name" class="form-control" type="text"
                                               id="blogCategoryName"
                                               name="blogCategoryName"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_blog_category.js"></script>




