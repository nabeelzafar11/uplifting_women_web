<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Blog Listings </h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="sortable2">
                <?php foreach ($blogs as $blog) { ?>
                    <li data-id="<?php echo $blog->blogId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                    <h4><?php echo $blog->blogTitle; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <label class="switch">
                                    <input type="checkbox" <?php if ($blog->blogIsActive) { ?> checked="checked" <?php } ?>
                                           onchange="trigger_is_active(this, <?php echo $blog->blogId; ?>, '<?php echo base_url('api/update_blog_status');?>')">
                                    <span class="slider round"></span>
                                </label>
                                <a href="<?php echo base_url('admin/blog_listings/edit_blog/' . $blog->blogId); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);"
                                   onclick="delete_item(this, <?php echo $blog->blogId; ?>, '<?php echo base_url('api/delete_blog'); ?>');"
                                   class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i
                                            class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($blogs) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    //var sortUrl = '<?php //echo base_url('api/sort_menu'); ?>//';
</script>





