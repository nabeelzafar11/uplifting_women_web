<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Blog Listings / Edit Blog</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="blog_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_blog'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="blogId" value="<?php echo $blog->blogId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Blog</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $blog->blogFeatureImage; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Icon
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="blogFeatureImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="blogFeatureImage"
                                               value="<?php echo $blog->blogFeatureImage; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Blog Title</label>
                                        <input placeholder="Blog Title" class="form-control" type="text"
                                               id="blogTitle" value="<?php echo $blog->blogTitle; ?>"
                                               name="blogTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogBlogCategoryId">Category</label>
                                        <select data-placeholder="Select Text Position" id="blogBlogCategoryId"
                                                name="blogBlogCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Parent Category</option>
                                            <?php foreach ($blogCategories as $blogCategory) { ?>
                                                <option
                                                    <?php if ($blog->blogBlogCategoryId == $blogCategory->blogCategoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $blogCategory->blogCategoryId; ?>"><?php echo $blogCategory->blogCategoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogDescription">
                                            Blog Description
                                        </label>
                                        <textarea name="blogDescription" id="blogDescription" rows="5"
                                                  placeholder="Blog Description"><?php echo $blog->blogDescription; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogContent">
                                            Blog Content
                                        </label>
                                        <textarea name="blogContent" id="blogContent" rows="5"
                                                  placeholder="Blog Content"><?php echo $blog->blogContent; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Interested In</h4>
                    <div class="dashboard-list-box-static">
                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="blogInterestedOneCategoryId">Interested One Category</label>
                                        <select data-placeholder="Select Text Position" id="blogInterestedOneCategoryId"
                                                name="blogInterestedOneCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Interested Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($blog->blogInterestedOneCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="blogInterestedTwoCategoryId">Interested Two Category</label>
                                        <select data-placeholder="Select Text Position" id="blogInterestedTwoCategoryId"
                                                name="blogInterestedTwoCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Interested Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($blog->blogInterestedTwoCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="blogInterestedThreeCategoryId">Interested Three Category</label>
                                        <select data-placeholder="Select Text Position" id="blogInterestedThreeCategoryId"
                                                name="blogInterestedThreeCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Interested Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($blog->blogInterestedThreeCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Seo Settings</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogPageTitle">Page Title</label>
                                        <input placeholder="Page Title" class="form-control" type="text"
                                               id="blogPageTitle" value="<?php echo $blog->blogPageTitle; ?>"
                                               name="blogPageTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogSeoTitle">Seo Title</label>
                                        <input placeholder="Seo Title" class="form-control" type="text"
                                               id="blogSeoTitle" value="<?php echo $blog->blogSeoTitle; ?>"
                                               name="blogSeoTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogSeoKeywords">Seo Keywords</label>
                                        <input placeholder="Seo Keywords" class="form-control" type="text"
                                               id="blogSeoKeywords" value="<?php echo $blog->blogSeoKeywords; ?>"
                                               name="blogSeoKeywords"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogSeoDescription">
                                            Seo Description
                                        </label>
                                        <textarea name="blogSeoDescription" id="blogSeoDescription" rows="5"
                                                  placeholder="Seo Description"><?php echo $blog->blogSeoDescription; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/edit_blog.js"></script>




