<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Media Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_media_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_media_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Media Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataMediaTitle">Title</label>
                                        <input placeholder="Title" class="form-control" type="text"
                                               id="homeDataMediaTitle"
                                               value="<?php echo $homeData->homeDataMediaTitle; ?>"
                                               name="homeDataMediaTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataMediaSubTitle">
                                            Sub Title
                                        </label>
                                        <textarea name="homeDataMediaSubTitle" id="homeDataMediaSubTitle" rows="5"
                                                  placeholder="Sub Title"><?php echo $homeData->homeDataMediaSubTitle; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataMediaDescription">
                                            Description
                                        </label>
                                        <textarea name="homeDataMediaDescription" id="homeDataMediaDescription" rows="5"
                                                  placeholder="Description"><?php echo $homeData->homeDataMediaDescription; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="homeDataMediaButtonText">Button Text</label>
                                        <input placeholder="Button Text" class="form-control" type="text"
                                               id="homeDataMediaButtonText"
                                               value="<?php echo $homeData->homeDataMediaButtonText; ?>"
                                               name="homeDataMediaButtonText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="homeDataMediaButtonCategoryId">Button Href</label>
                                        <select data-placeholder="Select Text Position" id="homeDataMediaButtonCategoryId"
                                                name="homeDataMediaButtonCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataMediaButtonCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_media_section.js"></script>




