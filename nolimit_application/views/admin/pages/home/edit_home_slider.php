<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Slider Listings / Edit Home Slider</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_slider_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_home_slider'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="homeSliderId" value="<?php echo $homeSlider->homeSliderId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Home Slider</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $homeSlider->homeSliderImage; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Image
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="homeSliderImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="homeSliderImage"
                                               value="<?php echo $homeSlider->homeSliderImage; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeSliderTitleOne">Title One</label>
                                        <input placeholder="Title One" class="form-control" type="text"
                                               id="homeSliderTitleOne"
                                               value="<?php echo $homeSlider->homeSliderTitleOne; ?>"
                                               name="homeSliderTitleOne"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeSliderTitleTwo">Title Two</label>
                                        <textarea placeholder="Title Two"  id="homeSliderTitleTwo" name="homeSliderTitleTwo" name=""><?php echo $homeSlider->homeSliderTitleTwo; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/edit_home_slider.js"></script>




