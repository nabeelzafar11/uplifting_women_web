<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home About Section One</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_about_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_about_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home About Section One</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataAboutTitle">Title</label>
                                        <textarea name="homeDataAboutTitle" id="homeDataAboutTitle" rows="5"
                                                  placeholder="Sub Title"><?php echo $homeData->homeDataAboutTitle; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12" style='display:none;'>
                                    <div class="form-group">
                                        <label for="homeDataAboutSubTitle">
                                            Sub Title
                                        </label>
                                        <textarea name="homeDataAboutSubTitle" id="homeDataAboutSubTitle" rows="5"
                                                  placeholder="Sub Title"><?php echo $homeData->homeDataAboutSubTitle; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataAboutDescription">
                                            Description
                                        </label>
                                        <textarea name="homeDataAboutDescription" id="homeDataAboutDescription" rows="5"
                                                  placeholder="Description"><?php echo $homeData->homeDataAboutDescription; ?></textarea>
                                    </div>
                                </div>
                                <div  style='display:none;'>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="homeDataAboutCategoryOne">Category One</label>
                                            <select data-placeholder="Select Text Position" id="homeDataAboutCategoryOne"
                                                    name="homeDataAboutCategoryOne"
                                                    class="form-control chosen-select" tabindex="2">
                                                        <?php foreach ($categories as $category) { ?>
                                                    <option
                                                    <?php if ($homeData->homeDataAboutCategoryOne == $category->categoryId) { ?>
                                                            selected="selected"
                                                        <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="homeDataAboutCategoryTwo">Category Two</label>
                                            <select data-placeholder="Select Text Position" id="homeDataAboutCategoryTwo"
                                                    name="homeDataAboutCategoryTwo"
                                                    class="form-control chosen-select" tabindex="2">
                                                        <?php foreach ($categories as $category) { ?>
                                                    <option
                                                    <?php if ($homeData->homeDataAboutCategoryOne == $category->categoryId) { ?>
                                                            selected="selected"
                                                        <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="homeDataAboutCategoryThree">Category Three</label>
                                            <select data-placeholder="Select Text Position" id="homeDataAboutCategoryThree"
                                                    name="homeDataAboutCategoryThree"
                                                    class="form-control chosen-select" tabindex="2">
                                                        <?php foreach ($categories as $category) { ?>
                                                    <option
                                                    <?php if ($homeData->homeDataAboutCategoryThree == $category->categoryId) { ?>
                                                            selected="selected"
                                                        <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="homeDataAboutCategoryFour">Category Four</label>
                                            <select data-placeholder="Select Text Position" id="homeDataAboutCategoryFour"
                                                    name="homeDataAboutCategoryFour"
                                                    class="form-control chosen-select" tabindex="2">
                                                        <?php foreach ($categories as $category) { ?>
                                                    <option
                                                    <?php if ($homeData->homeDataAboutCategoryFour == $category->categoryId) { ?>
                                                            selected="selected"
                                                        <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_about_section.js"></script>




