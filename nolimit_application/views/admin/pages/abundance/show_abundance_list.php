<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Abundance Listings</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                     <?php } ?>
                 <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($abundance as $b) { ?>
                    <li data-id="<?php echo $b->id; ?>">
                        <div class="small-listing-box white-bg  <?php echo $b->isDeleted == 1 ? "alert alert-danger " : ""; ?> ">
                            <div class="small-list-detail">
                                <h4><?php echo $b->message; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <a href="<?php echo base_url('admin/abundance/edit_abundance_item/' . $b->id); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);"
                                   onclick="delete_item(this, <?php echo $b->id; ?>, '<?php echo base_url('api/delete_abundance_list'); ?>');"
                                   class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i
                                        class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
    </div>
</div>  

<script>
    var sortUrl = '<?php echo base_url('api/sort_abundance_list'); ?>';
</script>



