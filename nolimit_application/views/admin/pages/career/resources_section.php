<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Career Page/ Resources Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                  <form id="career-resources-form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_career_resources_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Resources Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                
                            <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogContent">
                                            Resources Content
                                        </label>
                                        <textarea name="resourcesContent" id="resourcesContent" rows="5" placeholder="Blog Content">
                                        <?php echo $careerPage->careerPageResourcesSection; ?>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>


            </form>
        </div>

    </div>


    

</div>

<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        CKEDITOR.replace('resourcesContent', {
            extraPlugins: 'accordion,downloader',
            height: 400
        });

        $("#career-resources-form").validate({
            rules: {
                resourcesContent: {
                    required: true
                }
            },
            submitHandler: function (form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });

    }
}
</script>