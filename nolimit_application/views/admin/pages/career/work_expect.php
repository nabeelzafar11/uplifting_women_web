<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Career Page/ Work Expectations Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                  <form id="career-work-expect-form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_career_work_exp_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Work Expectations Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                             
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $careerPage->careerPageWorkExpImage; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Icon
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="career-work-expect-image" value="<?php echo MEDIA_PATH . $careerPage->careerPageWorkExpImage; ?>" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="career-work-expect-image1"
                                               value="<?php echo MEDIA_PATH . $careerPage->careerPageWorkExpImage; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Heading</label>
                                        <input placeholder="Heading" class="form-control" type="text" id="blogTitle" value="<?php echo $careerPage->careerPageWorkExpHeading; ?>" name="career-work-expect-heading"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogDescription">
                                            Text
                                        </label>
                                        <textarea name="career-work-expect-text" id="blogDescription" rows="5"
                                                  placeholder="Text"><?php echo $careerPage->careerPageWorkExpText; ?></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>


            </form>
        </div>

    </div>

</div>

<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#career-work-expect-form").validate({
            rules: {
                'career-join-now-heading': {
                    required: true
                },
                'career-work-expect-heading': {
                    required: true
                },

                'career-work-expect-text': {
                    required: true
                }

            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
</script>