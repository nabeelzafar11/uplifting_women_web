<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Career Page/ Support Role Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                  <form id="career-support-role-form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_career_support_role_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Join Now Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Heading</label>
                                        <input placeholder="Heading" class="form-control" type="text" id="blogTitle" value="<?php echo $careerPage->careerPageSupportHeading; ?>" name="career-support-role-heading"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="role1">
                                            Role 1
                                        </label>
                                        <textarea name="career-support-role-1" id="role1" rows="5"
                                                  placeholder="Role 1"><?php echo $careerPage->careerPageSupportRole1; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="role2">
                                            Role 2
                                        </label>
                                        <textarea name="career-support-role-2" id="role2" rows="5"
                                                  placeholder="Role 2"><?php echo $careerPage->careerPageSupportRole2; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="role3">
                                            Role 3
                                        </label>
                                        <textarea name="career-support-role-3" id="role3" rows="5"
                                                  placeholder="Role 3"><?php echo $careerPage->careerPageSupportRole3; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="role4">
                                            Role 4
                                        </label>
                                        <textarea name="career-support-role-4" id="role4" rows="5"
                                                  placeholder="Role 4"><?php echo $careerPage->careerPageSupportRole4; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="role5">
                                            Role 5
                                        </label>
                                        <textarea name="career-support-role-5" id="role5" rows="5"
                                                  placeholder="Role 5"><?php echo $careerPage->careerPageSupportRole5; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="role6">
                                            Role 6
                                        </label>
                                        <textarea name="career-support-role-6" id="role6" rows="5"
                                                  placeholder="Role 6"><?php echo $careerPage->careerPageSupportRole6; ?></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>


            </form>
        </div>

    </div>

</div>

<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#career-support-role-form").validate({
            rules: {
                'career-support-role-heading': {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
</script>