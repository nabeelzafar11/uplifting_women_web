<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Career Page/ Why Work for us Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                  <form id="career-why-work-form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_career_work_for_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Work for us Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Heading</label>
                                        <input placeholder="Heading" class="form-control" type="text" id="blogTitle" value="<?php echo $careerPage->careerPageWorkHeading; ?>" name="career-work-for-heading"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogDescription">
                                            Text
                                        </label>
                                        <textarea name="career-work-for-text" id="blogDescription" rows="5"
                                                  placeholder="Text"><?php echo $careerPage->careerPageWorkText; ?></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>


            </form>
        </div>

    </div>

</div>

<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#career-why-work-form").validate({
            rules: {
                'career-work-for-heading': {
                    required: true
                },
                'career-work-for-text': {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
</script>