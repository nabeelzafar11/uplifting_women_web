<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Positive Affirmations / Add New Item</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="dashboard_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_positive_affirmation_dashboard_item'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Positive Affirmations Item</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo ASSETS_PATH; ?>listeo_updated/images/placeholder.png"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Image
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="image" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input placeholder="Name" class="form-control" type="text"
                                               id="name"
                                               name="name"/>
                                    </div>
                                </div>
<!--                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="popupMessage">
                                            Message
                                        </label>
                                        <textarea name="popupMessage" id="popupMessage" rows="5"
                                                  placeholder="Please Enter the message you want to appear when the user clicks on the 'i' Button"></textarea>
                                    </div>
                                </div>-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="seo_title">SEO Title</label>
                                        <input placeholder="Please Enter SEO Title" class="form-control" type="text"
                                               id="seo_title"
                                               name="seo_title"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="seo_keywords">SEO Keywords</label>
                                        <input placeholder="Please Enter SEO Keywords" class="form-control" type="text"
                                               id="seo_keywords"
                                               name="seo_keywords"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="popupMessage">
                                            SEO Descriptipn
                                        </label>
                                        <textarea name="seo_description" rows="5"
                                                  placeholder="Please Enter page's SEO descritpion"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_positiveaffirmation_dashboard_item.js"></script>




