<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Self Love/Self Care Dashboard</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                     <?php } ?>
                 <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="">
                <?php foreach ($dashboard as $d) { ?>
                    <li data-id="<?php echo $d->month; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <img src="<?php echo MEDIA_PATH . $d->imageWeb; ?>" class="img-responsive" alt="">
                                <p><?php
                                    $monthNum = $d->month;
                                    $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                    $monthName = $dateObj->format('F');
                                    echo $monthName
                                    ?></p>
                            </div>
                            <div class="small-list-action">
                                <a href="<?php echo base_url('admin/selflovecaredashboard/edit_dashboard_item/' . $d->month); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Edit Item"><i class="ti-pencil"></i></a>
                            </div>
                        </div>
                    </li>
<?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
    </div>
</div>  
<script>
    var sortUrl = '<?php echo base_url('api/sort_positiveaffirmation_dashboard_list'); ?>';
</script>





