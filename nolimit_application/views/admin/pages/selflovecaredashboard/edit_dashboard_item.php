<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Self Love/Self Care Dashboard / Edit Item</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="dashboard_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_selflovecare_dashboard_item'); ?>"
                  onsubmit="return false;" enctype="multipart/form-data">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Self Love/Self Care Dashboard Item</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $dashboard->imageWeb; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                   Upload Web Image
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="imageWeb" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $dashboard->imageMobile; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                   Upload Mobile Image
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="imageMobile" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h3 for="name">Month - <?php
                                    $monthNum = $dashboard->month;
                                    $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                    $monthName = $dateObj->format('F');
                                    echo $monthName
                                    ?></h3>
                                    </div>
                                </div>
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="web_header">
                                            Heading
                                        </label>
                                        <textarea name="web_header" id="web_header" rows="5"
                                                  placeholder="Please Enter the message you want to appear when the user clicks on the 'i' Button"><?php echo $dashboard->web_header; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="web_message">
                                            Message
                                        </label>
                                        <textarea name="web_message" id="web_message" rows="5"
                                                  placeholder="Please Enter the message you want to display on the Self Love/Self Care list"><?php echo $dashboard->web_message; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="seo_title">SEO Title</label>
                                        <input placeholder="Please Enter SEO Title" class="form-control" value="<?php echo $dashboard->seo_title; ?>" type="text"
                                               id="seo_title"
                                               name="seo_title"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="seo_keywords">SEO Keywords</label>
                                        <input placeholder="Please Enter SEO Keywords" class="form-control" type="text"
                                               id="seo_keywords" value="<?php echo $dashboard->seo_keywords; ?>"
                                               name="seo_keywords"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="seo_description">
                                            SEO Descriptipn
                                        </label>
                                        <textarea name="seo_description" rows="5"
                                                  placeholder="Please Enter page's SEO descritpion"><?php echo $dashboard->seo_description; ?></textarea>
                                    </div>
                                </div>
                                <input type='hidden' name='month' value='<?php echo $dashboard->month?>'>
                                <input type='hidden' name='defaultImageWeb' value='<?php echo $dashboard->imageWeb?>'>
                                <input type='hidden' name='defaultImageMobile' value='<?php echo $dashboard->imageMobile?>'>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/edit_selflovecare_dashboard.js"></script>




