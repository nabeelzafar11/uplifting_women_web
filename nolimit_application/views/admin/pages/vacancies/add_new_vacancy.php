<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Career Page/ Vacancy Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                  <form id="newVacancy" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_vacancy'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add a new Vacancy</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Designation</label>
                                        <input placeholder="Designation" class="form-control" type="text" id="blogTitle" value="" name="vacancyDesignation"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exp">Experience Required</label>
                                        <input placeholder="Experience Required" class="form-control" type="text" id="exp" value="" name="vacancyExp"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qual">Qualification Required</label>
                                        <input placeholder="Qualification Required" class="form-control" type="text" id="qual" value="" name="vacancyQual"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qual">Apply Email</label>
                                        <input placeholder="Apply Email" class="form-control" type="email" id="qual" value="" name="vacancyEmail"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="menuParentId">Vacancy Area</label>
                                        <select data-placeholder="Select Vacancy Area" id="menuParentId"
                                                name="vacancyArea"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value=""></option>
                                            <option value="melbourne">Melbourne</option>
                                            <option value="darwin">Darwin</option>
                                            <option value="tiwiIsland">Tiwi Island</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Skill 1</label>
                                        <input placeholder="Skill 1" class="form-control" type="text" id="blogTitle" value="" name="skill1"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Skill 2</label>
                                        <input placeholder="Skill 2" class="form-control" type="text" id="blogTitle" value="" name="skill2"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Skill 3</label>
                                        <input placeholder="Skill 3" class="form-control" type="text" id="blogTitle" value="" name="skill3"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Skill 4</label>
                                        <input placeholder="Skill 4" class="form-control" type="text" id="blogTitle" value="" name="skill4"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="blogTitle">Skill 5</label>
                                        <input placeholder="Skill 5" class="form-control" type="text" id="blogTitle" value="" name="skill5"/>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
               
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>


            </form>
        </div>

    </div>

</div>

<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#newVacancy").validate({
            rules: {
                'vacancyDesignation': {
                    required: true
                },
                'vacancyExp': {
                    required: true
                },
                'vacancyQual': {
                    required: true
                },
                'vacancyEmail': {
                    required: true
                },
                'vacancyArea': {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
</script>