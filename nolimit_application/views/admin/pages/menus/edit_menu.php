<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Menu Listings / Edit Menu</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="menu_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_menu'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="menuId" value="<?php echo $menu->menuId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Menu</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="edit-profile-photo gray-bg imageDiv">
                                    <img src="<?php echo MEDIA_PATH . $menu->menuIcon; ?>"
                                         alt="">
                                    <div class="change-photo-btn">
                                        <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Mobile Icon
                                            </span>
                                        </div>
                                    </div>
                                    <input type="file" name="menuIcon" value="" onchange="readURL(this);"
                                           style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    <input type="hidden" name="menuIcon"
                                           value="<?php echo $menu->menuIcon; ?>"/>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="categoryName">Menu Name</label>
                                        <input placeholder="Menu Name" class="form-control" type="text" id="menuName"
                                               name="menuName" value="<?php echo $menu->menuName; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuIconName">Menu Icon Name</label>
                                        <input placeholder="Menu Icon Name" class="form-control" type="text"
                                               id="menuIconName" value="<?php echo $menu->menuIconName; ?>"
                                               name="menuIconName"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuCategoryId">Menu Href</label>
                                        <select data-placeholder="Select Text Position" id="menuCategoryId"
                                                name="menuCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if($menu->menuCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuParentId">Parent Menu</label>
                                        <select data-placeholder="Select Text Position" id="menuParentId"
                                                name="menuParentId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Parent Menu</option>
                                            <?php foreach ($menus as $menu1) { ?>
                                                <option
                                                    <?php if($menu->menuParentId == $menu1->menuId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $menu1->menuId; ?>"><?php echo $menu1->menuName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_menu.js"></script>




