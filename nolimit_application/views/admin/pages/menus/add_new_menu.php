<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Menu Listings / Add New Menu</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="menu_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_menu'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Menu</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo ASSETS_PATH; ?>listeo_updated/images/placeholder.png"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Mobile Icon
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="menuIcon" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuName">Menu Name</label>
                                        <input placeholder="Menu Name" class="form-control" type="text"
                                               id="menuName"
                                               name="menuName"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuIconName">Menu Icon Name</label>
                                        <input placeholder="Menu Icon Name" class="form-control" type="text"
                                               id="menuIconName"
                                               name="menuIconName"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuCategoryId">Menu Href</label>
                                        <select data-placeholder="Select Text Position" id="menuCategoryId"
                                                name="menuCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Parent Menu</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="menuParentId">Parent Menu</label>
                                        <select data-placeholder="Select Text Position" id="menuParentId"
                                                name="menuParentId"
                                                class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Parent Menu</option>
                                            <?php foreach ($menus as $menu) { ?>
                                                <option value="<?php echo $menu->menuId; ?>"><?php echo $menu->menuName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_menu.js"></script>




