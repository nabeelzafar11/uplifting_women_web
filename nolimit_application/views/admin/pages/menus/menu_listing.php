<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Menu Listings / Menu Button
                </h2>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>
    <div class="row">
        <div class="col-sm-6">
            <form id="header_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_header_button'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Menu Button</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="menuButtonText">Menu Button Text</label>
                                        <input placeholder="Menu Button Text" class="form-control" type="text"
                                               id="menuButtonText" value="<?php echo $header->menuButtonText; ?>"
                                               name="menuButtonText"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="menuButtonCategoryId">Menu Button Href</label>
                                        <select data-placeholder="Menu Button Href" id="menuButtonCategoryId"
                                                name="menuButtonCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category1) { ?>
                                                <option
                                                        <?php if($header->menuButtonCategoryId == $category1->categoryId){ ?>
                                                            selected="selected"
                                                        <?php } ?>
                                                        value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Save Changes</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

    <!-- Titlebar -->
    <div id="titlebar" style="margin-top: 40px;">
        <div class="row">
            <div class="col-md-12">
                <h2>Menu Listings </h2>
                <?php if ($parentMenuNames) { ?>
                    <b><?php echo $parentMenuNames; ?></b>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper"> <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($menus as $menu) { ?>
                    <li data-id="<?php echo $menu->menuId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <?php if ($menu->childCount) { ?>
                                    <a href="<?php echo base_url('admin/menu_listings/sub_menu/' . $menu->menuId); ?>">
                                        <h4><?php echo $menu->menuName; ?>
                                            <span>(<b><?php echo $menu->childCount; ?></b>)</span></h4>
                                    </a>
                                <?php } else { ?>
                                    <h4><?php echo $menu->menuName; ?></h4>
                                <?php } ?>
                            </div>
                            <div class="small-list-action">
                                <label class="switch">
                                    <input type="checkbox" <?php if ($menu->menuIsActive) { ?> checked="checked" <?php } ?>
                                           onchange="trigger_is_active(this, <?php echo $menu->menuId; ?>, '<?php echo base_url('api/update_menu_status');?>')">
                                    <span class="slider round"></span>
                                </label>
                                <a href="<?php echo base_url('admin/menu_listings/edit_menu/' . $menu->menuId); ?>"
                                   class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip"
                                   title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);"
                                   onclick="delete_item(this, <?php echo $menu->menuId; ?>, '<?php echo base_url('api/delete_menu'); ?>');"
                                   class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i
                                            class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($menus) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    var sortUrl = '<?php echo base_url('api/sort_menu'); ?>';
</script>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/header.js"></script>





