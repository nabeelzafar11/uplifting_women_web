<?php defined('BASEPATH') OR exit('No direct script access allowed');

function encode($value)
{
    if (!$value) {
        return false;
    }
    $ci =& get_instance();
    return strtr($ci->encryption->encrypt($value), array('+' => '--1', '=' => '--2', '/' => '--3'));
}

function decode($value)
{
    if (!$value) {
        return false;
    }
    $ci =& get_instance();
    return $ci->encryption->decrypt(strtr($value, array('--1' => '+', '--2' => '=', '--3' => '/')));
}

function validateDate($date, $format = 'm/d/Y')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

function print_result($result)
{
    echo "<pre>";
    print_r($result);
    echo "</pre>";
    die();
}

function print_api_result($result)
{
    $res = array(
        'status' => "success",
        'message' => $result
    );

    echo json_encode($res);
    die();
}