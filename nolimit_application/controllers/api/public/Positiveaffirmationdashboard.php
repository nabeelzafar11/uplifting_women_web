<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Positiveaffirmationdashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function show_app_positiveaffirmation_listing() {
        $dashboard = $this->positiveaffirmationdashboard_model->get_records();
        $this->send_api_respone('', '', 'success', $dashboard);
    }

}
