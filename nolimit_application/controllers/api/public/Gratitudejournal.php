<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gratitudejournal extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    function fetch_current_date_type_gratitude_journal() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('date', 'date', 'trim|required');
        $this->form_validation->set_rules('type', 'type', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $date = trim(html_escape($this->input->post('date')));
            $type = trim(html_escape($this->input->post('type')));
            $whereCondition = array("user_id" => $user_id, "type" => $type, "date" => $date);
            $records = $this->gratitudejournal_model->get_record($whereCondition);
            if (empty($records)) {
                $current_date = date("Y-m-d");
                if ($type == 1) {
                    $records = (object) array("header" => "Things I am grateful for today", "message" => "", "date" => "$current_date");
                } else if ($type == 2) {
                    $records = (object) array("header" => "Things I am grateful for about my body", "message" => "", "date" => "$current_date");
                } else {
                    $records = (object) array("header" => "Things I am grateful for about my family", "message" => "", "date" => "$current_date");
                }
            }
            $this->send_api_respone('', '', 'success', $records);
        }
    }

    public function fetch_list() {
        $user_id = $this->user->userId;
        $current_date = (!empty($this->input->post("date")) && $this->validateDate($this->input->post("date"))) ? $this->input->post("date") : date("Y-m-d");
        $whereCondition = array("user_id" => $user_id, "type" => 1, "date" => "$current_date");
        $default_type1 = $this->gratitudejournal_model->get_record($whereCondition);

        $default_type1_all_dates = $this->gratitudejournal_model->get_records($whereCondition);
        $type1_dates_array = array();
        foreach ($default_type1_all_dates as $r) {
            array_push($type1_dates_array, $r->date);
        }

        $whereCondition = array("user_id" => $user_id, "type" => 2, "date" => "$current_date");
        $default_type2 = $this->gratitudejournal_model->get_record($whereCondition);

        $default_type2_all_dates = $this->gratitudejournal_model->get_records($whereCondition);
        $type2_dates_array = array();
        foreach ($default_type2_all_dates as $r) {
            array_push($type2_dates_array, $r->date);
        }

        $whereCondition = array("user_id" => $user_id, "type" => 3, "date" => "$current_date");
        $default_type3 = $this->gratitudejournal_model->get_record($whereCondition);
        $default_type3_all_dates = $this->gratitudejournal_model->get_records($whereCondition);
        $type3_dates_array = array();
        foreach ($default_type3_all_dates as $r) {
            array_push($type3_dates_array, $r->date);
        }
        if (!$default_type1) {
            $default_type1 = (object) array("header" => "Things I am grateful for today", "message" => "", "date" => "$current_date");
        }
        if (!$default_type2) {
            $default_type2 = (object) array("header" => "Things I am grateful for about my body", "message" => "", "date" => "$current_date");
        }
        if (!$default_type3) {
            $default_type3 = (object) array("header" => "Things I am grateful for about my family", "message" => "", "date" => "$current_date");
        }

        $data["default_type1"] = $default_type1;
        $data["default_type2"] = $default_type2;
        $data["default_type3"] = $default_type3;

        $data["type1_dates_array"] = $type1_dates_array;
        $data["type2_dates_array"] = $type2_dates_array;
        $data["type3_dates_array"] = $type3_dates_array;

        $this->send_api_respone('', '', 'success', $data);
    }

    public function delete_gratitudejournal_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('date', 'Date', 'trim|required');
        $this->form_validation->set_rules('type', 'type', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $date = trim(html_escape($this->input->post('date')));
            $type = trim(html_escape($this->input->post('type')));
            $whereCondition = array("user_id" => $user_id, "date" => $date, "type" => $type);
            if ($this->gratitudejournal_model->delete_record($whereCondition)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function add_custom_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('header', 'Heading', 'trim|required');
        $this->form_validation->set_rules('date', 'Date', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('type', 'type', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $header = trim(html_escape($this->input->post('header')));
            $date = trim(html_escape($this->input->post('date')));
            $message = trim(html_escape($this->input->post('message')));
            $type = trim(html_escape($this->input->post('type')));
            if ($type < 0 || $type > 3) {
                $type = 1;
            }
            $updatedAt = date("Y-m-d h:i:s");
            $whereCondition = array("user_id" => $user_id, "date" => $date, "type" => $type);
            $recordData = array("header" => $header, "date" => $date, "message" => $message, "updatedAt" => $updatedAt);
            $result = $this->gratitudejournal_model->get_record($whereCondition);
            if ($result) { //record exists, UPDATE existing record
                if ($this->gratitudejournal_model->update_record($whereCondition, $recordData)) {
                    $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
                } else {
                    $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
                }
            } else { //NO record exists, Insert record
                $recordData = array("user_id" => $user_id, "header" => $header, "date" => $date, "message" => $message, "type" => $type);
                if ($this->gratitudejournal_model->insert_record($recordData)) {
                    $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
                } else {
                    $this->send_api_respone('', '', 'danger', FAILED_ADDED);
                }
            }
        }
    }

}
