<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function update_fcm_token() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fcm_token', "Token", 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            $fcm_token = html_escape($this->input->post('fcm_token'));
            $recordData = array(
                'fcm-tokenid' => $fcm_token
            );
            $user_id = $this->user->userId;
            $whereCondition = array("userId" => $user_id);
            if ($this->user_model->update_record($whereCondition, $recordData)) {
                $this->send_api_respone('', '', 'success', 'Your Device token has been updated!');
            } else {
                $this->send_api_respone('', '', 'danger', 'Failed to update.');
            }
        }
    }

    public function fetch_profile() {
        $user_id = $this->user->userId;
        $whereConditionArray = array('userId' => $user_id, "typeId" => 3);
        $resultUser = $this->user_model->get_record($whereConditionArray);
        $this->send_api_respone('', '', 'success', $resultUser);
    }

    public function update_profile() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('image', "Profile Picture", 'trim|required');
        $this->form_validation->set_rules('firstName', "First Name", 'trim|required');
        $this->form_validation->set_rules('lastName', "Last Name", 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            $user_id = $this->user->userId;

            $whereConditionArray = array('userId' => $user_id, "typeId" => 3);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if (empty($resultUser)) {
                $this->send_api_respone('', '', 'danger', "The user doesn't exists in out system.");
            }

            $image = html_escape($this->input->post('image'));
            $firstName = html_escape($this->input->post('firstName'));
            $lastName = html_escape($this->input->post('lastName'));
            $google_event_active = !empty($this->input->post('google_event_active')) ? 1 : 0;
            $recordData = array(
                'image' => $image,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'google_event_active' => $google_event_active
            );
            $whereCondition = array("userId" => $user_id);
            if ($this->user_model->update_record($whereCondition, $recordData)) {
                $this->send_api_respone('', '', 'success', 'Your personal settings have been updated!');
            } else {
                $this->send_api_respone('', '', 'danger', 'Failed to updated');
            }
        }
    }

    public function update_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', "Password", 'trim|required');
        $this->form_validation->set_rules('confirmPassword', "Confirm Password", 'trim|required|matches[password]');
        $this->form_validation->set_rules('currentPassword', "Current Password", 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            $userId = $this->user->userId;
            $whereConditionArray = array('userId' => $userId, "typeId" => 3);
            $resultUser = $this->user_model->get_record($whereConditionArray);

            if (empty($resultUser)) {
                $this->send_api_respone('', '', 'danger', "The user doesn't exists in out system.");
            }


            if (decode($resultUser->password) != html_escape($this->input->post('currentPassword'))) {
                $this->send_api_respone('', '', 'danger', "Current Password doesn't match.");
            }

            $password = encode(html_escape($this->input->post('password')));
            $recordData = array(
                'password' => $password,
            );
            $whereCondition = array("userId" => $userId);
            if ($this->user_model->update_record($whereCondition, $recordData)) {
                $this->send_api_respone('', '', 'success', 'Your password has been successfully updated.');
            } else {
                $this->send_api_respone('', '', 'danger', 'Failed to update.');
            }
        }
    }

}
