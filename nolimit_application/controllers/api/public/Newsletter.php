<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add_new_newsletter()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subscriberName', 'Name', 'trim|required');
        $this->form_validation->set_rules('subscriberEmail', 'Email', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $subscriberEmail = html_escape($this->input->post('subscriberEmail'));
            $whereCondition = array('subscriberEmail' => $subscriberEmail);
            $subscriptionResult = $this->subscription_model->get_record($whereCondition);
            if(!$subscriptionResult) {
                $recordData = array(
                    'subscriberEmail' => $subscriberEmail,
                    'subscriberName' => html_escape($this->input->post('subscriberName')),
                );
                $this->subscription_model->insert_record($recordData);
            }
            $this->send_api_respone('', '', 'success', "Thank you for subscribing to our news letter.");
        }
    }
}
