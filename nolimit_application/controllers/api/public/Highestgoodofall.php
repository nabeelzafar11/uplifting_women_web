<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Highestgoodofall extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function fetch_list() {
        $user_id = $this->user->userId;
        $whereCondition = array("isDeleted" => 0, "user_id" => $user_id);
        $records = $this->highestgoodofall_model->get_records($whereCondition);
        $this->send_api_respone('', '', 'success', $records);
    }

    public function delete_user_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Item Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("id" => html_escape($this->input->post('id')), 'user_id' => $user_id);
            $this->highestgoodofall_model->delete_record($whereCondition);
            $this->send_api_respone('', '', 'success', SUCCESSFULLY_DETELED);
        }
    }

    
    public function add_custom_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $message = trim(html_escape($this->input->post('message')));
            $whereCondition = $recordData = array("user_id" => $user_id, 'message' => $message);
            $result = $this->highestgoodofall_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            if ($id = $this->highestgoodofall_model->insert_record($recordData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED, $id);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $id = html_escape($this->input->post('id'));
            $name = html_escape($this->input->post('message'));
            $user_id = $this->user->userId;
            $whereCondition = array('message' => $name, 'id !=' => $id, 'user_id' => $user_id);
            $result = $this->highestgoodofall_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $whereCondition = array('id' => $id);
            $updateData = array(
                'message' => html_escape($this->input->post('message'))
            );
            if ($this->highestgoodofall_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
