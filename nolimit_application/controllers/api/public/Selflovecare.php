<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Selflovecare extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function fetch_month_information() {
        $month = !empty(html_escape($this->input->post('month_id'))) ? $this->input->post('month_id') : date("m");
        $records = $this->selflovecaredashboard_model->get_record(array("month" => $month));
        $this->send_api_respone('', '', 'success', $records);
    }

    public function fetch_events_dates_for_current_month() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('month_id', 'Month', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $month = html_escape($this->input->post('month_id'));
            $year = html_escape($this->input->post('year'));
            $query = "SELECT DISTINCT(DATE(dateandtime)) as dates from events WHERE MONTH(dateandtime) = '$month' AND YEAR(dateandtime) = '$year' AND user_id = $user_id";
            $records = $this->userselflovecare_model->generic_query($query);
            $this->send_api_respone('', '', 'success', $records);
        }
    }

    public function fetch_current_date_events() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('date', 'Date', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $date = html_escape($this->input->post('date'));
            $query = "SELECT usc.id, usc.user_id as userId, e.id as eventId, slc.message, DATE(e.dateandtime) as date, TIME(e.dateandtime) as time, e.dateandtime  from user_selflovecare_list as usc  LEFT JOIN selflovecare as slc ON slc.id = "
                    . " usc.selflovecare_id LEFT JOIN events as e ON usc.id = e.user_selflovecare_list_id where DATE(e.dateandtime) = '$date' "
                    . " AND usc.user_id = '$user_id'";
            $records = $this->userselflovecare_model->generic_query($query);
            $this->send_api_respone('', '', 'success', $records);
        }
    }

    public function fetch_list() {
        $user_id = $this->user->userId;
        $curr_month = date("m");
        if (!empty($this->input->post("month_id"))) {
            $curr_month = $this->input->post("month_id");
        }
         $whereCondition = array("selflovecare.isDeleted" => 0, "user_selflovecare_list.user_id" => $user_id, "selflovecare.month_id" => $curr_month);
        $records["event_list"] = $this->userselflovecare_model->get_records_join_bucketlist($whereCondition);
        $records["curr_month"] = $curr_month;
        $this->send_api_respone('', '', 'success', $records);
    }

    public function delete_user_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Item Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("id" => html_escape($this->input->post('id')), 'user_id' => $user_id);
            $this->userselflovecare_model->delete_record($whereCondition);
            $whereCondition = array("user_selflovecare_list_id" => html_escape($this->input->post('id')));
            $this->events_model->delete_record($whereCondition);
            $this->send_api_respone('', '', 'success', SUCCESSFULLY_DETELED);
        }
    }

    public function delete_single_event() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('eventId', 'Event Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $eventId = html_escape($this->input->post('eventId'));
            $query = "SELECT e.id, e.dateandtime FROM `user_selflovecare_list` as usl LEFT JOIN events as e ON usl.id = e.user_selflovecare_list_id WHERE usl.user_id = '$user_id' AND e.id = '$eventId'";
            $result = $this->userselflovecare_model->generic_query($query);
            if (!$result) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $whereCondition = array("id" => $eventId);
            $this->events_model->delete_record($whereCondition);
            $this->send_api_respone('', '', 'success', EVENT_SUCCESSFULLY_DETELED);
        }
    }

    public function get_single_event_details() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("user_selflovecare_list_id" => trim(html_escape($this->input->post('id'))), "user_id" => $user_id);
            $results = $this->events_model->get_records($whereCondition);
            $this->send_api_respone('', '', 'success', $results);
        }
    }

    public function delete_all_future_event() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('dateandtime', 'Date and Time', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $dateandtime = trim(html_escape($this->input->post('dateandtime')));
            $whereCondition = array("user_id" => $user_id, "dateandtime > " => $dateandtime);
            $this->events_model->delete_record($whereCondition);
            $this->send_api_respone('', '', 'success', FUTURE_EVENT_SUCCESSFULLY_DETELED);
        }
    }

    public function add_custom_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('dates[]', 'Date and Time', 'trim|required');
        $this->form_validation->set_rules('month_id', 'Month', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $message = trim(html_escape($this->input->post('message')));
            $month_id = trim(html_escape($this->input->post('month_id')));
            $whereCondition = $recordData = array("user_id" => $user_id, 'message' => $message, "month_id" => $month_id);
            $result = $this->selflovecare_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $selflovecare_id = $this->selflovecare_model->insert_record($recordData);
            if (empty($selflovecare_id)) {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
            $recordData = array(
                'user_id' => $user_id,
                'selflovecare_id' => $selflovecare_id
            );

            $expiry_google_event_token = $this->user->expiry_google_event_token;
            $google_event_access_token = $this->user->google_event_access_token;
            $google_event_active = $this->user->google_event_active;
            $google_event_refresh_token = $this->user->google_event_refresh_token;

            $capi = new GoogleCalendarApi();
            if ($userselflovecare_id = $this->userselflovecare_model->insert_record($recordData)) {
                $dates = $this->input->post("dates[]");
                $recordData = array();
                for ($i = 0; $i < sizeof($dates); $i++) {
                    if ($google_event_active == 1) {
                        if (time() > $expiry_google_event_token) {
                            $data = $this->GetRefreshedAccessToken(CLIENT_ID, $google_event_refresh_token, CLIENT_SECRET);
                            $expiry_google_event_token = time() + $data["expires_in"];
                            $google_event_access_token = $data["access_token"];
                            $this->update_user_records_for_google_events_in_database($user_id, $data);
                        }
                        $user_timezone = $capi->GetUserCalendarTimezone($google_event_access_token);
                        $currentDate = str_replace(" ", "T", $dates[$i]);
                        $event_time = array("start_time" => $currentDate, "end_time" => $currentDate);
                        $event_id = $capi->CreateCalendarEvent('primary', $message, 0, $event_time, $user_timezone, $google_event_access_token);
                    }
                    $recordData[$i] = array(
                        'user_selflovecare_list_id' => html_escape($userselflovecare_id),
                        'user_id' => $user_id,
                        'dateandtime' => html_escape($dates[$i])
                    );
                }

                if ($this->events_model->insert_batch($recordData)) {
                    $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
                } else {
                    $this->send_api_respone('', '', 'danger', FAILED_ADDED);
                }
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function fetch_add_new_items() {
        $user_id = $this->user->userId;
        $month = !empty(html_escape($this->input->post('month_id'))) ? $this->input->post('month_id') : date("m");
        $whereCondition = " WHERE ((user_id = 0 OR user_id = '$user_id') AND month_id = '$month') AND isDeleted = 0 AND id NOT IN (SELECT selflovecare_id FROM user_selflovecare_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $records = $this->selflovecare_model->fetch_add_new_abundance_items($whereCondition);
        $this->send_api_respone('', '', 'success', $records);
    }

    public function edit_existing_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Id', 'trim|required');
        $this->form_validation->set_rules('dates[]', 'Date and Time', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("user_id" => $user_id, 'id' => html_escape($this->input->post('id')));
            $result = $this->userselflovecare_model->get_record($whereCondition);
            if ($result) {
                $whereCondition = array("user_selflovecare_list_id" => html_escape($this->input->post('id')));
                $this->events_model->delete_record($whereCondition);
                $dates = $this->input->post("dates[]");
                $recordData = array();
                for ($i = 0; $i < sizeof($dates); $i++) {
                    $recordData[$i] = array(
                        'user_selflovecare_list_id' => html_escape($this->input->post('id')),
                        'dateandtime' => html_escape($dates[$i]),
                        'user_id' => $user_id
                    );
                }
                if ($this->events_model->insert_batch($recordData)) {
                    $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
                } else {
                    $this->send_api_respone('', '', 'danger', FAILED_ADDED);
                }
            } else {

                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function add_new_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Id', 'trim|required');
        $this->form_validation->set_rules('dates[]', 'Date and Time', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("user_id" => $user_id, 'selflovecare_id' => html_escape($this->input->post('id')));
            $result = $this->userselflovecare_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $expiry_google_event_token = $this->user->expiry_google_event_token;
            $google_event_access_token = $this->user->google_event_access_token;
            $google_event_active = $this->user->google_event_active;
            $google_event_refresh_token = $this->user->google_event_refresh_token;

            $capi = new GoogleCalendarApi();
            $whereCondition = array('id' => html_escape($this->input->post('id')));
            $itemMessage = $this->selflovecare_model->get_record($whereCondition);
            $itemMessage = $itemMessage->message;
            $recordData = array(
                'user_id' => $user_id,
                'selflovecare_id' => html_escape($this->input->post('id'))
            );
            if ($userselflovecare_id = $this->userselflovecare_model->insert_record($recordData)) {
                $dates = $this->input->post("dates[]");
                $recordData = array();
                for ($i = 0; $i < sizeof($dates); $i++) {
                    if ($google_event_active == 1) {
                        if (time() > $expiry_google_event_token) {
                            $data = $this->GetRefreshedAccessToken(CLIENT_ID, $google_event_refresh_token, CLIENT_SECRET);
                            $expiry_google_event_token = time() + $data["expires_in"];
                            $google_event_access_token = $data["access_token"];
                            $this->update_user_records_for_google_events_in_database($user_id, $data);
                        }
                        $user_timezone = $capi->GetUserCalendarTimezone($google_event_access_token);
                        $currentDate = str_replace(" ", "T", $dates[$i]);
                        $event_time = array("start_time" => $currentDate, "end_time" => $currentDate);
                        $event_id = $capi->CreateCalendarEvent('primary', $itemMessage, 0, $event_time, $user_timezone, $google_event_access_token);
                    }
                    //Use $itemMessage and $date[$i] to create google events here.
                    $recordData[$i] = array(
                        'user_selflovecare_list_id' => html_escape($userselflovecare_id),
                        'dateandtime' => html_escape($dates[$i]),
                        'user_id' => $user_id
                    );
                }
                if ($this->events_model->insert_batch($recordData)) {
                    $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
                } else {
                    $this->send_api_respone('', '', 'danger', FAILED_ADDED);
                }
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('eventId', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('dateandtime', 'Date and Time', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $eventId = html_escape($this->input->post('eventId'));
            $id = html_escape($this->input->post('id'));
            $dateandtime = html_escape($this->input->post('dateandtime'));
            $query = "SELECT e.id, e.dateandtime FROM `user_selflovecare_list` as usl LEFT JOIN events as e ON usl.id = e.user_selflovecare_list_id WHERE usl.user_id = '$user_id' AND e.id != '$eventId' AND e.user_selflovecare_list_id  = '$id' AND e.dateandtime = '$dateandtime'";
            $result = $this->userselflovecare_model->generic_query($query);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $query = "SELECT e.id, e.dateandtime FROM `user_selflovecare_list` as usl LEFT JOIN events as e ON usl.id = e.user_selflovecare_list_id WHERE usl.user_id = '$user_id' AND e.id = '$eventId' AND e.user_selflovecare_list_id  = '$id'";
            $result = $this->userselflovecare_model->generic_query($query);
            if (!$result) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $whereCondition = array('id' => $eventId);
            $updateData = array(
                'dateandtime' => $dateandtime
            );
            if ($this->events_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
