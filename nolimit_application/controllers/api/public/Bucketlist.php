<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bucketlist extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function fetch_user_bucketlist() {
        $user_id = $this->user->userId;
        $whereCondition = array("bucket_list.isDeleted" => 0, "user_bukcet_list.user_id" => $user_id);
        $records["bucket_list"] = $this->userbucketlist_model->get_records_join_bucketlist($whereCondition);
        $records["bucket_list_share_url"] = base_url() . "user-bucket-list/" . encode($user_id);
        $this->send_api_respone('', '', 'success', $records);
    }

    public function delete_user_bucketlist_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Bucket List Item Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("id" => html_escape($this->input->post('id')), 'user_id' => $user_id);
            $this->userbucketlist_model->delete_record($whereCondition);
            if (!$this->mobile_api_check()) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => "Item has been deleted successfully!"
                    )
                );
                $this->session->set_flashdata($notificationData);
            }
            $this->send_api_respone('', '', 'success', SUCCESSFULLY_DETELED);
        }
    }

    public function add_custom_bucketlist_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Bucket List Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $message = trim(html_escape($this->input->post('message')));
            $whereCondition = $recordData = array("user_id" => $user_id, 'message' => $message);
            $result = $this->bucketlist_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $bucketlist_id = $this->bucketlist_model->insert_record($recordData);
            if (empty($bucketlist_id)) {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
            $recordData = array("user_id" => $user_id, 'bucketlist_id' => $bucketlist_id);
            if ($id = $this->userbucketlist_model->insert_record($recordData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED, $id);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function fetch_add_new_bucketlist_items() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT bucketlist_id FROM user_bukcet_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $records = $this->bucketlist_model->fetch_add_new_bucketlist_items($whereCondition);
        $this->send_api_respone('', '', 'success', $records);
    }

    public function add_new_bucketlist_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('bucketlist_id[]', 'Bucket List Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $buckerIdsArray = $this->input->post('bucketlist_id');
            if (sizeof($buckerIdsArray) > 0) {
                foreach ($buckerIdsArray as $bucketid) {
                    $user_id = $this->user->userId;
                    $whereCondition = array("user_id" => $user_id, 'bucketlist_id' => $bucketid);
                    $result = $this->userbucketlist_model->get_record($whereCondition);
                    if ($result) {
                        $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
                    }
                    $recordData = array(
                        'user_id' => $user_id,
                        'bucketlist_id' => $bucketid
                    );
                    if ($id = $this->userbucketlist_model->insert_record($recordData)) {
                        //do nothing
                    } else {
                        $this->send_api_respone('', '', 'danger', FAILED_ADDED);
                    }
                }
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_bucketlist_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $id = html_escape($this->input->post('id'));
            $name = html_escape($this->input->post('message'));
            $whereCondition = array('message' => $name, 'id !=' => $id);
            $result = $this->bucketlist_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $whereCondition = array('id' => $id);
            $updateData = array(
                'message' => html_escape($this->input->post('message'))
            );
            if ($this->bucketlist_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
