<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function show_app_dashoboard_listing() {
        $dashboard = $this->dashboard_model->get_records();
        $this->send_api_respone('', '', 'success', $dashboard);
    }

}
