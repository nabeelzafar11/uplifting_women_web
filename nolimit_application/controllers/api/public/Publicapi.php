<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Publicapi extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function get_public_info() {
        $data["header"] = $this->header_model->custom_query(" gratitude_mobile,positiveaffirmation_mobile,highestgoodofall_mobile,login_mobile ");
        $curr_month = date("m");
        $data["calendardailyaffirmation_all_years"] = $this->calendardailyaffirmation_model->get_records();
        $data["calender_daily_affirmation"] = $this->calendardailyaffirmation_model->get_record(array("month" => $curr_month));
        $data["selflovecare_dashboard"] = $this->selflovecaredashboard_model->get_record(array("month" => $curr_month));
        $data["base_url"] = base_url();
        $data["media_path"] = MEDIA_PATH;
        $this->send_api_respone('', '', 'success', $data);
    }

    public function mobile_google_login() {
        if (isset($_GET['code']) && (!empty($_GET["state"]) && is_numeric(decode($_GET["state"])))) {

            $user_id = decode($_GET["state"]);
            try {
                $capi = new GoogleCalendarApi();
                // Get the access token 
                $data = $capi->GetAccessToken(CLIENT_ID,base_url("mobile/google-login"), CLIENT_SECRET, $_GET['code']);
                $this->update_user_records_for_google_events_in_database($user_id, $data);
                redirect(base_url("success_google_confirmation"));
            } catch (Exception $e) {
                redirect(base_url("fail_google_confirmation"));
            }
        }
    }

    public function success_google_login() {
        die();
    }

    public function fail_google_login() {
        die();
    }

    public function push_notification() {
        define('API_ACCESS_KEY', 'AIzaSyC8krlfaV8b4FAqadyi-KB-kw5MKBMKKmA');
        $date = date("Y-m-d H:i:00");
        $query = "SELECT e.dateandtime, u.`fcm-tokenid` as fcm_tokenid,slc.message FROM events as e LEFT JOIN user_selflovecare_list as usl ON e.user_selflovecare_list_id = usl.id LEFT JOIN selflovecare as slc ON slc.id = usl.selflovecare_id LEFT JOIN users as u on u.userId = usl.user_id WHERE e.dateandtime = '$date'";
        $records = $this->userselflovecare_model->generic_query($query);
        foreach ($records as $r) {
            if (trim($r->fcm_tokenid) != "not_registered") {
                $msg = array(
                    'body' => "Event: $r->message, starting at $r->dateandtime",
                    'title' => "Uplifting Women"
                );

                $fields = array(
                    'to' => $r->fcm_tokenid,
                    'notification' => $msg
                );

                $headers = array(
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
            }
        }
    }

}
