<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->mobile_api_check();
    }

    public function get_menus()
    {
        $whereCondition = array('menuParentId' => '0' ,'menuIsActive' => 1);
        $menus = $this->menu_model->get_records_with_join($whereCondition);
        $resultArray = array();
        foreach ($menus as $key => $menu) {
            $resultArray[$key]['menuId'] = $menu->menuId;
            $resultArray[$key]['menuIconName'] = $menu->menuIconName;
            $resultArray[$key]['menuParentId'] = $menu->menuParentId;
            $resultArray[$key]['menuName'] = htmlspecialchars_decode($menu->menuName);
            $resultArray[$key]['menuHref'] = '';
            $menuCategoryId = $menu->menuCategoryId;
            while ($menuCategoryId > 0){
                $whereCondition = array('categoryId' => $menuCategoryId);
                $menuCategory = $this->category_model->get_record($whereCondition);
                if($menuCategory){
                    if($resultArray[$key]['menuHref']){
                        $resultArray[$key]['menuHref'] = "/".$resultArray[$key]['menuHref'];
                    }
                    $resultArray[$key]['menuHref'] = $menuCategory->categorySlug . $resultArray[$key]['menuHref'];
                }else{
                    $resultArray[$key]['menuHref'] = '';
                    break;
                }
                $menuCategoryId = $menuCategory->categoryParentId;
            }
            if($resultArray[$key]['menuHref']){
                $resultArray[$key]['menuHref'] = base_url() . $resultArray[$key]['menuHref'] . "?mobile=1";
            }
            $resultArray[$key]['menuIcon'] = base_url() . "assets/uploaded_media/" . $menu->menuIcon;
            $resultArray[$key]['subMenu'] = array();
            $whereCondition = array('menuParentId' => $menu->menuId,'menuIsActive' => 1);
            $subMenus = $this->menu_model->get_records_with_join($whereCondition);
            foreach ($subMenus as $key1 => $menu1) {
                $resultArray[$key]['subMenu'][$key1]['menuId'] = $menu1->menuId;
                $resultArray[$key]['subMenu'][$key1]['menuIconName'] = $menu1->menuIconName;
                $resultArray[$key]['subMenu'][$key1]['menuParentId'] = $menu1->menuParentId;
                $resultArray[$key]['subMenu'][$key1]['menuName'] = htmlspecialchars_decode($menu1->menuName);
                $resultArray[$key]['subMenu'][$key1]['menuHref'] = '';
                $menuCategoryId = $menu1->menuCategoryId;
                while ($menuCategoryId > 0){
                    $whereCondition = array('categoryId' => $menuCategoryId);
                    $menuCategory = $this->category_model->get_record($whereCondition);
                    if($menuCategory){
                        if($resultArray[$key]['subMenu'][$key1]['menuHref']){
                            $resultArray[$key]['subMenu'][$key1]['menuHref'] = "/".$resultArray[$key]['subMenu'][$key1]['menuHref'];
                        }
                        $resultArray[$key]['subMenu'][$key1]['menuHref'] = $menuCategory->categorySlug . $resultArray[$key]['subMenu'][$key1]['menuHref'];
                    }else{
                        $resultArray[$key]['subMenu'][$key1]['menuHref'] = '';
                        break;
                    }
                    $menuCategoryId = $menuCategory->categoryParentId;
                }
                if($resultArray[$key]['subMenu'][$key1]['menuHref']){
                    $resultArray[$key]['subMenu'][$key1]['menuHref'] = base_url() . $resultArray[$key]['subMenu'][$key1]['menuHref'] . "?mobile=1";
                }
                $resultArray[$key]['subMenu'][$key1]['menuIcon'] = base_url() . "assets/uploaded_media/" . $menu1->menuIcon;
            }
            $resultArray[$key]['hasChild'] = count($resultArray[$key]['subMenu']) ? 1 : 0;
        }
        $this->send_api_respone('', '', 'success', $resultArray);
    }
}
