<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Love extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function fetch_list() {
        $user_id = $this->user->userId;
        $whereCondition = array("abundance_list.isDeleted" => 0, "user_abundance_list.user_id" => $user_id);
        $records = $this->positiveaffirmationsuserlist_model->get_records_join_bucketlist($whereCondition);
        $this->send_api_respone('', '', 'success', $records);
    }

    public function delete_user_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Item Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("id" => html_escape($this->input->post('id')), 'user_id' => $user_id);
            $this->positiveaffirmationsuserlist_model->delete_record($whereCondition);
            if (!$this->mobile_api_check()) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => "Item has been deleted successfully!"
                    )
                );
                $this->session->set_flashdata($notificationData);
            }
            $this->send_api_respone('', '', 'success', SUCCESSFULLY_DETELED);
        }
    }

    public function add_custom_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $message = trim(html_escape($this->input->post('message')));
            $whereCondition = $recordData = array("user_id" => $user_id, 'message' => $message);
            $result = $this->positive_affirmationslist_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $bucketlist_id = $this->positive_affirmationslist_model->insert_record($recordData);
            if (empty($bucketlist_id)) {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
            $recordData = array("user_id" => $user_id, 'positive_affirmations_list_id' => $bucketlist_id);
            if ($id = $this->positiveaffirmationsuserlist_model->insert_record($recordData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED, $id);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function fetch_add_new_items() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND type = 3 AND isDeleted = 0 AND id NOT IN (SELECT positive_affirmations_list_id FROM positive_affirmations_user_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $records = $this->positive_affirmationslist_model->fetch_add_new_abundance_items($whereCondition);
        $this->send_api_respone('', '', 'success', $records);
    }

    public function add_new_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $user_id = $this->user->userId;
            $whereCondition = array("user_id" => $user_id, 'positive_affirmations_list_id' => html_escape($this->input->post('id')));
            $result = $this->positiveaffirmationsuserlist_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $recordData = array(
                'user_id' => $user_id,
                'positive_affirmations_list_id' => html_escape($this->input->post('id'))
            );
            if ($id = $this->positiveaffirmationsuserlist_model->insert_record($recordData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED, $id);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $id = html_escape($this->input->post('id'));
            $name = html_escape($this->input->post('message'));
            $whereCondition = array('message' => $name, 'id !=' => $id);
            $result = $this->positive_affirmationslist_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS); 
            }
            $whereCondition = array('id' => $id);
            $updateData = array(
                'message' => html_escape($this->input->post('message'))
            );
            if ($this->positive_affirmationslist_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
