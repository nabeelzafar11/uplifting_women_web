<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function update_join_now_section()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('career-join-now-heading', 'Title One', 'trim|required');
        $this->form_validation->set_rules('career-join-now-text', 'Title Two', 'trim|required');
        $this->form_validation->set_rules('career-join-now-btn-text', 'Button Text', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {


            if (isset($_FILES['career-join-us-image']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('career-join-us-image')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {

                $oldData = $this->career_page_model->get_record();
                $image = $oldData->careerPageJoinNowImage;
            }

            $recordData = array(
                'careerPageJoinNowHeading' => html_escape($this->input->post('career-join-now-heading')),
                'careerPageJoinNowText' => html_escape($this->input->post('career-join-now-text')),
                'careerPageJoinNowBtnText' => html_escape($this->input->post('career-join-now-btn-text')),
                'careerPageJoinNowImage' => $image,
            );

            if ($this->career_page_model->update_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_UPDATED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }

    }

    public function update_support_role_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('career-support-role-heading', 'Heading', 'trim|required');
        $this->form_validation->set_rules('career-support-role-1', 'Role 1', 'trim');
        $this->form_validation->set_rules('career-support-role-2', 'Role 2', 'trim');
        $this->form_validation->set_rules('career-support-role-3', 'Role 3', 'trim');
        $this->form_validation->set_rules('career-support-role-4', 'Role 4', 'trim');
        $this->form_validation->set_rules('career-support-role-5', 'Role 5', 'trim');
        $this->form_validation->set_rules('career-support-role-6', 'Role 6', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $recordData = array(
                'careerPageSupportHeading'=> html_escape($this->input->post('career-support-role-heading')),
                'careerPageSupportRole1'=> html_escape($this->input->post('career-support-role-1')),
                'careerPageSupportRole2'=> html_escape($this->input->post('career-support-role-2')),
                'careerPageSupportRole3'=> html_escape($this->input->post('career-support-role-3')),
                'careerPageSupportRole4'=> html_escape($this->input->post('career-support-role-4')),
                'careerPageSupportRole5'=> html_escape($this->input->post('career-support-role-5')),
                'careerPageSupportRole6'=> html_escape($this->input->post('career-support-role-6'))
            );



            if ($this->career_page_model->update_record($recordData)) {


                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_UPDATED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );

                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }

        }
    }


    public function update_work_for_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('career-work-for-heading', 'Heading', 'trim|required');
        $this->form_validation->set_rules('career-work-for-text', 'Text', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $recordData = array(
                'careerPageWorkHeading'=> html_escape($this->input->post('career-work-for-heading')),
                'careerPageWorkText'=> html_escape($this->input->post('career-work-for-text')),
            );

            if ($this->career_page_model->update_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_UPDATED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );

                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }

        }
    }

    public function update_work_exp_section()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('career-work-expect-heading', 'Heading', 'trim|required');
        $this->form_validation->set_rules('career-work-expect-text', 'Text', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {


            if (isset($_FILES['career-work-expect-image']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('career-work-expect-image')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {

                $oldData = $this->career_page_model->get_record();
                $image = $oldData->careerPageWorkExpImage;
            }

            $recordData = array(
                'careerPageWorkExpHeading' => html_escape($this->input->post('career-work-expect-heading')),
                'careerPageWorkExpText' => html_escape($this->input->post('career-work-expect-text')),
                'careerPageWorkExpImage' => $image,
            );

            if ($this->career_page_model->update_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_UPDATED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }

    }

    public function update_resources_section()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('resourcesContent', 'Resourves Content', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {

            $recordData = array(
                '	careerPageResourcesSection' => $this->input->post('resourcesContent'),
            );

            if ($this->career_page_model->update_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_UPDATED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }

    }
}


