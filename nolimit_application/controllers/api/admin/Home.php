<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function add_new_home_slider() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeSliderTitleOne', 'Title One', 'trim');
        $this->form_validation->set_rules('homeSliderTitleTwo', 'Title Two', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['homeSliderImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeSliderImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'homeSliderTitleOne' => html_escape($this->input->post('homeSliderTitleOne')),
                'homeSliderTitleTwo' => html_escape($this->input->post('homeSliderTitleTwo')),
                'homeSliderImage' => $image,
            );
            if ($this->home_slider_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_slider_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_home_slider() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeSliderId', 'Home Slider Id', 'trim|required');
        $this->form_validation->set_rules('homeSliderTitleOne', 'Title One', 'trim');
        $this->form_validation->set_rules('homeSliderTitleTwo', 'Title Two', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('homeSliderImage'));
            if (isset($_FILES['homeSliderImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeSliderImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $homeSliderId = html_escape($this->input->post('homeSliderId'));
            $whereCondition = array('homeSliderId' => $homeSliderId);
            $updateData = array(
                'homeSliderTitleOne' => html_escape($this->input->post('homeSliderTitleOne')),
                'homeSliderTitleTwo' => html_escape($this->input->post('homeSliderTitleTwo')),
                'homeSliderImage' => $image,
            );
            if ($this->home_slider_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_slider_status() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Home Slider Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Home Slider Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeSliderId' => html_escape($this->input->post('id')));
            $updateData = array(
                'homeSliderIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->home_slider_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function delete_home_slider() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Home Slider Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeSliderId' => html_escape($this->input->post('id')));
            $updateData = array(
                'homeSliderIsDeleted' => 1,
            );
            if ($this->home_slider_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_home_slider() {
        $ids = $this->input->post('id');
        $updateData = array(
            'homeSliderSortOrder' => 0
        );
        $whereCondition = array('homeSliderId >' => 0);
        $this->home_slider_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'homeSliderSortOrder' => $key + 1
            );
            $whereCondition = array('homeSliderId' => $id);
            $this->home_slider_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function update_home_blog_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataBlogBlogOne', 'Blog One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataBlogBlogTwo', 'Blog Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataBlogBlogThree', 'Blog Three', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataBlogBlogOne' => html_escape($this->input->post('homeDataBlogBlogOne')),
                'homeDataBlogBlogTwo' => html_escape($this->input->post('homeDataBlogBlogTwo')),
                'homeDataBlogBlogThree' => html_escape($this->input->post('homeDataBlogBlogThree')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_about_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataAboutTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataAboutSubTitle', 'Sub Title', 'trim');
        $this->form_validation->set_rules('homeDataAboutDescription', 'Description', 'trim');
        $this->form_validation->set_rules('homeDataAboutCategoryOne', 'Category One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataAboutCategoryTwo', 'Category Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataAboutCategoryThree', 'Category Three', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataAboutCategoryFour', 'Category Four', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataAboutTitle' => html_escape($this->input->post('homeDataAboutTitle')),
                'homeDataAboutSubTitle' => html_escape($this->input->post('homeDataAboutSubTitle')),
                'homeDataAboutDescription' => html_escape($this->input->post('homeDataAboutDescription')),
                'homeDataAboutCategoryOne' => html_escape($this->input->post('homeDataAboutCategoryOne')),
                'homeDataAboutCategoryTwo' => html_escape($this->input->post('homeDataAboutCategoryTwo')),
                'homeDataAboutCategoryThree' => html_escape($this->input->post('homeDataAboutCategoryThree')),
                'homeDataAboutCategoryFour' => html_escape($this->input->post('homeDataAboutCategoryFour')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_about_section_two() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataAboutTwoTitleOne', 'Title One', 'trim');
        $this->form_validation->set_rules('homeDataAboutTwoTitleTwo', 'Title Two', 'trim');
        $this->form_validation->set_rules('homeDataAboutTwoButtonText', 'Button Text', 'trim');
        $this->form_validation->set_rules('homeDataAboutTwoButtonCategoryId', 'Category', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('homeDataAboutTwoImage'));
            if (isset($_FILES['homeDataAboutTwoImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeDataAboutTwoImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataAboutTwoTitleOne' => html_escape($this->input->post('homeDataAboutTwoTitleOne')),
                'homeDataAboutTwoTitleTwo' => html_escape($this->input->post('homeDataAboutTwoTitleTwo')),
                'homeDataAboutTwoButtonText' => html_escape($this->input->post('homeDataAboutTwoButtonText')),
                'homeDataAboutTwoButtonCategoryId' => html_escape($this->input->post('homeDataAboutTwoButtonCategoryId')),
                'homeDataAboutTwoImage' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_media_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataMediaTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataMediaSubTitle', 'Sub Title', 'trim');
        $this->form_validation->set_rules('homeDataMediaDescription', 'Description', 'trim');
        $this->form_validation->set_rules('homeDataMediaButtonText', 'Button Text', 'trim');
        $this->form_validation->set_rules('homeDataMediaButtonCategoryId', 'Category', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataMediaTitle' => html_escape($this->input->post('homeDataMediaTitle')),
                'homeDataMediaSubTitle' => html_escape($this->input->post('homeDataMediaSubTitle')),
                'homeDataMediaDescription' => html_escape($this->input->post('homeDataMediaDescription')),
                'homeDataMediaButtonText' => html_escape($this->input->post('homeDataMediaButtonText')),
                'homeDataMediaButtonCategoryId' => html_escape($this->input->post('homeDataMediaButtonCategoryId'))
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_interested_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataInterestedTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataInterestedCategoryOne', 'Category One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataInterestedCategoryTwo', 'Category Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataInterestedCategoryThree', 'Category Three', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataInterestedTitle' => html_escape($this->input->post('homeDataInterestedTitle')),
                'homeDataInterestedCategoryOne' => html_escape($this->input->post('homeDataInterestedCategoryOne')),
                'homeDataInterestedCategoryTwo' => html_escape($this->input->post('homeDataInterestedCategoryTwo')),
                'homeDataInterestedCategoryThree' => html_escape($this->input->post('homeDataInterestedCategoryThree'))
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_testimonial_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataTestimonialTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataTestimonialSubTitle', 'Title', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('homeDataTestimonialImage'));
            if (isset($_FILES['homeDataTestimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeDataTestimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataTestimonialTitle' => html_escape($this->input->post('homeDataTestimonialTitle')),
                'homeDataTestimonialSubTitle' => html_escape($this->input->post('homeDataTestimonialSubTitle')),
                'homeDataTestimonialImage' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_fourth_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('home_fourth_section_title', 'Title', 'trim');
        $this->form_validation->set_rules('home_fourth_section_href', 'Href', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('home_fourth_section_image'));
            if (isset($_FILES['home_fourth_section_image']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('home_fourth_section_image')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'home_fourth_section_title' => html_escape($this->input->post('home_fourth_section_title')),
                'home_fourth_section_href' => html_escape($this->input->post('home_fourth_section_href')),
                'home_fourth_section_image' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_facebook_section() {

        $image = html_escape($this->input->post('homepage_video'));
        if (isset($_FILES['homepage_video']['name'])) {
            $config['upload_path'] = './assets/uploaded_media';
            $config['allowed_types'] = 'mp4';
            $config['max_size'] = 5120;
            $image_name = time();
            $config['file_name'] = $image_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('homepage_video')) {
                $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
            } else {
                $data = $this->upload->data();
                $image = $image_name . $data['file_ext'];
            }
        }
        $whereCondition = array('homeDataId >' => 0);
        $updateData = array(
            'facebook_script' => $this->input->post('facebook_script'),
            'homepage_video' => $image,
        );
        if ($this->home_data_model->update_record($whereCondition, $updateData)) {
            $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
        } else {
            $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
        }
    }

    public function add_new_testimonial() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialOccupation', 'Occupation', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialImage' => $image,
            );
            if ($this->testimonial_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_testimonial_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_testimonial() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialId', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialOccupation', 'Occupation', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('testimonialImage'));
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $testimonialId = html_escape($this->input->post('testimonialId'));
            $whereCondition = array('testimonialId' => $testimonialId);
            $updateData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialImage' => $image,
            );
            if ($this->testimonial_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function delete_testimonial() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsDeleted' => 1,
            );
            if ($this->testimonial_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function update_testimonial_status() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Testimonial Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->testimonial_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function sort_testimonials() {
        $ids = $this->input->post('id');
        $updateData = array(
            'testimonialSortOrder' => 0
        );
        $whereCondition = array('testimonialId >' => 0);
        $this->testimonial_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'testimonialSortOrder' => $key + 1
            );
            $whereCondition = array('testimonialId' => $id);
            $this->testimonial_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function add_new_thirdsection() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialImage' => $image,
            );
            if ($this->thirdsection_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_third_section_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_thirdsection() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialId', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('testimonialImage'));
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $testimonialId = html_escape($this->input->post('testimonialId'));
            $whereCondition = array('testimonialId' => $testimonialId);
            $updateData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialImage' => $image,
            );
            if ($this->thirdsection_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function delete_thirdsection() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsDeleted' => 1,
            );
            if ($this->thirdsection_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function update_thirdsection_status() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Testimonial Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->thirdsection_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function sort_thirdsection() {
        $ids = $this->input->post('id');
        $updateData = array(
            'testimonialSortOrder' => 0
        );
        $whereCondition = array('testimonialId >' => 0);
        $this->testimonial_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'testimonialSortOrder' => $key + 1
            );
            $whereCondition = array('testimonialId' => $id);
            $this->thirdsection_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

}
