<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function add_new_dashboard_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Title', 'trim|required');
        $this->form_validation->set_rules('popupMessage', 'Alert Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            if (isset($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $name = html_escape($this->input->post('name'));
            $whereCondition = array('name' => $name);
            $results = $this->dashboard_model->get_record($whereCondition);
            if ($results) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $recordData = array(
                'name' => html_escape($this->input->post('name')),
                'image' => $image,
                'popupMessage' => html_escape($this->input->post('popupMessage')),
                'popupMessageMobile' => strip_tags($this->input->post('popupMessage')),
                'seo_title' => strip_tags($this->input->post('seo_title')),
                'seo_description' => strip_tags($this->input->post('seo_description')),
                'seo_keywords' => strip_tags($this->input->post('seo_keywords'))
            );
            if ($this->dashboard_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/app-dashboard-listing'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_dashboard_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('name', 'Title', 'trim|required');
        $this->form_validation->set_rules('popupMessage', 'Alert Message', 'trim|required');
        $this->form_validation->set_rules('seo_title', 'SEO Title', 'trim|required');
        $this->form_validation->set_rules('seo_description', 'SEO Description', 'trim|required');
        $this->form_validation->set_rules('seo_keywords', 'SEO Keywords', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('defaultImage'));
            if (isset($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $id = html_escape($this->input->post('id'));
            $name = html_escape($this->input->post('name'));
            $whereCondition = array('name' => $name, 'id !=' => $id);
            $result = $this->dashboard_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $whereCondition = array('id' => $id);
            $updateData = array(
                'name' => html_escape($this->input->post('name')),
                'image' => $image,
                'popupMessage' => html_escape($this->input->post('popupMessage')),
                'popupMessageMobile' => strip_tags($this->input->post('popupMessage')),
                'seo_title' => strip_tags($this->input->post('seo_title')),
                'seo_description' => strip_tags($this->input->post('seo_description')),
                'seo_keywords' => strip_tags($this->input->post('seo_keywords'))
            );
            if ($this->dashboard_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
