<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Selflovecaredashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function edit_dashboard_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('month', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('web_header', 'Title', 'trim|required');
        $this->form_validation->set_rules('web_message', 'Message', 'trim|required');
        $this->form_validation->set_rules('seo_title', 'SEO Title', 'trim|required');
        $this->form_validation->set_rules('seo_description', 'SEO Description', 'trim|required');
        $this->form_validation->set_rules('seo_keywords', 'SEO Keywords', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $imageWeb = html_escape($this->input->post('defaultImageWeb'));
            if (isset($_FILES['imageWeb']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('imageWeb')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $imageWeb = $image_name . $data['file_ext'];
                }
            }
            if (!$imageWeb) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $this->upload = null;
            $imageMobile = html_escape($this->input->post('defaultImageMobile'));
            if (isset($_FILES['imageMobile']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('imageMobile')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $imageMobile = $image_name . $data['file_ext'];
                }
            }
            if (!$imageMobile) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $whereCondition = array('month' => html_escape($this->input->post('month')));
            $updateData = array(
                'imageWeb' => $imageWeb,
                'imageMobile' => $imageMobile,
                'web_header' => $this->input->post('web_header'),
                'web_message' => $this->input->post('web_message'),
                'mobile_header' => str_replace("&nbsp;", "", strip_tags(preg_replace("/[\r\n]+/", "\n", $this->input->post('web_header')))),
                'mobile_message' => str_replace("&nbsp;", "", strip_tags(preg_replace("/[\r\n]+/", "\n", $this->input->post('web_message')))),
                'seo_title' => strip_tags($this->input->post('seo_title')),
                'seo_description' => strip_tags($this->input->post('seo_description')),
                'seo_keywords' => strip_tags($this->input->post('seo_keywords'))
            );
            if ($this->selflovecaredashboard_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
