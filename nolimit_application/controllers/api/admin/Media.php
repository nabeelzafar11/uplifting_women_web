<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function add_new_image_media()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mediaName', 'Media Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            if (isset($_FILES['mediaHref']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 262144;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('mediaHref')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $recordData = array(
                'mediaName' => html_escape($this->input->post('mediaName')),
                'mediaHref' => $image,
                'mediaType' => 1,
            );
            if ($this->media_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/image_media_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }

    }

    public function delete_image_media()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Image Media Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('mediaId' => html_escape($this->input->post('id')), 'mediaType' => 1);
            $updateData = array(
                'mediaIsDeleted' => 1,
            );
            if ($this->media_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function add_new_document_media()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mediaName', 'Media Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            if (isset($_FILES['mediaHref']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'pdf|dox|doc|docx';
                $config['max_size'] = 262144;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('mediaHref')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $recordData = array(
                'mediaName' => html_escape($this->input->post('mediaName')),
                'mediaHref' => $image,
                'mediaType' => 2,
            );
            if ($this->media_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/document_media_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }

    }

    public function delete_document_media()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Documant Media Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('mediaId' => html_escape($this->input->post('id')), 'mediaType' => 2);
            $updateData = array(
                'mediaIsDeleted' => 1,
            );
            if ($this->media_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }
}
