<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function add_new_menu()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('menuName', 'Menu Name', 'trim|required');
        $this->form_validation->set_rules('menuIconName', 'Menu Icon Name', 'trim');
        $this->form_validation->set_rules('menuCategoryId', 'Menu Category Id', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('menuParentId', 'Menu Parent Id', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['menuIcon']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('menuIcon')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $menuParentId = html_escape($this->input->post('menuParentId'));

            $recordData = array(
                'menuName' => html_escape($this->input->post('menuName')),
                'menuIconName' => html_escape($this->input->post('menuIconName')),
                'menuParentId' => $menuParentId,
                'menuCategoryId' => html_escape($this->input->post('menuCategoryId')),
                'menuIcon' => $image,
            );
            if ($this->menu_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                if($menuParentId) {
                    $redirectionData = array(
                        'redirection' => 'admin/menu_listings/sub_menu/'.$menuParentId
                    );
                }else{
                    $redirectionData = array(
                        'redirection' => 'admin/menu_listings'
                    );
                }
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }

    }

    public function edit_menu()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('menuId', 'Menu Id', 'trim|required');
        $this->form_validation->set_rules('menuName', 'Menu Name', 'trim|required');
        $this->form_validation->set_rules('menuIconName', 'Menu Icon Name', 'trim');
        $this->form_validation->set_rules('menuCategoryId', 'Menu Category Id', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('menuParentId', 'Menu Parent Id', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('menuIcon'));
            if (isset($_FILES['menuIcon']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('menuIcon')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $menuId = html_escape($this->input->post('menuId'));
            $whereCondition = array('menuId' => $menuId);
            $updateData = array(
                'menuName' => html_escape($this->input->post('menuName')),
                'menuIconName' => html_escape($this->input->post('menuIconName')),
                'menuParentId' => html_escape($this->input->post('menuParentId')),
                'menuCategoryId' => html_escape($this->input->post('menuCategoryId')),
                'menuIcon' => $image,
            );
            if ($this->menu_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }

    }

    public function update_menu_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Menu Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Menu Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('menuId' => html_escape($this->input->post('id')));
            $updateData = array(
                'menuIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->menu_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }

    }

    public function delete_menu()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Menu Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('menuId' => html_escape($this->input->post('id')));
            $updateData = array(
                'menuIsDeleted' => 1,
            );
            if ($this->menu_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_menu()
    {
        $ids = $this->input->post('id');
        if(count($ids)){
            $whereCondition = array('menuId' => $ids[0]);
            $menu = $this->menu_model->get_record($whereCondition);
            $updateData = array(
                'menuSortOrder' => 0
            );
            $whereCondition = array('menuParentId' => $menu->menuParentId);
            $this->menu_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'menuSortOrder' => $key + 1
                );
                $whereCondition = array('menuId' => $id);
                $this->menu_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }
}
