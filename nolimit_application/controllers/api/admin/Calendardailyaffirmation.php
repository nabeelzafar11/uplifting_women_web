<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calendardailyaffirmation extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function edit_dashboard_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('month', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('web_message', 'Message', 'trim|required');
        $this->form_validation->set_rules('seo_title', 'SEO Title', 'trim|required');
        $this->form_validation->set_rules('seo_description', 'SEO Description', 'trim|required');
        $this->form_validation->set_rules('seo_keywords', 'SEO Keywords', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('month' => html_escape($this->input->post('month')));
            $updateData = array(
                'web_message' => $this->input->post('web_message'),
                'mobile_message' => strip_tags($this->input->post('web_message')),
                'seo_title' => strip_tags($this->input->post('seo_title')),
                'seo_description' => strip_tags($this->input->post('seo_description')),
                'seo_keywords' => strip_tags($this->input->post('seo_keywords'))
            );
            if ($this->calendardailyaffirmation_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

}
