<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function update_header() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('siteTitle', 'Site Title', 'trim');
        $this->form_validation->set_rules('seoTitle', 'Seo Title', 'trim');
        $this->form_validation->set_rules('seoKeywords', 'Seo Keywords', 'trim');

        $this->form_validation->set_rules('gratitude_web', 'Gratitude Text', 'trim');
        $this->form_validation->set_rules('positiveaffirmation_web', 'Positive Affirmation Text', 'trim');
        $this->form_validation->set_rules('highestgoodofall_web', 'Highest Good Of All Text', 'trim');
        $this->form_validation->set_rules('login_web', 'Login Screen', 'trim');
        $this->form_validation->set_rules('seoDescription', 'Seo Description', 'trim');
        $this->form_validation->set_rules('phone', 'Top Header Phone', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('headerLogo'));
            if (isset($_FILES['headerLogo']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('headerLogo')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $image2 = html_escape($this->input->post('favicon'));
            if (isset($_FILES['favicon']['name'])) {
                $config1['upload_path'] = './assets/uploaded_media';
                $config1['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config1['max_size'] = 5120;
                $image_name = time();
                $config1['file_name'] = $image_name;
                $this->load->library('upload', $config1);
                if (!$this->upload->do_upload('favicon')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image2 = $image_name . $data['file_ext'];
                }
            }
            $image3 = html_escape($this->input->post('headerDarkLogo'));
            if (isset($_FILES['headerDarkLogo']['name'])) {
                $config2['upload_path'] = './assets/uploaded_media';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config2['max_size'] = 5120;
                $image_name = time();
                $config2['file_name'] = $image_name;
                $this->load->library('upload', $config2);
                if (!$this->upload->do_upload('headerDarkLogo')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image3 = $image_name . $data['file_ext'];
                }
            }
            $updateData = array(
                'siteTitle' => html_escape($this->input->post('siteTitle')),
                'seoTitle' => html_escape($this->input->post('seoTitle')),
                'seoKeywords' => html_escape($this->input->post('seoKeywords')),
                'seoDescription' => html_escape($this->input->post('seoDescription')),
                'gratitude_web' => html_escape($this->input->post('gratitude_web')),
                'positiveaffirmation_web' => html_escape($this->input->post('positiveaffirmation_web')),
                'highestgoodofall_web' => html_escape($this->input->post('highestgoodofall_web')),
                'login_web' => html_escape($this->input->post('login_web')),
                'gratitude_mobile' => (strip_tags($this->input->post('gratitude_web'))),
                'positiveaffirmation_mobile' => (strip_tags($this->input->post('positiveaffirmation_web'))),
                'highestgoodofall_mobile' => (strip_tags($this->input->post('highestgoodofall_web'))),
                'login_mobile' => (strip_tags($this->input->post('login_web'))),
                'phone' => html_escape($this->input->post('phone')),
                'headerLogo' => $image,
                'favicon' => $image2,
                'headerDarkLogo' => $image3,
            );
            if ($this->header_model->update_record($updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_header_button() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('menuButtonText', 'Menu Button Text', 'trim');
        $this->form_validation->set_rules('menuButtonCategoryId', 'Menu Button Href', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $updateData = array(
                'menuButtonText' => html_escape($this->input->post('menuButtonText')),
                'menuButtonCategoryId' => html_escape($this->input->post('menuButtonCategoryId')),
            );
            if ($this->header_model->update_record($updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_footer() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('contactUsText', 'Contact Us Text', 'trim');
        $this->form_validation->set_rules('phoneText', 'Phone One Text', 'trim');
        $this->form_validation->set_rules('phone', 'Phone One', 'trim');
        $this->form_validation->set_rules('phoneTwoText', 'Phone Two Text', 'trim');
        $this->form_validation->set_rules('phoneTwo', 'Phone Two', 'trim');
        $this->form_validation->set_rules('emailText', 'Email Text', 'trim');
        $this->form_validation->set_rules('email', 'Email', 'trim');
        $this->form_validation->set_rules('abnText', 'ABN Text', 'trim');
        $this->form_validation->set_rules('abn', 'ABN', 'trim');
        $this->form_validation->set_rules('footerText', 'Footer About', 'trim');
        $this->form_validation->set_rules('copyright', 'Copyright Text', 'trim');
        $this->form_validation->set_rules('facebook', 'Facebook Link', 'trim');
        $this->form_validation->set_rules('twitter', 'Twitter Link', 'trim');
        $this->form_validation->set_rules('google', 'Google Link', 'trim');
        $this->form_validation->set_rules('linkedin', 'LinkedIn Link', 'trim');
        $this->form_validation->set_rules('youtube', 'Youtube Link', 'trim');
        $this->form_validation->set_rules('skype', 'Skype Link', 'trim');
        $this->form_validation->set_rules('vimeo', 'Vimeo Link', 'trim');
        $this->form_validation->set_rules('instagram', 'Instagram Link', 'trim');
        $this->form_validation->set_rules('pinterest', 'Pinterest Link', 'trim');
        $this->form_validation->set_rules('rss', 'RSS Link', 'trim');
        $this->form_validation->set_rules('behance', 'Behance Link', 'trim');
        $this->form_validation->set_rules('github', 'Github Link', 'trim');
        $this->form_validation->set_rules('newsLetterText', 'Subscribe Title', 'trim');
        $this->form_validation->set_rules('newsLetterNamePlaceHolderText', 'Subscribe Name Placeholder', 'trim');
        $this->form_validation->set_rules('newsLetterEmailPlaceHolderText', 'Subscribe Email Placeholder', 'trim');
        $this->form_validation->set_rules('newsLetterPostcodePlaceHolderText', 'Subscribe Postcode Placeholder', 'trim');
        $this->form_validation->set_rules('newsLetterButtonText', 'Subscribe Button text', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('footerLogo'));
            if (isset($_FILES['footerLogo']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('footerLogo')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $updateData = array(
                'contactUsText' => html_escape($this->input->post('contactUsText')),
                'phoneText' => html_escape($this->input->post('phoneText')),
                'phone' => html_escape($this->input->post('phone')),
                'phoneTwoText' => html_escape($this->input->post('phoneTwoText')),
                'phoneTwo' => html_escape($this->input->post('phoneTwo')),
                'emailText' => html_escape($this->input->post('emailText')),
                'email' => html_escape($this->input->post('email')),
                'abnText' => html_escape($this->input->post('abnText')),
                'abn' => html_escape($this->input->post('abn')),
                'footerText' => html_escape($this->input->post('footerText')),
                'copyright' => html_escape($this->input->post('copyright')),
                'facebook' => html_escape($this->input->post('facebook')),
                'twitter' => html_escape($this->input->post('twitter')),
                'google' => html_escape($this->input->post('google')),
                'linkedin' => html_escape($this->input->post('linkedin')),
                'youtube' => html_escape($this->input->post('youtube')),
                'skype' => html_escape($this->input->post('skype')),
                'instagram' => html_escape($this->input->post('instagram')),
                'pinterest' => html_escape($this->input->post('pinterest')),
                'rss' => html_escape($this->input->post('rss')),
                'behance' => html_escape($this->input->post('behance')),
                'github' => html_escape($this->input->post('github')),
                'newsLetterText' => html_escape($this->input->post('newsLetterText')),
                'newsLetterNamePlaceHolderText' => html_escape($this->input->post('newsLetterNamePlaceHolderText')),
                'newsLetterEmailPlaceHolderText' => html_escape($this->input->post('newsLetterEmailPlaceHolderText')),
                'newsLetterPostcodePlaceHolderText' => html_escape($this->input->post('newsLetterPostcodePlaceHolderText')),
                'newsLetterButtonText' => html_escape($this->input->post('newsLetterButtonText')),
                'footerLogo' => $image,
            );
            if ($this->footer_model->update_record($updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

}
