<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Selflovecare extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function add_new_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('monthId', 'monthId', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $name = html_escape($this->input->post('message'));
            $monthId = html_escape($this->input->post('monthId'));
            $whereCondition = array('message' => $name, 'isDeleted' => 0, 'month_id' => $monthId);
            $result = $this->selflovecare_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }

            $recordData = array(
                'message' => html_escape($this->input->post('message')),
                'month_id' => html_escape($this->input->post('monthId')),
            );
            if ($this->selflovecare_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/selflovecare-list/' . $monthId,
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Please refresh your page and try again', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $id = html_escape($this->input->post('id'));
            $name = html_escape($this->input->post('message'));
            $monthId = html_escape($this->input->post('monthId'));
            $whereCondition = array('message' => $name, 'month_id' => $monthId, 'id !=' => $id, 'isDeleted' => 0);
            $result = $this->selflovecare_model->get_record($whereCondition);
            if ($result) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $whereCondition = array('id' => $id);
            $updateData = array(
                'message' => html_escape($this->input->post('message'))
            );
            if ($this->selflovecare_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function delete_item() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Bucket List Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('id' => html_escape($this->input->post('id')));
            $updateData = array(
                'isDeleted' => 1,
            );
            if ($this->selflovecare_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_list() {
        $ids = $this->input->post('id');
        if (count($ids)) {
            $whereCondition = array('id' => $ids[0]);
            $category = $this->selflovecare_model->get_record($whereCondition);
            $updateData = array(
                'sortOrder' => 0
            );
            $this->selflovecare_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'sortOrder' => $key + 1
                );
                $whereCondition = array('id' => $id);
                $this->selflovecare_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

}
