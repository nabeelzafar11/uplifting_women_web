<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function add_new_blog_category()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('blogCategoryName', 'Category Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $recordData = array(
                'blogCategoryName' => html_escape($this->input->post('blogCategoryName')),
            );
            if ($this->blog_category_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/blog_category_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }

    }

    public function edit_blog_category()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('blogCategoryId', 'Blog Category Id', 'trim|required');
        $this->form_validation->set_rules('blogCategoryName', 'Category Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $blogCategoryId = html_escape($this->input->post('blogCategoryId'));
            $whereCondition = array('blogCategoryId' => $blogCategoryId);
            $updateData = array(
                'blogCategoryName' => html_escape($this->input->post('blogCategoryName'))
            );
            if ($this->blog_category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }

    }

    public function update_blog_category_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Blog Category Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Blog Category Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('blogCategoryId' => html_escape($this->input->post('id')));
            $updateData = array(
                'blogCategoryIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->blog_category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }

    }

    public function delete_blog_category()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Blog Category Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('blogCategoryId' => html_escape($this->input->post('id')));
            $updateData = array(
                'blogCategoryIsDeleted' => 1,
            );
            if ($this->blog_category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function add_new_blog()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('blogTitle', 'Blog Title', 'trim|required');
        $this->form_validation->set_rules('blogDescription', 'Blog Description', 'trim|required');
        $this->form_validation->set_rules('blogContent', 'Blog Content', 'trim|required');
        $this->form_validation->set_rules('blogBlogCategoryId', 'Blog Category Id', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogInterestedOneCategoryId', 'Interested One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogInterestedTwoCategoryId', 'Interested Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogInterestedThreeCategoryId', 'Interested Three', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogPageTitle', 'Page Title', 'trim');
        $this->form_validation->set_rules('blogSeoTitle', 'Seo Title', 'trim');
        $this->form_validation->set_rules('blogSeoKeywords', ' Seo Keywords', 'trim');
        $this->form_validation->set_rules('blogSeoDescription', ' Seo Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            if (isset($_FILES['blogFeatureImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('blogFeatureImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $blogTitle = html_escape($this->input->post('blogTitle'));
            $whereCondition = array('blogTitle' => $blogTitle);
            $blogResult = $this->blog_model->get_record($whereCondition);
            if($blogResult){
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $recordData = array(
                'blogTitle' => $blogTitle,
                'blogSlug' => url_title(($blogTitle), 'dash', true),
                'blogDescription' => html_escape($this->input->post('blogDescription')),
                'blogContent' => html_escape($this->input->post('blogContent')),
                'blogBlogCategoryId' => html_escape($this->input->post('blogBlogCategoryId')),
                'blogInterestedOneCategoryId' => html_escape($this->input->post('blogInterestedOneCategoryId')),
                'blogInterestedTwoCategoryId' => html_escape($this->input->post('blogInterestedTwoCategoryId')),
                'blogInterestedThreeCategoryId' => html_escape($this->input->post('blogInterestedThreeCategoryId')),
                'blogPageTitle' => html_escape($this->input->post('blogPageTitle')),
                'blogSeoKeywords' => html_escape($this->input->post('blogSeoKeywords')),
                'blogSeoTitle' => html_escape($this->input->post('blogSeoTitle')),
                'blogSeoDescription' => html_escape($this->input->post('blogSeoDescription')),
                'blogFeatureImage' => $image,
            );
            if ($this->blog_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/blog_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }

    }

    public function edit_blog()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('blogId', 'Blog Id', 'trim|required');
        $this->form_validation->set_rules('blogTitle', 'Blog Title', 'trim|required');
        $this->form_validation->set_rules('blogDescription', 'Blog Description', 'trim|required');
        $this->form_validation->set_rules('blogContent', 'Blog Content', 'trim|required');
        $this->form_validation->set_rules('blogBlogCategoryId', 'Blog Category Id', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogInterestedOneCategoryId', 'Interested One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogInterestedTwoCategoryId', 'Interested Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogInterestedThreeCategoryId', 'Interested Three', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('blogPageTitle', 'Page Title', 'trim');
        $this->form_validation->set_rules('blogSeoTitle', 'Seo Title', 'trim');
        $this->form_validation->set_rules('blogSeoKeywords', ' Seo Keywords', 'trim');
        $this->form_validation->set_rules('blogSeoDescription', ' Seo Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('blogFeatureImage'));
            if (isset($_FILES['blogFeatureImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('blogFeatureImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $blogId = html_escape($this->input->post('blogId'));
            $blogTitle = html_escape($this->input->post('blogTitle'));
            $whereCondition = array('blogTitle' => $blogTitle, 'blogId !=' => $blogId);
            $blogResult = $this->blog_model->get_record($whereCondition);
            if($blogResult){
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $whereCondition = array('blogId' => $blogId);
            $updateData = array(
                'blogTitle' => $blogTitle,
                'blogSlug' => url_title(($blogTitle), 'dash', true),
                'blogDescription' => html_escape($this->input->post('blogDescription')),
                'blogContent' => html_escape($this->input->post('blogContent')),
                'blogBlogCategoryId' => html_escape($this->input->post('blogBlogCategoryId')),
                'blogInterestedOneCategoryId' => html_escape($this->input->post('blogInterestedOneCategoryId')),
                'blogInterestedTwoCategoryId' => html_escape($this->input->post('blogInterestedTwoCategoryId')),
                'blogInterestedThreeCategoryId' => html_escape($this->input->post('blogInterestedThreeCategoryId')),
                'blogPageTitle' => html_escape($this->input->post('blogPageTitle')),
                'blogSeoKeywords' => html_escape($this->input->post('blogSeoKeywords')),
                'blogSeoTitle' => html_escape($this->input->post('blogSeoTitle')),
                'blogSeoDescription' => html_escape($this->input->post('blogSeoDescription')),
                'blogFeatureImage' => $image,
            );
            if ($this->blog_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }

    }

    public function update_blog_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Blog Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Blog Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('blogId' => html_escape($this->input->post('id')));
            $updateData = array(
                'blogIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->blog_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }

    }

    public function delete_blog()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Blog Category Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('blogId' => html_escape($this->input->post('id')));
            $updateData = array(
                'blogIsDeleted' => 1,
            );
            if ($this->blog_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }
}
