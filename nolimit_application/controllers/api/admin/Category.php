<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function add_new_category() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('categoryName', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('categoryParentId', 'Category Parent Id', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryInterestedOneCategoryId', 'Category Interested Category One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryInterestedTwoCategoryId', 'Category Interested Category Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryInterestedThreeCategoryId', 'Category Interested Category Three', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryDescription', 'Category Description', 'trim');
        $this->form_validation->set_rules('categoryPageType', 'Category Page Type', 'trim|required');
        $this->form_validation->set_rules('categoryPageTitle', 'Category Page Title', 'trim');
        $this->form_validation->set_rules('categorySeoTitle', 'Category Seo Title', 'trim');
        $this->form_validation->set_rules('categorySeoKeywords', 'Category Seo Keywords', 'trim');
        $this->form_validation->set_rules('categorySeoDescription', 'Category Seo Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
           
            $image = "default.png";
            if (isset($_FILES['categoryImage']['name'])) {
                $config1['upload_path'] = './assets/uploaded_media';
                $config1['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config1['max_size'] = 5120;
                $image_name = time();
                $config1['file_name'] = $image_name;
                $this->load->library('upload', $config1);
                if (!$this->upload->do_upload('categoryImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } 
            $categoryName = html_escape($this->input->post('categoryName'));
            $categoryParentId = html_escape($this->input->post('categoryParentId'));

            $whereCondition = array('categoryName' => $categoryName, 'categoryParentId' => $categoryParentId);
            $categoryResult = $this->category_model->get_record($whereCondition);
            if ($categoryResult) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $recordData = array(
                'categoryName' => $categoryName,
                'categoryParentId' => $categoryParentId,
                'categoryPageType' => html_escape($this->input->post('categoryPageType')),
                'categoryShortDescription' => html_escape($this->input->post('categoryShortDescription')),
                'categoryDescription' => html_escape($this->input->post('categoryDescription')),
                'categorySlug' => url_title(($categoryName), 'dash', true),
                'categoryInterestedOneCategoryId' => html_escape($this->input->post('categoryInterestedOneCategoryId')),
                'categoryInterestedTwoCategoryId' => html_escape($this->input->post('categoryInterestedTwoCategoryId')),
                'categoryInterestedThreeCategoryId' => html_escape($this->input->post('categoryInterestedThreeCategoryId')),
                'categoryPageTitle' => html_escape($this->input->post('categoryPageTitle')),
                'categorySeoTitle' => html_escape($this->input->post('categorySeoTitle')),
                'categorySeoKeywords' => html_escape($this->input->post('categorySeoKeywords')),
                'categorySeoDescription' => html_escape($this->input->post('categorySeoDescription')),
                'categoryImage' => $image
            );
            if ($this->category_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                if ($categoryParentId) {
                    $redirectionData = array(
                        'redirection' => 'admin/category_listings/sub_category/' . $categoryParentId
                    );
                } else {
                    $redirectionData = array(
                        'redirection' => 'admin/category_listings'
                    );
                }
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_category() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('categoryId', 'Category Id', 'trim|required');
        $this->form_validation->set_rules('categoryName', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('categoryParentId', 'Category Parent Id', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryInterestedOneCategoryId', 'Category Interested Category One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryInterestedTwoCategoryId', 'Category Interested Category Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryInterestedThreeCategoryId', 'Category Interested Category Three', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('categoryDescription', 'Category Description', 'trim');
        $this->form_validation->set_rules('categoryPageType', 'Category Page Type', 'trim|required');
        $this->form_validation->set_rules('categoryPageTitle', 'Category Page Title', 'trim');
        $this->form_validation->set_rules('categorySeoTitle', 'Category Seo Title', 'trim');
        $this->form_validation->set_rules('categorySeoKeywords', 'Category Seo Keywords', 'trim');
        $this->form_validation->set_rules('categorySeoDescription', 'Category Seo Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {

            $image = html_escape($this->input->post('categoryImage'));
            if (isset($_FILES['categoryImage']['name'])) {
                $config1['upload_path'] = './assets/uploaded_media';
                $config1['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config1['max_size'] = 5120;
                $image_name = time();
                $config1['file_name'] = $image_name;
                $this->load->library('upload', $config1);
                if (!$this->upload->do_upload('categoryImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $categoryId = html_escape($this->input->post('categoryId'));
            $categoryName = html_escape($this->input->post('categoryName'));
            $categoryParentId = html_escape($this->input->post('categoryParentId'));

            $whereCondition = array('categoryName' => $categoryName, 'categoryParentId' => $categoryParentId, 'categoryId !=' => $categoryId);
            $categoryResult = $this->category_model->get_record($whereCondition);
            if ($categoryResult) {
                $this->send_api_respone('', '', 'danger', ALREADY_EXISTS);
            }
            $whereCondition = array('categoryId' => $categoryId);
            $updateData = array(
                'categoryName' => $categoryName,
                'categoryParentId' => $categoryParentId,
                'categoryPageType' => html_escape($this->input->post('categoryPageType')),
                'categoryDescription' => html_escape($this->input->post('categoryDescription')),
                'categorySlug' => url_title(($categoryName), 'dash', true),
                'categoryInterestedOneCategoryId' => html_escape($this->input->post('categoryInterestedOneCategoryId')),
                'categoryInterestedTwoCategoryId' => html_escape($this->input->post('categoryInterestedTwoCategoryId')),
                'categoryInterestedThreeCategoryId' => html_escape($this->input->post('categoryInterestedThreeCategoryId')),
                'categoryPageTitle' => html_escape($this->input->post('categoryPageTitle')),
                'categorySeoTitle' => html_escape($this->input->post('categorySeoTitle')),
                'categorySeoKeywords' => html_escape($this->input->post('categorySeoKeywords')),
                'categorySeoDescription' => html_escape($this->input->post('categorySeoDescription')),
                'categoryImage' => $image,
                'categoryShortDescription' => html_escape($this->input->post('categoryShortDescription'))
            );
            if ($this->category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function delete_category() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Category Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('categoryId' => html_escape($this->input->post('id')));
            $updateData = array(
                'categoryIsDeleted' => 1,
            );
            if ($this->category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function update_category_status() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Category Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Category Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('categoryId' => html_escape($this->input->post('id')));
            $updateData = array(
                'categoryIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function sort_category() {
        $ids = $this->input->post('id');
        if (count($ids)) {
            $whereCondition = array('categoryId' => $ids[0]);
            $category = $this->category_model->get_record($whereCondition);
            $updateData = array(
                'categorySortOrder' => 0
            );
            $whereCondition = array('categoryParentId' => $category->categoryParentId);
            $this->category_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'categorySortOrder' => $key + 1
                );
                $whereCondition = array('categoryId' => $id);
                $this->category_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function add_new_category_page_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pageSectionCategoryId', 'Category Id', 'trim|required');
        $this->form_validation->set_rules('pageSectionTitle', 'Page Section Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $pageSectionCategoryId = html_escape($this->input->post('pageSectionCategoryId'));
            $recordData = array(
                'pageSectionCategoryId' => $pageSectionCategoryId,
                'pageSectionTitle' => html_escape($this->input->post('pageSectionTitle'))
            );
            if ($this->page_section_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                if ($pageSectionCategoryId) {
                    $redirectionData = array(
                        'redirection' => 'admin/category_listings/category_page/' . $pageSectionCategoryId
                    );
                } else {
                    $redirectionData = array(
                        'redirection' => 'admin/category_listings'
                    );
                }
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_category_page_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pageSectionCategoryId', 'Category Id', 'trim|required');
        $this->form_validation->set_rules('pageSectionId', 'Page Section Id', 'trim|required');
        $this->form_validation->set_rules('pageSectionTitle', 'Page Section Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('pageSectionId' => html_escape($this->input->post('pageSectionId')));
            $updateData = array(
                'pageSectionCategoryId' => html_escape($this->input->post('pageSectionCategoryId')),
                'pageSectionTitle' => html_escape($this->input->post('pageSectionTitle'))
            );
            if ($this->page_section_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function delete_category_page_section() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Category Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('pageSectionId' => html_escape($this->input->post('id')));
            $updateData = array(
                'pageSectionIsDeleted' => 1,
            );
            if ($this->page_section_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_category_page_sections() {
        $ids = $this->input->post('id');
        if (count($ids)) {
            $whereCondition = array('pageSectionId' => $ids[0]);
            $pageSection = $this->page_section_model->get_record($whereCondition);
            $updateData = array(
                'pageSectionSortOrder' => 0
            );
            $whereCondition = array('pageSectionCategoryId' => $pageSection->pageSectionCategoryId);
            $this->page_section_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'pageSectionSortOrder' => $key + 1
                );
                $whereCondition = array('pageSectionId' => $id);
                $this->page_section_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function add_new_category_page_section_category() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pageSectionCategoryPageSectionId', 'Page Section Id', 'trim|required');
        $this->form_validation->set_rules('pageSectionCategoryCategoryId', 'Page Section Category Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $recordData = array(
                'pageSectionCategoryCategoryId' => html_escape($this->input->post('pageSectionCategoryCategoryId')),
                'pageSectionCategoryPageSectionId' => html_escape($this->input->post('pageSectionCategoryPageSectionId'))
            );
            if ($this->page_section_category_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function delete_category_page_section_category() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Category Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('pageSectionCategoryId' => html_escape($this->input->post('id')));
            $updateData = array(
                'pageSectionCategoryIsDeleted' => 1,
            );
            if ($this->page_section_category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_category_page_section_categories() {
        $ids = $this->input->post('id');
        if (count($ids)) {
            $whereCondition = array('pageSectionCategoryId' => $ids[0]);
            $pageSectionCategory = $this->page_section_category_model->get_record($whereCondition);
            $updateData = array(
                'pageSectionCategorySortOrder' => 0
            );
            $whereCondition = array('pageSectionCategoryPageSectionId' => $pageSectionCategory->pageSectionCategoryPageSectionId);
            $this->page_section_category_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'pageSectionCategorySortOrder' => $key + 1
                );
                $whereCondition = array('pageSectionCategoryId' => $id);
                $this->page_section_category_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function edit_category_page_content() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('categoryId', 'Category Id', 'trim|required');
        $this->form_validation->set_rules('categoryPageContent', 'Category Page Content', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $categoryId = html_escape($this->input->post('categoryId'));
            $whereCondition = array('categoryId' => $categoryId);
            $updateData = array(
                'categoryPageContent' => ($this->input->post('categoryPageContent')),
            );
            if ($this->category_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

}
