<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_dailyaffirmation_listing() {
        $data['page'] = DAILYAFFIRMATION_LISTINGS;
//        $data['addPositiveAffirmationDashboardButton'] = true;
        $data['dashboard'] = $this->calendardailyaffirmation_model->get_records();
        $this->admin_view('calendar/show_dailyaffirmation_listing', $data);
    }

    public function edit_dailyaffirmation_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = CALENDAR_DAILYAFFIRMATION_LISTINGS_EDIT_ITEM;
        $whereCondition = array('month' => $id);
        $data["dashboard"] = $this->calendardailyaffirmation_model->get_record($whereCondition);
        $data['backsCalendarDailyAffirmationButton'] = true;
        $this->admin_view('calendar/edit_dashboard_item', $data);
    }

}
