<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancy_page extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_vacancy_listing() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = VACANCY;
        $data['addNewVacancy'] = true;
        $data['vacancies'] = $this->vacancy_model->get_records();
        $this->admin_view('vacancies/vacancy_listing', $data);
    }


    public function show_add_vacancy() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = VACANCY;
        //$data['addNewVacancy'] = true;
        //$data['vacancies'] = $this->vacancy_model->get_records();
        $this->admin_view('vacancies/add_new_vacancy', $data);
    }

    public function show_edit_vacancy($id) {
    
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = VACANCY;
        //$data['addNewVacancy'] = true;
        $whereArray = array('vacancyId' => $id);
        $data['vacancy'] = $this->vacancy_model->get_record($whereArray);
        $this->admin_view('vacancies/edit_vacancy', $data);
    }
}