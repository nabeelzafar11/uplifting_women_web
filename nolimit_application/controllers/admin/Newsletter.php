<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Newsletter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_newsletter_listings()
    {
        $data['page'] = NEWSLETTER_LISTINGS;
        $data['subscribers'] = $this->subscription_model->get_records();
        $this->admin_view('newsletter/newsletter_listing', $data);
    }

    public function newsletter_export()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Name');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('C1', 'PostCode');

        $subscribers = $this->subscription_model->get_records();
        foreach ($subscribers as $key => $subscriber) {
            $sheet->setCellValue('A' . ($key + 3), $subscriber->subscriberName);
            $sheet->setCellValue('B' . ($key + 3), $subscriber->subscriberEmail);
            $sheet->setCellValue('C' . ($key + 3), $subscriber->subscriberPostcode);
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('assets/newsletters.xlsx');
        redirect('assets/newsletters.xlsx', 'refresh');
    }
}
