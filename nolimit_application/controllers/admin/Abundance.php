<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Abundance extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_abundance_list() {
        $data['addNewAbundanceItem'] = true;
        $data['page'] = ABUNDANCE_LIST;
        $whereCondition = array("user_id" => 0, "isDeleted" => 0, "type" => '1');
        $data['abundance'] = $this->positive_affirmationslist_model->get_records($whereCondition);
        $this->admin_view('abundance/show_abundance_list', $data);
    }

    public function add_new_abundance_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = ABUNDANCE_ADD_ITEM;
        $data['backAbundanceButton'] = true;
        $this->admin_view('abundance/add_new_item', $data);
    }

    public function edit_abundance_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = ABUNDANCE_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["abundance"] = $this->positive_affirmationslist_model->get_record($whereCondition);
        $data['backAbundanceButton'] = true;
        $this->admin_view('abundance/edit_abundance_item', $data);
    }

}
