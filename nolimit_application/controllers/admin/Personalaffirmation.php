<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Personalaffirmation extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_list() {
        $data['addPersonalAffirmationItem'] = true;
        $data['page'] = PERSONAL_AFFIRMATION_LIST;
        $whereCondition = array("user_id" => 0, "isDeleted" => 0);
        $data['personalaffirmation'] = $this->personalaffirmation_model->get_records($whereCondition);
        $this->admin_view('personalaffirmation/show_list', $data);
    }

    public function add_new_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = PERSONAL_AFFIRMATION_ADD_ITEM;
        $data['backPersonalAffirmationButton'] = true;
        $this->admin_view('personalaffirmation/add_new_item', $data);
    }

    public function edit_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = PERSONAL_AFFIRMATION_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["personalaffirmation"] = $this->personalaffirmation_model->get_record($whereCondition);
        $data['backPersonalAffirmationButton'] = true;
        $this->admin_view('personalaffirmation/edit_item', $data);
    }

}
