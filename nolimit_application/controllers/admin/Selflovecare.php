<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Selflovecare extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_monthly_list()
    {
        $data['page'] = SELFLOVECARE_LIST;
        $this->admin_view('selflovecare/show_monthly_list', $data);
    }

    public function show_list($monthId)
    {
        $dateObj = DateTime::createFromFormat('!m', $monthId);
        $data["monthName"] = $dateObj->format('F');
        $data['addSelfLoveCareItem'] = true;
        $data['page'] = SELFLOVECARE_LIST;
        $whereCondition = array("user_id" => 0, "isDeleted" => 0, "month_id" => "$monthId");
        $data['selflovecare'] = $this->selflovecare_model->get_records($whereCondition);
        $data['monthId'] = $monthId;
        $this->admin_view('selflovecare/show_list', $data);
    }

    public function add_new_item($monthId)
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = SELFLOVECARE_ADD_ITEM;
        $data['backSelflovecareButton'] = true;
        $data['monthId'] = $monthId;
        $this->admin_view('selflovecare/add_new_item', $data);
    }

    public function edit_item($id)
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = SELFLOVECARE_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["selflovecare"] = $this->selflovecare_model->get_record($whereCondition);
        $data['backSelflovecareButton'] = true;
        $this->admin_view('selflovecare/edit_item', $data);
    }
}
