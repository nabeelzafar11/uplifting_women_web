<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_blog_category_listings()
    {
        $data['page'] = BLOG_CATEGORY_LISTINGS;
        $data['addBlogCategoryButton'] = true;
        $data['blogCategories'] = $this->blog_category_model->get_records();
        $this->admin_view('blogs/blog_category_listing', $data);
    }

    public function show_new_blog_category_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = BLOG_CATEGORY_LISTINGS;
        $data['backBlogCategoryButton'] = true;
        $this->admin_view('blogs/add_new_blog_category', $data);
    }

    public function show_edit_blog_category_form($blogCategoryId)
    {
        $whereCondition = array('blogCategoryId' => $blogCategoryId);
        $data['blogCategory'] = $this->blog_category_model->get_record($whereCondition);
        if ($data['blogCategory']) {
            $data['page'] = BLOG_CATEGORY_LISTINGS;
            $data['backBlogCategoryButton'] = true;
            $this->admin_view('blogs/edit_blog_category', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/blog_category_listings', 'refresh');
        }
    }

    public function show_blog_listings()
    {
        $data['page'] = BLOG_LISTINGS;
        $data['addBlogButton'] = true;
        $data['blogs'] = $this->blog_model->get_records();
        $this->admin_view('blogs/blog_listing', $data);
    }

    public function show_new_blog_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = BLOG_LISTINGS;
        $data['backBlogButton'] = true;
        $data['blogCategories'] = $this->blog_category_model->get_records();
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('blogs/add_new_blog', $data);
    }

    public function show_edit_blog_form($blogId)
    {
        $whereCondition = array('blogId' => $blogId);
        $data['blog'] = $this->blog_model->get_record($whereCondition);
        if ($data['blog']) {
            $data['page'] = $blogId;
            $data['backBlogButton'] = true;
            $data['blogCategories'] = $this->blog_category_model->get_records();
            $data['categories'] = $this->category_model->get_records();
            $this->admin_view('blogs/edit_blog', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/blog_listings', 'refresh');
        }
    }
}
