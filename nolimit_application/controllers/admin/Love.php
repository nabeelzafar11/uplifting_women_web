<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Love extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_list() {
        $data['addLoveItem'] = true;
        $data['page'] = LOVE_LIST;
        $whereCondition = array("user_id" => 0, "isDeleted" => 0, "type" => 3);
        $data['love'] = $this->positive_affirmationslist_model->get_records($whereCondition);
        $this->admin_view('love/show_list', $data);
    }

    public function add_new_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = LOVE_ADD_ITEM;
        $data['backLoveButton'] = true;
        $this->admin_view('love/add_new_item', $data);
    }

    public function edit_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = LOVE_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["love"] = $this->positive_affirmationslist_model->get_record($whereCondition);
        $data['backLoveButton'] = true;
        $this->admin_view('love/edit_item', $data);
    }

}
