<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_category_listings($categoryId = 0)
    {
        $data['page'] = CATEGORY_LISTINGS;
        $data['addCategoryButton'] = true;
        $whereCondition = array('categoryParentId' => $categoryId);
        $data['categories'] = $this->category_model->get_records($whereCondition);
        foreach ($data['categories'] as $key => $value) {
            $whereCondition = array('categoryParentId' => $value->categoryId);
            $data['categories'][$key]->childCount = count($this->category_model->get_records($whereCondition));
        }
        $data['parentCategoryNames'] = "";
        while ($categoryId != 0) {
            $whereCondition = array('categoryId' => $categoryId);
            $parentCategory = $this->category_model->get_record($whereCondition);
            if ($parentCategory) {
                if ($data['parentCategoryNames']) {
                    $data['parentCategoryNames'] .= "/";
                }
                $data['parentCategoryNames'] .= $parentCategory->categoryName;
                $categoryId = $parentCategory->categoryParentId;
            } else {
                $categoryId = 0;
            }
        }
        $this->admin_view('categories/category_listing', $data);
    }

    public function show_new_category_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = CATEGORY_LISTINGS;
        $data['backCategoryButton'] = true;
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('categories/add_new_category', $data);
    }

    public function show_edit_category_form($categoryId)
    {
        $whereCondition = array('categoryId' => $categoryId);
        $data['category'] = $this->category_model->get_record($whereCondition);
        if ($data['category']) {
            $data['page'] = CATEGORY_LISTINGS;
            $data['backCategoryButton'] = true;
            $whereCondition = array('categoryId !=' => $data['category']->categoryId);
            $data['categories'] = $this->category_model->get_records($whereCondition);
            $this->admin_view('categories/edit_category', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/category_listings', 'refresh');
        }
    }

    public function show_category_page($categoryId)
    {
        $whereCondition = array('categoryId' => $categoryId);
        $data['category'] = $this->category_model->get_record($whereCondition);
        if ($data['category']) {
            $data['page'] = CATEGORY_LISTINGS;
            $data['backCategoryButton'] = true;
            if ($data['category']->categoryPageType == 3) {
                $this->admin_view('categories/edit_category_page_content', $data);
            } else {
                $whereCondition = array('pageSectionCategoryId' => $categoryId);
                $data['pageSections'] = $this->page_section_model->get_records($whereCondition);
                $this->admin_view('categories/category_page_sections', $data);
            }
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/category_listings', 'refresh');
        }
    }

    public function show_add_new_category_page_section_form($categoryId)
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $whereCondition = array('categoryId' => $categoryId);
        $data['category'] = $this->category_model->get_record($whereCondition);
        if ($data['category']) {
            $data['page'] = CATEGORY_LISTINGS;
            $data['categories'] = $this->category_model->get_records();
            $this->admin_view('categories/add_new_category_page_section', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/category_listings', 'refresh');
        }
    }

    public function show_edit_category_page_section_form($pageSectionId)
    {
        $whereCondition = array('pageSectionId' => $pageSectionId);
        $data['pageSection'] = $this->page_section_model->get_record($whereCondition);
        if ($data['pageSection']) {
            $data['page'] = CATEGORY_LISTINGS;
            $whereCondition = array('categoryId' => $data['pageSection']->pageSectionCategoryId);
            $data['category'] = $this->category_model->get_record($whereCondition);
            $this->admin_view('categories/edit_category_page_section', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/category_listings', 'refresh');
        }
    }

    public function show_category_page_section($pageSectionId)
    {
        $whereCondition = array('pageSectionId' => $pageSectionId);
        $data['pageSection'] = $this->page_section_model->get_record($whereCondition);
        if ($data['pageSection']) {
            $data['page'] = CATEGORY_LISTINGS;
            $whereCondition = array('categoryId' => $data['pageSection']->pageSectionCategoryId);
            $data['category'] = $this->category_model->get_record($whereCondition);
            $data['categories'] = $this->category_model->get_records();
            $whereCondition = array('pageSectionCategoryPageSectionId' => $pageSectionId);
            $data['pageSectionCategories'] = $this->page_section_category_model->get_records($whereCondition);
            foreach ($data['pageSectionCategories'] as $key => $pageSectionCategory){
                $whereCondition = array('categoryId' => $pageSectionCategory->pageSectionCategoryCategoryId);
                $category = $this->category_model->get_record($whereCondition);
                if($category) {
                    $data['pageSectionCategories'][$key]->categoryName = $category->categoryName;
                }else{
                    unset($data['pageSectionCategories'][$key]);
                }
            }
            $this->admin_view('categories/category_page_section_categories', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/category_listings', 'refresh');
        }
    }
}
