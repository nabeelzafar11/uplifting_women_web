<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_menu_listings($menuId = 0)
    {
        $data['page'] = MENU_LISTINGS;
        $data['addMenuButton'] = true;

        $whereCondition = array('menuParentId' => $menuId);
        $data['menus'] = $this->menu_model->get_records($whereCondition);
        foreach ($data['menus'] as $key => $value){
            $whereCondition = array('menuParentId' => $value->menuId);
            $data['menus'][$key]->childCount = count($this->menu_model->get_records($whereCondition));
        }
        $data['parentMenuNames'] = "";
        while($menuId != 0){
            $whereCondition = array('menuId' => $menuId);
            $parentMenu = $this->menu_model->get_record($whereCondition);
            if($parentMenu){
                if($data['parentMenuNames']){
                    $data['parentMenuNames'] .= "/";
                }
                $data['parentMenuNames'] .= $parentMenu->menuName;
                $menuId = $parentMenu->menuParentId;
            }else{
                $menuId = 0;
            }
        }
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('menus/menu_listing', $data);
    }

    public function show_new_menu_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = MENU_LISTINGS;
        $data['backMenuButton'] = true;
        $data['categories'] = $this->category_model->get_records();
        $whereCondition = array('menuParentId' => '0');
        $data['menus'] = $this->menu_model->get_records($whereCondition);
        $this->admin_view('menus/add_new_menu', $data);
    }

    public function show_edit_menu_form($menuId)
    {
        $whereCondition = array('menuId' => $menuId);
        $data['menu'] = $this->menu_model->get_record($whereCondition);
        if ($data['menu']) {
            $data['page'] = MENU_LISTINGS;
            $data['backMenuButton'] = true;
            $whereCondition = array('menuId !=' => $data['menu']->menuId, 'menuParentId' => '0');
            $data['menus'] = $this->menu_model->get_records($whereCondition);
            $data['categories'] = $this->category_model->get_records();
            $this->admin_view('menus/edit_menu', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/menu_listings', 'refresh');
        }
    }
}
