<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bucketlist extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_bucket_list() {
        $data['addNewBucketListItem'] = true;
        $data['page'] = BUCKET_LIST;
        $whereCondition = array("user_id" => 0, "isDeleted" => 0);
        $data['bucketlist'] = $this->bucketlist_model->get_records($whereCondition);
        $this->admin_view('bucketlist/show_bucket_list', $data);
    }

    public function add_new_bucketlist_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = BUCKETLIST_ADD_ITEM;
        $data['backBucketListButton'] = true;
        $this->admin_view('bucketlist/add_new_item', $data);
    }

    public function edit_bucketlist_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = BUCKETLIT_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["bucketlist"] = $this->bucketlist_model->get_record($whereCondition);
        $data['backBucketListButton'] = true;
        $this->admin_view('bucketlist/edit_bucketlist_item', $data);
    }

}
