<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Headermenus extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_all_header_menus() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = HEADER_MENUS;
        $data['categories'] = $this->category_model->get_records();
        $data['footerMenus'] = $this->headermenu_model->get_records();
        $data['addNewHeaderMenu'] = true;
        $this->admin_view('header-menus/header_menu_listing', $data);
    }

    public function show_add_header_menus() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = HEADER_MENUS;
        $data['categories'] = $this->category_model->get_records();
        $data['footerMenus'] = $this->headermenu_model->get_records();
        $data['headerMenuListing'] = true;
        $this->admin_view('header-menus/add_new_header_menu', $data);

    }


    //admin/header_menus/
    public function show_edit_header_menu($id) {

        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = HEADER_MENUS;
        $data['headerMenuListing'] = true;
        $whereArray = array('footerMenuId' => $id);
        $data['footerMenu'] = $this->headermenu_model->get_record($whereArray);
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('header-menus/edit_header_menu', $data);
    }
}
