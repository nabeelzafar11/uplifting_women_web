<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Footermenus extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }


    public function show_all_footer_menus() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = FOOTER_MENUS;
        $data['categories'] = $this->category_model->get_records();
        $data['footerMenus'] = $this->footermenu_model->get_records();
        $data['addNewFooterMenu'] = true;
        $this->admin_view('footer-menus/footer_menu_listing', $data);
    }

    public function show_add_footer_menus() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = FOOTER_MENUS;
        $data['categories'] = $this->category_model->get_records();
        $data['footerMenus'] = $this->footermenu_model->get_records();
        $data['footerMenuListing'] = true;
        $this->admin_view('footer-menus/add_new_footer_menu', $data);

    }


    //admin/footer_menus/
    public function show_edit_footer_menu($id) {

        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = FOOTER_MENUS;
        $data['footerMenuListing'] = true;
        $whereArray = array('footerMenuId' => $id);
        $data['footerMenu'] = $this->footermenu_model->get_record($whereArray);
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('footer-menus/edit_footer_menu', $data);
    }
}
