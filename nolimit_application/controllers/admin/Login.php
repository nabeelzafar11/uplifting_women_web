<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->not_login_check();
    }

    public function show_login()
    {
        $this->login_view();
    }
}
