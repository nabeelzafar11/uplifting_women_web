<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_home_page()
    {
        $data['page'] = HOME_PAGE;
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('pages/home_page', $data);
    }

    public function show_new_category_form()
    {
        if ($this->session->new_category_data && isset($this->session->new_category_data['redirectUrl'])) {
            $redirect = $this->session->new_category_data['redirectUrl'];
            $this->session->unset_userdata('new_category_data');
            redirect($redirect, 'refresh');
        }
        $data['page'] = CATEGORY_LISTINGS;
        $data['backCategoryButton'] = true;
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('categories/add_new_category', $data);
    }

    public function show_edit_menu_form($menuId)
    {
        $whereCondition = array('menuId' => $menuId, 'menuIsDeleted' => '0');
        $data['menu'] = $this->menu_model->get_record($whereCondition);
        if ($data['menu']) {
            $data['page'] = MENU_LISTINGS;
            $data['backMenuButton'] = true;
            $whereCondition = array('pageIsDeleted' => '0');
            $data['pages'] = $this->page_model->get_records($whereCondition);
            $this->dashboard_view('menu/edit_menu', $data);
        } else {
            $newdata = array(
                'menus_data' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_userdata($newdata);
            redirect('menu_listings', 'refresh');
        }
    }
}
