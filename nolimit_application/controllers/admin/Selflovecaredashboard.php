<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Selflovecaredashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_app_dashoboard_listing() {
        $data['page'] = SELLOVECARE_DASHBOARD_LISTINGS;
//        $data['addPositiveAffirmationDashboardButton'] = true;
        $data['dashboard'] = $this->selflovecaredashboard_model->get_records();
        $this->admin_view('selflovecaredashboard/dashboard_listing', $data);
    }

    public function edit_dashboard_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = SELLOVECARE_DASHBOARD_LISTINGS_EDIT_ITEM;
        $whereCondition = array('month' => $id);
        $data["dashboard"] = $this->selflovecaredashboard_model->get_record($whereCondition);
        $data['backselflovecaredashboardButton'] = true;
        $this->admin_view('selflovecaredashboard/edit_dashboard_item', $data);
    }

}
