<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career_page extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_career_join_now_section() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = CAREER_JOIN_NOW;
        // $data['backHomeSliderButton'] = true;
        $data['careerPage'] = $this->career_page_model->get_record();
        $this->admin_view('career/join_now_section', $data);
    }

    public function show_career_support_role_section() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = CAREER_SUPPORT_ROLE;
        // $data['backHomeSliderButton'] = true;
        $data['careerPage'] = $this->career_page_model->get_record();
        $this->admin_view('career/support_role', $data);
    }

    public function show_career_why_work_section() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = CAREER_SUPPORT_ROLE;
        $data['careerPage'] = $this->career_page_model->get_record();
        $this->admin_view('career/why_work', $data);
    }

    public function show_career_work_exp_section() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = CAREER_WORK_EXP;
        $data['careerPage'] = $this->career_page_model->get_record();
        $this->admin_view('career/work_expect', $data);
    }

    public function show_career_resources_section() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = CAREER_RESOURCES;
        $data['careerPage'] = $this->career_page_model->get_record();
        $this->admin_view('career/resources_section', $data);
    }
}