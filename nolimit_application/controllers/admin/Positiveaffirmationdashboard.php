<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Positiveaffirmationdashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_app_dashoboard_listing() {
        $data['page'] = POSITIVE_AFFIRMATION_DASHBOARD_LISTINGS;
//        $data['addPositiveAffirmationDashboardButton'] = true;
        $data['dashboard'] = $this->positiveaffirmationdashboard_model->get_records();
        $this->admin_view('positiveaffirmationdashboard/dashboard_listing', $data);
    }

    public function add_new_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = POSITIVE_AFFIRMATION_DASHBOARD_ADD_ITEM;
        $data['backPositiveAffirmationDashboardButton'] = true;
        $this->admin_view('positiveaffirmationdashboard/add_new_item', $data);
    }

    public function edit_dashboard_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = POSITIVE_AFFIRMATION_DASHBOARD_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["dashboard"] = $this->positiveaffirmationdashboard_model->get_record($whereCondition);
        $data['backPositiveAffirmationDashboardButton'] = true;
        $this->admin_view('positiveaffirmationdashboard/edit_dashboard_item', $data);
    }

}
