<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $whereCondition = array('homeSliderIsActive' => 1);
        $data['homeSliders'] = $this->home_slider_model->get_records($whereCondition);
        foreach ($data['homeSliders'] as $key => $homeSlider) {
            $data['homeSliders'][$key]->homeSliderButtonHref = $this->get_category_link($homeSlider->homeSliderButtonCategoryId);
        }
        $data['homeData'] = $this->home_data_model->get_record();     
        $whereCondition = array('testimonialIsActive' => 1);
        $data['thirdsection'] = $this->thirdsection_model->get_records($whereCondition);
        $data['testimonials'] = $this->testimonial_model->get_records($whereCondition);
        $this->public_view('home', $data);
    }

    public function logout()
    {
        if ($this->user) {
            $this->session->unset_userdata('loginId');
        }
        redirect('', 'refresh');
    }
}
