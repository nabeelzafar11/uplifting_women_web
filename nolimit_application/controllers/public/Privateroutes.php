<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Privateroutes extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function self_love() {
        $user_id = $this->user->userId;
        $curr_month = date("m");
        if (!empty($this->input->get("id")) && is_numeric(decode($this->input->get("id")))) {
            $curr_month = decode($this->input->get("id"));
        }
        $whereCondition = array("selflovecare.isDeleted" => 0, "user_selflovecare_list.user_id" => $user_id, "selflovecare.month_id" => $curr_month);
        $data["user_list"] = $this->userselflovecare_model->get_records_join_bucketlist($whereCondition);
        $data["selflovecare_dashboard"] = $this->selflovecaredashboard_model->get_record(array("month" => $curr_month));
        $data["curr_month"] = $curr_month;
        $this->public_view('self_love', $data);
    }

    public function self_love_self_care_list_add_item() {
        $user_id = $this->user->userId;

        $month = date('m');
        if (!empty($this->input->get("id")) && is_numeric(decode($this->input->get("id")))) {
            $month = decode($this->input->get("id"));
        }
        $whereCondition = " WHERE ((user_id = 0 OR user_id = '$user_id') AND month_id='$month') AND isDeleted = 0 AND id NOT IN (SELECT selflovecare_id FROM user_selflovecare_list WHERE user_id = $user_id) ORDER BY id ASC, sortOrder ASC";
        $data["list"] = $this->selflovecare_model->fetch_add_new_abundance_items($whereCondition);
        $data["curr_month"] = $month;
        $this->public_view('self_love_add_item', $data);
    }

    public function calendar() {
        $user_id = $this->user->userId;
        $month = date('m');
        $year = date('Y');
        $date = date("Y-m-d");
        $datecompelte = date("F d Y");
        $query = "SELECT DISTINCT(DATE(dateandtime)) as dates from events WHERE MONTH(dateandtime) = '$month' AND YEAR(dateandtime) = '$year' AND user_id = $user_id";
        $records = $this->userselflovecare_model->generic_query($query);
        $dates = array();
        foreach ($records as $r) {
            array_push($dates, $r->dates);
        }
        $encoded_month = array();
        for ($i = 1; $i <= 12; $i++) {
            $encoded_month[$i] = encode($i);
        }
        $data["encoded_month"] = $encoded_month;
        $data["dates"] = $dates;
        $data["date"] = $date;
        $data["datecompelte"] = $datecompelte;
        $data["calendardailyaffirmation"] = $this->calendardailyaffirmation_model->get_record(array("month" => $month));
        $data["calendardailyaffirmation_all_years"] = $this->calendardailyaffirmation_model->get_records();
        $this->public_view('calendar', $data);
    }

    public function gratitude_journal() {

        $current_date = (!empty($_GET["date"]) && $this->validateDate($_GET["date"])) ? $_GET["date"]: date("Y-m-d");
        $data["header"] = $this->header_model->custom_query(" gratitude_mobile ");
        $user_id = $this->user->userId;
        $whereCondition = array("user_id" => $user_id, "type" => 1, "date" => $current_date);
        $default_type1 = $this->gratitudejournal_model->get_record($whereCondition);

        $whereCondition = array("user_id" => $user_id, "type" => 2, "date" => $current_date);
        $default_type2 = $this->gratitudejournal_model->get_record($whereCondition);

        $whereCondition = array("user_id" => $user_id, "type" => 3, "date" => $current_date);
        $default_type3 = $this->gratitudejournal_model->get_record($whereCondition);

        
        if (!$default_type1) {
            $default_type1 = (object) array("header" => "Things I am grateful for today", "message" => "", "date" => "$current_date");
        }
        if (!$default_type2) {
            $default_type2 = (object) array("header" => "Things I am grateful for about my body", "message" => "", "date" => "$current_date");
        }

        if (!$default_type3) {
            $default_type3 = (object) array("header" => "Things I am grateful for about my family", "message" => "", "date" => "$current_date");
        }

        $data["default_type1"] = $default_type1;
        $data["default_type2"] = $default_type2;
        $data["default_type3"] = $default_type3;

        $this->public_view('gratitude_journal', $data);
    }

    public function highest_goods_of_all() {
        $data["header"] = $this->header_model->custom_query(" highestgoodofall_mobile ");
        $user_id = $this->user->userId;
        $whereCondition = array("isDeleted" => 0, "user_id" => $user_id);
        $data["records"] = $this->highestgoodofall_model->get_records($whereCondition);
        $this->public_view('highest_goods_of_all', $data);
    }

    public function bucket_list() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT bucketlist_id FROM user_bukcet_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->bucketlist_model->fetch_add_new_bucketlist_items($whereCondition);
        $whereCondition = array("bucket_list.isDeleted" => 0, "user_bukcet_list.user_id" => $user_id);
        $data["users_list"] = $this->userbucketlist_model->get_records_join_bucketlist($whereCondition);
        $data["bucket_list_share_url"] = base_url() . "user-bucket-list/" . encode($user_id);
        $this->public_view('bucket_list', $data);
    }

    public function user_bucket_list($id) {
        $id = decode($id);
        if (is_numeric($id)) {
            $user_id = $id;
            $data["user_email"] = $this->user_model->get_record(array("userId" => $user_id))->email;
            $whereCondition = array("bucket_list.isDeleted" => 0, "user_bukcet_list.user_id" => $user_id);
            $data["users_list"] = $this->userbucketlist_model->get_records_join_bucketlist($whereCondition);
            $this->public_view('user_bucket_list', $data);
        } else {
            redirect(base_url());
        }
    }

    public function positive_affirmation() {
        $data["header"] = $this->header_model->custom_query(" positiveaffirmation_mobile ");
        $data["dashboard"] = $this->positiveaffirmationdashboard_model->get_records();
        $this->public_view('positive_affirmation', $data);
    }

    public function abundance() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND type = 1 AND isDeleted = 0 AND id NOT IN (SELECT positive_affirmations_list_id FROM positive_affirmations_user_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->positive_affirmationslist_model->fetch_add_new_abundance_items($whereCondition);
        $this->public_view('abundance', $data);
    }

    public function health_and_beauty() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND type = 2 AND isDeleted = 0 AND id NOT IN (SELECT positive_affirmations_list_id FROM positive_affirmations_user_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->positive_affirmationslist_model->fetch_add_new_abundance_items($whereCondition);
        $this->public_view('health_and_beauty', $data);
    }

    public function love() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND type = 3 AND isDeleted = 0 AND id NOT IN (SELECT positive_affirmations_list_id FROM positive_affirmations_user_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->positive_affirmationslist_model->fetch_add_new_abundance_items($whereCondition);
        $this->public_view('love', $data);
    }

    public function my_personal_affirmation() {
        $user_id = $this->user->userId;
        $whereCondition1 = array("positive_affirmations_list.isDeleted" => 0, "positive_affirmations_user_list.user_id" => $user_id);
        $data["users_list"] = $this->positiveaffirmationsuserlist_model->get_records_join_bucketlist($whereCondition1);
        $this->public_view('my_personal_affirmation', $data);
    }

    public function user_profile() {
        if (isset($_GET['code'])) {
            $user_id = $this->user->userId;
            try {
                $capi = new GoogleCalendarApi();
                // Get the access token 
                $data = $capi->GetAccessToken(CLIENT_ID, base_url("profile"), CLIENT_SECRET, $_GET['code']);
                $act = $data["access_token"];
                $this->update_user_records_for_google_events_in_database($user_id, $data);
                redirect(base_url("profile"));
            } catch (Exception $e) {
                redirect(base_url("profile"));
            }
        }
        $data['current_user'] = $this->user;
        $this->public_view('user_profile', $data);
    }

}
