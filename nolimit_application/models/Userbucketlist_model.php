<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userbucketlist_model extends CI_Model {

    public $table = 'user_bukcet_list';
    private $tableId = "id";

    public function insert_record($recordData) {
        $this->db->insert($this->table, $recordData);
        return $this->db->insert_id();
    }

    public function get_record($whereConditionArray = null) {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_records($whereConditionArray = null) {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $this->db->order_by($this->tableId, 'ASC');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_records_join_bucketlist($whereConditionArray = null) {
        $this->db->select('user_bukcet_list.id,bucket_list.message');
        $this->db->from($this->table);
        $this->db->join('bucket_list', 'bucket_list.id = user_bukcet_list.bucketlist_id', 'left');
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $this->db->order_by('bucket_list.sortOrder', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function delete_record($whereConditionArray) {
        $this->db->where($whereConditionArray);
        $query = $this->db->delete($this->table);
        if ($query) {
            return true;
        } else
            return false;
    }

    public function update_record($whereConditionArray, $updateData) {
        $this->db->where($whereConditionArray);
        $query = $this->db->update($this->table, $updateData);
        if ($query) {

            return true;
        } else
            return false;
    }

}
