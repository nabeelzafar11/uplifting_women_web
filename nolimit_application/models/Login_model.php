<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    private $table = "logins";
    private $tableId = "loginId";

    public function insert_record($recordData)
    {
        $this->db->insert($this->table, $recordData);
        return $this->db->insert_id();
    }

    public function get_record($whereConditionArray = null, $orderBy = null)
    {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        if($orderBy)
            $this->db->order_by($this->tableId, 'DESC');
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function update_records($whereConditionArray, $updateData)
    {
        $this->db->where($whereConditionArray);
        $query = $this->db->update($this->table, $updateData);
    }
}