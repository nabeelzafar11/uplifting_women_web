<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the admin guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
//$route['default_controller'] = 'home';
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//USER ROUTES

$route['signin'] = 'public/login/show_login';
$route['signup'] = 'public/login/show_signup';
$route['forgot-password'] = 'public/login/forgot_password';
$route['reset-password/(:any)'] = 'public/login/reset_password/$1';

//AFTER USER LOGIN ROUTES
$route["self_love"] = 'public/privateroutes/self_love';
$route["self-love-self-care-list/add_item"] = 'public/privateroutes/self_love_self_care_list_add_item';
$route["calendar"] = 'public/privateroutes/calendar';
$route["gratitude_journal"] = 'public/privateroutes/gratitude_journal';
$route["highest_good_of_all"] = 'public/privateroutes/highest_goods_of_all';
$route["profile"] = 'public/privateroutes/user_profile';
$route["bucket_list"] = 'public/privateroutes/bucket_list';
$route["user-bucket-list/(:any)"] = 'public/privateroutes/user_bucket_list/$1';
$route["positive_affirmation"] = 'public/privateroutes/positive_affirmation';

$route["positive_affirmation/abundance"] = 'public/privateroutes/abundance';
$route["positive_affirmation/health-and-beauty"] = 'public/privateroutes/health_and_beauty';
$route["positive_affirmation/love"] = 'public/privateroutes/love';
$route["positive_affirmation/my-personal-affirmation"] = 'public/privateroutes/my_personal_affirmation';

//ADMIN ROUTES
$route['logout'] = 'home/logout';
$route['admin'] = 'admin/login/show_login';
$route['admin/dashboard'] = 'admin/dashboard/show_dashboard';

$route['admin/category_listings'] = 'admin/category/show_category_listings';
$route['admin/category_listings/sub_category/(:num)'] = 'admin/category/show_category_listings/$1';
$route['admin/category_listings/add_new_category'] = 'admin/category/show_new_category_form';
$route['admin/category_listings/edit_category/(:num)'] = 'admin/category/show_edit_category_form/$1';
$route['admin/category_listings/category_page/(:num)'] = 'admin/category/show_category_page/$1';
$route['admin/category_listings/add_new_category_page_section/(:num)'] = 'admin/category/show_add_new_category_page_section_form/$1';
$route['admin/category_listings/edit_category_page_section/(:num)'] = 'admin/category/show_edit_category_page_section_form/$1';
$route['admin/category_listings/category_page_section/(:num)'] = 'admin/category/show_category_page_section/$1';

$route['admin/menu_listings'] = 'admin/menu/show_menu_listings';
$route['admin/menu_listings/sub_menu/(:num)'] = 'admin/menu/show_menu_listings/$1';
$route['admin/menu_listings/add_new_menu'] = 'admin/menu/show_new_menu_form';
$route['admin/menu_listings/edit_menu/(:num)'] = 'admin/menu/show_edit_menu_form/$1';

$route['admin/blog_category_listings'] = 'admin/blog/show_blog_category_listings';
$route['admin/blog_category_listings/add_new_blog_category'] = 'admin/blog/show_new_blog_category_form';
$route['admin/blog_category_listings/edit_blog_category/(:num)'] = 'admin/blog/show_edit_blog_category_form/$1';

$route['admin/blog_listings'] = 'admin/blog/show_blog_listings';
$route['admin/blog_listings/add_new_blog'] = 'admin/blog/show_new_blog_form';
$route['admin/blog_listings/edit_blog/(:num)'] = 'admin/blog/show_edit_blog_form/$1';

$route['admin/image_media_listings'] = 'admin/media/show_image_media_listings';
$route['admin/image_media_listings/add_new_image_media'] = 'admin/media/show_new_image_media_form';

$route['admin/document_media_listings'] = 'admin/media/show_document_media_listings';
$route['admin/document_media_listings/add_new_document_media'] = 'admin/media/show_new_document_media_form';

$route['admin/newsletter_listings'] = 'admin/newsletter/show_newsletter_listings';
$route['admin/newsletter_export'] = 'admin/newsletter/newsletter_export';

$route['admin/home_slider_listings'] = 'admin/home/show_home_slider_listings';
$route['admin/home_slider_listings/add_new_home_slider'] = 'admin/home/show_new_home_slider_form';
$route['admin/home_slider_listings/edit_home_slider/(:num)'] = 'admin/home/show_edit_home_slider_form/$1';
$route['admin/home_blog_section'] = 'admin/home/show_home_blog_section';
$route['admin/home_about_section_one'] = 'admin/home/show_home_about_section_one';
$route['admin/home_about_section_two'] = 'admin/home/show_home_about_section_two';
$route['admin/home_media_section'] = 'admin/home/show_home_media_section';
$route['admin/home_interested_section'] = 'admin/home/show_home_interested_section';
$route['admin/home_testimonial_section'] = 'admin/home/show_home_testimonial_section';
$route['admin/show_home_fourth_section'] = 'admin/home/show_home_fourth_section';

$route['admin/home_testimonial_listings'] = 'admin/home/show_home_testimonial_listings';
$route['admin/home_testimonial_listings/add_new_testimonial'] = 'admin/home/show_add_new_testimonial_form';
$route['admin/home_testimonial_listings/edit_testimonial/(:num)'] = 'admin/home/show_edit_testimonial_form/$1';

$route['admin/home_third_section_listings'] = 'admin/home/show_home_third_section_listings';
$route['admin/home_third_section_listings/add_new_testimonial'] = 'admin/home/show_add_new_third_section_form';
$route['admin/home_third_section_listings/edit_testimonial/(:num)'] = 'admin/home/show_edit_third_section_form/$1';

$route['admin/home_facebook_section'] = 'admin/home/home_facebook_section';

$route['admin/career_page/join_now_section'] = 'admin/career_page/show_career_join_now_section';
$route['admin/career_page/support_role'] = 'admin/career_page/show_career_support_role_section';
$route['admin/career_page/why_work'] = 'admin/career_page/show_career_why_work_section';
$route['admin/career_page/work_exp'] = 'admin/career_page/show_career_work_exp_section';
$route['admin/career_page/resources'] = 'admin/career_page/show_career_resources_section';


$route['admin/vacancies/vacancy_listing'] = 'admin/vacancy_page/show_vacancy_listing';
$route['admin/vacancis/add_new'] = 'admin/vacancy_page/show_add_vacancy';
$route['admin/vacancis/edit/(:num)'] = 'admin/vacancy_page/show_edit_vacancy/$1';

$route['admin/header_menus'] = 'admin/headermenus/show_all_header_menus';
$route['admin/header_menus/add'] = 'admin/headermenus/show_add_header_menus';
$route['admin/header_menus/(:num)'] = 'admin/headermenus/show_edit_header_menu/$1';

$route['admin/footer_menus'] = 'admin/footermenus/show_all_footer_menus';
$route['admin/footer_menus/add'] = 'admin/footermenus/show_add_footer_menus';
$route['admin/footer_menus/(:num)'] = 'admin/footermenus/show_edit_footer_menu/$1';

$route['admin/header'] = 'admin/settings/show_header';
$route['admin/footer'] = 'admin/settings/show_footer';

$route['api/add_new_category'] = 'api/admin/category/add_new_category';
$route['api/edit_category'] = 'api/admin/category/edit_category';
$route['api/delete_category'] = 'api/admin/category/delete_category';
$route['api/update_category_status'] = 'api/admin/category/update_category_status';
$route['api/sort_category'] = 'api/admin/category/sort_category';
$route['api/add_new_category_page_section'] = 'api/admin/category/add_new_category_page_section';
$route['api/edit_category_page_section'] = 'api/admin/category/edit_category_page_section';
$route['api/delete_category_page_section'] = 'api/admin/category/delete_category_page_section';
$route['api/sort_category_page_sections'] = 'api/admin/category/sort_category_page_sections';
$route['api/add_new_category_page_section_category'] = 'api/admin/category/add_new_category_page_section_category';
$route['api/delete_category_page_section_category'] = 'api/admin/category/delete_category_page_section_category';
$route['api/sort_category_page_section_categories'] = 'api/admin/category/sort_category_page_section_categories';
$route['api/edit_category_page_content'] = 'api/admin/category/edit_category_page_content';

$route['api/get_menus'] = 'api/public/menu/get_menus';
$route['api/add_new_menu'] = 'api/admin/menu/add_new_menu';
$route['api/edit_menu'] = 'api/admin/menu/edit_menu';
$route['api/delete_menu'] = 'api/admin/menu/delete_menu';
$route['api/update_menu_status'] = 'api/admin/menu/update_menu_status';
$route['api/sort_menu'] = 'api/admin/menu/sort_menu';

$route['api/add_new_blog_category'] = 'api/admin/blog/add_new_blog_category';
$route['api/edit_blog_category'] = 'api/admin/blog/edit_blog_category';
$route['api/delete_blog_category'] = 'api/admin/blog/delete_blog_category';
$route['api/update_blog_category_status'] = 'api/admin/blog/update_blog_category_status';

$route['api/add_new_blog'] = 'api/admin/blog/add_new_blog';
$route['api/edit_blog'] = 'api/admin/blog/edit_blog';
$route['api/delete_blog'] = 'api/admin/blog/delete_blog';
$route['api/update_blog_status'] = 'api/admin/blog/update_blog_status';

$route['api/add_new_image_media'] = 'api/admin/media/add_new_image_media';
$route['api/delete_image_media'] = 'api/admin/media/delete_image_media';

$route['api/add_new_document_media'] = 'api/admin/media/add_new_document_media';
$route['api/delete_documnet_media'] = 'api/admin/media/delete_document_media';

$route['api/add_new_home_slider'] = 'api/admin/home/add_new_home_slider';
$route['api/edit_home_slider'] = 'api/admin/home/edit_home_slider';
$route['api/update_home_slider_status'] = 'api/admin/home/update_home_slider_status';
$route['api/delete_home_slider'] = 'api/admin/home/delete_home_slider';
$route['api/sort_home_slider'] = 'api/admin/home/sort_home_slider';

$route['api/add_new_testimonial'] = 'api/admin/home/add_new_testimonial';
$route['api/edit_testimonial'] = 'api/admin/home/edit_testimonial';
$route['api/update_testimonial_status'] = 'api/admin/home/update_testimonial_status';
$route['api/delete_testimonial'] = 'api/admin/home/delete_testimonial';
$route['api/sort_testimonials'] = 'api/admin/home/sort_testimonials';

$route['api/add_new_thirdsection'] = 'api/admin/home/add_new_thirdsection';
$route['api/edit_thirdsection'] = 'api/admin/home/edit_thirdsection';
$route['api/update_thirdsection_status'] = 'api/admin/home/update_thirdsection_status';
$route['api/delete_thirdsection'] = 'api/admin/home/delete_thirdsection';
$route['api/sort_thirdsection'] = 'api/admin/home/sort_thirdsection';

$route['api/update_home_blog_section'] = 'api/admin/home/update_home_blog_section';
$route['api/update_home_about_section'] = 'api/admin/home/update_home_about_section';
$route['api/update_home_about_section_two'] = 'api/admin/home/update_home_about_section_two';
$route['api/update_home_media_section'] = 'api/admin/home/update_home_media_section';
$route['api/update_home_interested_section'] = 'api/admin/home/update_home_interested_section';
$route['api/update_home_testimonial_section'] = 'api/admin/home/update_home_testimonial_section';
$route['api/update_home_fourth_section'] = 'api/admin/home/update_home_fourth_section';
$route['api/update_home_facebook_section'] = 'api/admin/home/update_home_facebook_section';
$route['api/update_header'] = 'api/admin/settings/update_header';
$route['api/update_header_button'] = 'api/admin/settings/update_header_button';
$route['api/update_footer'] = 'api/admin/settings/update_footer';

$route['api/update_career_join_now_section'] = 'api/admin/career/update_join_now_section';
$route['api/update_career_support_role_section'] = 'api/admin/career/update_support_role_section';
$route['api/update_career_work_for_section'] = 'api/admin/career/update_work_for_section';
$route['api/update_career_work_exp_section'] = 'api/admin/career/update_work_exp_section';
$route['api/update_career_resources_section'] = 'api/admin/career/update_resources_section';


$route['api/add_new_vacancy'] = 'api/admin/vacancy/add_new_vacancy';
$route['api/update_vacancy_status'] = 'api/admin/vacancy/update_vacancy_status';
$route['api/update_vacancy'] = 'api/admin/vacancy/edit_vacancy';
$route['api/delete_vacancy'] = 'api/admin/vacancy/delete_vacancy';
$route['api/sort_vacancy'] = 'api/admin/vacancy/sort_vacancy';
$route['api/forgot_password'] = 'api/public/login/forgot_password';
$route['api/add_footer_menu'] = 'api/admin/footermenus/add_footer_menu';
$route['api/change_footer_menu_state'] = 'api/admin/footermenus/change_menu_state';
$route['api/edit_footer_menu'] = 'api/admin/footermenus/edit_footer_menu';
$route['api/sort_footer_menus'] = 'api/admin/footermenus/sort_footer_menu';

$route['api/add_header_menu'] = 'api/admin/headermenus/add_header_menu';
$route['api/change_header_menu_state'] = 'api/admin/headermenus/change_menu_state';
$route['api/edit_header_menu'] = 'api/admin/headermenus/edit_header_menu';
$route['api/sort_header_menus'] = 'api/admin/headermenus/sort_header_menu';

$route['api/delete_footer_menus'] = 'api/admin/footermenus/delete_footer_menu';
$route['api/delete_header_menus'] = 'api/admin/headermenus/delete_header_menu';
//Admin view for Dashboard listings
$route['admin/app-dashboard-listing'] = 'admin/dashboard/show_app_dashoboard_listing';
$route['admin/dashboard_listings/add_new_item'] = 'admin/dashboard/add_new_item';
$route['admin/dashboard_listings/edit_dashboard_item/(:num)'] = 'admin/dashboard/edit_dashboard_item/$1';

//Admin view for Bucket List

$route['admin/bucket-list'] = 'admin/bucketlist/show_bucket_list';
$route['admin/bucketlist/add_new_bucketlist_item'] = 'admin/bucketlist/add_new_bucketlist_item';
$route['admin/bucketlist/edit_bucketlist_item/(:num)'] = 'admin/bucketlist/edit_bucketlist_item/$1';

//Admin view for Positive Affirmation Dashboard listings
$route['admin/app-positiveaffirmationdashboard-listing'] = 'admin/positiveaffirmationdashboard/show_app_dashoboard_listing';
$route['admin/positiveaffirmationdashboard_listings/add_new_item'] = 'admin/positiveaffirmationdashboard/add_new_item';
$route['admin/positiveaffirmationdashboard_listings/edit_dashboard_item/(:num)'] = 'admin/positiveaffirmationdashboard/edit_dashboard_item/$1';

//Admin view for Abundance List - Positive affirmations
$route['admin/abundance-list'] = 'admin/abundance/show_abundance_list';
$route['admin/abundance/add_new_abundance_item'] = 'admin/abundance/add_new_abundance_item';
$route['admin/abundance/edit_abundance_item/(:num)'] = 'admin/abundance/edit_abundance_item/$1';

//Admin view for Health and Beauty List - Positive affirmations
$route['admin/healthandbeauty-list'] = 'admin/healthandbeauty/show_list';
$route['admin/healthandbeauty/add_new_item'] = 'admin/healthandbeauty/add_new_item';
$route['admin/healthandbeauty/edit_item/(:num)'] = 'admin/healthandbeauty/edit_item/$1';


//Admin view for Love List - Positive affirmations
$route['admin/love-list'] = 'admin/love/show_list';
$route['admin/love/add_new_item'] = 'admin/love/add_new_item';
$route['admin/love/edit_item/(:num)'] = 'admin/love/edit_item/$1';

//Admin view for My Personal Affirmation List - Positive affirmations
$route['admin/personalaffirmation-list'] = 'admin/personalaffirmation/show_list';
$route['admin/personalaffirmation/add_new_item'] = 'admin/personalaffirmation/add_new_item';
$route['admin/personalaffirmation/edit_item/(:num)'] = 'admin/personalaffirmation/edit_item/$1';

//ADMIN CALENDAR DAILY AFFIRMATION 
$route['admin/app-calendar-dailyaffirmation-listing'] = 'admin/calendar/show_dailyaffirmation_listing';
$route['admin/calendar/edit_dashboard_item/(:num)'] = 'admin/calendar/edit_dailyaffirmation_item/$1';

//Admin view for Positive Affirmation Dashboard listings
$route['admin/app-selflovecaredashboard-listing'] = 'admin/selflovecaredashboard/show_app_dashoboard_listing';
$route['admin/selflovecaredashboard/add_new_dashboard_item'] = 'admin/selflovecaredashboard/add_new_item';
$route['admin/selflovecaredashboard/edit_dashboard_item/(:num)'] = 'admin/selflovecaredashboard/edit_dashboard_item/$1';

//Admin view for My SELF CARE/SELF LOVE LIST
$route['admin/selflovecare-monthly-list'] = 'admin/selflovecare/show_monthly_list';
$route['admin/selflovecare-list/(:num)'] = 'admin/selflovecare/show_list/$1';
$route['admin/selflovecare/add_new_item/(:num)'] = 'admin/selflovecare/add_new_item/$1';
$route['admin/selflovecare/edit_item/(:num)'] = 'admin/selflovecare/edit_item/$1';

// ++++++++++++++++++ APIS ++++++++++++++++++++//
//ADMIN Dashboard APIS
$route['api/get_dashboard_listings'] = 'api/public/dashboard/show_app_dashoboard_listing';
$route['api/add_dashboard_item'] = 'api/admin/dashboard/add_new_dashboard_item';
$route['api/edit_dashboard_item'] = 'api/admin/dashboard/edit_dashboard_item';

//ADMIN Positive Affirmation Dashboard APIS
$route['api/get_positive_affirmation_dashboard_listings'] = 'api/public/positiveaffirmationdashboard/show_app_dashoboard_listing';
$route['api/add_positive_affirmation_dashboard_item'] = 'api/admin/positiveaffirmationdashboard/add_new_dashboard_item';
$route['api/edit_positive_affirmation_dashboard_item'] = 'api/admin/positiveaffirmationdashboard/edit_dashboard_item';


$route['api/edit_calendar_dailyaffirmation_item'] = 'api/admin/calendardailyaffirmation/edit_dashboard_item';

//ADMIN Bucket List APIS
$route['api/add_new_bucketlist_item'] = 'api/admin/bucketlist/add_new_bucketlist_item';
$route['api/edit_bucketlist_item'] = 'api/admin/bucketlist/edit_bucketlist_item';
$route['api/delete_bucket_list'] = 'api/admin/bucketlist/delete_bucketlist_item';
$route['api/sort_bucket_list'] = 'api/admin/bucketlist/sort_bucket_list';

//ADMIN Abundance List APIS
$route['api/add_new_abundance_item'] = 'api/admin/abundance/add_new_abundance_item';
$route['api/edit_abundance_item'] = 'api/admin/abundance/edit_abundance_item';
$route['api/delete_abundance_list'] = 'api/admin/abundance/delete_abundance_item';
$route['api/sort_abundance_list'] = 'api/admin/abundance/sort_abundance_list';

//ADMIN Health and Beauty List APIS
$route['api/add_new_healthandbeauty_item'] = 'api/admin/healthandbeauty/add_new_item';
$route['api/edit_healthandbeauty_item'] = 'api/admin/healthandbeauty/edit_item';
$route['api/delete_healthandbeauty_list'] = 'api/admin/healthandbeauty/delete_item';
$route['api/sort_healthandbeauty_list'] = 'api/admin/healthandbeauty/sort_list';

//ADMIN Love List APIS
$route['api/add_new_love_item'] = 'api/admin/love/add_new_item';
$route['api/edit_love_item'] = 'api/admin/love/edit_item';
$route['api/delete_love_list'] = 'api/admin/love/delete_item';
$route['api/sort_love_list'] = 'api/admin/love/sort_list';

//ADMIN Love List APIS
$route['api/add_new_personalaffirmation_item'] = 'api/admin/personalaffirmation/add_new_item';
$route['api/edit_personalaffirmation_item'] = 'api/admin/personalaffirmation/edit_item';
$route['api/delete_personalaffirmation_list'] = 'api/admin/personalaffirmation/delete_item';
$route['api/sort_personalaffirmation_list'] = 'api/admin/personalaffirmation/sort_list';

//ADMIN SELF LOVE/SELF CARE List APIS
//api/edit_selflovecare_dashboard_item
$route['api/edit_selflovecare_dashboard_item'] = 'api/admin/selflovecaredashboard/edit_dashboard_item';
$route['api/add_new_selflovecare_item'] = 'api/admin/selflovecare/add_new_item';
$route['api/edit_selflovecare_item'] = 'api/admin/selflovecare/edit_item';
$route['api/delete_selflovecare_list'] = 'api/admin/selflovecare/delete_item';
$route['api/sort_selflovecare_list'] = 'api/admin/selflovecare/sort_list';


//+++++++++++++++++ USERS ++++++++++++++++++++//
//
//User Bucket List APIS

$route['api/user/fetch-user-bucketlist'] = 'api/public/bucketlist/fetch_user_bucketlist';
$route['api/user/fetch-add-new-bucketlist-items'] = 'api/public/bucketlist/fetch_add_new_bucketlist_items';
$route['api/user/add-new-bucketlist-item'] = 'api/public/bucketlist/add_new_bucketlist_item';
$route['api/user/delete_user_bucket_list'] = 'api/public/bucketlist/delete_user_bucketlist_item';
$route['api/user/add-custom-bucketlist-item'] = 'api/public/bucketlist/add_custom_bucketlist_item';

//User Abundance List APIS - POSITIVE AFFIRMATION

$route['api/user/fetch-user-abundancelist'] = 'api/public/abundance/fetch_list';
$route['api/user/fetch-add-new-abundance-items'] = 'api/public/abundance/fetch_add_new_items';
$route['api/user/add-new-abundance-item'] = 'api/public/abundance/add_new_item';
$route['api/user/delete_user_abundance_list'] = 'api/public/abundance/delete_user_item';
$route['api/user/add-custom-abundance-item'] = 'api/public/abundance/add_custom_item';

//User Health and Beauty List APIS - POSITIVE AFFIRMATION

$route['api/user/fetch-user-healthandbeautylist'] = 'api/public/healthandbeauty/fetch_list';
$route['api/user/fetch-add-new-healthandbeauty-items'] = 'api/public/healthandbeauty/fetch_add_new_items';
$route['api/user/add-new-healthandbeauty-item'] = 'api/public/healthandbeauty/add_new_item';
$route['api/user/delete_user_healthandbeauty_list'] = 'api/public/healthandbeauty/delete_user_item';
$route['api/user/add-custom-healthandbeauty-item'] = 'api/public/healthandbeauty/add_custom_item';

//User Love List APIS - POSITIVE AFFIRMATION

$route['api/user/fetch-user-lovelist'] = 'api/public/love/fetch_list';
$route['api/user/fetch-add-new-love-items'] = 'api/public/love/fetch_add_new_items';
$route['api/user/add-new-love-item'] = 'api/public/love/add_new_item';
$route['api/user/delete_user_love_list'] = 'api/public/love/delete_user_item';
$route['api/user/add-custom-love-item'] = 'api/public/love/add_custom_item';

//User My Personal Affirmation List APIS - POSITIVE AFFIRMATION

$route['api/user/fetch-user-personalaffirmationlist'] = 'api/public/personalaffirmation/fetch_list';
$route['api/user/fetch-add-new-personalaffirmation-items'] = 'api/public/personalaffirmation/fetch_add_new_items';
$route['api/user/add-new-personalaffirmation-item'] = 'api/public/personalaffirmation/add_new_item';
$route['api/user/delete_user_personalaffirmation_list'] = 'api/public/personalaffirmation/delete_user_item';
$route['api/user/add-custom-personalaffirmation-item'] = 'api/public/personalaffirmation/add_custom_item';

//User HIGHEST GOOD OF ALL - APIs

$route['api/user/fetch-user-highestgoodofalllist'] = 'api/public/highestgoodofall/fetch_list';
$route['api/user/delete_user_highestgoodofall_list'] = 'api/public/highestgoodofall/delete_user_item';
$route['api/user/add-custom-highestgoodofall-item'] = 'api/public/highestgoodofall/add_custom_item';


//User GRATITUDE JOURNAL - APIs

$route['api/user/fetch-user-gratitudejournallist'] = 'api/public/gratitudejournal/fetch_list';
$route['api/user/add-custom-gratitudejournal-item'] = 'api/public/gratitudejournal/add_custom_item';
$route['api/user/fetch-current-date-type-gratitude-journal'] = 'api/public/gratitudejournal/fetch_current_date_type_gratitude_journal';
$route['api/user/delete-gratitudejournal-item'] = 'api/public/gratitudejournal/delete_gratitudejournal_item';
//User Positive Affirmation Dashboard Listing API
$route['api/fetch-user-positiveaffirmation'] = 'api/public/positiveaffirmationdashboard/show_app_positiveaffirmation_listing';

//User SELF LOVE/SELF CARE List APIS
$route['api/user/fetch-month-information'] = 'api/public/selflovecare/fetch_month_information';
$route['api/user/fetch-user-selflovecare-list'] = 'api/public/selflovecare/fetch_list';
$route['api/user/fetch-add-new-selflovecare-items'] = 'api/public/selflovecare/fetch_add_new_items';
$route['api/user/add-new-selflovecare-item'] = 'api/public/selflovecare/add_new_item';
$route['api/user/delete_user_selflovecare_list'] = 'api/public/selflovecare/delete_user_item';
$route['api/user/add-custom-selflovecare-item'] = 'api/public/selflovecare/add_custom_item';
$route['api/user/edit-selflovecare-item'] = 'api/public/selflovecare/edit_existing_item';
$route['api/user/fetch-current-date-events'] = 'api/public/selflovecare/fetch_current_date_events';

$route['api/user/edit-single-event'] = 'api/public/selflovecare/edit_item';
$route['api/user/delete-single-event'] = 'api/public/selflovecare/delete_single_event';
$route['api/user/delete-all-future-event'] = 'api/public/selflovecare/delete_all_future_event';
$route['api/user/get-single-event-details'] = 'api/public/selflovecare/get_single_event_details';
$route['api/user/fetch-events-dates-for-current-month'] = 'api/public/selflovecare/fetch_events_dates_for_current_month';

//User PERSONAL SETTINGS AND RESET PASSWORD
$route['api/user/fetch-personal-settings'] = 'api/public/user/fetch_profile';
$route['api/user/personal-settings'] = 'api/public/user/update_profile';
$route['api/user/reset-password'] = 'api/public/user/update_password';
$route['api/user/fcm-update-user'] = 'api/public/user/update_fcm_token';
$route['api/user/pushnotification'] = 'api/public/publicapi/push_notification';


$route['api/public-settings'] = 'api/public/publicapi/get_public_info';
$route['mobile/google-login'] = 'api/public/publicapi/mobile_google_login';
$route["success_google_confirmation"]= 'api/public/publicapi/mobile_google_login';
$route["fail_google_confirmation"]= 'api/public/publicapi/fail_google_login';


$route['api/login'] = 'api/admin/login/do_login';
$route['api/user-login'] = 'api/public/login/do_login';
$route['api/register'] = 'api/public/register/do_register';
$route['api/resend-email-verification'] = 'api/public/login/resend_verification_email';
$route['api/choose_new_password'] = 'api/public/register/choose_new_password';

$route['verify-email/(:any)/(:any)'] = 'api/public/login/verify_email/$1/$2';

$route['api/add_newsletter'] = 'api/public/newsletter/add_new_newsletter';

$route['(:any)'] = 'category/show_category/$1';
$route['(:any)/(:any)'] = 'category/show_category/$1/$2';
$route['(:any)/(:any)/(:any)'] = 'category/show_category/$1/$2/$3';
$route['(:any)/(:any)/(:any)/(:any)'] = 'category/show_category/$1/$2/$3/$4';
