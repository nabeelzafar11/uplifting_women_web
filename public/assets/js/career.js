
var mySwiper = new Swiper ('.career-testimonial-slider', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    // pagination: {
    //   el: '.swiper-pagination',
    // },

    // Navigation arrows
    navigation: {
      nextEl: '.career-swiper-button-next',
      prevEl: '.career-swiper-button-prev',
    },

    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
  });

  ///////////////////////////////////////
  /////// vacancy sort
 
  var melbournBtn = document.getElementById('melbournBtn');
  var darwinBtn = document.getElementById('darwinBtn');
  var tiwiBtn = document.getElementById('tiwiBtn');
  var areaHeading = document.getElementById('vacancy-area-heading');
  var noVacancy = document.querySelector('.no-vacancies');
  console.log(noVacancy);
  var availibility = false;
  melbournBtn.addEventListener('click', function(e) {
    activeMelbourn();
    
    console.log(availibility);
    e.preventDefault();
  });

  darwinBtn.addEventListener('click', function(e) {
    activeDarwin();
    
    console.log(availibility);
    e.preventDefault();
    
  });

  tiwiBtn.addEventListener('click', function(e) {
    activeTiwi();
    console.log(availibility);
    e.preventDefault();
  });
  
  function vacancySort(area) {
    
    availibility = false;
    var list = document.querySelectorAll('.vacancy');
      for(var i = 0; i < list.length; i++) {
        if(list[i].classList.contains(area)) {
          list[i].style.display = 'block';
          availibility = true;
        } else {
          list[i].style.display = 'none';
        }
      }

      if (!availibility) {
        noVacancy.style.display = 'block';
      } else {
        noVacancy.style.display = 'none';
      }
  }

  function activeMelbourn() {
    melbournBtn.parentElement.classList.add('active');
    darwinBtn.parentElement.classList.remove('active');
    tiwiBtn.parentElement.classList.remove('active');
    areaHeading.textContent = "Melbourne";
    vacancySort('area-melbourne');
  }
  function activeDarwin() {
    melbournBtn.parentElement.classList.remove('active');
    darwinBtn.parentElement.classList.add('active');
    tiwiBtn.parentElement.classList.remove('active');
    areaHeading.textContent = "Darwin";
    vacancySort('area-darwin');
  }
  function activeTiwi() {
    melbournBtn.parentElement.classList.remove('active');
    darwinBtn.parentElement.classList.remove('active');
    tiwiBtn.parentElement.classList.add('active');
    areaHeading.textContent = "Tiwi Island";
    vacancySort('area-tiwiIsland');
  }

  
  activeMelbourn();