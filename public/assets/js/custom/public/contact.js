document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#contact_form").validate({
            rules: {
                contactName: {
                    required: true
                },
                contactEmail: {
                    required: true,
                    email: true,
                },
                contactSubject: {
                    required: true,
                },
                contactMessage: {
                    required: true,
                },

            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}

function submit_contact_form() {
    if ($("#contact_form").valid()) {
        $("#contact_form").submit();
    }
}
