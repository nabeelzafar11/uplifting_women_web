$(document).ready(function () {
    $("#login_form").validate({
        rules: {
            email: {
                required: true
            },
            password: {
                required: true
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                success: function (response) {
                    ajax_success_function(form, response);
                    response = jQuery.parseJSON(response);
                    if (response.status == 'warning') {
                        $(form).find(".alertDiv a").attr('href', 'javascript:resend_email("' + $(form).find("input[name=email]").val() + '");');
                    }
                },
                beforeSubmit: function () {
                    ajax_start_function(form);
                },
                error: function (response) {
                    show_request_failed_alert(form);
                    ajax_end_function();
                }
            });
        }
    });
});

