var itemid = 0;
var id = 0;
var dates = new Array();
var dateandtimes = new Array();
var customEvents = false;
document.onreadystatechange = function () {
    if (document.readyState == "complete") {

        $(".removeItem").on("click", function () {
            itemid = $(this).data('id');
            $("#delete").val(itemid);
        });


        $("#delete_event").validate({
            rules: {
                id: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        $(".removeItem-" + itemid).remove();
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });

        $(".canceltimemodal").click(function () {
            $("#timemodal").modal('hide');
        });
        $(".canceleventmodal").click(function () {
            $("#event").modal('hide');
        });
        $(".cancelcustom").click(function () {
            $("#custom").modal('hide');
        });

        $(".datepicker").multiDatesPicker({
            onSelect: function (dateText, inst) {
                addOrRemoveDate(dateText);
            },
            prevText: '<i class="fa fa-fw fa-angle-left"></i>',
            nextText: '<i class="fa fa-fw fa-angle-right"></i>',
//            minDate: 0, // today
            defaultDate: formattedDate,
            dateFormat: "yy-mm-dd",
        });
        $(".additemselflove, .customitemselflove").on("click", function (e) {
            e.preventDefault();
            $(".datepicker").multiDatesPicker('resetDates');
        });

        $(".addeventbutton").on("click", function (e) {
            e.preventDefault();
            if (dates.length > 0) {
                var thiscomeshere = "<label class='input-inline'>Default Time: <input type='text' class='defaulttime' value='' ><label class='container1 mr-4'><input type='checkbox' id='defaultchecked' /> <span class='checkmark-1 '></span></label></label>";
                for (var i = 0; i < dates.length; i++) {
                    thiscomeshere += "<label class='input-inline'>" + dates[i] + "<input required data-time='' data-date='" + dates[i] + "' type='text' class='time'/></label>";
                }
                $(".modeltime").html(thiscomeshere);
                $('.time, .defaulttime').wickedpicker({
                });
                $("#timemodal").modal('show');
            }
        });


        $(".modeltime").on("change", ".defaulttime", function () {
            if ($("#defaultchecked").is(":checked") && $.trim($(".defaulttime").val()) !== "") {
                defaultTime = format24Hours($(".defaulttime").val().toLowerCase());
                $(".time").each(function () {
                    date = $(this).data("date");
                    date = date + " " + defaultTime;
                    $(this).attr("data-time", date);
                    $(this).val($(".defaulttime").val());
                });
            }
        });

        $(".modeltime").on("change", "#defaultchecked", function () {
            if ($("#defaultchecked").is(":checked") && $.trim($(".defaulttime").val()) !== "") {
                defaultTime = format24Hours($(".defaulttime").val().toLowerCase());
                $(".time").each(function () {
                    date = $(this).data("date");
                    date = date + " " + defaultTime;
                    $(this).attr("data-time", date);
                    $(this).val($(".defaulttime").val());
                });
            }
        });

        $("#saveevents").on("click", function () {
            dateandtimes = [];
            $(".time").each(function () {
                if (typeof ($(this).attr("data-time")) === "undefined" || $.trim($(this).attr("data-time")) === "") {
                    alert("Please make sure that you have selected time against each of the dates!");
                    return false;
                } else {
                    dateandtimes.push($(this).attr("data-time"));
                }
            });
            $("#saveevents, .canceltimemodal").hide();
            $("#myLoader").show();
            $.ajax({
                type: "POST",
                url: base_url + "api/user/edit-selflovecare-item",
                data: {dates: dateandtimes, id: id},
                success: function (response) {
                    var resp = JSON.parse(response);
                    $("#alert").html(resp.message);
                    if (resp.status == "success") {
                        $("#alert").removeClass("alert-danger").removeClass("alert-success").addClass("alert-success").show();
                    } else {
                        $("#alert").removeClass("alert-danger").removeClass("alert-success").addClass("alert-danger").show();
                    }
                    $("#timemodal").modal('hide');
                    $("#event").modal('hide');
                    $("#myLoader").hide();
                    $("#saveevents, .canceltimemodal").show();

                }
            });

        });

        $(".item").on("click", function () {
            customEvents = false;
            $("#alert").hide();
            id = $(this).data('id');
            var message = $(this).data('message');
            $("#titleplaceholder").attr("placeholder", message);
        });

        $(".customitemselflove").on("click", function () {
            customEvents = true;
            $("#alert").hide();
            $('#custom').modal('show');
        });

    }

    $(".delete-this-event").on("click", function (e) {
        $('#exampleModalScrollable').modal('hide');
        $('#exampleModalScrollable2').modal('show');
    });
    $(".additemselflove").on("click", function () {
        dates = [];
        dateandtimes = [];
        $('#exampleModalScrollable').modal('hide');
        $('#event').modal('show');
    });

}


function delete_event() {
    $("#delete_event").submit();
    $('#exampleModalScrollable2').modal('toggle');
}

function addDate(date) {
    if (jQuery.inArray(date, dates) < 0)
        dates.push(date);
}

function removeDate(index) {
    dates.splice(index, 1);
}

// Adds a date if we don't have it yet, else remove it
function addOrRemoveDate(date) {
    var index = jQuery.inArray(date, dates);
    if (index >= 0)
        removeDate(index);
    else
        addDate(date);
}

// Takes a 1-digit number and inserts a zero before it
function padNumber(number) {
    var ret = new String(number);
    if (ret.length == 1)
        ret = "0" + ret;
    return ret;
}
function GetTodayDate() {
    var tdate = new Date();
    var dd = tdate.getDate(); //yields day
    var MM = tdate.getMonth(); //yields month
    var yyyy = tdate.getFullYear(); //yields year
    var currentDate = yyyy + "-" + (MM + 1) + "-" + dd;

    return currentDate;
}