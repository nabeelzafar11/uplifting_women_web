$(document).ready(function () {
    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[\w-]+$/i.test(value);
    }, "Letters, numbers, and underscores only.");
    $("#register_form").validate({
        rules: {
            username: {
                alphanumeric: true,
                required: true,
                maxlength: 50
            },
            email: {
                required: true,
                email: true,
                maxlength: 100

            },
            password: {
                required: true,
                maxlength: 100,
                minlength: 6,
            },
            confirmPassword: {
                required: true,
                equalTo: "#r-password"
            },
            
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                success: function (response) {
                    ajax_success_function(form, response);
                    response = jQuery.parseJSON(response);
                    if (response.status == 'success') {
                        $(form).find('[type=reset]').trigger('click');
                    }
                },
                beforeSubmit: function () {
                    ajax_start_function(form);
                },
                error: function (response) {
                    show_request_failed_alert(form);
                    ajax_end_function();
                }
            });
        }
    });
});
