var internalHtml = "";
var data_id = 0;
var event_id = 0;
var dateandtime = 0;
var eventId = 0;
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#edit-icon-h1").on("click", function () {
            $("#h1_date").val(moment(h1_date, "YYYY-MM-DD").format("MM-DD-YYYY"));
            $("#h1_heading").val(h1_title);
            $("#h1_message").val(h1_message);
        });

        $("#edit-icon-h2").on("click", function () {
            $("#h2_date").val(moment(h2_date, "YYYY-MM-DD").format("MM-DD-YYYY"));
            $("#h2_heading").val(h2_title);
            $("#h2_message").val(h2_message);
        });

        $("#edit-icon-h3").on("click", function () {
            $("#h3_date").val(moment(h3_date, "YYYY-MM-DD").format("MM-DD-YYYY"));
            $("#h3_heading").val(h3_title);
            $("#h3_message").val(h3_message);
        });

        $("#modal-1-delete-button").on("click", function () {
            $("#h1Confirmationmodal").modal('show');
        });

        $("#modal-2-delete-button").on("click", function () {
            $("#h2Confirmationmodal").modal('show');
        });

        $("#modal-3-delete-button").on("click", function () {
            $("#h3Confirmationmodal").modal('show');
        });
        $("#deletethisevent1").on("click", function () {
            var new_h1_date = $.trim($("#h1_date").val());
            if (new_h1_date.length > 0) {
                new_h1_date = formatDateToUS(new_h1_date);
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/delete-gratitudejournal-item",
                    data: {
                        date: new_h1_date,
                        type: 1
                    },
                    success: function (response) {
                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            location.reload();
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#h1modal").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            } else {
                alert("Date field cannot be empty!");
            }
        });

        $("#deletethisevent2").on("click", function () {
            var new_h2_date = $.trim($("#h2_date").val());
            if (new_h2_date.length > 0) {
                new_h2_date = formatDateToUS(new_h2_date);
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/delete-gratitudejournal-item",
                    data: {
                        date: new_h2_date,
                        type: 2
                    },
                    success: function (response) {
                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            location.reload();
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#h2modal").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            } else {
                alert("Date field cannot be empty!");
            }
        });

        $("#deletethisevent3").on("click", function () {
            var new_h3_date = $.trim($("#h3_date").val());
            if (new_h3_date.length > 0) {
                new_h3_date = formatDateToUS(new_h3_date);
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/delete-gratitudejournal-item",
                    data: {
                        date: new_h3_date,
                        type: 1
                    },
                    success: function (response) {
                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            location.reload();
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#h3modal").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            } else {
                alert("Date field cannot be empty!");
            }
        });


        $("#modal-1-submit-button").on("click", function () {
            var new_h1_date = $.trim($("#h1_date").val());
            var new_h1_heading = $.trim($("#h1_heading").val());
            var new_h1_message = $.trim($("#h1_message").val());

            if (new_h1_date.length > 0 && new_h1_heading.length > 0 && new_h1_message.length > 0) {
                new_h1_date = formatDateToUS(new_h1_date);
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/add-custom-gratitudejournal-item",
                    data: {
                        header: new_h1_heading,
                        message: new_h1_message,
                        date: new_h1_date,
                        type: 1
                    },
                    success: function (response) {
                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            h1_date = new_h1_date;
                            window.location = currentGratitudeLocation + "?date=" + h1_date;
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#h1modal").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            } else {
                alert("Please fill all fields.");
            }
        });

        $("#modal-2-submit-button").on("click", function () {
            var new_h2_date = $.trim($("#h2_date").val());
            var new_h2_heading = $.trim($("#h2_heading").val());
            var new_h2_message = $.trim($("#h2_message").val());

            if (new_h2_date.length > 0 && new_h2_heading.length > 0 && new_h2_message.length > 0) {
                new_h2_date = formatDateToUS(new_h2_date);
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/add-custom-gratitudejournal-item",
                    data: {
                        header: new_h2_heading,
                        message: new_h2_message,
                        date: new_h2_date,
                        type: 2
                    },
                    success: function (response) {

                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            h2_date = new_h2_date;
                            window.location = currentGratitudeLocation + "?date=" + h2_date;
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#h2modal").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            } else {
                alert("Please fill all fields.");
            }
        });

        $("#modal-3-submit-button").on("click", function () {
            var new_h3_date = $.trim($("#h3_date").val());
            var new_h3_heading = $.trim($("#h3_heading").val());
            var new_h3_message = $.trim($("#h3_message").val());

            if (new_h3_date.length > 0 && new_h3_heading.length > 0 && new_h3_message.length > 0) {
                new_h3_date = formatDateToUS(new_h3_date);
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/add-custom-gratitudejournal-item",
                    data: {
                        header: new_h3_heading,
                        message: new_h3_message,
                        date: new_h3_date,
                        type: 3
                    },
                    success: function (response) {
                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            h3_date = new_h3_date;
                            window.location = currentGratitudeLocation + "?date=" + h3_date;
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#h3modal").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            } else {
                alert("Please fill all fields.");
            }
        });

        $("#h1_date").datepicker({

            prevText: '<i class="fa fa-fw fa-angle-left"></i>',
            nextText: '<i class="fa fa-fw fa-angle-right"></i>',
            dateFormat: "mm-dd-yy",
            onSelect: function (dateText) {
                call_events_function(dateText, 1);
            }
        });
        $("#h2_date").datepicker({

            prevText: '<i class="fa fa-fw fa-angle-left"></i>',
            nextText: '<i class="fa fa-fw fa-angle-right"></i>',
            dateFormat: "mm-dd-yy",
            onSelect: function (dateText) {
                call_events_function(dateText, 2);
            }
        });
        $("#h3_date").datepicker({
            prevText: '<i class="fa fa-fw fa-angle-left"></i>',
            nextText: '<i class="fa fa-fw fa-angle-right"></i>',
            dateFormat: "mm-dd-yy",
            onSelect: function (dateText) {
                call_events_function(dateText, 3);
            }
        });
        function call_events_function(dateText, type) {
            dateText = formatDateToUS(dateText);
            $.ajax({
                type: "post",
                url: base_url + "api/user/fetch-current-date-type-gratitude-journal",
                data: {date: dateText, type: type},
                success: function (response) {
                    var resp = JSON.parse(response);
                    console.log(resp);
                    if (resp.status == "success") {
                        respArray = resp.message;
                        if (type == 1) {
                            $("#h1_heading").val(respArray.header);
                            $("#h1_message").val(respArray.message);
                        } else if (type == 2) {
                            $("#h2_heading").val(respArray.header);
                            $("#h2_message").val(respArray.message);
                        } else if (type == 3) {
                            $("#h3_heading").val(respArray.header);
                            $("#h3_message").val(respArray.message);
                        }
                    }
                }
            });
        }
    }
    function formatDateToUS(dateText) {
        var date_string = moment(dateText, "MM/DD/YYYY").format("YYYY-MM-DD");
        console.log(date_string);
        return date_string;
    }

};
