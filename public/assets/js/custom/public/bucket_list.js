document.onreadystatechange = function () {
    $('input[class^="class"]').click(function () {
        var $this = $(this);
        if ($this.is(".classadmin")) {
            if ($(".classadmin:checked").length > 0) {
                $(".classcustom").prop({disabled: true, checked: false});
            } else {
                $(".classcustom").prop("disabled", false);
            }
        } else if ($this.is(".classcustom")) {
            if ($this.is(":checked")) {
                $(".classadmin").not($this).prop({disabled: true, checked: false});
                $(".classcustom").prop("checked", true);
            } else {
                $(".classadmin").prop("disabled", false);
            }
        }
    });

    if (document.readyState == "complete") {
        $("#abundanceList").on("click", ".abundanceListItem", function () {
            var abundanceId = $(this).data("id");
            $("#abundanceID").val(abundanceId);
        });
    }
};

function on_click_add_bucklist_item() {

    var idsArray = [];
    $("input:checkbox[name=newListItem]:checked").each(function () {
        idsArray.push($(this).val());
    });

    var bucket_list_message = $("input[name='newListItem']:checked").data(
            "message"
            );
    if (idsArray.length > 0) {
        if (idsArray[0] === "customListItem") {
            $("#custom").modal("show");
        } else {
            $("#myLoader").show();
            $.ajax({
                type: "POST",
                url: base_url + "api/user/add-new-bucketlist-item",
                data: {bucketlist_id: idsArray},
                success: function (response) {
                    var resp = JSON.parse(response);
                    $("#alert").html(resp.message);
                    if (resp.status == "success") {
                        new_bucketlist_item_id = resp.id;
                        $("#alert")
                                .removeClass("alert-danger")
                                .removeClass("alert-success")
                                .addClass("alert-success")
                                .show();
                        location.reload();
//                        $("#remove-newlist" + checked).remove();
//                        $("#abundanceList").append(`
//			            <li  id="remove-list${new_bucketlist_item_id}" class="bg-modle-li-2 alert-white   fade show d-flex justify-content-between"  >
//                            <span>${bucket_list_message}</span>
//                            <button data-id="${new_bucketlist_item_id}" type="button" data-toggle="modal" data-target="#exampleModalScrollable2" class="close abundanceListItem" aria-label="Close">
//                                <span aria-hidden="true"><img src="${ASSETS_PATH}newtheme/img/Remove Circle.png" class="img-fluid" alt=""></span>
//                            </button>
//                        </li>`);
                    } else {
                        $("#alert")
                                .removeClass("alert-danger")
                                .removeClass("alert-success")
                                .addClass("alert-danger")
                                .show();
                    }
                    $("#exampleModalScrollableStart").modal("hide");
                    $("#myLoader").hide();
                }
            });
        }
    }
}

function delete_bucklist_item() {
    var abundanceId = $.trim($("#abundanceID").val());
    if (abundanceId) {
        $("#myLoader").show();
        $.ajax({
            type: "POST",
            url: base_url + "api/user/delete_user_bucket_list",
            data: {id: abundanceId},
            success: function (response) {
                var resp = JSON.parse(response);
                if (resp.status == "success") {
                    window.location.reload();
                } else {
                    $("#alert").html(resp.message);
                    $("#alert")
                            .removeClass("alert-danger")
                            .removeClass("alert-success")
                            .addClass("alert-danger")
                            .show();
                }

                $("#exampleModalScrollable2").modal("hide");
                $("#myLoader").hide();
            }
        });
    }
}

function add_bucklist_item() {
    var abundanceNewItem = $.trim($("#pwd").val());
    if (abundanceNewItem) {
        $("#myLoader").show();
        $.ajax({
            type: "POST",
            url: base_url + "api/user/add-custom-bucketlist-item",
            data: {message: abundanceNewItem},
            success: function (response) {
                var resp = JSON.parse(response);
                $("#alert").html(resp.message);
                if (resp.status == "success") {
                    new_bucketlist_item_id = resp.id;
                    $("#alert")
                            .removeClass("alert-danger")
                            .removeClass("alert-success")
                            .addClass("alert-success")
                            .show();
                    $("#shareButton").before(`
			            <li  id="remove-list${new_bucketlist_item_id}" class="bg-modle-li-2 alert-white   fade show d-flex justify-content-between"  >
                            <span>${abundanceNewItem}</span>
                            <button data-id="${new_bucketlist_item_id}" type="button" data-toggle="modal" data-target="#exampleModalScrollable2" class="close abundanceListItem" aria-label="Close">
                                <span aria-hidden="true"><img src="${ASSETS_PATH}newtheme/img/Remove Circle.png" class="img-fluid" alt=""></span>
                            </button>
                        </li>`);
                } else {
                    $("#alert")
                            .removeClass("alert-danger")
                            .removeClass("alert-success")
                            .addClass("alert-danger")
                            .show();
                }
                $("#custom").modal("hide");
                $("#exampleModalScrollableStart").modal("hide");
                $("#myLoader").hide();
            }
        });
    }
}
