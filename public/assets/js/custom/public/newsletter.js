$(document).ready(function () {
    $("#newsletter_form").validate({
        rules: {
			subscriberName: {
                required: true,
            },
			subscriberEmail: {
                required: true,
            },
			subscriberPostcode: {
                required: true,
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                success: function (response) {
                    ajax_success_function(form, response);
                    $(form).find("button[type=reset]").trigger("click");
                },
                beforeSubmit: function () {
                    ajax_start_function(form);
                },
                error: function (response) {
                    show_request_failed_alert(form);
                    ajax_end_function();
                }
            });
        }
    });
});
