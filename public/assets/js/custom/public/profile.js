document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#personal_info_form").validate({
            rules: {
                firstName: {
                    maxlength: 100
                },
                lastName: {
                    maxlength: 100

                },
                nickName: {
                    maxlength: 100
                },
                phone: {
                    maxlength: 100
                },

            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                        response = jQuery.parseJSON(response);
                        if (response.status == 'success') {
                            $(form).find('[type=reset]').trigger('click');
                        }
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
        $("#social_info_form").validate({
            rules: {
                twitterLink: {
                    maxlength: 255
                },
                facebookLink: {
                    maxlength: 255

                },
                googleLink: {
                    maxlength: 255
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                        response = jQuery.parseJSON(response);
                        if (response.status == 'success') {
                            $(form).find('[type=reset]').trigger('click');
                        }
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
        $("#change_password_form").validate({
            rules: {
                currentPassword: {
                    required: true
                },
                password: {
                    required: true,
                    maxlength: 100,
                    minlength: 6,
                },
                confirmPassword: {
                    required: true,
                    equalTo: "#password"
                },
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                        response = jQuery.parseJSON(response);
                        if (response.status == 'success') {
                            $(form).find('[type=reset]').trigger('click');
                        }
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}

function upload_profile_image(elem) {
    // $(elem).val('');
    if ($("#imageForm input[type=file]").val()) {

        $('#imageForm').ajaxSubmit({
            success: function (response) {
                response = jQuery.parseJSON(response);
                $("#personal_info_form").find('img').attr('src', mediaPath + response.message);
                $("#personal_info_form").find('input[name=image]').val(response.message);
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            },
            beforeSubmit: function () {
                ajax_start_function('#personal_info_form');
            },
            error: function (response) {
                show_request_failed_alert('#personal_info_form');
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            }
        });
    }

}
function open_profile_image() {
    $("#imageForm input[type=file]").trigger('click');
}
