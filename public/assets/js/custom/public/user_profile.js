document.onreadystatechange = function() {
	if (document.readyState == "complete") {
		$("#personal_tab").click(function() {
			$(this).css({
				color: "#FC5C9B",
				"text-decoration": "underline"
			});
			$("#password_tab").css({
				color: "black",
				"text-decoration": "none"
			});
			$("#personal_container").css("display", "block");
			$("#password_container").css("display", "none");
		});
		$("#password_tab").click(function() {
			$(this).css({
				color: "#FC5C9B",
				"text-decoration": "underline"
			});
			$("#personal_tab").css({
				color: "black",
				"text-decoration": "none"
			});
			$("#personal_container").css("display", "none");
			$("#password_container").css("display", "block");
		});

		$("#personal_form").validate({
			rules: {
				firstName: {
					maxlength: 25,
					required: true
				},
				lastName: {
					maxlength: 25,
					required: true
				},
				email: {
					maxlength: 200,
					email: true,
					required: true
				}
			},
			submitHandler: function(form) {
				$(form).ajaxSubmit({
					success: function(response) {
						ajax_success_function(form, response);
					},
					beforeSubmit: function() {
						ajax_start_function(form);
					},
					error: function(response) {
						show_request_failed_alert(form);
						ajax_end_function();
					}
				});
			}
		});
		$("#password_form").validate({
			rules: {
				currentPassword: {
					maxlength: 25,
					required: true
				},
				password: {
					maxlength: 25,
					required: true
				},
				confirmPassword: {
					maxlength: 25,
					required: true
				}
			},
			submitHandler: function(form) {
				$(form).ajaxSubmit({
					success: function(response) {
						ajax_success_function(form, response);
					},
					beforeSubmit: function() {
						ajax_start_function(form);
					},
					error: function(response) {
						show_request_failed_alert(form);
						ajax_end_function();
					}
				});
			}
		});

		$("#imagePreview").click(function() {
			$("#imageUpload").trigger("click");
		});
	}
};

function getBase64(input) {
	var file = input.files[0];
	var reader = new FileReader();
	reader.onloadend = function() {
		$("#image").val(reader.result);
		$("#imagePreview").attr("src", reader.result);
	};
	reader.readAsDataURL(file);
}
