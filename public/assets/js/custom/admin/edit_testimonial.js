document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#testimonial_form").validate({
            rules: {
                testimonialName: {
                    required: true
                },
                testimonialOccupation: {
                    required: true
                },
                testimonialComment: {
                    required: true
                },
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}