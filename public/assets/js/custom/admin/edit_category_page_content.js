document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        CKEDITOR.replace('categoryPageContent', {
            extraPlugins: 'accordion,downloader',
            height: 400
        });
        $("#category_page_form").validate({
            rules: {
                categoryName: {
                    maxlength: 255,
                    required: true
                },

            },
            submitHandler: function (form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}