document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        CKEDITOR.replace('web_message', {
            height: 400
        });
        $("#dashboard_form").validate({
            rules: {
                web_message: {
                    required: true
                },
                seo_title: {
                    required: true
                },
                seo_keywords: {
                    required: true
                },
                seo_description: {
                    required: true
                }

            },
            submitHandler: function (form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}