document.onreadystatechange = function() {
	if (document.readyState == "complete") {
		$(".image1").change(function() {
			readURLx(this, "#image1");
		});

		$(".image2").change(function() {
			readURLx(this, "#image2");
		});

		$("#dashboard_form").validate({
			rules: {
				name: {
					required: true
				},
				seo_keywords: {
					required: true
				},
				seo_title: {
					required: true
				},
				seo_description: {
					required: true
				}
			},
			submitHandler: function(form) {
				$(form).ajaxSubmit({
					success: function(response) {
						ajax_success_function(form, response);
					},
					beforeSubmit: function() {
						ajax_start_function(form);
					},
					error: function(response) {
						show_request_failed_alert(form);
						ajax_end_function();
					}
				});
			}
		});
	}
};

function readURLx(input, classId) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$(classId).attr("src", e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	} else {
		$(classId).attr("src", defaultImage);
	}
}
