document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        CKEDITOR.replace('gratitude_web', {
            height: 400
        });
        CKEDITOR.replace('positiveaffirmation_web', {
            height: 400
        });
        CKEDITOR.replace('highestgoodofall_web', {
            height: 400
        });
        CKEDITOR.replace('login_web', {
            height: 400
        });

        $("#header_form").validate({
            submitHandler: function (form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}