CKEDITOR.dialog.add( 'accordionDialog', function ( editor ) {
    return {
        title: 'Accordion Configuration',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'number',
                        label: 'Mention the number of sections the Accordion Shall have',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Must have a number" )
                    }
                ]
            }
        ],
        onOk: function() {
            var dialog = this;
            var sections = parseInt(dialog.getValueOf('tab-basic','number')); //Número de seções que serão criadas

            section = "<h3>Title of Accordion</h3><div><p>Please write details for this Accordion here ...</p></div>"
            intern = ""
            for (i=0;i<sections;i++){
                intern = intern + section
            }

            editor.insertHtml('<div class="accordion">'+ intern +'</div>');

        }
    };
});
