$(document).ready(function () {

    $('#owl-one').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: [
            // "<i class='fa fa-angle-left fa-3x'></i>",
            // "<i class='fa fa-angle-right fa-3x'></i>"
        ],

        autoplay: false,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            320: {
                items: 1
            },
            600: {
                items: 2
            },

            800: {
                items: 2
            },
            900: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    })
});
function myConfirm() {
    var result = confirm("Want to delete?");
    if (result == true) {
        return true;
    } else {
        return false;
    }
}
mobiscroll.settings = {
    theme: 'ios',
    themeVariant: 'light'
};

var fromMonday = [],
        fromSaturday = [],
        twoWeeks = [],
        i = 0,
        j = 0;

for (i; i < 7; i++) {
    fromMonday.push(new Date(2018, 0, 8 + i));
    fromSaturday.push(new Date(2018, 0, 6 + i));
}

for (j; j < 14; j++) {
    twoWeeks.push(new Date(2018, 0, 8 + j));
}

mobiscroll.calendar('#demo-mon-sun', {
    display: 'inline',
    selectType: 'week',
    defaultValue: fromMonday,
    firstSelectDay: 1,
    firstDay: 1
});

mobiscroll.calendar('#demo-sat-fri', {
    display: 'inline',
    selectType: 'week',
    defaultValue: fromSaturday,
    firstSelectDay: 6,
    firstDay: 1
});

mobiscroll.calendar('#demo-multi', {
    display: 'inline',
    selectType: 'week',
    defaultValue: twoWeeks,
    firstSelectDay: 1,
    firstDay: 1,
    select: 'multiple'
});