-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 08, 2020 at 11:27 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `women_empowerment`
--

-- --------------------------------------------------------

--
-- Table structure for table `abundance_list`
--

CREATE TABLE `abundance_list` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abundance_list`
--

INSERT INTO `abundance_list` (`id`, `type`, `user_id`, `message`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(1, 1, 0, 'Abundance item 12', 0, 2, '2020-01-27 17:04:19'),
(2, 1, 0, 'Abundance item 1', 0, 1, '2020-01-27 18:03:00'),
(3, 1, 8, 'Abundance Item 3', 0, 0, '2020-01-27 18:30:47'),
(4, 1, 8, 'Abundance Item 4', 0, 0, '2020-01-27 18:32:10'),
(5, 1, 0, 'test', 0, 0, '2020-03-18 17:00:18'),
(6, 1, 0, 'test', 0, 0, '2020-03-18 17:00:30'),
(7, 1, 8, 'test', 0, 0, '2020-04-05 19:49:14');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blogId` int(11) NOT NULL,
  `blogTitle` varchar(255) NOT NULL,
  `blogFeatureImage` varchar(255) NOT NULL,
  `blogDescription` text NOT NULL,
  `blogContent` text NOT NULL,
  `blogBlogCategoryId` int(11) NOT NULL,
  `blogSlug` varchar(255) NOT NULL,
  `blogInterestedOneCategoryId` int(11) NOT NULL,
  `blogInterestedTwoCategoryId` int(11) NOT NULL,
  `blogInterestedThreeCategoryId` int(11) NOT NULL,
  `blogIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `blogIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `blogPageTitle` text NOT NULL,
  `blogSeoTitle` text NOT NULL,
  `blogSeoDescription` text NOT NULL,
  `blogSeoKeywords` text NOT NULL,
  `blogCreatedUserId` int(11) NOT NULL,
  `blogModifiedUserId` int(11) NOT NULL,
  `blogCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blogModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`blogId`, `blogTitle`, `blogFeatureImage`, `blogDescription`, `blogContent`, `blogBlogCategoryId`, `blogSlug`, `blogInterestedOneCategoryId`, `blogInterestedTwoCategoryId`, `blogInterestedThreeCategoryId`, `blogIsActive`, `blogIsDeleted`, `blogPageTitle`, `blogSeoTitle`, `blogSeoDescription`, `blogSeoKeywords`, `blogCreatedUserId`, `blogModifiedUserId`, `blogCreatedAt`, `blogModifiedAt`) VALUES
(1, 'sfsdf', '', 'fsdfsdfsdf', 'dsfsdfsfsdfsdfs', 2, 'sfsdf', 0, 0, 0, 0, 1, '', '', '', '', 0, 0, '2019-02-14 00:17:54', '2019-02-14 00:22:52'),
(2, 'sfsdff', '1550103655.png', 'fafdsf', 'fsdfsdf', 2, 'sfsdff', 5, 6, 7, 1, 1, '', '', '', '', 0, 0, '2019-02-14 00:20:55', '2019-02-14 18:05:10'),
(3, 'DJ Josh achieves his NDIS goal', '1550167499.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.&lt;/p&gt;\n\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.&lt;/p&gt;', 0, 'dj-josh-achieves-his-ndis-goal', 0, 0, 0, 1, 0, '', '', '', '', 0, 0, '2019-02-14 18:04:59', '2019-02-14 18:05:07'),
(4, 'Provide your feedback on the new NDIS website', '1550167580.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.&lt;/p&gt;', 0, 'provide-your-feedback-on-the-new-ndis-website', 0, 0, 0, 1, 0, '', '', '', '', 0, 0, '2019-02-14 18:06:20', '2019-02-14 18:07:23'),
(5, 'Zander follows his volunteering dream', '1550167617.jpg', 'Zander follows his volunteering dream', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.&lt;/p&gt;', 0, 'zander-follows-his-volunteering-dream', 0, 0, 0, 1, 0, '', '', '', '', 0, 0, '2019-02-14 18:06:57', '2019-02-14 18:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `blogCategoryId` int(11) NOT NULL,
  `blogCategoryName` varchar(255) NOT NULL,
  `blogCategoryDescription` text NOT NULL,
  `blogCategorySortOrder` int(11) NOT NULL DEFAULT '0',
  `blogCategoryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `blogCategoryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `blogCategoryCreatedUserId` int(11) NOT NULL,
  `blogCategoryModifiedUserId` int(11) NOT NULL,
  `blogCategoryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blogCategoryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`blogCategoryId`, `blogCategoryName`, `blogCategoryDescription`, `blogCategorySortOrder`, `blogCategoryIsActive`, `blogCategoryIsDeleted`, `blogCategoryCreatedUserId`, `blogCategoryModifiedUserId`, `blogCategoryCreatedAt`, `blogCategoryModifiedAt`) VALUES
(1, 'ada', '', 0, 1, 1, 0, 0, '2019-02-13 23:37:06', '2019-02-13 23:39:25'),
(2, 'sdasdasdczc', '', 0, 1, 0, 0, 0, '2019-02-13 23:37:12', '2019-02-13 23:46:10');

-- --------------------------------------------------------

--
-- Table structure for table `bucket_list`
--

CREATE TABLE `bucket_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bucket_list`
--

INSERT INTO `bucket_list` (`id`, `user_id`, `message`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(1, 0, 'Its a great day to visit opera house ...', 1, 0, '2020-01-24 16:24:11'),
(2, 0, 'Thankful to be in Sydney! A dream come true.', 0, 3, '2020-01-24 17:07:07'),
(3, 0, 'To buy a Ferrari by end of 2020', 0, 2, '2020-01-24 17:08:59'),
(4, 0, 'Test Item 3', 0, 1, '2020-01-25 15:07:49'),
(5, 8, 'Visit Dubai by end of 2020', 0, 0, '2020-01-25 15:30:50'),
(6, 8, 'test', 0, 0, '2020-04-07 19:26:59'),
(7, 8, 'testtetete', 0, 0, '2020-04-07 19:45:01'),
(8, 8, 'testttt', 0, 0, '2020-04-07 19:46:28'),
(9, 8, 'tewtewtwetwe', 0, 0, '2020-04-07 19:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `calendar_dailyaffirmation`
--

CREATE TABLE `calendar_dailyaffirmation` (
  `month` int(11) NOT NULL,
  `web_message` text NOT NULL,
  `mobile_message` text NOT NULL,
  `seo_title` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calendar_dailyaffirmation`
--

INSERT INTO `calendar_dailyaffirmation` (`month`, `web_message`, `mobile_message`, `seo_title`, `seo_keywords`, `seo_description`, `updatedAt`) VALUES
(1, '<p><strong>January is a month some </strong>moneth message TEST</p>', 'January is a month some moneth message TEST', 'test', 'test', 'test', '2020-02-18 02:20:04'),
(2, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(3, '<h5>I happily Claim My Own Powers</h5>', 'I happily Claim My Own Powers', 'tssds', 'dsds', 'dsds', '2020-02-18 02:20:04'),
(4, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(5, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(6, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(7, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(8, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(9, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(10, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(11, 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(12, 'test', 'test', '', '', '', '2020-02-18 02:20:04');

-- --------------------------------------------------------

--
-- Table structure for table `career_page`
--

CREATE TABLE `career_page` (
  `careerPageId` int(11) NOT NULL,
  `careerPageJoinNowHeading` varchar(255) NOT NULL,
  `careerPageJoinNowText` text NOT NULL,
  `careerPageJoinNowBtnText` varchar(255) NOT NULL,
  `careerPageJoinNowImage` varchar(255) NOT NULL,
  `careerPageSupportHeading` varchar(255) NOT NULL,
  `careerPageSupportRole1` varchar(255) NOT NULL,
  `careerPageSupportRole2` varchar(255) NOT NULL,
  `careerPageSupportRole3` varchar(255) NOT NULL,
  `careerPageSupportRole4` varchar(255) NOT NULL,
  `careerPageSupportRole5` varchar(255) NOT NULL,
  `careerPageSupportRole6` varchar(255) NOT NULL,
  `careerPageCreatedUserId` int(11) NOT NULL,
  `careerPageWorkHeading` varchar(255) NOT NULL,
  `careerPageWorkText` text NOT NULL,
  `careerPageWorkExpHeading` varchar(255) NOT NULL,
  `careerPageWorkExpText` text NOT NULL,
  `careerPageWorkExpImage` varchar(255) NOT NULL,
  `careerPageResourcesSection` text NOT NULL,
  `careerPageLastModifiedUserId` int(11) NOT NULL,
  `careerPageCreaterAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `careerPageModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career_page`
--

INSERT INTO `career_page` (`careerPageId`, `careerPageJoinNowHeading`, `careerPageJoinNowText`, `careerPageJoinNowBtnText`, `careerPageJoinNowImage`, `careerPageSupportHeading`, `careerPageSupportRole1`, `careerPageSupportRole2`, `careerPageSupportRole3`, `careerPageSupportRole4`, `careerPageSupportRole5`, `careerPageSupportRole6`, `careerPageCreatedUserId`, `careerPageWorkHeading`, `careerPageWorkText`, `careerPageWorkExpHeading`, `careerPageWorkExpText`, `careerPageWorkExpImage`, `careerPageResourcesSection`, `careerPageLastModifiedUserId`, `careerPageCreaterAt`, `careerPageModifiedAt`) VALUES
(1, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 14:57:17', '2019-03-20 14:57:17'),
(2, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:14:26', '2019-03-20 15:14:26'),
(3, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:14:39', '2019-03-20 15:14:39'),
(4, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:16:10', '2019-03-20 15:16:10'),
(5, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:16:52', '2019-03-20 15:16:52'),
(6, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:20:29', '2019-03-20 15:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `categorySlug` varchar(255) NOT NULL,
  `categoryIcon` varchar(255) NOT NULL,
  `categoryImage` varchar(255) NOT NULL,
  `categoryShortDescription` text NOT NULL,
  `categoryDescription` text NOT NULL,
  `categoryParentId` int(11) NOT NULL,
  `categoryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `categoryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `categorySortOrder` int(11) NOT NULL DEFAULT '0',
  `categoryPageType` int(11) NOT NULL,
  `categoryPageContent` text NOT NULL,
  `categoryInterestedOneCategoryId` int(11) NOT NULL,
  `categoryInterestedTwoCategoryId` int(11) NOT NULL,
  `categoryInterestedThreeCategoryId` int(11) NOT NULL,
  `categoryPageTitle` text NOT NULL,
  `categorySeoTitle` text NOT NULL,
  `categorySeoKeywords` text NOT NULL,
  `categorySeoDescription` text NOT NULL,
  `categoryCreatedUserId` int(11) NOT NULL,
  `categoryModifiedUserId` int(11) NOT NULL,
  `categoryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `categoryName`, `categorySlug`, `categoryIcon`, `categoryImage`, `categoryShortDescription`, `categoryDescription`, `categoryParentId`, `categoryIsActive`, `categoryIsDeleted`, `categorySortOrder`, `categoryPageType`, `categoryPageContent`, `categoryInterestedOneCategoryId`, `categoryInterestedTwoCategoryId`, `categoryInterestedThreeCategoryId`, `categoryPageTitle`, `categorySeoTitle`, `categorySeoKeywords`, `categorySeoDescription`, `categoryCreatedUserId`, `categoryModifiedUserId`, `categoryCreatedAt`, `categoryModifiedAt`) VALUES
(174, 'About Us', 'about-us', '', '1583326401.png', '&lt;div class=&quot;col-lg-3 col-md-12&quot;&gt;\n&lt;div class=&quot;about-text&quot;&gt;\n&lt;h5&gt;History&lt;/h5&gt;\n\n&lt;h1&gt;how we&lt;span class=&quot;d-block&quot;&gt;&lt;/span&gt;Founded&lt;/h1&gt;\n&lt;/div&gt;\n&lt;/div&gt;\n', '&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;Being a woman is not easy. The expectations that are placed on us vastly differ from expectations placed on men. There are impossible standards that women are expected to live up to, to have it all. As a result, we beat up on ourselves to do a good job and find ourselves going above and beyond.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;I believe, as women we are multifaceted, and we can do anything we want. There are absolutely no limits on what we can achieve, but we need to support and empower not only each other, but also the next generation.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;I have realized while trying to do and be everything, we end up neglecting ourselves. That is the reason why I came up with this App. I have included elements that I use and practice in my life and truly find them to not only uplift me, but also create focus, joy, love, abundance and empowerment. I hope that you will also be uplifted and enjoy every aspect of this App and remember to accept and appreciate your uniqueness.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;So make yourself a priority today and every day. Because you deserve it!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;', 0, 1, 0, 0, 3, '', 0, 0, 0, 'About Us', 'About Us', 'About Us, Uplifting Women', 'Being a woman is not easy. The expectations that are placed on us vastly differ from expectations placed on men. There are impossible standards that women are expected to live up to, to have it all. As a result, we beat up on ourselves to do a good job and find ourselves going above and beyond.', 0, 0, '2020-03-04 12:53:22', '2020-03-04 13:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `category_pages`
--

CREATE TABLE `category_pages` (
  `categoryPageId` int(11) NOT NULL,
  `categoryPageCategoryId` int(11) NOT NULL,
  `categoryPageTitle` varchar(255) NOT NULL,
  `categoryPageContent` text NOT NULL,
  `categoryPageIsLeftNav` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageLeftNavType` tinyint(4) NOT NULL DEFAULT '1',
  `categoryPageLeftNavContent` text NOT NULL,
  `categoryPageIsRightNav` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageRightNavType` tinyint(4) NOT NULL DEFAULT '1',
  `categoryPageRightNavContent` text NOT NULL,
  `categoryPageType` int(11) NOT NULL,
  `categoryPagePageTitle` text NOT NULL,
  `categoryPageSeoTitle` text NOT NULL,
  `categoryPageSeoDescription` text NOT NULL,
  `categoryPageSeoKeywords` text NOT NULL,
  `categoryPageIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageCreatedUserId` int(11) NOT NULL,
  `categoryPageModifiedUserId` int(11) NOT NULL,
  `categoryPageCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryPageModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `popupMessage` text,
  `popupMessageMobile` text NOT NULL,
  `seo_title` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard`
--

INSERT INTO `dashboard` (`id`, `name`, `image`, `popupMessage`, `popupMessageMobile`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 'Calendar 2020', '1579738428.png', '&lt;p&gt;&lt;strong&gt;The calendar &lt;/strong&gt;is set up to remind you to love, spoil and make yourself a priority, even if it&amp;rsquo;s for 5-10 minutes. You have the option to choose from a list of items from the Self Love/Self Care section to schedule &amp;ldquo;me&amp;rdquo; time. You can choose items for every day of the week or several times a week or a month, your choice. So, go ahead and love yourself.&lt;/p&gt;', 'The calendar is set up to remind you to love, spoil and make yourself a priority, even if it&rsquo;s for 5-10 minutes. You have the option to choose from a list of items from the Self Love/Self Care section to schedule &ldquo;me&rdquo; time. You can choose items for every day of the week or several times a week or a month, your choice. So, go ahead and love yourself.', 'Calendar 2020', 'calendar 2020, test', 'just some dummy description just some dummy description just some dummy description just some dummy description'),
(2, 'Self Love/Self Care List', '1579735983.png', '&lt;p&gt;A simple yet important list of items to help you make yourself a priority. Self Love/Self Care is important to all of us. Each month offers a list of items that we hope you choose from to spoil yourself.&lt;/p&gt;', 'A simple yet important list of items to help you make yourself a priority. Self Love/Self Care is important to all of us. Each month offers a list of items that we hope you choose from to spoil yourself.', 'test', 'test', 'test'),
(3, 'Positive Affirmations', '1579736077.png', '&lt;p&gt;Affirmations are positive statements that can counteract some of our negative thoughts and habits, resonating with the alpha brain waves and enabling us to achieve empowerment. Positive affirmations can make us feel better about ourselves and help us focus on our goals. Scientific research suggests that they also produce chemical changes in the brain and create tangible benefits for those who use them. Say it and Believe it.&lt;/p&gt;', 'Affirmations are positive statements that can counteract some of our negative thoughts and habits, resonating with the alpha brain waves and enabling us to achieve empowerment. Positive affirmations can make us feel better about ourselves and help us focus on our goals. Scientific research suggests that they also produce chemical changes in the brain and create tangible benefits for those who use them. Say it and Believe it.', 'test', 'test', 'test'),
(4, 'Gratitude Journal', '1579736270.png', '&lt;p&gt;The text needs to be uploaded here&lt;/p&gt;', 'The text needs to be uploaded here', 'test', 'test', 'test'),
(5, 'Bucket List', '1579736368.png', '&lt;p&gt;Have you been thinking about creating a bucket list for yourself? Would you consider sharing your bucket list with your family/friends? We&amp;rsquo;ve listed some popular items that people have in their Bucket List. You can also create your own.&lt;/p&gt;', 'Have you been thinking about creating a bucket list for yourself? Would you consider sharing your bucket list with your family/friends? We&rsquo;ve listed some popular items that people have in their Bucket List. You can also create your own.', 'test', 'test', 'test'),
(6, 'Highest Good of All', '1579736596.png', '&lt;p&gt;Client needs to update message from admin panel&lt;/p&gt;', 'Client needs to update message from admin panel', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `directorId` int(11) NOT NULL,
  `directorName` varchar(255) NOT NULL,
  `directorDescription` text NOT NULL,
  `directorImage` varchar(255) NOT NULL,
  `directorSortOrder` int(11) NOT NULL DEFAULT '0',
  `directorIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `directorIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `directorCreatedUserId` int(11) NOT NULL,
  `directorModifiedUserId` int(11) NOT NULL,
  `directorCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `directorModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_selflovecare_list_id` int(11) DEFAULT NULL,
  `dateandtime` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `user_id`, `user_selflovecare_list_id`, `dateandtime`, `createdAt`) VALUES
(125, 8, NULL, '2020-04-15 01:11:00', '2020-04-09 01:32:49'),
(126, 8, 40, '2020-04-29 02:22:00', '2020-04-09 02:22:41');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `fileId` int(11) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  `fileUrl` varchar(255) NOT NULL,
  `fileSlug` varchar(255) NOT NULL,
  `fileIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `fileIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `fileCreatedUserId` int(11) NOT NULL,
  `fileModifiedUserId` int(11) NOT NULL,
  `fileCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fileModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `footerId` int(11) NOT NULL,
  `footerLogo` text NOT NULL,
  `footerText` text NOT NULL,
  `contactUsText` text NOT NULL,
  `phoneText` text NOT NULL,
  `phoneTwoText` text NOT NULL,
  `emailText` text NOT NULL,
  `abnText` text NOT NULL,
  `phone` text NOT NULL,
  `phoneTwo` text NOT NULL,
  `email` text NOT NULL,
  `abn` text NOT NULL,
  `address` text NOT NULL,
  `newsLetterText` text NOT NULL,
  `newsLetterNamePlaceHolderText` text NOT NULL,
  `newsLetterEmailPlaceHolderText` text NOT NULL,
  `newsLetterPostcodePlaceHolderText` text NOT NULL,
  `newsLetterButtonText` text NOT NULL,
  `helpfulLinksText` text NOT NULL,
  `copyright` text NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `google` text NOT NULL,
  `linkedin` text NOT NULL,
  `youtube` text NOT NULL,
  `skype` text NOT NULL,
  `vimeo` text NOT NULL,
  `instagram` text NOT NULL,
  `pinterest` text NOT NULL,
  `dribbble` text NOT NULL,
  `rss` text NOT NULL,
  `behance` text NOT NULL,
  `github` text NOT NULL,
  `modifiedUserId` int(11) NOT NULL,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`footerId`, `footerLogo`, `footerText`, `contactUsText`, `phoneText`, `phoneTwoText`, `emailText`, `abnText`, `phone`, `phoneTwo`, `email`, `abn`, `address`, `newsLetterText`, `newsLetterNamePlaceHolderText`, `newsLetterEmailPlaceHolderText`, `newsLetterPostcodePlaceHolderText`, `newsLetterButtonText`, `helpfulLinksText`, `copyright`, `facebook`, `twitter`, `google`, `linkedin`, `youtube`, `skype`, `vimeo`, `instagram`, `pinterest`, `dribbble`, `rss`, `behance`, `github`, `modifiedUserId`, `modifiedAt`) VALUES
(1, '1583745077.png', 'Uplifting Women About Section', 'Contact Us', 'TEL:', 'TTY:', 'Email:', 'ABN:', '+923244444202', '+923244444202', 'abc@xyz.com', '+923244444202', '', 'Subscribe to Our Newsletter', 'Name', 'Email', 'Postcode', 'Subscribe', 'Liens utiles', 'All rights reserved © Uplifting Women %YEAR%', '#', '#', '', '', '#', '', '', '', '', '', '', '', '', 6, '2020-03-09 09:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `footer_menus`
--

CREATE TABLE `footer_menus` (
  `footerMenuId` int(11) NOT NULL,
  `footerMenuName` varchar(255) NOT NULL,
  `footerMenuHref` text NOT NULL,
  `footerMenuCategoryId` int(11) NOT NULL,
  `footerMenuSortOrder` int(11) NOT NULL,
  `footerMenuIsDeleted` int(11) NOT NULL,
  `footerMenuIsActive` int(11) NOT NULL,
  `footerMenuCreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuCreatedUserId` int(11) NOT NULL,
  `footerMenuModifiedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuModifiedUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `galleryId` int(11) NOT NULL,
  `galleryImage` varchar(255) NOT NULL,
  `galleryTitle` varchar(255) NOT NULL,
  `gallerySlug` text NOT NULL,
  `gallerySortOrder` int(11) NOT NULL DEFAULT '0',
  `galleryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `galleryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `galleryCreatedUserId` int(11) NOT NULL,
  `galleryModifiedUserId` int(11) NOT NULL,
  `galleryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `galleryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gratitude_journal`
--

CREATE TABLE `gratitude_journal` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `header` text NOT NULL,
  `date` date NOT NULL,
  `message` text NOT NULL,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gratitude_journal`
--

INSERT INTO `gratitude_journal` (`id`, `user_id`, `type`, `header`, `date`, `message`, `updatedAt`) VALUES
(8, 8, 1, 'Things I am grateful for today', '0000-00-00', 'TEST TEST TEST TEST', '2020-04-06 07:38:29'),
(9, 8, 1, 'Things I am grateful for todayttt', '2020-04-14', 'TEST TEST TEST TESTttttt', '2020-04-06 04:23:10'),
(10, 8, 2, 'Things I am grateful for about my body', '2020-04-14', 'TESTING IS 2020-04-14', '2020-04-06 09:03:38'),
(11, 8, 1, 'Things I am grateful for today', '2020-04-15', 'TEST TEST TEST TEST 4 15 2020', '2020-04-06 09:15:34'),
(12, 8, 1, 'Things I am grateful for today', '2020-04-16', 'TEST TEST TEST TEST', '2020-04-06 09:19:41'),
(13, 8, 1, 'Things I am grateful for todayrewre', '2020-04-25', 'TEST TEST TEST TEST 4 25 2020', '2020-04-06 04:19:50'),
(14, 8, 1, 'Things I am grateful for today', '2020-04-10', 'TEST TEST TEST TEST', '2020-04-06 09:23:35'),
(15, 8, 1, 'Things I am grateful for today', '2020-04-09', 'TEST TEST TEST TEST', '2020-04-06 09:24:39'),
(16, 8, 1, 'Things I am grateful for today', '2020-04-12', 'TEST TEST TEST TEST', '2020-04-06 09:24:51'),
(17, 8, 1, 'Things I am grateful for todaytt', '2020-04-22', 'TEST TEST TEST TESTtt', '2020-04-06 04:24:53'),
(18, 8, 1, 'Things I am grateful for today', '2020-04-11', 'TEST TEST TEST TEST', '2020-04-06 09:33:50'),
(19, 8, 1, 'Things I am grateful for today', '2020-04-18', 'TEST TEST TEST TEST', '2020-04-06 09:34:02'),
(20, 8, 1, 'Things I am grateful for todaytest', '2020-04-01', 'test', '2020-04-06 10:24:44'),
(21, 8, 1, 'Things I am grateful for todaytest', '2020-04-02', 'test', '2020-04-06 10:25:31'),
(22, 8, 1, 'Things I am grateful for todayte', '2020-04-03', 'test', '2020-04-06 10:27:16'),
(23, 8, 1, 'Things I am grateful for todaytest', '2020-04-04', 'test', '2020-04-06 10:28:04'),
(24, 8, 2, 'Things I am grateful foetwetwer about my body', '2020-04-16', 'ewtwetwet', '2020-04-06 10:29:48'),
(25, 8, 1, 'Things I am grateful for todaytest', '2020-04-23', 'test', '2020-04-07 01:22:55');

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `headerId` int(11) NOT NULL,
  `headerLogo` text NOT NULL,
  `headerDarkLogo` text NOT NULL,
  `menuButtonText` text NOT NULL,
  `menuButtonCategoryId` int(11) NOT NULL,
  `favicon` text NOT NULL,
  `siteTitle` text NOT NULL,
  `seoTitle` text NOT NULL,
  `seoDescription` text NOT NULL,
  `seoKeywords` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `host` text NOT NULL,
  `port` text NOT NULL,
  `encryption` text NOT NULL,
  `gratitude_web` text NOT NULL,
  `gratitude_mobile` text NOT NULL,
  `positiveaffirmation_web` text NOT NULL,
  `positiveaffirmation_mobile` text NOT NULL,
  `highestgoodofall_web` text NOT NULL,
  `highestgoodofall_mobile` text NOT NULL,
  `login_web` text NOT NULL,
  `login_mobile` text NOT NULL,
  `modifiedUserId` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`headerId`, `headerLogo`, `headerDarkLogo`, `menuButtonText`, `menuButtonCategoryId`, `favicon`, `siteTitle`, `seoTitle`, `seoDescription`, `seoKeywords`, `phone`, `email`, `password`, `host`, `port`, `encryption`, `gratitude_web`, `gratitude_mobile`, `positiveaffirmation_web`, `positiveaffirmation_mobile`, `highestgoodofall_web`, `highestgoodofall_mobile`, `login_web`, `login_mobile`, `modifiedUserId`, `createdAt`, `modifiedAt`) VALUES
(1, '1583745062.png', '1578332676.png', 'DONATE', 171, '1578332676.png', 'Uplifting Women', 'Uplifting Women', 'Uplifting Women', 'Uplifting Women', '03 9422 5470', 'bookmystudio.fr@gmail.com', 's4323399', 'smtp.gmail.com', '587', 'tls', '&lt;p&gt;Some dymmy Gratitude Text&lt;/p&gt;', 'Some dymmy Gratitude Text', '&lt;p&gt;Some dymmy POSTIVE Text&lt;/p&gt;', 'Some dymmy POSTIVE Text', '&lt;p&gt;Some dymmy HIGHEST Text&lt;/p&gt;', 'Some dymmy HIGHEST Text', '&lt;p&gt;Some dymmy LOGIN Text&lt;/p&gt;', 'Some dymmy LOGIN Text', 6, '2018-10-16 19:15:22', '2020-03-09 09:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `header_menus`
--

CREATE TABLE `header_menus` (
  `footerMenuId` int(11) NOT NULL,
  `footerMenuName` varchar(255) NOT NULL,
  `footerMenuHref` text NOT NULL,
  `footerMenuCategoryId` int(11) NOT NULL,
  `footerMenuSortOrder` int(11) NOT NULL,
  `footerMenuIsDeleted` int(11) NOT NULL,
  `footerMenuIsActive` int(11) NOT NULL,
  `footerMenuCreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuCreatedUserId` int(11) NOT NULL,
  `footerMenuModifiedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuModifiedUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header_menus`
--

INSERT INTO `header_menus` (`footerMenuId`, `footerMenuName`, `footerMenuHref`, `footerMenuCategoryId`, `footerMenuSortOrder`, `footerMenuIsDeleted`, `footerMenuIsActive`, `footerMenuCreatedAt`, `footerMenuCreatedUserId`, `footerMenuModifiedAt`, `footerMenuModifiedUserId`) VALUES
(11, 'Calendar', 'calendar', 0, 1, 0, 1, '2020-03-04 16:49:10', 0, '2020-03-04 16:49:10', 0),
(12, 'Self Love/Self Care List', 'self_love', 0, 2, 0, 1, '2020-03-04 16:49:27', 0, '2020-03-04 16:49:27', 0),
(13, 'Postive Affirmation', 'positive_affirmation', 0, 3, 0, 1, '2020-03-04 16:49:44', 0, '2020-03-04 16:49:44', 0),
(14, 'Bucket List', 'bucket_list', 0, 4, 0, 1, '2020-03-04 16:49:58', 0, '2020-03-04 16:49:58', 0),
(15, 'Gratitude Journal', 'gratitude_journal', 0, 5, 0, 1, '2020-03-04 16:50:14', 0, '2020-03-04 16:50:14', 0),
(16, 'Highest Good of all', 'highest_good_of_all', 0, 6, 0, 1, '2020-03-04 16:50:26', 0, '2020-03-04 16:50:26', 0),
(17, 'About Us', '', 174, 0, 0, 1, '2020-03-09 12:40:42', 0, '2020-03-09 12:40:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `healthandbeauty`
--

CREATE TABLE `healthandbeauty` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `healthandbeauty`
--

INSERT INTO `healthandbeauty` (`id`, `user_id`, `message`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(1, 0, 'Item 1', 0, 1, '2020-01-27 21:10:58'),
(2, 0, 'item 2', 1, 2, '2020-01-27 21:11:42'),
(3, 0, 'Item 2', 0, 2, '2020-01-27 22:07:45'),
(4, 0, 'item 2', 1, 0, '2020-01-27 22:10:41'),
(5, 8, 'Item 5', 0, 0, '2020-01-27 22:19:52'),
(6, 8, 'Item 6', 0, 0, '2020-01-27 22:20:27');

-- --------------------------------------------------------

--
-- Table structure for table `highest_good_of_all`
--

CREATE TABLE `highest_good_of_all` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `highest_good_of_all`
--

INSERT INTO `highest_good_of_all` (`id`, `user_id`, `message`, `isDeleted`, `createdAt`) VALUES
(6, 8, 'test', 0, '0000-00-00 00:00:00'),
(7, 8, 'tetss', 0, '0000-00-00 00:00:00'),
(8, 8, 'testat', 0, '0000-00-00 00:00:00'),
(9, 8, 'testatt', 0, '0000-00-00 00:00:00'),
(10, 8, 'testatt4', 0, '0000-00-00 00:00:00'),
(11, 8, 'rherherh', 0, '0000-00-00 00:00:00'),
(12, 8, 'rherherh1', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `home_data`
--

CREATE TABLE `home_data` (
  `homeDataId` int(11) NOT NULL,
  `homeDataBlogBlogOne` int(11) NOT NULL,
  `homeDataBlogBlogTwo` int(11) NOT NULL,
  `homeDataBlogBlogThree` int(11) NOT NULL,
  `homeDataAboutTitle` text NOT NULL,
  `homeDataAboutSubTitle` text NOT NULL,
  `homeDataAboutDescription` text NOT NULL,
  `homeDataAboutCategoryOne` int(11) NOT NULL,
  `homeDataAboutCategoryTwo` int(11) NOT NULL,
  `homeDataAboutCategoryThree` int(11) NOT NULL,
  `homeDataAboutCategoryFour` int(11) NOT NULL,
  `homeDataAboutTwoImage` text NOT NULL,
  `homeDataAboutTwoTitleOne` text NOT NULL,
  `homeDataAboutTwoTitleTwo` text NOT NULL,
  `homeDataAboutTwoButtonText` text NOT NULL,
  `homeDataAboutTwoButtonCategoryId` int(11) NOT NULL,
  `homeDataMediaTitle` text NOT NULL,
  `homeDataMediaSubTitle` text NOT NULL,
  `homeDataMediaDescription` text NOT NULL,
  `homeDataMediaButtonText` text NOT NULL,
  `homeDataMediaButtonCategoryId` int(11) NOT NULL,
  `homeDataInterestedTitle` text NOT NULL,
  `homeDataInterestedCategoryOne` int(11) NOT NULL,
  `homeDataInterestedCategoryTwo` int(11) NOT NULL,
  `homeDataInterestedCategoryThree` int(11) NOT NULL,
  `homeDataTestimonialImage` text NOT NULL,
  `homeDataTestimonialTitle` text NOT NULL,
  `homeDataTestimonialSubTitle` text NOT NULL,
  `homepage_video` varchar(100) NOT NULL,
  `facebook_script` text NOT NULL,
  `home_fourth_section_image` text NOT NULL,
  `home_fourth_section_title` text NOT NULL,
  `home_fourth_section_href` text NOT NULL,
  `homeDataModifiedUserId` int(11) NOT NULL,
  `homeDataModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_data`
--

INSERT INTO `home_data` (`homeDataId`, `homeDataBlogBlogOne`, `homeDataBlogBlogTwo`, `homeDataBlogBlogThree`, `homeDataAboutTitle`, `homeDataAboutSubTitle`, `homeDataAboutDescription`, `homeDataAboutCategoryOne`, `homeDataAboutCategoryTwo`, `homeDataAboutCategoryThree`, `homeDataAboutCategoryFour`, `homeDataAboutTwoImage`, `homeDataAboutTwoTitleOne`, `homeDataAboutTwoTitleTwo`, `homeDataAboutTwoButtonText`, `homeDataAboutTwoButtonCategoryId`, `homeDataMediaTitle`, `homeDataMediaSubTitle`, `homeDataMediaDescription`, `homeDataMediaButtonText`, `homeDataMediaButtonCategoryId`, `homeDataInterestedTitle`, `homeDataInterestedCategoryOne`, `homeDataInterestedCategoryTwo`, `homeDataInterestedCategoryThree`, `homeDataTestimonialImage`, `homeDataTestimonialTitle`, `homeDataTestimonialSubTitle`, `homepage_video`, `facebook_script`, `home_fourth_section_image`, `home_fourth_section_title`, `home_fourth_section_href`, `homeDataModifiedUserId`, `homeDataModifiedAt`) VALUES
(1, 3, 4, 5, '&lt;h1&gt;&lt;strong&gt;What is Uplifting Women?&lt;/strong&gt;&lt;/h1&gt;', 'We support people with disability of all ages and needs in their homes and the community with quality services', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', 174, 174, 174, 174, '1550167686.jpg', 'No limit community service’s free guide on the National Disability Insurance Scheme (NDIS) for your child', 'This easy to follow guide is based on No Limits&#039; experience with the NDIS and working with families and children.', 'Read More', 52, 'News &amp; Media', 'Read the latest news, blogs, statistics', 'Learn about disability from the personal experiences of people with disability, carers and the wider community.', 'READ MORE', 16, 'You Might Be Interested In', 52, 52, 52, '', 'Testimonials', 'What people say about us', '1554579479.mp4', '   <div class=\"fb-page\" data-href=\"https://www.facebook.com/nolimitscommunity/\" data-tabs=\"timeline\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"><blockquote cite=\"https://www.facebook.com/nolimitscommunity/\" class=\"fb-xfbml-parse-ignore\"><a href=\"https://www.facebook.com/nolimitscommunity/\">No Limits Community Services - providing lifelong support . your way</a></blockquote></div>', '1583408329.png', '&lt;h4&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy industry&amp;#39;s standard&lt;/h4&gt;\n\n&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer.&lt;/p&gt;\n\n&lt;h4&gt;&amp;nbsp;&lt;/h4&gt;', '#', 0, '2020-03-09 10:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `home_sliders`
--

CREATE TABLE `home_sliders` (
  `homeSliderId` int(11) NOT NULL,
  `homeSliderTitleOne` text NOT NULL,
  `homeSliderTitleTwo` text NOT NULL,
  `homeSliderButtonText` text NOT NULL,
  `homeSliderButtonCategoryId` int(11) NOT NULL,
  `homeSliderImage` text NOT NULL,
  `homeSliderSortOrder` int(11) NOT NULL DEFAULT '0',
  `homeSliderIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `homeSliderIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `homeSliderCreatedUserId` int(11) NOT NULL,
  `homeSliderModifiedUserId` int(11) NOT NULL,
  `homeSliderCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `homeSliderModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_sliders`
--

INSERT INTO `home_sliders` (`homeSliderId`, `homeSliderTitleOne`, `homeSliderTitleTwo`, `homeSliderButtonText`, `homeSliderButtonCategoryId`, `homeSliderImage`, `homeSliderSortOrder`, `homeSliderIsActive`, `homeSliderIsDeleted`, `homeSliderCreatedUserId`, `homeSliderModifiedUserId`, `homeSliderCreatedAt`, `homeSliderModifiedAt`) VALUES
(3, 'Uplifting Women', '&lt;p&gt;Lorem ipusm dolor sit amet,&lt;br /&gt;\nconsectetur adipiscing elit, sed do&lt;br /&gt;\neiusmod tempor incididunt ut labore et&lt;br /&gt;\ndolor manga alique.Quis ipusm&lt;br /&gt;\nsuspendisse ultrices gravida.Risus&lt;br /&gt;\nCommodo viverra maecenes accumsan&lt;/p&gt;', '', 0, '1583327727.png', 0, 1, 0, 0, 0, '2020-03-04 13:15:27', '2020-03-09 10:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `loginId` int(11) NOT NULL,
  `loginUserId` int(11) NOT NULL,
  `loginSource` varchar(100) NOT NULL,
  `loginDevice` varchar(20) NOT NULL,
  `loginIsLogout` tinyint(4) NOT NULL,
  `loginCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `loginModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`loginId`, `loginUserId`, `loginSource`, `loginDevice`, `loginIsLogout`, `loginCreatedAt`, `loginModifiedAt`) VALUES
(59, 1, 'form', 'web', 0, '2020-01-22 01:44:49', '2020-01-22 01:44:49'),
(61, 1, 'form', 'web', 0, '2020-01-22 01:46:59', '2020-01-22 01:46:59'),
(62, 1, 'form', 'web', 0, '2020-01-22 22:19:12', '2020-01-22 22:19:12'),
(63, 8, '', 'app', 0, '2020-01-23 00:16:43', '2020-01-23 00:16:43'),
(64, 1, 'form', 'web', 0, '2020-01-23 03:40:30', '2020-01-23 03:40:30'),
(65, 1, 'form', 'web', 0, '2020-01-24 10:04:59', '2020-01-24 10:04:59'),
(66, 1, 'form', 'web', 0, '2020-01-24 21:53:20', '2020-01-24 21:53:20'),
(67, 1, 'form', 'web', 0, '2020-01-25 09:07:25', '2020-01-25 09:07:25'),
(68, 1, 'form', 'web', 0, '2020-01-25 21:25:34', '2020-01-25 21:25:34'),
(69, 1, 'form', 'web', 0, '2020-01-27 11:31:22', '2020-01-27 11:31:22'),
(70, 1, 'form', 'web', 0, '2020-01-27 16:08:21', '2020-01-27 16:08:21'),
(71, 1, 'form', 'web', 0, '2020-01-28 13:05:30', '2020-01-28 13:05:30'),
(72, 1, 'form', 'web', 0, '2020-01-28 13:35:00', '2020-01-28 13:35:00'),
(73, 1, 'form', 'web', 0, '2020-01-30 12:02:07', '2020-01-30 12:02:07'),
(74, 1, 'form', 'web', 0, '2020-01-30 18:17:51', '2020-01-30 18:17:51'),
(75, 1, 'form', 'web', 0, '2020-02-03 19:33:10', '2020-02-03 19:33:10'),
(76, 1, 'form', 'web', 0, '2020-02-04 12:19:15', '2020-02-04 12:19:15'),
(77, 1, 'form', 'web', 0, '2020-02-11 10:45:30', '2020-02-11 10:45:30'),
(78, 1, 'form', 'web', 0, '2020-02-11 18:05:20', '2020-02-11 18:05:20'),
(79, 1, 'form', 'web', 0, '2020-02-11 23:06:02', '2020-02-11 23:06:02'),
(80, 1, 'form', 'web', 0, '2020-02-12 15:40:59', '2020-02-12 15:40:59'),
(81, 1, 'form', 'web', 0, '2020-02-17 18:08:56', '2020-02-17 18:08:56'),
(82, 1, 'form', 'web', 0, '2020-02-17 21:26:53', '2020-02-17 21:26:53'),
(83, 1, 'form', 'web', 0, '2020-02-19 12:09:50', '2020-02-19 12:09:50'),
(84, 1, 'form', 'web', 0, '2020-03-04 10:34:18', '2020-03-04 10:34:18'),
(85, 1, 'form', 'web', 0, '2020-03-05 10:42:09', '2020-03-05 10:42:09'),
(86, 1, 'form', 'web', 0, '2020-03-09 07:32:12', '2020-03-09 07:32:12'),
(87, 1, 'form', 'web', 0, '2020-03-10 09:45:12', '2020-03-10 09:45:12'),
(88, 8, '', 'web', 0, '2020-03-10 12:55:19', '2020-03-10 12:55:19'),
(89, 1, '', 'web', 0, '2020-03-10 13:11:23', '2020-03-10 13:11:23'),
(90, 8, '', 'web', 0, '2020-03-10 13:26:56', '2020-03-10 13:26:56'),
(91, 29, '', 'web', 0, '2020-03-10 16:39:07', '2020-03-10 16:39:07'),
(92, 29, '', 'web', 0, '2020-03-10 16:39:21', '2020-03-10 16:39:21'),
(93, 8, '', 'web', 0, '2020-03-10 18:28:17', '2020-03-10 18:28:17'),
(94, 29, '', 'web', 0, '2020-03-10 19:08:46', '2020-03-10 19:08:46'),
(95, 8, '', 'web', 0, '2020-03-10 19:10:57', '2020-03-10 19:10:57'),
(96, 1, 'form', 'web', 0, '2020-03-10 19:11:26', '2020-03-10 19:11:26'),
(97, 1, 'form', 'web', 0, '2020-03-10 19:11:44', '2020-03-10 19:11:44'),
(98, 8, '', 'web', 0, '2020-03-10 19:23:26', '2020-03-10 19:23:26'),
(99, 8, '', 'web', 0, '2020-03-10 19:36:15', '2020-03-10 19:36:15'),
(100, 8, '', 'web', 0, '2020-03-10 20:37:33', '2020-03-10 20:37:33'),
(101, 8, '', 'web', 0, '2020-03-10 20:47:43', '2020-03-10 20:47:43'),
(102, 8, '', 'web', 0, '2020-03-10 21:11:02', '2020-03-10 21:11:02'),
(103, 8, '', 'web', 0, '2020-03-10 21:12:15', '2020-03-10 21:12:15'),
(104, 8, '', 'web', 0, '2020-03-10 21:14:16', '2020-03-10 21:14:16'),
(105, 8, '', 'web', 0, '2020-03-11 13:21:38', '2020-03-11 13:21:38'),
(106, 8, '', 'web', 0, '2020-03-11 16:40:22', '2020-03-11 16:40:22'),
(107, 8, '', 'web', 0, '2020-03-11 22:22:58', '2020-03-11 22:22:58'),
(108, 8, '', 'web', 0, '2020-03-12 17:50:19', '2020-03-12 17:50:19'),
(109, 1, 'form', 'web', 0, '2020-03-12 20:19:18', '2020-03-12 20:19:18'),
(110, 8, '', 'web', 0, '2020-03-13 13:06:51', '2020-03-13 13:06:51'),
(111, 8, '', 'web', 0, '2020-03-13 13:57:03', '2020-03-13 13:57:03'),
(112, 8, '', 'web', 0, '2020-03-15 22:39:47', '2020-03-15 22:39:47'),
(113, 1, 'form', 'web', 0, '2020-03-17 21:01:46', '2020-03-17 21:01:46'),
(114, 8, '', 'web', 0, '2020-03-17 21:16:41', '2020-03-17 21:16:41'),
(115, 1, 'form', 'web', 0, '2020-03-18 10:52:07', '2020-03-18 10:52:07'),
(116, 8, '', 'web', 0, '2020-03-18 11:05:18', '2020-03-18 11:05:18'),
(117, 1, 'form', 'web', 0, '2020-03-18 11:12:44', '2020-03-18 11:12:44'),
(118, 1, 'form', 'web', 0, '2020-03-18 14:40:27', '2020-03-18 14:40:27'),
(119, 8, '', 'web', 0, '2020-03-18 19:47:37', '2020-03-18 19:47:37'),
(120, 8, '', 'web', 0, '2020-03-18 21:00:10', '2020-03-18 21:00:10'),
(121, 29, '', 'web', 0, '2020-03-18 21:23:53', '2020-03-18 21:23:53'),
(122, 8, '', 'web', 0, '2020-03-19 10:48:04', '2020-03-19 10:48:04'),
(123, 8, '', 'web', 0, '2020-03-20 11:25:30', '2020-03-20 11:25:30'),
(124, 8, '', 'web', 0, '2020-03-22 15:54:38', '2020-03-22 15:54:38'),
(125, 8, '', 'web', 0, '2020-03-31 23:53:33', '2020-03-31 23:53:33'),
(126, 1, '', 'web', 0, '2020-04-02 14:17:52', '2020-04-02 14:17:52'),
(127, 8, '', 'web', 0, '2020-04-02 14:18:03', '2020-04-02 14:18:03'),
(128, 8, '', 'web', 0, '2020-04-02 14:18:51', '2020-04-02 14:18:51'),
(129, 8, '', 'web', 0, '2020-04-02 17:41:55', '2020-04-02 17:41:55'),
(130, 8, '', 'web', 0, '2020-04-02 21:00:33', '2020-04-02 21:00:33'),
(131, 8, '', 'web', 0, '2020-04-05 12:58:36', '2020-04-05 12:58:36'),
(132, 1, '', 'web', 0, '2020-04-05 13:25:51', '2020-04-05 13:25:51'),
(133, 8, '', 'web', 0, '2020-04-05 14:38:22', '2020-04-05 14:38:22'),
(134, 8, '', 'web', 0, '2020-04-05 20:04:13', '2020-04-05 20:04:13'),
(135, 8, '', 'web', 0, '2020-04-06 01:11:55', '2020-04-06 01:11:55'),
(136, 8, '', 'web', 0, '2020-04-06 08:04:51', '2020-04-06 08:04:51'),
(137, 8, '', 'web', 0, '2020-04-06 20:14:42', '2020-04-06 20:14:42'),
(138, 8, '', 'web', 0, '2020-04-06 22:52:51', '2020-04-06 22:52:51'),
(139, 8, '', 'web', 0, '2020-04-07 14:13:00', '2020-04-07 14:13:00'),
(140, 1, 'form', 'web', 0, '2020-04-07 19:28:56', '2020-04-07 19:28:56'),
(141, 8, '', 'web', 0, '2020-04-07 19:29:25', '2020-04-07 19:29:25'),
(142, 8, '', 'web', 0, '2020-04-07 22:47:26', '2020-04-07 22:47:26'),
(143, 1, 'form', 'web', 0, '2020-04-07 22:59:17', '2020-04-07 22:59:17'),
(144, 1, 'form', 'web', 0, '2020-04-07 23:18:34', '2020-04-07 23:18:34'),
(145, 8, '', 'web', 0, '2020-04-07 23:18:41', '2020-04-07 23:18:41'),
(146, 1, 'form', 'web', 0, '2020-04-08 03:15:30', '2020-04-08 03:15:30'),
(147, 8, '', 'web', 0, '2020-04-08 03:18:10', '2020-04-08 03:18:10'),
(148, 8, '', 'web', 0, '2020-04-08 08:01:18', '2020-04-08 08:01:18'),
(149, 8, '', 'web', 0, '2020-04-08 08:45:02', '2020-04-08 08:45:02'),
(150, 8, '', 'web', 0, '2020-04-08 15:28:29', '2020-04-08 15:28:29'),
(151, 1, 'form', 'web', 0, '2020-04-08 16:26:52', '2020-04-08 16:26:52'),
(152, 8, '', 'web', 0, '2020-04-08 16:27:29', '2020-04-08 16:27:29'),
(153, 8, '', 'web', 0, '2020-04-08 19:45:14', '2020-04-08 19:45:14'),
(154, 8, '', 'web', 0, '2020-04-08 21:08:45', '2020-04-08 21:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `love`
--

CREATE TABLE `love` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `love`
--

INSERT INTO `love` (`id`, `user_id`, `message`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(1, 0, 'test1', 0, 1, '2020-01-27 23:05:29'),
(2, 0, 'test2', 0, 2, '2020-01-27 23:05:37'),
(3, 8, 'test3', 0, 0, '2020-01-27 23:12:38'),
(4, 0, 'Item 1', 0, 0, '2020-02-11 23:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `mediaId` int(11) NOT NULL,
  `mediaName` text NOT NULL,
  `mediaHref` text NOT NULL,
  `mediaSortOrder` int(11) NOT NULL DEFAULT '0',
  `mediaType` tinyint(4) NOT NULL DEFAULT '1',
  `mediaIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `mediaCreatedUserId` int(11) NOT NULL,
  `mediaModifiedUserId` int(11) NOT NULL,
  `mediaCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mediaModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`mediaId`, `mediaName`, `mediaHref`, `mediaSortOrder`, `mediaType`, `mediaIsDeleted`, `mediaCreatedUserId`, `mediaModifiedUserId`, `mediaCreatedAt`, `mediaModifiedAt`) VALUES
(1, 'dfsdfsf', '1550229924.jpg', 0, 1, 0, 0, 0, '2019-02-15 11:25:25', '2019-02-15 11:25:25'),
(2, 'dasdasdasd', '1550230581.jpg', 0, 1, 0, 0, 0, '2019-02-15 11:36:21', '2019-02-15 11:36:21'),
(3, 'hffhfh', '1550230667.jpg', 0, 1, 1, 0, 0, '2019-02-15 11:37:47', '2019-02-15 11:42:43'),
(4, 'xdsadad', '1550232160.pdf', 0, 2, 0, 0, 0, '2019-02-15 12:02:40', '2019-02-15 12:02:40'),
(5, 'file', '1550232301.png', 0, 1, 0, 0, 0, '2019-02-15 12:05:01', '2019-02-15 12:05:01'),
(6, 'fdssdfsd', '1550243246.docx', 0, 2, 1, 0, 0, '2019-02-15 15:07:26', '2019-02-15 15:24:47'),
(7, 'Download: Support for schools factsheet (PDF, 2,136kb)', '1550308013.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:06:53', '2019-02-16 09:06:53'),
(8, 'Download: Support for schools factsheet (Word, 387kb)', '1550308271.docx', 0, 2, 0, 0, 0, '2019-02-16 09:11:11', '2019-02-16 09:11:11'),
(9, 'Download: Strategic Plan summary (PDF, 73kb)', '1550308315.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:11:55', '2019-02-16 09:11:55'),
(10, 'Download: Professional development for childcare, kindergarten and early childhood educators flyer (PDF, 469kb)', '1550308329.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:12:09', '2019-02-16 09:12:09'),
(11, 'Download: Yooralla Constitution (PDF, 278kb)', '1550308418.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:13:38', '2019-02-16 09:13:38'),
(12, 'Download: Professional development for childcare kindergarten and early childhood educators flyer (Word, 975kb)', '1550308450.docx', 0, 2, 0, 0, 0, '2019-02-16 09:14:10', '2019-02-16 09:14:10'),
(13, 'Download: Yooralla 2017/18 Annual Report (Word, 589kb)', '1550308736.docx', 0, 2, 0, 0, 0, '2019-02-16 09:18:56', '2019-02-16 09:18:56'),
(14, 'Download: Bequest guide (PDF, 892kb)', '1550308750.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:19:10', '2019-02-16 09:19:10'),
(15, 'Download: Bequest guide (Word, 4.2mb)', '1550308812.docx', 0, 2, 0, 0, 0, '2019-02-16 09:20:13', '2019-02-16 09:20:13'),
(16, 'Download: Yooralla 2016/17 Annual Report (PDF, 1.1mb)', '1550308885.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:21:25', '2019-02-16 09:21:25'),
(17, 'Download: Will guide (PDF, 345kb)', '1550308889.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:21:29', '2019-02-16 09:21:29'),
(18, 'Download: Will guide (Word, 1.9mb)', '1550308944.docx', 0, 2, 0, 0, 0, '2019-02-16 09:22:24', '2019-02-16 09:22:24'),
(19, 'Download: Yooralla 2016/17 Annual Report (Word, 1.2mb)', '1550308980.docx', 0, 2, 0, 0, 0, '2019-02-16 09:23:00', '2019-02-16 09:23:00'),
(20, 'Download: Yooralla 2015/16 Annual Report (PDF, 1.97mb)', '1550309042.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:24:05', '2019-02-16 09:24:05'),
(21, 'Download: Book a Speaktank speaker (PDF, 822kb)', '1550309075.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:24:35', '2019-02-16 09:24:35'),
(22, 'Download: Book a Speaktank speaker (Word, 375kb)', '1550309127.docx', 0, 2, 0, 0, 0, '2019-02-16 09:25:27', '2019-02-16 09:25:27'),
(23, 'Download: Yooralla 2014/15 Annual Report (PDF, 997kb)', '1550309127.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:25:28', '2019-02-16 09:25:28'),
(24, 'Download: Yooralla 2014/15 Annual Report (Word, 1.17mb)', '1550309179.docx', 0, 2, 0, 0, 0, '2019-02-16 09:26:19', '2019-02-16 09:26:19'),
(25, 'Download: Yooralla 2013/14 Annual Report (PDF, 4mb)', '1550309249.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:27:32', '2019-02-16 09:27:32'),
(26, 'Download: Yooralla 2013/14 Annual Report (Word, 1.9mb)', '1550309341.docx', 0, 2, 0, 0, 0, '2019-02-16 09:29:01', '2019-02-16 09:29:01'),
(27, 'Download: Yooralla 2012/13 Annual Report (PDF, 1.5mb)', '1550309459.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:30:59', '2019-02-16 09:30:59'),
(28, 'Download: Yooralla 2012/13 Annual Report (Word, 88kb)', '1550309512.docx', 0, 2, 0, 0, 0, '2019-02-16 09:31:52', '2019-02-16 09:31:52'),
(29, 'Download: Yooralla - Board of Directors (PDF, 1.8MB)', '1550309589.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:33:09', '2019-02-16 09:33:09'),
(30, 'Download: Yooralla - Executive Management Team (PDF,2.5MB)', '1550309662.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:34:22', '2019-02-16 09:34:22'),
(31, 'Download: Customer Charter of Rights and Responsibilities (Word, 56kb)', '1550309735.docx', 0, 2, 0, 0, 0, '2019-02-16 09:35:35', '2019-02-16 09:35:35'),
(32, 'Download: Quality and Empowerment Framework (PDF, 1.1mb)', '1550309781.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:36:21', '2019-02-16 09:36:21'),
(33, 'Download: Quality and Empowerment Framework (Word, 3.3mb)', '1550309832.docx', 0, 2, 0, 0, 0, '2019-02-16 09:37:15', '2019-02-16 09:37:15'),
(34, 'Download: Yooralla&#039;s Reconciliation Action Plan', '1550309917.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:38:37', '2019-02-16 09:38:37'),
(35, 'Download: What does equality mean to you - video text (Word, 33KB)', '1550310285.docx', 0, 2, 0, 0, 0, '2019-02-16 09:44:45', '2019-02-16 09:44:45'),
(36, 'Download: Become a Speaktank speaker (PDF, 761kb)', '1550310390.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:46:30', '2019-02-16 09:46:30'),
(37, 'Download: Become a Speaktank speaker (Word, 374kb)', '1550310428.docx', 0, 2, 0, 0, 0, '2019-02-16 09:47:08', '2019-02-16 09:47:08'),
(38, 'Download: Support for schools factsheet (PDF, 2,136kb)', '1550310435.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:47:15', '2019-02-16 09:47:15'),
(39, 'Download: Support for schools factsheet (Word, 387kb)', '1550310466.docx', 0, 2, 0, 0, 0, '2019-02-16 09:47:46', '2019-02-16 09:47:46'),
(40, 'Download: Professional development for childcare, kindergarten and early childhood educators flyer (PDF, 469kb)', '1550310518.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:48:38', '2019-02-16 09:48:38'),
(41, 'Download: VASS Registered Nurse - Position description (PDF, 238kb)', '1550310557.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:49:17', '2019-02-16 09:49:17'),
(42, 'Download: Professional development for childcare kindergarten and early childhood educators flyer (Word, 975kb)', '1550310566.docx', 0, 2, 0, 0, 0, '2019-02-16 09:49:26', '2019-02-16 09:49:26'),
(43, 'Download: VASS Registered Nurse - Job advertisement (PDF, 180kb)', '1550310613.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:50:13', '2019-02-16 09:50:13'),
(44, 'Download: Support Coordinator - Position description', '1550310688.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:51:28', '2019-02-16 09:51:28'),
(45, 'Download: Disability Support Worker - Job advertisement (PDF, 315kb)', '1550310772.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:52:52', '2019-02-16 09:52:52'),
(46, 'Download: Disability Support Worker - Position Description (PDF, 346kb)', '1550310867.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:54:27', '2019-02-16 09:54:27'),
(47, 'Download: Volunteer Registration - Application Form (Word, 66.4kb)', '1550310935.docx', 0, 2, 0, 0, 0, '2019-02-16 09:55:35', '2019-02-16 09:55:35'),
(48, 'Download: NDIS - A guide to accessing the NDIS (PDF, 1.19mb)', '1550311006.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:56:46', '2019-02-16 09:56:46'),
(49, 'Download: NDIS - A guide to accessing the NDIS (Word, 110kb)', '1550311100.docx', 0, 2, 0, 0, 0, '2019-02-16 09:58:20', '2019-02-16 09:58:20'),
(50, 'Download: NDIS - What&#039;s important to you? (PDF, 304kb)', '1550311148.pdf', 0, 2, 0, 0, 0, '2019-02-16 09:59:08', '2019-02-16 09:59:08'),
(51, 'Download: NDIS - What&#039;s important to you (Word, 373kb)', '1550311197.docx', 0, 2, 0, 0, 0, '2019-02-16 09:59:57', '2019-02-16 09:59:57'),
(52, 'Download: Your Yooralla Experience (PDF, 113kb)', '1550311468.pdf', 0, 2, 1, 0, 0, '2019-02-16 10:04:28', '2019-02-16 10:05:09'),
(53, 'Download: Your No Limits Community Services Experience (PDF, 113kb)', '1550311523.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:05:23', '2019-02-16 10:05:23'),
(54, 'Download: Your No Limits Community Services experience_Accessible (Word, 56kb)', '1550311581.docx', 0, 2, 0, 0, 0, '2019-02-16 10:06:21', '2019-02-16 10:06:21'),
(55, 'Download: Statement of Accessibility - No Limits Community Services (PDF, 77kb)', '1550311702.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:08:22', '2019-02-16 10:08:22'),
(56, 'Download: Privacy Policy (Word, 97kb)', '1550311762.docx', 0, 2, 0, 0, 0, '2019-02-16 10:09:22', '2019-02-16 10:09:22'),
(57, 'Download: NDIS Fact Sheet - Vietnamese (Word, 43kb)', '1550311918.docx', 0, 2, 0, 0, 0, '2019-02-16 10:11:58', '2019-02-16 10:11:58'),
(58, 'Download: NDIS Fact Sheet - Vietnamese (PDF, 1.96mb)', '1550312023.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:13:43', '2019-02-16 10:13:43'),
(59, 'Download: NDIS factsheet - Easy English (Word, 11.9mbkb)', '1550312197.docx', 0, 2, 0, 0, 0, '2019-02-16 10:16:47', '2019-02-16 10:16:47'),
(60, 'Download: NDIS - A guide to accessing the NDIS - Easy English (Word, 13.8mb)', '1550312318.docx', 0, 2, 0, 0, 0, '2019-02-16 10:18:52', '2019-02-16 10:18:52'),
(61, 'Download: NDIS Fact Sheet - Turkish (Word, 267kb)', '1550312401.docx', 0, 2, 0, 0, 0, '2019-02-16 10:20:01', '2019-02-16 10:20:01'),
(62, 'Download: NDIS Fact Sheet - Turkish (PDF, 621kb)', '1550312452.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:20:52', '2019-02-16 10:20:52'),
(63, 'Download: NDIS Fact Sheet - Traditional Chinese (Word, 270kb)', '1550312487.docx', 0, 2, 0, 0, 0, '2019-02-16 10:21:27', '2019-02-16 10:21:27'),
(64, 'Download: NDIS Fact Sheet - Traditional Chinese (PDF, 693kb)', '1550312535.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:22:15', '2019-02-16 10:22:15'),
(65, 'Download: NDIS Fact Sheet - Spanish (Word, 186kb)', '1550312579.docx', 0, 2, 0, 0, 0, '2019-02-16 10:22:59', '2019-02-16 10:22:59'),
(66, 'Download: NDIS Fact Sheet - Spanish (PDF, 601kb)', '1550312651.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:24:11', '2019-02-16 10:24:11'),
(67, 'Download: NDIS Fact Sheet - Punjabi (Word, 255kb)', '1550312746.docx', 0, 2, 0, 0, 0, '2019-02-16 10:25:46', '2019-02-16 10:25:46'),
(68, 'Download: What We do - No Limit Community Service Services Flyer (PDF, 1.7MB)', '1550312756.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:25:56', '2019-02-16 10:25:56'),
(69, 'Download: What We Do - No Limit Community Service Services Flyer Accessible (WRD, 358kb)', '1550312800.docx', 0, 2, 0, 0, 0, '2019-02-16 10:26:40', '2019-02-16 10:26:40'),
(70, 'Download: NDIS Fact Sheet - Punjabi (PDF, 2.65,b)', '1550312820.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:27:00', '2019-02-16 10:27:00'),
(71, 'Download: NDIS Fact Sheet - Mandarin (Word, 71kb)', '1550312869.docx', 0, 2, 0, 0, 0, '2019-02-16 10:27:49', '2019-02-16 10:27:49'),
(72, 'Download: Accommodation factsheet (Word, 373.1kb)', '1550312910.docx', 0, 2, 0, 0, 0, '2019-02-16 10:28:30', '2019-02-16 10:28:30'),
(73, 'Download: NDIS Fact Sheet - Mandarin (PDF, 128kb)', '1550312912.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:28:32', '2019-02-16 10:28:32'),
(74, 'Download: Accommodation factsheet (PDF, 344kb)', '1550312942.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:29:02', '2019-02-16 10:29:02'),
(75, 'Download: NDIS Fact Sheet - Macedonian (Word, 270kb)', '1550312978.docx', 0, 2, 0, 0, 0, '2019-02-16 10:29:38', '2019-02-16 10:29:38'),
(76, 'Download: Ventilator Accommodation Support Service flyer (PDF, 475kb)', '1550313012.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:30:12', '2019-02-16 10:30:12'),
(77, 'Download: NDIS Fact Sheet - Macedonian (PDF, 813kb)', '1550313025.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:30:25', '2019-02-16 10:30:25'),
(78, 'Download: Ventilator Accommodation Support Service flyer (Word, 379kb)', '1550313051.docx', 0, 2, 0, 0, 0, '2019-02-16 10:30:51', '2019-02-16 10:30:51'),
(79, 'Download: NDIS Fact Sheet - Korean (Word, 20kb)', '1550313113.docx', 0, 2, 0, 0, 0, '2019-02-16 10:31:53', '2019-02-16 10:31:53'),
(80, 'Download: Assistance with Daily Living factsheet (Word, 380kb)', '1550313124.docx', 0, 2, 0, 0, 0, '2019-02-16 10:32:04', '2019-02-16 10:32:04'),
(81, 'Download: NDIS Fact Sheet - Korean (PDF, 2.00mb)', '1550313163.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:32:43', '2019-02-16 10:32:43'),
(82, 'Download: Assistance With Daily Living factsheet (PDF, 144kb)', '1550313167.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:32:47', '2019-02-16 10:32:47'),
(83, 'Download: NDIS Fact Sheet - Japanese (Word, 18kb)', '1550313199.DOCX', 0, 2, 0, 0, 0, '2019-02-16 10:33:19', '2019-02-16 10:33:19'),
(84, 'Download: NDIS Fact Sheet - Japanese (PDF, 174kb)', '1550313269.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:34:29', '2019-02-16 10:34:29'),
(85, 'Download: Communication &amp; Assistive Technology factsheet (Word, 373kb)', '1550313279.docx', 0, 2, 0, 0, 0, '2019-02-16 10:34:39', '2019-02-16 10:34:39'),
(86, 'Download: Communication &amp; Assistive Technology factsheet (PDF, 260kb)', '1550313313.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:35:13', '2019-02-16 10:35:13'),
(87, 'Download: NDIS Fact Sheet - Italian (Word, 41kb)', '1550313318.docx', 0, 2, 0, 0, 0, '2019-02-16 10:35:18', '2019-02-16 10:35:18'),
(88, 'Download: NDIS Fact Sheet - Italian (PDF, 1.64mb)', '1550313358.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:35:58', '2019-02-16 10:35:58'),
(89, 'Download: Apps for Communication Flyer (PDF, 686KB)', '1550313444.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:37:24', '2019-02-16 10:37:24'),
(90, 'Download: NDIS Fact Sheet - Hindi (Word, 148kb)', '1550313445.docx', 0, 2, 0, 0, 0, '2019-02-16 10:37:25', '2019-02-16 10:37:25'),
(91, 'Download: NDIS Fact Sheet - Hindi (PDF, 3.31mb)', '1550313489.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:38:11', '2019-02-16 10:38:11'),
(92, 'Download: Communication Technology referral form (PDF, 421kb)', '1550313495.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:38:15', '2019-02-16 10:38:15'),
(93, 'Download: Communication Technology referral form (Word, 135kb)', '1550313522.docx', 0, 2, 0, 0, 0, '2019-02-16 10:38:42', '2019-02-16 10:38:42'),
(94, 'Download: NDIS Fact Sheet - Greek (Word, 21kb)', '1550313540.docx', 0, 2, 0, 0, 0, '2019-02-16 10:39:00', '2019-02-16 10:39:00'),
(95, 'Download: ComTEC Schedule of Fees 2018 (PDF, 95.7kb)', '1550313548.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:39:08', '2019-02-16 10:39:08'),
(96, 'Download: NDIS Fact Sheet - Greek (PDF, 174kb)', '1550313575.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:39:35', '2019-02-16 10:39:35'),
(97, 'Download: ComTEC Schedule of Fees 2018 (Word, 119kb)', '1550313576.docx', 0, 2, 0, 0, 0, '2019-02-16 10:39:36', '2019-02-16 10:39:36'),
(98, 'Download: NDIS Fact Sheet - Arabic (Word, 36kb)', '1550313636.docx', 0, 2, 0, 0, 0, '2019-02-16 10:40:36', '2019-02-16 10:40:36'),
(99, 'Download: ECDS and NDIS Applications – FAQs (PDF, 142.5kb', '1550313660.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:41:00', '2019-02-16 10:41:00'),
(100, 'Download: ECDS and NDIS applications - FAQs (Word 1.3Mb)', '1550313689.docx', 0, 2, 0, 0, 0, '2019-02-16 10:41:29', '2019-02-16 10:41:29'),
(101, 'Download: NDIS Fact Sheet - Arabic (PDF, 1.81mb)', '1550313702.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:41:42', '2019-02-16 10:41:42'),
(102, 'Download: Speech Pathology Report Form (294kb)', '1550313792.docx', 0, 2, 0, 0, 0, '2019-02-16 10:43:12', '2019-02-16 10:43:12'),
(103, 'Download: Yooralla 2017/18 Annual Report (PDF, 6MB)', '1550313816.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:43:41', '2019-02-16 10:43:41'),
(104, 'Download: ECDS Occupational Therapy Report Form (Word, 179kb)', '1550313831.docx', 0, 2, 0, 0, 0, '2019-02-16 10:43:51', '2019-02-16 10:43:51'),
(105, 'Download: Trialing Equipment Guidelines (Word, 68kb)', '1550313879.docx', 0, 2, 0, 0, 0, '2019-02-16 10:44:39', '2019-02-16 10:44:39'),
(106, 'Download: VAEP Application Form (Word, 155kb)', '1550313915.doc', 0, 2, 0, 0, 0, '2019-02-16 10:45:15', '2019-02-16 10:45:15'),
(107, 'Download: ILC factsheet (PDF, 172kb)', '1550313990.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:46:30', '2019-02-16 10:46:30'),
(108, 'Download: ILC factsheet (Word, 381kb)', '1550314014.docx', 0, 2, 0, 0, 0, '2019-02-16 10:46:54', '2019-02-16 10:46:54'),
(109, 'Download: Community Hubs factsheet (PDF, 122.5kb)', '1550314085.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:48:05', '2019-02-16 10:48:05'),
(110, 'Download: Community Hubs factsheet (Word, 379.6kb)', '1550314109.docx', 0, 2, 0, 0, 0, '2019-02-16 10:48:29', '2019-02-16 10:48:29'),
(111, 'Download: Therapy factsheet (PDF, 278kb)', '1550314149.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:49:09', '2019-02-16 10:49:09'),
(112, 'Download: Life Skills factsheet (PDF, 165kb)', '1550314168.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:49:28', '2019-02-16 10:49:28'),
(113, 'Download: Therapy factsheet (Word, 369kb)', '1550314186.docx', 0, 2, 0, 0, 0, '2019-02-16 10:49:46', '2019-02-16 10:49:46'),
(114, 'Download: Life Skills factsheet (Word, 379kb)', '1550314193.docx', 0, 2, 0, 0, 0, '2019-02-16 10:49:53', '2019-02-16 10:49:53'),
(115, 'Download: Allied Services and Wellbeing Referral Form (non NDIS) (Word, 58.4kb)', '1550314222.docx', 0, 2, 0, 0, 0, '2019-02-16 10:50:22', '2019-02-16 10:50:22'),
(116, 'Download: Job Skills and Employment Pathways (PDF, 133 KB)', '1550314280.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:51:20', '2019-02-16 10:51:20'),
(117, 'Download: Job Skills and Employment Pathways - Accessible Word (Word, 375 KB)', '1550314310.docx', 0, 2, 0, 0, 0, '2019-02-16 10:51:50', '2019-02-16 10:51:50'),
(118, 'Download: Employability program pathway (PDF, 56kb)', '1550314335.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:52:15', '2019-02-16 10:52:15'),
(119, 'Download: Support Coordination (PDF, 149.5kb)', '1550314337.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:52:17', '2019-02-16 10:52:17'),
(120, 'Download: Employability program pathway (Word, 271kb)', '1550314363.docx', 0, 2, 0, 0, 0, '2019-02-16 10:52:43', '2019-02-16 10:52:43'),
(121, 'Download: School Leavers Brochure - Accessible (Word, 365kb)', '1550314388.docx', 0, 2, 0, 0, 0, '2019-02-16 10:53:08', '2019-02-16 10:53:08'),
(122, 'Download: Support Coordination (Word, 377kb)', '1550314400.docx', 0, 2, 0, 0, 0, '2019-02-16 10:53:20', '2019-02-16 10:53:20'),
(123, 'Download: Support for children with autism flyer (PDF, 266KB)', '1550314593.pdf', 0, 2, 0, 0, 0, '2019-02-16 10:56:33', '2019-02-16 10:56:33'),
(124, 'Download: Support for children with autism flyer (Word, 738Kb)', '1550314689.docx', 0, 2, 0, 0, 0, '2019-02-16 10:58:09', '2019-02-16 10:58:09'),
(125, 'Download: Support for children with autism flyer (PDF, 266KB)', '1550314823.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:00:23', '2019-02-16 11:00:23'),
(126, 'Download: Support for children with autism flyer (Word, 738Kb)', '1550314868.docx', 0, 2, 0, 0, 0, '2019-02-16 11:01:08', '2019-02-16 11:01:08'),
(127, 'Download: Specialist Children&#039;s Supports factsheet (PDF, 528kb)', '1550315224.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:07:04', '2019-02-16 11:07:04'),
(128, 'Download: Specialist Children&#039;s factsheet (Word, 370kb)', '1550315270.docx', 0, 2, 0, 0, 0, '2019-02-16 11:07:50', '2019-02-16 11:07:50'),
(129, 'Download: Employability program pathway (PDF, 56kb)', '1550315351.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:09:11', '2019-02-16 11:09:11'),
(130, 'Download: Employability program pathway (Word, 271kb)', '1550315377.docx', 0, 2, 0, 0, 0, '2019-02-16 11:09:37', '2019-02-16 11:09:37'),
(131, 'Download: Inclusion support equipment catalogue (PDF, 2.4mb)', '1550315407.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:10:07', '2019-02-16 11:10:07'),
(132, 'Download: Employability - Preparation for Work factsheet (PDF, 586kb)', '1550315407.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:10:07', '2019-02-16 11:10:07'),
(133, 'Download: Employability - Preparation for Work factsheet (Word, 375kb)', '1550315435.docx', 0, 2, 0, 0, 0, '2019-02-16 11:10:35', '2019-02-16 11:10:35'),
(134, 'Download: Employability - Job Ready factsheet (Word, 48kb)', '1550315581.docx', 0, 2, 0, 0, 0, '2019-02-16 11:13:01', '2019-02-16 11:13:01'),
(135, 'Download: Inclusion support equipment catalogue (Word, 6.2mb)', '1550315652.docx', 0, 2, 0, 0, 0, '2019-02-16 11:14:16', '2019-02-16 11:14:16'),
(136, 'Download: Employability - Preparation for Work factsheet (PDF, 586kb)', '1550315691.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:14:51', '2019-02-16 11:14:51'),
(137, 'Download: Specialist Equipment Library - Item Request Form (Word, 79kb)', '1550315704.docx', 0, 2, 0, 0, 0, '2019-02-16 11:15:04', '2019-02-16 11:15:04'),
(138, 'Download: Employability - Preparation for Work factsheet (Word, 375kb)', '1550315716.docx', 0, 2, 0, 0, 0, '2019-02-16 11:15:16', '2019-02-16 11:15:16'),
(139, 'Download: Employability program pathway (PDF, 56kb)', '1550315742.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:15:42', '2019-02-16 11:15:42'),
(140, 'Download: Employability program pathway (Word, 271kb)', '1550315766.docx', 0, 2, 0, 0, 0, '2019-02-16 11:16:06', '2019-02-16 11:16:06'),
(141, 'Download: Finishing school - What&#039;s next? factsheet (PDF, 1.58mb)', '1550315797.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:16:37', '2019-02-16 11:16:37'),
(142, 'Download: Finishing school - What&#039;s next? factsheet (Word, 381kb)', '1550315820.docx', 0, 2, 0, 0, 0, '2019-02-16 11:17:00', '2019-02-16 11:17:00'),
(143, 'Download: Positive Behaviour Support factsheet (PDF, 579kb)', '1550315835.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:17:15', '2019-02-16 11:17:15'),
(144, 'Download: Positive Behaviour Support factsheet (Word, 368kb)', '1550315890.docx', 0, 2, 0, 0, 0, '2019-02-16 11:18:10', '2019-02-16 11:18:10'),
(145, 'Download: Employability - Job Ready factsheet (PDF, 358kb)', '1550315904.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:18:24', '2019-02-16 11:18:24'),
(146, 'Download: Employability - Job Ready factsheet (Word, 48kb)', '1550315932.docx', 0, 2, 0, 0, 0, '2019-02-16 11:18:52', '2019-02-16 11:18:52'),
(147, 'Download: Recreation factsheet (PDF, 824kb)', '1550315950.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:19:10', '2019-02-16 11:19:10'),
(148, 'Download: Employability program pathway (PDF, 56kb)', '1550315959.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:19:19', '2019-02-16 11:19:19'),
(149, 'Download: Employability program pathway (Word, 271kb)', '1550315981.docx', 0, 2, 0, 0, 0, '2019-02-16 11:19:41', '2019-02-16 11:19:41'),
(150, 'Download: Recreation factsheet (Word, 45kb)', '1550315990.docx', 0, 2, 0, 0, 0, '2019-02-16 11:19:50', '2019-02-16 11:19:50'),
(151, 'Download: Finishing school - What&#039;s next? factsheet (PDF, 1.58mb)', '1550316008.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:20:08', '2019-02-16 11:20:08'),
(152, 'Download: Finishing school - What&#039;s next? factsheet (Word, 381kb)', '1550316034.docx', 0, 2, 0, 0, 0, '2019-02-16 11:20:34', '2019-02-16 11:20:34'),
(153, 'Download: Supported employment factsheet (PDF, 1.2MB)', '1550316113.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:21:53', '2019-02-16 11:21:53'),
(154, 'Download: Supported employment factsheet (Word, 370kb)', '1550316137.docx', 0, 2, 0, 0, 0, '2019-02-16 11:22:17', '2019-02-16 11:22:17'),
(155, 'Download: No Limits Community Services  Catering - Full Menu (PDF, 1.6mb)', '1550316231.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:23:51', '2019-02-16 11:23:51'),
(156, 'Download: No Limits Community Services  Catering - Full Menu (Word, 1.5mb)', '1550316257.docx', 0, 2, 0, 0, 0, '2019-02-16 11:24:17', '2019-02-16 11:24:17'),
(157, 'Download: Employability program pathway (PDF, 56kb)', '1550316338.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:25:38', '2019-02-16 11:25:38'),
(158, 'Download: Employability program pathway (Word, 271kb)', '1550316360.docx', 0, 2, 0, 0, 0, '2019-02-16 11:26:00', '2019-02-16 11:26:00'),
(159, 'Download: Employability - Preparation for Work factsheet (PDF, 586kb)', '1550316388.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:26:28', '2019-02-16 11:26:28'),
(160, 'Download: Respite accommodation - for children and young people (PDF, 119.9KB)', '1550316399.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:26:39', '2019-02-16 11:26:39'),
(161, 'Download: Employability - Preparation for Work factsheet (Word, 375kb)', '1550316412.docx', 0, 2, 0, 0, 0, '2019-02-16 11:26:52', '2019-02-16 11:26:52'),
(162, 'Download: Supported employment factsheet (PDF, 1.2MB)', '1550316435.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:27:15', '2019-02-16 11:27:15'),
(163, 'Download: Respite accommodation for children and young people - accessible (Word, 347.6mb)', '1550316437.docx', 0, 2, 0, 0, 0, '2019-02-16 11:27:17', '2019-02-16 11:27:17'),
(164, 'Download: Supported employment factsheet (Word, 370kb)', '1550316457.docx', 0, 2, 0, 0, 0, '2019-02-16 11:27:37', '2019-02-16 11:27:37'),
(165, 'Download: Respite factsheet (PDF, 691kb)', '1550316599.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:29:59', '2019-02-16 11:29:59'),
(166, 'Download: Respite factsheet (Word, 382kb)', '1550316623.docx', 0, 2, 0, 0, 0, '2019-02-16 11:30:23', '2019-02-16 11:30:23'),
(167, 'Download: Respite Coordination Referral Form (PDF, 380kb)', '1550316644.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:30:44', '2019-02-16 11:30:44'),
(168, 'Download: Respite Coordination Referral Form (Word, 176kb)', '1550316666.doc', 0, 2, 0, 0, 0, '2019-02-16 11:31:06', '2019-02-16 11:31:06'),
(169, 'Download: Breakaway Flexible Respite Guidelines (PDF, 240kb)', '1550316732.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:32:12', '2019-02-16 11:32:12'),
(170, 'Download: Breakaway Flexible Respite Guidelines (Word, 512kb)', '1550316754.docx', 0, 2, 0, 0, 0, '2019-02-16 11:32:34', '2019-02-16 11:32:34'),
(171, 'Download: Breakaway Flexible Respite application form (PDF, 608kb)', '1550316777.pdf', 0, 2, 0, 0, 0, '2019-02-16 11:32:57', '2019-02-16 11:32:57'),
(172, 'Download: Breakaway Flexible Respite application form (Word, 370kb)', '1550316798.docx', 0, 2, 0, 0, 0, '2019-02-16 11:33:18', '2019-02-16 11:33:18'),
(173, 'join us', '1553768153.pdf', 0, 2, 0, 0, 0, '2019-03-28 10:15:53', '2019-03-28 10:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `menuId` int(11) NOT NULL,
  `menuName` varchar(255) NOT NULL,
  `menuType` tinyint(4) NOT NULL DEFAULT '1',
  `menuCategoryId` int(11) NOT NULL,
  `menuHref` varchar(255) NOT NULL,
  `menuIcon` text NOT NULL,
  `menuIconName` text NOT NULL,
  `menuParentId` int(11) NOT NULL,
  `menuSortOrder` int(11) NOT NULL DEFAULT '0',
  `menuIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `menuIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `menuCreatedUserId` int(11) NOT NULL,
  `menuModifiedUserId` int(11) NOT NULL,
  `menuCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `menuModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`menuId`, `menuName`, `menuType`, `menuCategoryId`, `menuHref`, `menuIcon`, `menuIconName`, `menuParentId`, `menuSortOrder`, `menuIsActive`, `menuIsDeleted`, `menuCreatedUserId`, `menuModifiedUserId`, `menuCreatedAt`, `menuModifiedAt`) VALUES
(1, 'Services', 1, 35, '', '1550078543.png', 'Services Icon.png', 0, 2, 1, 0, 0, 0, '2019-02-06 17:28:24', '2019-02-14 15:51:25'),
(2, 'What we do', 1, 79, '', '1550051807.png', '', 1, 1, 1, 0, 0, 0, '2019-02-06 17:29:03', '2019-02-15 12:53:06'),
(3, 'Accommodation', 1, 41, '', '1550048476.png', '', 1, 2, 1, 0, 0, 0, '2019-02-06 17:29:19', '2019-02-15 12:53:06'),
(4, 'Assistance with Daily Living', 1, 45, '', '1550048476.png', '', 1, 3, 1, 0, 0, 0, '2019-02-06 17:29:28', '2019-02-16 06:01:24'),
(5, 'Communication &amp; Assistive Technology', 1, 48, '', '1550048476.png', '', 1, 4, 1, 0, 0, 0, '2019-02-06 17:29:44', '2019-02-16 06:02:06'),
(6, 'Community Hubs &amp; Supports', 1, 38, '', '1550048476.png', '', 1, 5, 1, 0, 0, 0, '2019-02-06 17:29:55', '2019-02-16 06:02:45'),
(7, 'Job Skills &amp; Employment Pathways', 1, 42, '', '1550048476.png', '', 1, 6, 1, 0, 0, 0, '2019-02-06 17:30:10', '2019-02-16 06:03:05'),
(8, 'Life Skills', 1, 46, '', '1550048476.png', '', 1, 7, 1, 0, 0, 0, '2019-02-06 17:30:21', '2019-02-16 06:03:21'),
(9, 'NDIS', 1, 51, '', '1550078563.png', 'NDIS Icon.png', 0, 3, 1, 0, 0, 0, '2019-02-06 17:30:31', '2019-02-14 15:51:36'),
(10, 'I am a person with disability', 1, 52, '', '1550048476.png', '', 9, 0, 1, 0, 0, 0, '2019-02-06 17:30:44', '2019-02-16 06:06:41'),
(11, 'I am a carer of an adult with disability', 1, 54, '', '1550048476.png', '', 9, 0, 1, 0, 0, 0, '2019-02-06 17:31:06', '2019-02-16 06:07:09'),
(12, 'News', 1, 58, '', '1550078582.png', 'News Icon.png', 0, 4, 1, 0, 0, 0, '2019-02-06 17:36:17', '2019-02-14 18:32:34'),
(13, 'News', 1, 108, '', '1550048476.png', '', 12, 0, 1, 0, 0, 0, '2019-02-06 17:36:32', '2019-02-16 06:08:36'),
(14, 'Blog', 1, 123, '', '1550048476.png', '', 12, 0, 1, 0, 0, 0, '2019-02-06 17:37:45', '2019-02-16 06:09:27'),
(15, 'Get involved', 1, 64, '', '1550078598.png', 'Get Involved Icon.png', 0, 5, 1, 0, 0, 0, '2019-02-06 17:38:37', '2019-02-14 18:33:07'),
(16, 'Events', 1, 66, '', '1550048476.png', '', 15, 2, 1, 0, 0, 0, '2019-02-06 17:38:49', '2019-02-16 06:10:24'),
(17, 'About us', 1, 69, '', '1550054561.png', 'About us Icon.png', 0, 6, 1, 0, 0, 0, '2019-02-06 17:38:59', '2019-02-14 18:33:35'),
(18, 'Vision &amp; mission', 1, 70, '', '1550048476.png', '', 17, 0, 1, 0, 0, 0, '2019-02-06 17:39:16', '2019-02-16 06:12:31'),
(19, 'News', 1, 5, '', '1550048476.png', '', 1, 0, 1, 1, 0, 0, '2019-02-12 16:46:19', '2019-02-15 12:48:21'),
(20, 'test', 1, 4, '', '1550048476.png', '', 0, 0, 0, 1, 0, 0, '2019-02-13 09:01:16', '2019-02-13 09:58:46'),
(21, 'I need...', 1, 98, '', '1550078520.png', 'I Need Icon.png', 0, 1, 1, 0, 0, 0, '2019-02-13 10:20:42', '2019-02-14 15:33:02'),
(22, 'Jobs', 1, 67, '', '1550054570.png', 'Jobs Icon.png', 0, 7, 1, 0, 0, 0, '2019-02-13 10:29:23', '2019-02-14 18:33:42'),
(23, 'Contact', 1, 35, '', '1550054592.png', 'Contact Icon.png', 0, 8, 1, 0, 0, 0, '2019-02-13 10:39:07', '2019-02-14 18:33:58'),
(24, 'Support as a professional working with people with disability', 1, 144, '', '1550234580.png', 'person', 21, 0, 1, 0, 0, 0, '2019-02-15 12:43:00', '2019-02-15 12:43:04'),
(25, 'Support as a carer of people with disability', 1, 146, '', '1550234615.png', 'heart', 21, 0, 1, 0, 0, 0, '2019-02-15 12:43:35', '2019-02-15 12:43:42'),
(26, 'Support as a person with disability', 1, 145, '', '1550234726.png', 'people', 21, 0, 1, 0, 0, 0, '2019-02-15 12:45:26', '2019-02-15 12:45:31'),
(27, 'Positive Behaviour Support', 1, 49, '', '1550234893.png', 'pluse', 1, 8, 1, 0, 0, 0, '2019-02-15 12:48:13', '2019-02-15 12:53:06'),
(28, 'Recreation', 1, 39, '', '1550234942.png', 'boats', 1, 9, 1, 0, 0, 0, '2019-02-15 12:49:02', '2019-02-15 12:53:06'),
(29, 'Respite', 1, 43, '', '1550234988.png', 'watch', 1, 10, 1, 0, 0, 0, '2019-02-15 12:49:48', '2019-02-16 06:04:19'),
(30, 'Specialist Children&#039;s Supports', 1, 47, '', '1550235032.png', 'kite', 1, 11, 1, 0, 0, 0, '2019-02-15 12:50:32', '2019-02-15 12:53:06'),
(31, 'Support for children with autism', 1, 50, '', '1550235074.png', 'hand shake', 1, 12, 1, 0, 0, 0, '2019-02-15 12:51:14', '2019-02-15 12:53:06'),
(32, 'Support Coordination', 1, 40, '', '1550235123.png', '360', 1, 13, 1, 0, 0, 0, '2019-02-15 12:52:03', '2019-02-15 12:53:06'),
(33, 'Therapy', 1, 44, '', '1550235171.png', 'Therapy', 1, 14, 1, 0, 0, 0, '2019-02-15 12:52:51', '2019-02-16 06:04:32'),
(34, 'I am a carer of a child with developmental delay or disability', 1, 56, '', '1550235258.png', 'people', 9, 0, 1, 0, 0, 0, '2019-02-15 12:54:18', '2019-02-15 12:54:25'),
(35, 'Confused about the NDIS? Free, one-on-one NDIS sessions', 1, 57, '', '1550235305.png', 'calender', 9, 0, 1, 0, 0, 0, '2019-02-15 12:55:05', '2019-02-15 12:55:10'),
(36, 'NDIS guides', 1, 53, '', '1550235385.png', 'book', 9, 0, 1, 0, 0, 0, '2019-02-15 12:56:25', '2019-02-15 12:56:30'),
(37, 'Meet our Community Engagement Team', 1, 55, '', '', 'hand shake', 9, 0, 1, 0, 0, 0, '2019-02-15 12:57:19', '2019-02-15 12:57:24'),
(38, 'Media releases', 1, 125, '', '1550235539.png', 'no icon', 12, 0, 1, 0, 0, 0, '2019-02-15 12:58:59', '2019-02-15 12:59:05'),
(39, 'Research', 1, 126, '', '1550235577.png', 'no icon', 12, 0, 1, 0, 0, 0, '2019-02-15 12:59:37', '2019-02-15 12:59:42'),
(40, 'Infographics', 1, 127, '', '1550235614.png', 'no icon', 12, 0, 1, 0, 0, 0, '2019-02-15 13:00:14', '2019-02-15 13:00:19'),
(41, 'Donate', 1, 65, '', '1550235708.png', 'Donate', 15, 1, 1, 0, 0, 0, '2019-02-15 13:01:48', '2019-02-15 13:10:39'),
(42, 'Jobs', 1, 67, '', '1550236183.png', 'hand shake', 15, 3, 1, 0, 0, 0, '2019-02-15 13:09:43', '2019-02-15 13:10:39'),
(43, 'Volunteering', 1, 68, '', '1550236231.png', 'hand shake', 15, 4, 1, 0, 0, 0, '2019-02-15 13:10:31', '2019-02-15 13:10:39'),
(44, 'Acknowledgement of Country', 1, 71, '', '1550236372.png', 'world on hand', 17, 0, 1, 0, 0, 0, '2019-02-15 13:12:52', '2019-02-16 06:12:46'),
(45, 'Celebrating 100 years', 1, 72, '', '1550236428.png', 'history', 17, 0, 1, 0, 0, 0, '2019-02-15 13:13:48', '2019-02-15 13:13:53'),
(46, 'Annual reports', 1, 73, '', '1550236472.png', 'notes', 17, 0, 1, 0, 0, 0, '2019-02-15 13:14:32', '2019-02-15 13:14:37'),
(47, 'Board of Directors', 1, 74, '', '1550236526.png', 'people connected', 17, 0, 1, 0, 0, 0, '2019-02-15 13:15:26', '2019-02-15 13:15:34'),
(48, 'Executive Management Team', 1, 75, '', '1550236582.png', 'team', 17, 0, 1, 0, 0, 0, '2019-02-15 13:16:22', '2019-02-15 13:16:26'),
(49, 'Customer wellbeing &amp; safeguards', 1, 76, '', '1550236625.png', 'person lock', 17, 0, 1, 0, 0, 0, '2019-02-15 13:17:05', '2019-02-15 13:17:10'),
(50, 'Yooralla&#039;s Reconciliation Action Plan', 1, 77, '', '1550236664.png', 'world on hand', 17, 0, 1, 0, 0, 0, '2019-02-15 13:17:44', '2019-02-15 13:17:49'),
(51, 'Career', 1, 0, '', '', '', 0, 0, 0, 1, 0, 0, '2019-03-21 17:57:48', '2019-03-21 17:57:54'),
(52, 'Career', 1, 173, '', '', '', 22, 0, 0, 0, 0, 0, '2019-03-21 17:58:23', '2019-03-21 17:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `pageCategoryId` int(11) NOT NULL,
  `pageTitle` varchar(255) NOT NULL,
  `pageContent` text NOT NULL,
  `pageIsLeftNav` tinyint(4) NOT NULL DEFAULT '0',
  `pageLeftNavType` tinyint(4) NOT NULL DEFAULT '1',
  `pageLeftNavContent` text NOT NULL,
  `pageIsRightNav` tinyint(4) NOT NULL DEFAULT '0',
  `pageRightNavType` tinyint(4) NOT NULL DEFAULT '1',
  `pageRightNavContent` text NOT NULL,
  `pagePageTitle` text NOT NULL,
  `pageSeoTitle` text NOT NULL,
  `pageSeoDescription` text NOT NULL,
  `pageSeoKeywords` text NOT NULL,
  `pageIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `pageIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `pageCreatedUserId` int(11) NOT NULL,
  `pageModifiedUserId` int(11) NOT NULL,
  `pageCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page_sections`
--

CREATE TABLE `page_sections` (
  `pageSectionId` int(11) NOT NULL,
  `pageSectionTitle` varchar(255) NOT NULL,
  `pageSectionCategoryId` int(11) NOT NULL,
  `pageSectionSortOrder` int(11) NOT NULL DEFAULT '0',
  `pageSectionIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionCreatedUserId` int(11) NOT NULL,
  `pageSectionModifiedUserId` int(11) NOT NULL,
  `pageSectionCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageSectionModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_sections`
--

INSERT INTO `page_sections` (`pageSectionId`, `pageSectionTitle`, `pageSectionCategoryId`, `pageSectionSortOrder`, `pageSectionIsActive`, `pageSectionIsDeleted`, `pageSectionCreatedUserId`, `pageSectionModifiedUserId`, `pageSectionCreatedAt`, `pageSectionModifiedAt`) VALUES
(1, 'Sections1', 34, 0, 0, 1, 0, 0, '2019-02-13 14:51:12', '2019-02-13 14:54:07'),
(2, 'daasdasda', 34, 2, 0, 0, 0, 0, '2019-02-13 14:54:13', '2019-02-13 14:56:48'),
(3, 'sadasdasdasdsd554', 34, 1, 0, 1, 0, 0, '2019-02-13 14:54:17', '2019-02-13 16:16:16'),
(4, 'Support as a professional working with people with disability', 98, 0, 0, 1, 0, 0, '2019-02-15 07:56:09', '2019-02-15 08:13:18'),
(5, 'Support as a professional working with people with disability', 98, 0, 0, 0, 0, 0, '2019-02-15 08:13:55', '2019-02-15 08:13:55'),
(6, 'Support as a person with disability', 98, 0, 0, 0, 0, 0, '2019-02-15 08:16:26', '2019-02-15 08:16:26'),
(7, 'Support as a carer of people with disability', 98, 0, 0, 0, 0, 0, '2019-02-15 08:20:16', '2019-02-15 08:20:16'),
(8, 'You might be interested in', 98, 0, 0, 1, 0, 0, '2019-02-15 08:21:40', '2019-02-15 08:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `page_section_categories`
--

CREATE TABLE `page_section_categories` (
  `pageSectionCategoryId` int(11) NOT NULL,
  `pageSectionCategoryPageSectionId` int(11) NOT NULL,
  `pageSectionCategoryCategoryId` int(11) NOT NULL,
  `pageSectionCategorySortOrder` int(11) NOT NULL DEFAULT '0',
  `pageSectionCategoryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionCategoryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionCategoryCreatedUserId` int(11) NOT NULL,
  `pageSectionCategoryModifiedUserId` int(11) NOT NULL,
  `pageSectionCategoryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageSectionCategoryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_section_categories`
--

INSERT INTO `page_section_categories` (`pageSectionCategoryId`, `pageSectionCategoryPageSectionId`, `pageSectionCategoryCategoryId`, `pageSectionCategorySortOrder`, `pageSectionCategoryIsActive`, `pageSectionCategoryIsDeleted`, `pageSectionCategoryCreatedUserId`, `pageSectionCategoryModifiedUserId`, `pageSectionCategoryCreatedAt`, `pageSectionCategoryModifiedAt`) VALUES
(1, 3, 5, 2, 0, 0, 0, 0, '2019-02-13 15:31:07', '2019-02-13 16:15:01'),
(2, 3, 6, 1, 0, 0, 0, 0, '2019-02-13 15:31:55', '2019-02-13 16:15:01'),
(3, 2, 5, 0, 0, 1, 0, 0, '2019-02-13 16:16:43', '2019-02-13 16:19:23'),
(4, 2, 5, 0, 0, 0, 0, 0, '2019-02-13 16:24:41', '2019-02-13 16:24:41'),
(5, 4, 86, 0, 0, 0, 0, 0, '2019-02-15 07:57:51', '2019-02-15 07:57:51'),
(6, 5, 142, 0, 0, 0, 0, 0, '2019-02-15 08:14:42', '2019-02-15 08:14:42'),
(7, 5, 141, 0, 0, 0, 0, 0, '2019-02-15 08:15:25', '2019-02-15 08:15:25'),
(8, 5, 86, 0, 0, 0, 0, 0, '2019-02-15 08:15:46', '2019-02-15 08:15:46'),
(9, 6, 41, 0, 0, 0, 0, 0, '2019-02-15 08:16:46', '2019-02-15 08:16:46'),
(10, 6, 45, 0, 0, 0, 0, 0, '2019-02-15 08:17:48', '2019-02-15 08:17:48'),
(11, 6, 48, 0, 0, 0, 0, 0, '2019-02-15 08:17:58', '2019-02-15 08:17:58'),
(12, 6, 38, 0, 0, 0, 0, 0, '2019-02-15 08:18:33', '2019-02-15 08:18:33'),
(13, 6, 42, 0, 0, 0, 0, 0, '2019-02-15 08:18:42', '2019-02-15 08:18:42'),
(14, 6, 46, 0, 0, 0, 0, 0, '2019-02-15 08:18:54', '2019-02-15 08:18:54'),
(15, 6, 49, 0, 0, 0, 0, 0, '2019-02-15 08:19:05', '2019-02-15 08:19:05'),
(16, 6, 39, 0, 0, 0, 0, 0, '2019-02-15 08:19:13', '2019-02-15 08:19:13'),
(17, 6, 40, 0, 0, 0, 0, 0, '2019-02-15 08:19:53', '2019-02-15 08:19:53'),
(18, 6, 44, 0, 0, 0, 0, 0, '2019-02-15 08:19:59', '2019-02-15 08:19:59'),
(19, 7, 43, 0, 0, 0, 0, 0, '2019-02-15 08:20:52', '2019-02-15 08:20:52'),
(20, 7, 143, 0, 0, 0, 0, 0, '2019-02-15 08:21:07', '2019-02-15 08:21:07'),
(21, 7, 47, 0, 0, 0, 0, 0, '2019-02-15 08:21:21', '2019-02-15 08:21:21'),
(22, 8, 129, 0, 0, 1, 0, 0, '2019-02-15 08:21:53', '2019-02-15 08:21:58'),
(23, 8, 52, 0, 0, 1, 0, 0, '2019-02-15 08:22:00', '2019-02-15 08:22:07'),
(24, 8, 136, 0, 0, 0, 0, 0, '2019-02-15 08:22:20', '2019-02-15 08:22:20'),
(25, 8, 108, 0, 0, 0, 0, 0, '2019-02-15 08:22:31', '2019-02-15 08:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `personalaffirmation`
--

CREATE TABLE `personalaffirmation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personalaffirmation`
--

INSERT INTO `personalaffirmation` (`id`, `user_id`, `message`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(1, 0, 'personal affirmation 3', 0, 1, '2020-01-27 23:32:36'),
(2, 0, 'personal affirmation 2', 0, 2, '2020-01-27 23:32:41'),
(3, 8, 'test5', 0, 0, '2020-01-27 23:42:49'),
(4, 8, 'test', 0, 0, '2020-04-07 03:57:59'),
(5, 8, 'test123123', 0, 0, '2020-04-07 03:58:07'),
(6, 8, 'testrewr435', 0, 0, '2020-04-07 03:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `positiveAffirmationDashboard`
--

CREATE TABLE `positiveAffirmationDashboard` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `web_image` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `popupMessage` text,
  `popupMessageMobile` text NOT NULL,
  `seo_title` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positiveAffirmationDashboard`
--

INSERT INTO `positiveAffirmationDashboard` (`id`, `name`, `web_image`, `image`, `popupMessage`, `popupMessageMobile`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(2, 'Abundance', '', '1579994304.jpeg', NULL, '', 'test', 'test', 'tests'),
(3, 'Health &amp; Beauty', '', '1579994390.jpeg', NULL, '', 'test', 'test', 'test'),
(4, 'Love', '', '1579994438.jpeg', NULL, '', 'test', 'test', 'test'),
(5, 'My Personal Affirmation', '', '1579994457.jpeg', NULL, '', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `positive_affirmations_list`
--

CREATE TABLE `positive_affirmations_list` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positive_affirmations_list`
--

INSERT INTO `positive_affirmations_list` (`id`, `type`, `user_id`, `message`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(8, 1, 0, 'test Abundance Item 1', 1, 2, '2020-04-08 04:04:20'),
(9, 1, 0, 'test Abundance Item 1', 0, 2, '2020-04-08 04:09:20'),
(10, 1, 0, 'test Abundance Item 2', 0, 1, '2020-04-08 04:10:15'),
(11, 2, 0, 'test Health * Beauty Item 1', 0, 1, '2020-04-08 04:13:36'),
(12, 2, 0, 'test Health * Beauty Item 22', 1, 1, '2020-04-08 04:13:42'),
(13, 2, 0, 'test Health * Beauty Item 2', 0, 2, '2020-04-08 04:14:10'),
(14, 3, 0, 'test Love Item 1', 0, 2, '2020-04-08 04:16:29'),
(15, 3, 0, 'test love item 2', 1, 1, '2020-04-08 04:16:35'),
(16, 3, 0, 'test love item 2', 0, 0, '2020-04-08 04:16:48'),
(17, 1, 8, 'testing health and beauty with custom item', 0, 0, '2020-04-08 04:37:28'),
(18, 1, 8, 'testing Custom Item on Abundance', 0, 0, '2020-04-08 04:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `positive_affirmations_user_list`
--

CREATE TABLE `positive_affirmations_user_list` (
  `id` int(11) NOT NULL,
  `positive_affirmations_list_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positive_affirmations_user_list`
--

INSERT INTO `positive_affirmations_user_list` (`id`, `positive_affirmations_list_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(3, 10, 8, 0, '2020-04-08 04:31:27'),
(8, 9, 8, 0, '2020-04-08 21:42:55'),
(9, 16, 8, 0, '2020-04-08 21:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `selflovecare`
--

CREATE TABLE `selflovecare` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `month_id` int(11) NOT NULL DEFAULT '0',
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `sortOrder` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selflovecare`
--

INSERT INTO `selflovecare` (`id`, `user_id`, `message`, `month_id`, `isDeleted`, `sortOrder`, `createdAt`) VALUES
(1, 0, 'Item 1', 3, 0, 1, '2020-02-11 23:35:05'),
(2, 0, 'Item 2', 3, 0, 2, '2020-02-11 23:35:11'),
(3, 0, 'Item 3', 3, 0, 3, '2020-02-11 23:35:16'),
(4, 8, 'Go To The Gym', 1, 0, 0, '2020-02-12 01:00:16'),
(5, 8, 'My Cusstom Message', 1, 0, 0, '2020-03-12 03:21:05'),
(6, 8, 'Custom MEssage YAS!', 3, 0, 0, '2020-03-12 03:22:37'),
(7, 8, 'testing with daud', 1, 0, 0, '2020-03-19 15:48:46'),
(8, 1, 'testing januart', 4, 0, 0, '2020-04-05 19:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `selflovecare_dashboard`
--

CREATE TABLE `selflovecare_dashboard` (
  `month` int(11) NOT NULL,
  `imageWeb` varchar(100) NOT NULL,
  `imageMobile` varchar(100) NOT NULL,
  `web_header` text NOT NULL,
  `mobile_header` text NOT NULL,
  `web_message` text NOT NULL,
  `mobile_message` text NOT NULL,
  `seo_title` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selflovecare_dashboard`
--

INSERT INTO `selflovecare_dashboard` (`month`, `imageWeb`, `imageMobile`, `web_header`, `mobile_header`, `web_message`, `mobile_message`, `seo_title`, `seo_keywords`, `seo_description`, `updatedAt`) VALUES
(1, '1581977065.png', '1581977065.png', '<p>January is a month</p>', 'January is a month', '<p><strong>January is a month some </strong>moneth message TEST</p>', 'January is a month some moneth message TEST', 'test', 'test', 'test', '2020-02-18 02:20:04'),
(2, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(3, '1583868175.png', '1583868175.png', '<p>March</p>\r\n\r\n<p>being health month</p>', 'March\r\n\r\nbeing health month', '<p>I am healthy</p>', 'I am healthy', 'I am healthy', 'I am healthy', 'I am healthy', '2020-02-18 02:20:04'),
(4, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(5, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:20:04'),
(6, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:20:51'),
(7, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:20:51'),
(8, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:35:08'),
(9, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:35:08'),
(10, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:20:51'),
(11, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:21:09'),
(12, '', '', 'test', 'test', 'test', 'test', '', '', '', '2020-02-18 02:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `subscriberId` int(11) NOT NULL,
  `subscriberEmail` varchar(255) NOT NULL,
  `subscriberName` varchar(255) NOT NULL,
  `subscriberPostcode` varchar(255) NOT NULL,
  `subscriberIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `subscriberIsFake` tinyint(4) NOT NULL DEFAULT '0',
  `subscriberCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscriberModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subscriberId`, `subscriberEmail`, `subscriberName`, `subscriberPostcode`, `subscriberIsDeleted`, `subscriberIsFake`, `subscriberCreatedAt`, `subscriberModifiedAt`) VALUES
(1, 'shajeelriaz65@gmail.com', 'shajeel', '54000', 0, 0, '2019-03-21 17:34:33', '2019-03-21 17:34:33'),
(2, 'shajeelriaz@gmail.com', 'shajeel', '45000', 0, 0, '2019-03-21 17:39:47', '2019-03-21 17:39:47'),
(3, 'nabeelzafar@hsra.com', 'Nabeel Zafar', '123456', 0, 0, '2020-03-17 21:04:35', '2020-03-17 21:04:35'),
(4, 'nabeelzafar1@hsra.com', 'Nabeel Zafar', '123456', 0, 0, '2020-03-17 21:08:04', '2020-03-17 21:08:04');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonialId` int(11) NOT NULL,
  `testimonialName` text NOT NULL,
  `testimonialOccupation` text NOT NULL,
  `testimonialImage` text NOT NULL,
  `testimonialComment` text NOT NULL,
  `testimonialSortOrder` int(11) NOT NULL DEFAULT '0',
  `testimonialIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `testimonialIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `testimonialCreatedUserId` int(11) NOT NULL,
  `testimonialModifiedUserId` int(11) NOT NULL,
  `testimonialCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `testimonialModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonialId`, `testimonialName`, `testimonialOccupation`, `testimonialImage`, `testimonialComment`, `testimonialSortOrder`, `testimonialIsActive`, `testimonialIsDeleted`, `testimonialCreatedUserId`, `testimonialModifiedUserId`, `testimonialCreatedAt`, `testimonialModifiedAt`) VALUES
(1, 'Shajeel ur Reman', 'Developer', '1550088240.jpg', 'No limits community services is a registered Fundraiser (under the Fundraising Act 1998) with Consumer Affairs Victoria. Registration number 12304.16.', 0, 1, 1, 0, 0, '2019-02-13 20:04:00', '2020-03-05 11:15:24'),
(2, 'Lorem Ipsum', 'Developer', '1550167755.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cum dolorum fuga incidunt.', 2, 1, 0, 0, 0, '2019-02-13 20:04:28', '2020-03-05 11:49:52'),
(3, 'Lorem Ipsum', '', '1583406566.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 0, 0, 1, 0, 0, '2020-03-05 11:09:26', '2020-03-05 11:09:38'),
(4, 'Anna Doe', 'test', '1583408989.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 1, 1, 0, 0, 0, '2020-03-05 11:49:49', '2020-03-05 11:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `thirdsection`
--

CREATE TABLE `thirdsection` (
  `testimonialId` int(11) NOT NULL,
  `testimonialName` text NOT NULL,
  `testimonialImage` text NOT NULL,
  `testimonialComment` text NOT NULL,
  `testimonialSortOrder` int(11) NOT NULL DEFAULT '0',
  `testimonialIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `testimonialCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `testimonialModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `testimonialIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `testimonialOccupation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thirdsection`
--

INSERT INTO `thirdsection` (`testimonialId`, `testimonialName`, `testimonialImage`, `testimonialComment`, `testimonialSortOrder`, `testimonialIsActive`, `testimonialCreatedAt`, `testimonialModifiedAt`, `testimonialIsDeleted`, `testimonialOccupation`) VALUES
(1, 'Lorem Ipsum', '1583406792.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 4, 1, '2020-03-05 11:13:12', '2020-03-05 11:15:25', 0, '#'),
(2, 'Lorem Ipsum', '1583406813.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 1, 1, '2020-03-05 11:13:33', '2020-03-05 11:15:48', 0, '#'),
(3, 'Lorem Ipsum', '1583406847.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 2, 1, '2020-03-05 11:14:07', '2020-03-05 11:15:24', 0, '#'),
(4, 'Lorem Ipsum', '1583406869.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 5, 1, '2020-03-05 11:14:29', '2020-03-05 11:15:25', 0, '#'),
(5, 'Lorem Ipsum', '1583406909.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 3, 1, '2020-03-05 11:15:09', '2020-03-05 11:15:25', 0, '#');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `image` longtext NOT NULL,
  `typeId` tinyint(4) NOT NULL DEFAULT '3',
  `isActive` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `resetToken` varchar(255) NOT NULL,
  `activationToken` text NOT NULL,
  `createdUserId` int(11) NOT NULL,
  `fcm-tokenid` text NOT NULL,
  `google_event_access_token` longtext NOT NULL,
  `google_event_refresh_token` longtext NOT NULL,
  `google_event_active` int(11) NOT NULL DEFAULT '0',
  `expiry_google_event_token` text NOT NULL,
  `modifiedUserId` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `email`, `password`, `firstName`, `lastName`, `phone`, `gender`, `image`, `typeId`, `isActive`, `isDeleted`, `resetToken`, `activationToken`, `createdUserId`, `fcm-tokenid`, `google_event_access_token`, `google_event_refresh_token`, `google_event_active`, `expiry_google_event_token`, `modifiedUserId`, `createdAt`, `modifiedAt`) VALUES
(1, 'admin@gmail.com', 'fdcd9fc08a3e3aa80221f26f704c8563ac358f4edfe0a4ca1973b30a5e2adbbe371f748583a57f83aad51bd2bd290896a78ae17d19ffffceba92c7b37de9a3a3--1tUEBCyaiKakKWeSRycgQcxuhdmDKpAwpQivZ9po3Vc--2', '', '', '', 0, '', 1, 1, 0, '', '', 0, '', '', '', 0, '', 0, '2019-01-24 13:13:22', '2020-02-17 23:01:49');
INSERT INTO `users` (`userId`, `email`, `password`, `firstName`, `lastName`, `phone`, `gender`, `image`, `typeId`, `isActive`, `isDeleted`, `resetToken`, `activationToken`, `createdUserId`, `fcm-tokenid`, `google_event_access_token`, `google_event_refresh_token`, `google_event_active`, `expiry_google_event_token`, `modifiedUserId`, `createdAt`, `modifiedAt`) VALUES
(8, 'nabeelzafar.hsra@gmail.com', 'd5eabfae3b9f7c0aef723321d06635d5f6b7a5867b87f6c1c88b432ad17ce5fd2adf15d69baf87871febaa83e4663ed544d48f3c0850cfb21cd1118354ba5fa79ZKQT3iN--3ity6fvaiM--18lOZj2RjE76--1lfWQmbuQjAuA--2', 'Nabeel', 'Zafar', '', 0, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAOcBQADASIAAhEBAxEB/8QAHgABAAEEAwEBAAAAAAAAAAAAAAkEBwgKAgMGAQX/xABvEAABAwIDBAUGBQcVCgwFAwUAAQIDBAUGBxEICRIhExQxQZFRUmF2gbQiMjc4cRVCdKGxstIWFxgZIzM1NldicnN1gpKUlaKzwdE0R4WTo7XC09ThJCU5Q1NWWGR3g5akREZUY4QmJ+IoZYbE4//EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EACIRAQEBAAIDAQEBAQADAAAAAAABERIhAjFRQWEigRMycf/aAAwDAQACEQMRAD8AlTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+OVGornKiIiaqq9wH0HmFzRyzRdFzFwwip3fVen/DKu145wVe6hKSzYwslfOvZHS3CKVy+xrlUD9wAAAAAAAAAAAAAAPyLzi/CeHJo6fEOKLTa5ZW8cbK2tigc9uumqI9yapr3gfrg8v8AjpZZfqi4Y/len/DH46WWX6ouGP5Xp/wwPUA8v+Olll+qLhj+V6f8M+pmjlmq6JmLhhVX/wDu9P8AhgenBSW672q7xdYtNzpK2Lz6eZsjfFqqVYAAAAAAAAAAAAAAB5ufMvLmlnkpqnH+G4ZoXrHJHJdYGuY5F0VqortUVF5aHX+Olll+qLhj+V6f8MD1APL/AI6WWX6ouGP5Xp/wx+Olll+qLhj+V6f8MD1APOU2ZGXlZKkFHjzDs8juxkV0gc5fYjj0MckczGyRSNexyatc1dUVPQoHIAAAAAAAAAAAAAAAAFu81tobJHJCk61mrmbYcOq5vFHTVNUi1UqfrIGayv8A3rVLD1W9a2KKaVYkzHuU+i6ccVhrFav0axoBl4DD38ti2Kf+v94/9P1f4BVW7eqbE9wqG07szq2k4lROkqbFWtYn0qkagZcA8hlzm9ldm9aUvmWOP7HiajVEVz7dWsldEvkkYi8Ubv1rkRfQevAAAAAAAAAApbndLZZaKS53i40tBRw6dJUVMzYo2aqiJxOcqImqqic+9UPwfx0ssv1RcMfyvT/hgeoB5ymzIy8rZ2UtHjzDtRNIujI4rpA9zl8iIjtVPRgAAAAAAAAADhLLFBG6aaRscbEVznOVERqJ2qqr2AcweZfmflrG9Y5MwsMsc1dFa67U6Ki+lOM4/jpZZfqi4Y/len/DA9QDooq6iuVJFX26rhqqadqPimhkR8cjV7HNcnJU9KHeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADxuc+I24Pyfxzi17+BLLhu53BXa6adDSyP/wBE9kYw7yzF8+DtizMappJejqLpTUtoYuumramqijlT2xOkA19nvdI9z3Lq5yqqr6TspKyroKiOroaqannicjo5Ynqx7HJ2KipzRTqAE4W6n2qMWZ+5V3rBGYt0numJcCTQRNuVQ/imraCZruhWR3a6Rjo3tVy83JwKuq6qZzEK+5fxm2ybSeIsH1E3BFiXC0yxN1+PUU88UjU/xbp1JqAAAAAAAAAAAAEPe+6+V7Ln1bqPenEwhD3vuvley59W6j3pwEbYAAAAD0eCcx8f5bXmDEOX+NL1h6407uKOottbJA9PQvCqcSL3tXVF7FQk12Lt7VcLreLflntRzUidcelPRYuhjSFGyKujWVkbU4URVXTpWI1E5cTdNXpFOANqZj2yNR7HI5rk1RUXVFTynIj/AN0TtM3LNXKi5ZM4wubKm95fNhS2ve782ntEmqMRdV+F0L0VnF3NfCnbzWQAAAAAAAAAAAANZLPz5dcxvW27++SnhD3efny65jett398lPCAAAARytVHNVUVOxULy5HbYG0Ns9V8c+XGY1yit7Xo+Wz10jqq3Tc9VR0D1VGqvYrmcLtPrizQA2AdibbzwLtcWaWz1FJHh3Htrh6W4WRZVeyaJFRFqKZ683x6qiK1fhMVURdUVHLlOavmWuYuLMpMd2XMfA1yWhvlgq2VdJNpq1XJ2sen1zHNVWub3tcqd5sl5IZq2TO/KXCua+HnsWjxJbo6tY2O4ugm+LNCq+dHK18a+lige4AAAAAAAAAAAjX3h+8vrssbvX5GbPdxg/FLSqsF9xE1Gyttr++mpk1Vqzp2Pc5FRi6tROPVWZcbaeeU+zxs24xzJtdXFT3uGlbQWRZER2tfUOSOJyNXk5WcTpVavJUjXXka5tZWVdxrJ7hX1MlRU1UjppppXK58kjlVXOcq81VVVVVQKi9329Ymu1VfsRXesulyrZHTVNXWTummmkcuque9yqrlVe9VKEH6+E8HYsx5fKfDOCcM3S/XarXSChttI+onk8ujGIq6J3r3AfkAv3NsFbY0FGtc/Z6xcsbWcaoyma+TT9rRyvVfRpqWRvNkvOHLpU2PENprbXcqKRYamjrIHwzwyJ2tex6I5qp5FQD9PAuYGNsscS0eMcv8UXGwXmgej4KyhndE9PK1dOTmqnJWrq1UVUVFRSaXd77w6j2loGZW5oJTW7MagplkhmiTggvkTE+HIxqJpHK1Obo05Kmrm6IitbB0ft4HxniLLrGFmx1hK4y0F4sVbFX0VRG5UVksbkcmuna1dNFTsVFVF5KBtGg8Lkbmla87MoMJZq2iPooMS2qCtdDxarBMrdJYlXvVkiPZr+tPdAAAAAAFi9ufDMeLtkPNezSM4uHDdTXMTTXWSm0qGfzommuWbQGaWHFxhlni3CbWcbrzY66ga3yrLA9ifbcav4H1rnMcj2OVrkXVFRdFRSdzdQZvYhzW2U6ekxVc5q+4YNvNTh5lRO9XyyUzIoZoeJyrqvC2fo0XyRoncQREuW4/vizYEzSw0r9Uo7tb65G69nTQyMVfb0CeAEmwAAAAAAABHBvp81cRYUyxwJlrYrzUUNPjCvram5sp5FYtRT0jYeGN6ovNivqGu4V5KrE8hI+Q577TELqzOnAOGUfqy14blquHXsfPUuRftQt8AI4wD93AVl/FJjnDuHeDi+qt2pKLh8vSzNZp/OA2S9nzCUmA8h8usFzMVs1kwtaqGZFTReljpY2vVfSrkVS4BwiYkcTI07GtRvghzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgHvnsWxWbZjsOFmTaVOIcV0ydHr8aCCCeR6+x/Q+Jn4RK78LEazYqyqwk2VdKO33O4uYi/8ATSQxtVU/8h2ntAjBAAGSm7ixImGNtLLGsdL0bay4z21V17esU0sKJ7XPRDYUNYXJ/GEuXubOC8ewqvFhzEFvuuid6QVDJFT2o3T2mzxFIyWNksbkcx7Uc1U7FRexQOQAAAAAAAAAAEPe+6+V7Ln1bqPenEwhD3vuvley59W6j3pwEbZeTY4wvh7Gu1FlphTFlnpbrZ7pf4KetoqqNHxTxLrq1zV7UUs2X42EPnh5S+stN/WBN/8AkHdkL/s64G/kqM8Fmpux9kTMmyVNDbctqfCFzfE5lNc7DI+ndA/T4Llh4uikRF01RW6qnLVO0yuAGsrnzk3iTIDNvEmUeK3Nkr8P1XRJUMarWVMD2o+GZqL2I+N7Had2unah4Ezn3x9NBBtdU0sUTWuqMI26WVUTm93TVLdV9PC1qexDBgDMzdI36qs+2bZKGnkc2O9Wa50E6IvxmJD06Iv76Bi+wncIDt1V89zBP2LdfcJifEAAAAAAAAAAANZLPz5dcxvW27++SnhD3efny65jett398lPCASV7ofIfJvOSwZk1GamW1hxTLa6y2so33OkbMsDXsmV6N17NVa3X6EJCn7DWyDIxWO2dcEIjk0XhtjEXxTmhhjuO/0tZsfZ1q/o6glBAjL2491rlZQ5ZXvNTZ3s9Th+84cpZblWWOOeSopq+mjarpUiSRznRytajnNRqq12nDwoqoqRFm07do45rVWRSsa9j6eRrmuTVFRWrqimrPVMbHVTMYmjWyORE9GoHUTe7nC/1V22Taq1VEjnR2TFNdSQIq/FY+OGZUT0cUrl9qkIRNRuWfmx4m9dKr3OkAkAAAAAAAAAAAEc2+1uNTFkZgG1MeqQVOK3TyN15K6OjmRv2pXEOBMLvuvkfy59ZZ/dXEPQBEVV0ROZsT7F2ytg/ZfygtFkobNAmLLnSQ1WI7m9iLUT1bmo58XHpqkUaqrWsTly1XVyqq681iRFvdvRU1RaqL79DaaAGEm9I2WsM5uZD3rNm0WSmixtgKkdc2V0beCSqt0fwqmCRU+OjY0dIzXVUVmiaI92ubZa/al+bPmx6k3v3KUDWnAAE9G6gudTcdirCkVQ5XdRuF0pY1VfrEq5HJ4censMwDDPdI/MusP7s3X3hTMwAAAAAAGsXnZhmPBeceOsIQs4IrLiO5UMSaafAjqZGt+0iGzoa6+37hx2FdsjNa1OZw9JfXV7U0+tqoo6hPtSoBj+SNbkvF77fnbj3Aznq2K9YZjuWmvJ0lJVMY1Pp4at6+xSOUzI3St8W0baGH6Lj4UvFoulCqedpTrNp/kdfYBPAAAAMZdtDbpwBsfWegpK61SYkxfeo3S26xwVDYeGFFVOsTyKirHFxNVqaNVXKioiaI5UjHxNvedsO+Vss9pu2F8PwOcqx09DZWSIxvcnFOsjl+nUCdIEfe612ws5NpW8Zh2HOXE1Nd6iy01tq7WsVBBSrGx7p2zoqRNbxaqkOmvZz8pIIAIJt7jiZt92zLza2ycSYeslrtyoi/FV0PWf/wDZQnZNc3brxG/FO1/mvdnv4+HEc9E1f1lMjadv82JALEl5djPCk2NNq7Kewws40XFlvrJW6a6w00zaiX+ZE4s0ZhbpyyJdttfCtarEclntt2reaa6a0ckOv+WAnmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIL973jpMWbYFXh6N35ng6wW+0KidiySNdWOX6dKtqfvSdA1y9unEP4qNsDNq59Jx9Fiert6LrrypVSn09nQ6AWLAABrla5HNXRUXVFNnHI7E7Ma5LYCxhG9Hpe8M2y4aouvOWljeqfTq5TWONgTdlYqmxXsV5ey1M3STWuKstTl17GwVUrY09kfAnsAykAAAAAAAAAAAh733XyvZc+rdR704mEIe9918r2XPq3Ue9OAjbL8bCHzw8pfWWm/rLDl+NhD54eUvrLTf1gbGAAAhD3yvztqD1Nt/vFUYJmdm+V+dtQeptv94qjBMDLfdVfPcwT9i3X3CYnxIDt1V89zBP2LdfcJifEAAAAAAAAAAANZLPz5dcxvW27++SnhD3efny65jett398lPCASz7jv9LWbH2dav6OoJQSL7cd/pazY+zrV/R1BKCBTXL9Dqr9of8Aeqas1b/dk/7a/wC6ptM3L9Dqr9of96pqzVv92T/tr/uqB0k1G5Z+bHib10qvc6QhXJqNyz82PE3rpVe50gEgAAAAAAAAAAAjc33XyP5c+ss/uriHomF33XyP5c+ss/uriHoCusP6OW77Lh+/Q2mjVlsP6OW77Lh+/Q2mgBa/al+bPmx6k3v3KUugWv2pfmz5sepN79ylA1pwABO5ukfmXWH92br7wpmYYZ7pH5l1h/dm6+8KZmAAAAAAAgz3wGF22La/qLzGzRMR4dt1e92na9iPpl/m07CcwiC33mHlpczctMUozRtysdbQ8WnatPOx6/aqUAjUL47DuL34H2usp7616sSTE1JbZHa9kdYq0r1X0cM66ljj0GXd6dhvMDDGImv4Ftd5oq1HeRYp2P1/mgbRAOEMjZoY5m9j2o5PoVDrraplDRVFbL8SnifK76Goqr9wDXl3geZNRmfte5kXd87pKa0XaTD9Iiu1RkVF/wAHXh9DnxyP/fqY8H7eOb5LifG2IMSzycct2utXXPd5zpZnPVfFx+IBnhubMZw4f2qK/C9TLwtxThispoG6/GqIZIp0/wAnHMTcGutsCYlXCe2PlTdUk4OlvzLcq691VG+mVPak2hsUgfFNYXN7EjcY5r4zxax/Ey84guFexf1stQ96fachsoZs4ldg3KzGOLmOVr7LYa+4MVF0Xiip3vT7bUNYIASEblbC09y2jMW4rWPWmsuEZadXafFmqKqDg/mRTEe5LPuOrG2HDWbWJVjTiq660ULXqndFHUvVE/xzftASggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFzlRyJpyA5A6lma12jnnLVyc1VNF7AOYOGrlTVAir3qhNTXMHDj9IR/nKg1XMHFXppyXmfOJ6dqDRzBwR+q6aHMoA+I5q958V2nYmoHIHxF8vIKundqB9B8Q+K9EXQDkDrRzvOQ5I5O9QOQAAAAAAAAAAAACku1yp7Paq271a6QUNPJUyr5GMarl+0hq54kv1wxViK6Ynu0nSV14rZ6+pf50ssivev8JymyDtXYtgwNsz5o4onmSJaTCdzbC7XT83kp3xwp7ZHsT2mtaAAPY5o4BqMvLvZrZUse1brhuzX5nF3traKKoTT0fmmnsA8cTQ7lrGkV42dMUYLkm4qnDmKZZkZr8WnqaeJzPGSOcheJNNx/iNKfHuaOElk0WvtFvuKM17erzSRqvs6yniBLoAAAAAAAAAABD3vuvley59W6j3pxMIQ977r5XsufVuo96cBG2X42EPnh5S+stN/WWHL8bCHzw8pfWWm/rA2MAABCHvlfnbUHqbb/eKowTM7N8r87ag9Tbf7xVGCYGW+6q+e5gn7FuvuExPiQHbqr57mCfsW6+4TE+IAAAAAAAAAAAayWfny65jett398lPCHu8/Pl1zG9bbv75KeEAln3Hf6Ws2Ps61f0dQSgkX247/S1mx9nWr+jqCUECmuX6HVX7Q/71TVmrf7sn/bX/AHVNpm5fodVftD/vVNWat/uyf9tf91QOkmo3LPzY8TeulV7nSEK5NRuWfmx4m9dKr3OkAkAAAAAAAAAAAEbm+6+R/Ln1ln91cQ9Ewu+6+R/Ln1ln91cQ9AV1h/Ry3fZcP36G00asth/Ry3fZcP36G00ALX7UvzZ82PUm9+5Sl0C1+1L82fNj1JvfuUoGtOAAJ3N0j8y6w/uzdfeFMzDDPdI/MusP7s3X3hTMwAAAAAAEb2+3wpHXZPZe40RiLLZ8Rz27XvRlVTK9fZrSs+0SQmFe93w+68bG90ubWa/UK+2yucvkR0q0/wB2dAIKx2AAbNuQ2LFx5khl/jZ7+N99wxa7jIq9vHLSxvci+lHKqKfm7TmL/wAQWzrmXjBkvRzWzCtzmp3a6fm/VnpEntkVqe0t7u6b87EOxdlfVvfxuprXJQKvogqJYkT2IxE9h5/ejYjTD2xTjpiTcD7q+325ia6K7jq4lVE/etd7EUCAYAAfuYFxVW4Exvh/G9t163h660l1g0XT80glbI37bUNoWgrKe40NPcKSRHwVUTJonJ9cxyIqL4Karxsr7LmKYsa7N+WOKIp0lWvwpbHyu110lbTsbI1fSj2uRfSgHm9ujETMLbIWa92e/gV2G6mjYuv19RpA3+dKhrmE7e9vxMth2Mr5a2ycK4hvNrtuid6NnSpX3YgkAE1+5iwzLadl+94gni4XX3FtVJE7z4YqeCNF/hpKnsIUDYK3bVlhwvsS5ZwPjSN1VSVde9dOb1nrZ5UVf3r2p9CIBk8DxWaGbuX2TGG5MZ5nYsobDZmObGk1Qq6ue7sRrU1c5fQiKpW4BzIwTmthamxllziigv1nq0Xoqukk4mqqdrVTta5O9FRFJsHqAUjauVWo/ol0Xlp3odjZ1VFXtT7hR3g+NXVNT6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOuTmqN8p2HTMkqubwacPeveB18TnycDYk4U+vXuKSrv1iti8Nyv8Ab6ZfJNUMYv21I8d7FtUZ3ZK3DC2AssLxVYbor3STVdVdqVESaZWu4Vha9UXh0RUVdOfNCJPEOM8XYtr5LpijE90utXKvE+asq3yvVfpcqk3Rs9MxJh+REWLEFtc1exUqWL/Wdn1Yszvi3iiX/wA9v9pq5R3a6xIiRXOrYidiNmcn9Z3txLiNnxL/AHJunkqpE/rHY2iUuVtVOVzpF/8ANb/aEuFAvZdKZfokb/aav8eN8aQ/nWLr0zTza+VP9IqWZk5iR6KzHmIW6dmlzm/CHY2eUrKNOf1Qg/hocuvUbl+DXw/w0NY+PN/NeHTosysUN07NLtP+EVMeeGcsS6x5qYrb9F3n/CJ2NmnrFO7/AOKjX6HIckmi00Soj0+lDWdi2g89IF4oc38XsX0Xif8ACKlm0rtBx/Ezoxkn+GJ/wi4NlhXRP7Zo/YpyR0f1sjPE1rGbUW0ZH8TO3Gaf4Xm/CO5m1ftKxrqzPLGaf4Wl/tHY2TeSrqsjfE5JqifBc3Q1tmbXW09Gurc9sZe26SL91Spj2ytqiL4mfWMU/wAIuUdjY/0ev/Op7BwP/wCk+0a5Ee2ztZQ/nefuL0+muVfuoVDNuna9jTRm0Di5P/y0X+oo2MdOSc08D45FTmiJy7tDXYbt87YzNOHaCxTy8ssa/dYdzN4LtmRqit2gMSLouvNIF+7GTsbETHq5ObVT6QqLxa68iJHZS3wGILVWU+EdqCnW526REZHiWgp0Sphd3dPCzRr2+VzERU81xKfhDGeFMe4bpMXYJxBSXu0V7Ekp6qjmSSN7V8ip2L5UXmgl32PQgpY6hXI9WuR/CumidqfSdsSq53FryVOzyFHaAAAAAAADDnezYkWxbFuJbc2VWOv10tduTRdFciVTJ1TwgX2EDpL9vuccJb8r8uMuWO0fe75VXd+nmUcCRoi+hVrEX976CIEDspoJKqoipYU1kme2NieVVXRDOPez5aU2X2amWb7dD0dHNl7brW3lprJRPki/o1hT2GJ+RmGnYzzrwBhBrFf9W8UWu3qieSWqjYvs0cpJJvwsPqtuymxQyH4EU11t7nonxeJtO9qe3hd4KBFIZm7pHG8mE9siz2fi0hxbZrjZpefLlGlU3+dStT2mGRezYmxOzCG1tlNeZJUjjdimhopHquiNZUyJTuVV8mkq6gbHgAAAAAAAAAAEPe+6+V7Ln1bqPenEwhD3vuvley59W6j3pwEbZfjYQ+eHlL6y039ZYcvxsIfPDyl9Zab+sDYwAAEIe+V+dtQeptv94qjBMzo3yE8U21zSxxvRzocIW6OREX4rumqXaL7HIvtMFwMt91V89zBP2LdfcJifEgO3VXz3ME/Yt19wmJ8QAAAAAAAAAAA1ks/Pl1zG9bbv75KeEPd5+fLrmN623f3yU8IBLPuO/wBLWbH2dav6OoJQSL7cd/pazY+zrV/R1BKCBTXL9Dqr9of96pqzVv8Adk/7a/7qm0xdHNZbKt73IjWwSKqr3Jwqas1W5r6qZ7F1a6RyovlTUDqJqNyz82PE3rpVe50hCuTUbln5seJvXSq9zpAJAAAAAAAAAAABG5vuvkfy59ZZ/dXEPRMLvuvkfy59ZZ/dXEPQFdYf0ct32XD9+htNGrLYf0ct32XD9+htNAC1+1L82fNj1JvfuUpdAtftS/NnzY9Sb37lKBrTgACdzdI/MusP7s3X3hTMwwz3SPzLrD+7N194UzMAAAAAABYXbxwozGmx9mvZXsR/R4emuTEXvfSObVN9vFAhfo8nm1h9cV5V4xwu1nG672C4ULW+VZKd7E+2oGsGAAJxNzvi12IdkZbE9+rsMYnuFua1V7GSNiqkX6OKpf4KeQ31+LvqbkNgrBcUvDJfMUdceiL8aKlppUVPo46iNfYh5ncf3102Cc0sMq/VKS626va3XsWWGVir7ehTwPEb7/E8c+N8r8GMk+HRWqvukrNexJpmRsX/ANu/wAjHAAAnk3TuKXYi2LsM2+WZZJMP3K52xVVdVROsvnansbOiJ6EQgbJfdyNjZLhljmPl5I/V9kvdJdmIvmVcDo9E9COo1X996QP0N9niBKPJPAeGkfo+54lkqeHysgpnoq+xZm+JDkSb78HFPWcb5WYKa/8AQ61XG6Pai9vWZoo2qv8AFXafSpGQANk7ZXw47C+zVlXh6qi4JaPCNq6eNU+LK6lY56exzlNcHDtolxBiC2WGDXpLlWQ0jNO3ikejU+6bSFHQ0tBSQUVHC2KGnjbFExOxrWpoiJ9CIL/BAXvJs6MfZobTWJ8O4pSqorTg+sfa7RbX6tbHE3/nlb3uk+NxeRUQtNkFtM5xbNeJW4jysxVNQske11bbpfzSirmp9bLEvJeXLiTRydyoS3byvYfp9oHBkmaeXVpYmYeHKdXOhhajXXejbzdE7yyNRFVi9/NvemkIE0UtPK+CeN0ckblY9jk0c1yLoqKncpnx9ZROPsw703JLPH6n4UzDRMB4vqdIliqpOK31MvZ+ZVC6cOvc2REXuRXdpmpAruiWZsrZ43prGrNFRU8uveasRkvs97wnaR2fKijorZi6bEmG6ViRfUK9yOnp2x+SN+vHEqd3CunoUt2K2D2O1amqaL5DkYIZJb3XZ5zKShsmZFPccAXmq4Y5JKpOnt6SeioZza30vY1E71M1cP4pw1i22Mu2EsR229UUjEdHPQ1Uc8bk8vExVQSo/aB0azoxquanGvancOscSaN04k7dSjvBT9LqvEinOJzl1R6pr3E0doAKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB0yIvStXnodx87QLK7UuzNgTary2rMA4wp0pa+BFns15ZHxS26q05PbzTiavxXMVdHNXuXRUgSz72bM3Nm/Fs+FMzcL1FGjXL1W4RNWSjrI9eT4pUThXXvaujk7FRDZX4U000PycSYbw9iy1VGH8VWKgu9srInRVFLW07Zo5WKnNqtcioqKS9dwatwJmNozdAZTY+fUYiyKvLsCXd8bnJaZmunts0ndpqvSQa/rVc1O5qEaeeexptEbPNU5uYWX1b9T+asu1uatXQvRO/pWJ8D6Ho1fQN+riyYAKgCptluq7vcaW00EfSVNbMynhZqicT3uRrU1Xs5qhdfPjZMz02b5KH8dHB0lJS3GJJYK6lkSppVXvY6VnwWvTzV9mpNk6FnwiK5dGoqqvch+hPh3EFLaIMQVNjuENrqXrHDWvpntglena1sipwqvoRSS3c95Y7PeJ4sQYqxMttvGZFDUOipLXcEY7q1FwN/NoY3fHVzlciu0Xh0Ts1FojEfFJHp0kbm69mqaHE2XcxdnvIzNW2NseYWV1hu9PH8NjHUbWOjXytezRzV+hTALaM3NtquLa/FWzZinqMrl6SPDd3croezm2Kp1VzfQj0d+yQdz2dI87LstbQOIrxZLJZMq77WTYhjZLb5YaZXU8rHJqjlmT4DU07VcqaFysc7tnbAwBhyqxTdssHVtHRM6SdlsrIqudre9UiYqudp38KKZobtnG21BlFmdUbJ+b+ALs2xUsM1VS1dXC5Utqomukcyaskif3IirovZ5CTN7+B6I5zl15IncZ8eVndW/xqxVNNU0VRJSVlPLBPE5WSRysVr2OTtRUXminWbDm1DsPZGbT9qndiSxw2TFPRolLiS3wtZVRqnYknYkzO7hf7FReZDTtWbFubOyNiGnfiaOG8YcrJv8Ai2/Ucblp5XJzSOVq/ncmn1q6ovPRVNbnsY8qit5Kip9IJd9jHOnZQ2wrZb8oM3MksLUePqOg6Jqtt7GQ3OKJiayRvbo5smiaqxfSqKvd77aB3RuQ2YVofV5Po/AN+gjcsLYnPnoah2nJskb1VzefLiYvLyKSVEJSIqqiImqr2IZt7N+G94Vsu4cps6MucA32pwVVNSsrLLUL01PWwKn546lR3SNXTmkjURyelOS48Z87NWcezRilMPZn4YloXKvHSXCnVZaOqai8nRyomi/sV0cnehMxu0dpC47QGQUVLiapinv+EHstVY5NEfLGjfzKRWp5Woia+gnl3izpV7K+37k/tMJFhKrfJgzHyN1qrHXu4Omkb8bq8jtOk/YqiPTyd5lXBxcTkdFwonYuuupi3tX7BOVu0nQy4ps1M3B+YlMiPoMQULeic+RnNrZ2t04017HfHb3Ly0MZcvttHaS2LcVRZRba2Gblf8OIqRW/FVNGs0iR9iL0uiNqG6dqLpInfqNsuUvaUPVPKh9PIZdZk5fZsYYpcX5bYpt19tdTGkkc1JOj+FVTXhenaxyd7XIiop6eGRzm/Dbo5O1PIbnc2IqAdfCi/CRVGrV7UUI7D5qnlOPEjU5Ip8arV+tUelQ577PEXXM8cB4WSXVLXhd9ardfirUVUjdfalOngRzmVm9BxfLi3bUx3Gs3HTWNtBZ6ZNfiNipInSJ/jnyr7TFMDJbdvYbjxPtp5Z000fHFQ189ycmnYtPTSysX2Pawkh3yuFfq3sqW3EEcWsmHcWUVS9+nZDLDPA5PoV8kXghhdudcMOve1pPe3R6x4dwtX1vGqcmvkkhp0T6VSZ/gpJHvJ7B+KPYrzIgib0klFSUtwRE8kNXC9y+xqOX2Aa+xV2e51dlu9DeaCZ0NVQVMVVBI3tZIxyOa5PoVEUpABtL4cvEGIsPWvEFNp0NzooKyPTs4ZGI9PtKfolldjTGkGYGytlXianm6RX4XoaKd2uutRSxpTzf5SF5eRVdr8VTPKDuB0cS9/I+LIicuY5Q6VAOhHarrxHJV7xzh07QdPEmnap91ROavTQunTtIe9918r2XPq3Ue9OJgEdH3O7SH3fcfK9lzz/8Aluo96cURul8thyspLftc5VVtfVQ01PDiOnfJNM9GMY3nzVy8kQsaANof8cTL/wD69Ye/lOD8I8VmjtTbP2T2HKrE2OM1cO08NOxXNpaeviqKyod3Migjcr3uX0JonaqomqmtbqvlAF1NqHPGu2jc9cV5u1VPLSwXmqRtBSyKiup6KJjYoGLoqojuBjVdpy4lcveWrAAzB3T1BNW7auFZ4mqraK3XWokVE7G9UkZqvtehPNqhFrubNnK8WRuIdpDFlpqaNlzpPqJhvp2KxKinc9r6mpai81aro4mNd2LpKSjIsac9ftgdoOlXd6KceNF+u5k5CoPh06qqoqLqfeJO1VQnKHTuB1IrV79foU5aJ3cQ2/BzB1qnLsU+prw6aF2jWVz8+XXMb1tu/vkp4Q93n38umY3rZd/fJTwhRKruUcSYdsOHM1GXy/W63OmrrWsaVdVHCr0SOfXTiVNdNUJMXZjZesRXOx3h1ETtVbpB+EavI1XygTjbfG39lLlllPiPLzLjHNvxDj3EdulttMyz1DKmO2Mmasck80rFVsb2sVysZqruLhVWo3VSDkAATZbmS3T0myxeKyRqoyuxfWSxqqdqNp6Zi/bapC3Y7FesTXijw9h21VdzulxmbTUlHSQulmnlcujWMY1FVyqvchsY7HmR0mzrs54PytrE47pRUjqu7PRUXWuqHrNM1FTkqMc/o2r3tY0C9IODVVPrF8QiuRV+CvMnY5g69XrryU+tVydrVUbRzBxVV05NU60WVF/O/bqTaO4HUqyLz0cntPqcf64u0Rwb7r5H8ufWWf3VxD0TB77hNMn8uuS/pln91cQ+CCusP6OW77Lh+/Q2mjVlsX6OW77Lh+/Q2l0RU7UUo5lr9qX5s+bHqTe/cpS5i69zC1+1JxfkaM2EVmifiJvff/3KUDWrAAE7m6R+ZdYf3ZuvvCmZhhjuk0//AKL7D8HX/jm6+8KZmpoiaagfQcER2uvEcl7OSoB9BxaiIgXTtRyIv0gcj4qIqaKcefnoHq5U+C5viBrF5z4RZgDN/HGBomIyPD+IrjbI0Ts4IamSNuno0ah44v8A7flidhzbHzWtzmI1ZL66t0TyVEUc6L/ldSwAEje5LxatBnZj7BDpOFl6wzHcURV5OfSVTGIn08NW9fYpbHe3YtkxLtlXi2q/ijw1ZbbaYufYixrUuT+HUvKPdQX91k20sMUyScLLxbbnQPTX4ydWdKifwomr7C1+3FipuMtrrNe8xydJHHiart7HIuqK2ld1ZFT0fmOoFjgD0dVgi5UuXVszHcutBcr1XWRiadk1NBSzOXX0tqm/wQPOEiW5RxKtBn3jXCrpuFt3wt1pGKvxn09TEie1End4qR2mUO7Pxo7BW2jl/O+oSKlu0lZaKniXRHNnpZWsRV/bUiX2AXI3x19bc9rOmtTJOL6j4WoKdyea58k0unhI1faYLGSu8gxWzGG2nmXcIJkkgo62mtkfCuqJ1akhhcifv2PX6VUxqAuhstWCTFG0plbYI4lkSsxfaWvT/wC2lVGr1+hGo5fYbLBr/wC7DslHeNtTAc9wfCymtLLjcXulcjWo5lDMka6r5JHMX2E9v4oLGz499t6fTUs/tArJuNr0dExFVeTlXyERG9S2HZcI3it2mcrbWrrFdp+kxPQQMTShqnqidaY1E/O5F1V/mvXXsdylnkxFh5E0df7Yiad9Wz+0obrcsD3u01ljvd3sdXbbhA+mqYJqqNzJY3tVrmuRV0VFRVQnatXwGRe3Ns2WnZuzmq7LhK/2+6YXvXFcLR1erZLLSxOdzgla1VVFYvJFX4zdF7dTHQSy+kD1eX+bGZuVNxddctse33DVU/k99trpIOkTyORq6OT0KinlAUZ35T74TaRwNbobRjm1WLHUMSonW61jqasVvkWSLRir6VYq/SZfZZ74jZrxZFBBjywYiwdcJVRsrpYG1dKir3pLGvFp9LEIUgTPg2U8B7S+z3mdU09LgbN/DN0qalvFHRxXCNJnejo3KjtfRoXMgkSV6vjWNzU7Fa7U1YWPfG5Hxvc1yditXRUPcYBz0zkytqXVeXuZuI7C9+nG2juEjY36dnEzXhd7UHcGzdrp3L7Ai8tez6SBnDG9b2x8O0sNHUYytV5ZFp8O4WqJ0jkTuVzOEvph7fdY3paKlp8T5G2i4VDERKielu0lOki+VrFjdw/Rqo0S5ap5UBHlgrfRZBXdsceNMAYrw/K7RHuhZFWRN9rXNcqfvS8OH95tsYX+JJkzVS3OXl0VfQzwuTxbp9snKEZWap5T6Wcwjtb7NeOpGU+Gs4cNVLpF0ajrhHGuvk0cqKXCpceYHrn9DRY0sE7u5sVyhe7wRxZ5eN9VbLHoQdMNRDUsR8E8UrF7FY9HIvgfXMXROB41HZqg1TyoU7+sNcnDEjkXtVXaaH10cWvwn6O8mpNvwd+qeUap5SmXXVNHLp2aHxX6uSOJzZFT42juaF7/AGCrBwZrrzTuOZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6ntcrl17O47QB1q1Gt0aiqunb3nTLBFV076aqp2TwyNVkkcjUc1zV7UVF5KhVAmfRhttBbrzZvzuZV3XDNofgDEkr3S9es0aJTyyL/ANLTL8BUVea8HAvpIzNoDdrbS2RHW7s3DjcX4dpkfJ9VbEjpVZE3nxSwqnSR6JzXkqJ5SftU1TQxo3g+cl1yP2V8U4ksC/8AGVz4LJTSqvOJ1Sqsc9PSjeJUJep0sa+DHvje2SN7mPYqOa5q6Kip2Kimc2zfvOcS4QsX41u0th38c3AssbadHVLGS19LGnJE1k+DM1E7nqjk05O7jBjt5qDXtE8dDiXYu24tnmqylwjiW1We0Mjb0Ftk6KirrTMzmyRsLl7lVeaKrV1VNSG/MLD+J9lvPS4WfBOYsFTdMK1yOt9/sdV8F6drXIrV5LpycxdU7UXVDwFBbMTx8NZa7fdGcSaNlp4pE1RfIrU7DrrbBiChp+v3Gy3Cngc7Tpp6Z7Gq7ycSppqY/V/EyOzHvY8m8W5f0Vv2gryuGsYW+NIauoSle6luHCnKZisReBzvrmLoiLrpy7LxJvMNjVsavTNen5Lpp1WTVftGv4DWX6ifZ+8/2MmVCt/HIcrkT88S3yqmnk10Kyl3lOxpWtSX8dimjVV00lp5GKnihAvhrBeLcZTupcKYdr7vOxURYqOFZX8+zknM/bq8kc5KCdKWtyrxZBKvYySzztVfFo2LlTyUu8D2Pa6V9MmdFjaiJqrpJeFq/Qqltc8tvLYDxfgWvwfmFiKLGFpqU4JLdRUb53yKnYrVRU4VTudxJoQxpkrm+5/RplfilXJ3fUmf8E7fxi85/wBSrFf8kz/gk/lq5/FTi7GeF8LZxVeNtnWfEGG7RQV/WrA+rnTrtKidmrmqvfryVV5cl1M/tmffH3a1Mo8LbSmH33KnY3o/xR2uNEqNe5Zqfk13pcxU/YqR+MyDzuk+JlNixf8ABM/4JUR7OufMr0jjyfxcrndifUmbn/NEkhl+Jbc2N5RsCZjYTqsG47prtiyz1zNZaNbI5yap2KiuVvC5O5UVFTykXuHtousyHz4umZGy5U3XDmHXVqrR2q4y9Kk9Hqi9BUtRVR6Lz79U15LrzPLW/Zxz6ui1aUOUWKpOooq1K/UyVqRInbqqpyPBXK13Cz1b6C6UctLURKrXxyN0c1RktTLIm5wJvZtljEODbPiLHd0ulgxGsSNrrQyilnbBN9crXtThexV5ovbovNNTrzM3l+wbjHBVwseLlrsW2+pjVklqksbpFm1Tu6TRrV/XapoQgnqcCZXZhZnVctDgDCNyvtRAnFJHRQrI5qfQW5nZNt6e9wNtK4wyEzZvOONnG73HDVlrK2R9Paqx6TxSUnGqshnZ8V+icte1O5e8kxye3yOSd8wlC7OawXjDmJKdEZOy3Uq1VJUL58bkXibr3tcnLyqRgVOydtLUblbU5HYyj073WqVE8dNCmXZf2h07cmsWfybJ/YTqTqmX4l6qN73shwrpDV4qmRfNtCp91x0S74LZJZHxsbi6R3mpakRftvIjPyMG0N+o5iv+TpP7DsZssbRb28TcmcVqn7nP/sJ/0ypYF3yeyxqrfqFjfROxfqdFz/yp0rvmtmBrlRMLY6ciLyXqMHP/ACxFN+RW2jv1F8WfydJ/YU1y2aM/7Pbqq73XKLE9LRUUL6ionloHtZFGxFc5zlVOSIiKqqX/AKuX4os/sxaXNvO3HOZlvinio8S36tuNJHOiJKynklcsTXoiqiORnCioiqmp4EA0yy/3d211lfsj3zGuIMwMN3u61WIKSjo6F1tjjd0Ucb5Hyo7jc34yrFpp5qmU+c29p2es0cpMa5bR5d4xhkxRYK+0RTSRU/DFJPA+Nj10k10a5yO9hHHl7s158ZrWH8VGXWVmIL/aesPpeuUdKr4ulaiK5nF2apxJr9J6aTYh2sofzzIbFbfpo1/tM2/1VjwVNzttfZblV2e60klLW0M8lNUwSN0fFKxytexydyoqKi/QUxpEj2xdvO8uNm7ICy5QYxwNiS61lmqq2SOooFh6Lop6h8yNTjei6o6R2vIvou+vyL0+DldjTX0rTf6wiZy+ypzEzWrai3ZeYUrb7U0qMdNFStRXMRyqjVVFVO3hXwLizbEe1TTPZFUZL32J8nxWPSNrl+hFcYvlJe/JUisu+zyhRy9DlHipzfK6anRfvlOh++1yrRPgZOYmcvkWqgT+sj1k2F9rqPmuQWLVTt1bRq5F+hUU4S7D21tDGkr8gsX8K+Sgcq+CDr6f8Z/Tb7rAbVXq+Rl8d5FfcoU/0VKd+++wv/zeRF0X9ldY/wAEj/XYw2qkXh/GGxjqnan1Nf8A2BuxjtUOXhbkVi1XeTqDtfAdfVy/GfLt99Y+L4GQ1bp6bsz8A65d9/auFehyFqlX9dd26feGBsexVtWSrozIjFq/TQuQ7V2IdrJqarkPitE+w1Ln9MvxnGu+/anxMg/G8/8A/Mw8229sB+2Fi/DmKXYKbhv6gW2S3pClX0/S8Uqv4teFNO3TQ88mxJtXr2ZFYq/ialvsx8o8ysobhSWrMvBtyw7V10K1FPDXRLG6WNHcKuTypryE/wDpZfjyIARFcuiIqr6DTIAAB3UdXUW+sgr6V6MnppGyxuVqORHNXVF0VFReadipodJ+phfC99xpf6LDGGqB1bc7jKkNNTtciLI9exEVVRCW53T2z/wPvoM3cM4QtOHr9lXhu+19upmU81xWofSrUq3kj1ijbwMVU01Ruia66Iick/Uk33WaqovQ5JYUavdxV1Qv9hitV7A+2BRO0lyGxM7Tvjga9PFqqdabB+14rUd+MLinRf8Auyf2jYuVlMu+3zj7smsGp/8Ak1X4RwXfbZ0d2TuC0/8APqvwzF1Ng7a9VNUyFxSun/dk/tOU2wVtfU8SzTZDYnaxrFeq9Ai6Iiar2KNnsyso4d9vnG16LUZNYNezvRlTVNXxVylcm+9zH0+FkZhpV/dOf8EjYqaGroq6W21lO+GqgldDLFInC5j2ro5q69ioqKhU0dgu1fQ11yo6VZaa2tR1TI1yaRoq6Ivbz5+QWye6dpGHb7nNLiXhyUwuje5Ov1Cr9w6ZN9tm6v53k5hRv01dQv8AWYbYE2T9ozM3C8GM8B5RYhvNkqXuZDWU9MqxyK1dF4de1EXlqnI/f/IJ7XS/3hcVfxT/AHk6v6uVlFLvss61/OcpsIN/ZTVC/wCkdLt9dnqvxcrsGJ++qV/0zGP8gltd/qCYr/in+87W7BO2A9NUyDxTp6aZE/rJkO1msZYmq8aYvvmMa+COGpvtyqbnNHFrwMknldI5rdeeiK5UQ/HKu72m42G7VtjvFJJS19uqJKSqgkTR0U0bla9i+lHIqewpDbIC4GV2QOcedUNwqMrMvrviWO1ujZWOoYeNIXPRVYjvJqjXeB7tNg3a+XsyCxVz/wC6/wC8mxcqwgL+wbBO2BUP6NmQmKEX9dTo1PFVKLEexNtR4RopLliTJ+8W+liTV8s6xta1Pp4iXy8Z+mV5TIrP3MfZ0xqmPMs7hSUtyWF1PL1mjjqGSRO7W/DRVb3c2qi+nTkZHu3ue12vZdcOp/gtPwjE7DOXWNsaYuiwHhDDdber9PKsUdFQxrNI5ydvxdU0TvXsQuzUbBe2BTP4JMg8Ury11ZTI5PFFFvjf0yrpu3tu2Avxb7h9v+Cm/wBpTTb2LbGl+Li2zRfsLTH/AF6li8x9l7P7KPDTMX5kZXXuwWeSdtMlVWQ8DOkdrwtXny10XQ/WwzsY7UWMcO2/FmGclMS3C03SFtRR1UNNq2aN3Y5Oeui+Uk439Mq6rt6xtmO7Md21PotMP9hwXep7ZqpomYFvT0paYP7C367B21+nbkFiv+Kf7w7YP2vWt41yCxXprpypOf3S/wCVyvdLvTNs9V1TMmkb9Fpp/wAE6fy0TbSRyuTNOLn3fUqm0+8PCXXYc2sbJbqu7XXI7EtNR0MLqiomfTojY42pqrlXXuQtFhXCWJcb4jocIYSstVdbzcpkp6Sipo1fLLIv1qIhM8Ttk63el7abU0/HOpl+m0U34BwXej7ai/30oE/wRS/gHh/yCW15qqfjBYs5Lp/ca/2nJNg7a+VFcmQWK1RO3/gv+8f5Mr8jPLa6z52jbPbbDm5jFl5orTUurKSNtFDB0crmcCu1jairyXsUs4e4zRyQzXyVq6KgzTwNdMNz3GN0tKyti4Olai6KrfLpqh4c1Ms6ZrnBNLTTx1ELuGSJ6PYvkci6opk6u8x21V/vzVKfRbaT/VGMMUUk8rIYmK58jka1qdqqq6Ihfhdg/a+T+8Fiv+Kf7xc/Vkt9PRO3lW2m7+/TWJr5LfSf6s/HxNt/bW2McP3TC2Is366std5o5qCtp3UlO1JoJWKyRiqkaKiK1ypy8pTJsG7Xy/3gsV/xX/effyBm1/8AqB4q/iv+8zniZVgwX7TYM2v1/vB4q/iv+8fkDdr5F0/GDxV/Ff8Aea5QyvO5d7V20NlPheHBeXeaV3sdlp5ZJo6OmViMa97uJ682qvNeZ6J23jtdP7c9MRfw2fgiLYM2wJpehZkFiri8rqVETxVdDx2aOzbnlktaqO95pZbXjDtDXzrTU89ZFwtfIia8PJe3RFUz/irvlHrHbdG1u7tz2xN/jm/gnW/bh2s5E0dntij2VKJ/Uc7DsN7V+J7RRX6xZJYhq7fcYGVVLOyJqNliemrXJqvYqLqWZv1gvWF71XYdxDbKi33O2TvpqylnYrJIJWro5rkXsVFLnj6N8ou6u2vtXO7c98V/xz/cdbts7aocurs9MWfx1f7C3d8y2x9hrD9rxZfsH3ahst7Z0lvuE1K5Kepb+sk04V+jXUubhbYk2o8bYfpMU4Tyiu11tddGksFRTPiej2r36cWqe1Cf4hvkoXbY+1G7tzzxb/H3HW/a+2npPjZ5Yu9lxeh6aHd+bY86OVmQuI04e3jYxvhq7mcot3vtlSpq3ITESfskib914/wb5LHYrxbibHV/qsU4xvlXeLvW8HWa2rkWSWXgY1jeJy810a1rU9CIfklws3dn/OHIee2U2beBa/Dcl5bK+hSq4fzdIlaknDwqvxeNmv7JC3pqZnTKvsOIL5ha7U9+w3d6y13KlVywVdHM6KaJVarV4XtVFTVFVOXcqlLV1VTXVU1dW1Ek9RUSOlmlkcrnyPcurnOVeaqqqqqp1AoFQ64V76CO1vrZ3UUUrp46dZFWNsjkRHPRvYjlRrUVe1URPIU4AHZT1NRRzx1VJPJBNE5HMkjcrXNVOxUVOaKdYA51FRPVzyVNVPJNNK5XySSOVznuXtVVXmqnAv8AWbYK2usQ2ukvdmyQvlTQV9PHVUs7XRIyaKRqOY9qq/mitVF9pW/leG2Yv94e+/w4fwycouVjxT1NTSSdNS1EkMmmnFG9Wrp9KFS693l3xrvWr9NQ/wDtL5XfYG2wLHbam73HIjETaajjWWZ0bY5XI1E1VUaxyuX2IparDeVeY+L66qtuG8F3euqqFjn1EMdK7jjRvbqipyX0Evl4/tMrzzrpc3/GuNUv0zO/tOP1Qr9NOu1Gn7Y7+0rrThXEF7xFBhO3WyV93qJ+rR0j9I3rLrpwaO00XXuUvnDu99smdyJFkPf14k4kVViRFT6VfoN8YdseZJJJXccsjnu8rl1U4mQ6bvbbKWTo/wAYXEOvev5lp48ehT12wPtfW5sslXkTiNkcETppJUjY5jWImqrxI7TsQcp9MqwAPf5YZBZx5z11xt2V+X12xDPaUTrqUkXEkGqqiI5y6IiqqLy17j3a7B+17xOamQWK1VvbpSf7y8oZVhQX6l2Dtr2HTjyExSnF2aUyL/Wdq7A21/0fS/jFYhVO1URseqezi1GynGrAAvrJsMbWsXx8isTeyBq/1nBNh3ayVNUyLxP/ABdP7RyhxvxY0F8nbD21i1NVyLxP/F0/tOmTYp2rIubsisVqnoo1X7g5Q434smC8y7Gu1J0scLcjcWPfL8VrKBzl+12Fa7Yb2tWMfJJkNitjWJq5X0nCiJ7VF8pJtpxvxYxFVF1RdDnHUTxPSSKaRjk5o5rlRU9p+xi3BWKcCXR9kxdZai11zPjQToiPT2IfiCWeU2J6e5wxnrnPgtGtwrmnim2NbpoynukzWp+94tC6mH94htjYbREoc67pM1O6rggn+/YqmOR9RrnfFaq/Qg4xdrMO1b1/bFt8jHVeL7RcGtXVWz2qNOL6eDQuraN9fnPR25kF1ymwlcKxqaLU9PURI76WIq/dI87fY71d5UgtVora2ReSMp6d8jl9jUU9NQ5LZwXNUbb8rcWVCr2cFnqF/wBAz/mfp3UimH999d06NuKsiKN3NOkfb7s5OXfo17F+6X9yM3sGzrmxjCDCN7s13wRXXKRsNLVXPo3000ruSMWRi/AVV5JxIiekiHZsybQ8jOlbkpjPg85bNOifbacqXZn2iZK6CmpcmcZJUSPakWlpmb8LXlovDonPvG+M/TGynTo9iaOl42u+E1fQd5arZep8zKLIDBNDnBTyw4vpbXFDcmSvR0iObybxqn13CjdfTqXVNS7NQABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+K5rfjORPpGp0OY1fjortV8AOb52t7EVfIYab2DDV1xLsdXae1UkszrVd6K41DGNVVSFr3Nc5fQnHr7DMiVEViMjR3k+D3FiNqrab2fsicEXKzZxX2lrZrrQyRR4ch4ZqytY5uiokf1rV1+O/RvpJ5eljXWO6hnipq2nqZ4kljilY97F+uaioqp7Tvvc9sqrzX1NmpJKW3zVMr6WCRyOdFErlVjFVO1UTRNfQUQ9ol4w7vaNlzDOFrNY6DKG7NWjo4YZoo6GnYxjmtRF4efNNU7Sz22nvGsndoPIqsyvwFl5XW+419ZBK6prKaJrYY438SqxWrrxLpp7SOuGGaokbDBE+SRy6Naxqqqr6EQvFgbY32oMx7X9W8HZJ4mraFW8TZ303QNen63pVbxewznjJlWas0DITBuwJtaYyxbDhCHJm+2uZ0jWT1lzh6vS07VXm98q8lRPI3VV7kUymzP3NOKcJZPy4jwZmC3EWNbdGtTV23oEhpqmNE1dHA5V1R6dyu5Lp3F5QxHVhjFeJMF3qmxFhO+VlpuVHIksFVSSrHIxydioqEgOz5vhMwsKQ/UTaAw3DjOkY1EgudJFHT1rNO6RERGSJ6dEXy6kdtVTT0VTLR1UTopoHuikY7ta5q6Ki/QqFwMndnvOPPy7Ps+VGArnfnwualRPFHw01Nr2LJM7RjPoVdeXJBczTv0k8dvpcnEevBlDflb3Lxw6lO/fV5WI/hZkze1b5y1EJYyg3LO0dVUsM9XmBgKkkkYjnxPqKpzo1VPiqrYdFVPQpXWvcpZ7vvNNDeszcExWt0idYnpH1MkzWd/DG6JqKv0uJ/wBTF4nb67LZF4Y8lL1Iv62qhQ5rvp8BRxpNPkPiGBjuTJHVMXCq+TXQyXwNsqbJOyVlhJer3hPDqU9kpesXTEV9p456iVyJ8Jyvei6ar2Mbp2oiIRx7Sed2YW8HxxR5M7MuUz24OsdQs1OkFFHE+d6at6zPJojYI9FXRmvfz1Xkjyufq447Wu9PxvndhxcCZT2eowRY6xml0qGyotbV6/8ANo9unAzy6c18uhghJJJM9ZJZHPe5dVc5dVVfpLx452Pdo7L/ADBp8srzlbeKm+VqItJHb4VqY6lq97JGfBVE79dNDzmKNnnPHBeK6bA+JsqsS0N9rXNZTUTrfI586r2cHCio72KWWfTKt6frYaxbijBtyivGE8Q3Gz1sLkeyeiqXwvRU7ObVQzxyD3PGceOI6DEGc1+o8E2qZUkltsf/AAi5rH5FRPzOJV9LnKne0kJyo3emyNlfbo4rblfbcQ1UT0c6vvyJXTK5O/4acDfoRqDd9HpHRs/b23PLLaaG05z0n4urH0aNjlkjbBXx6di9IiIkifsk19Jfe275GqxRcpbbgfZmvF6kYiubHT1iyScPlc1kbtEKvbSqcvtpvFFLsg7M2WWHb5i6mqopLziWmpI46TDkEbtHtdMxvb3ORNfNRFcuhmDsx7L+XGyzlzRYKwlbYqq4yMa+73eSJq1FfU6JxvV2mrWIuvCzsanlXVVzNNjEV29YzWa1XrsYYo4WrwqvFPoi/wCIH5alnK9/DFsV4lXVNU1fUa6f4gkebG1HqnQRpH6GockSFycTY2eBc8vpsRxflpOeOmv5CXEun7Oo/wBQeA2gt5LnLivJPGOFLvsqXnClBiG0z2iW81ckyR0jalvRcXwoWpr8PROac1QldVqKnwY2eBgrvi8ROs2yfS2ZJGsW+4poaVWpy4mxxzT6eMTV9hZL+0QjAA0jJ7KP8n7RZY2y2ZNW3MCHBU75ayh+pNKqU8rnvXjkRyJq7VyKmuvcenlpN6FWNRklFmy5E/8AtyITB7IuGpMG7L2VWHZoUjmpsJWySdmnxZpKdkkifw3uLucS+RCcV2tX3MOxY3w3jW72vMm219BiZKl1Rc4a9itqOml0lVz0Xvdxo798edMx97Ph59j2zb/WuZwsvdotlxYunxk6HoVX+FA7wMOCo9fldhXM/GeJ0suUluvNdfUhdUpDaXubP0bFTid8FUXROJPEu1WbNe3NdaltbX5eZk1M7Piyyvmc5v0KruR+hu2cVSYT21MtqpJlZFcKuqtcya8npUUk0bUX9+5i/SiGwXxr5EM3x26doRMvrxvXstrJHh3CttzDbb4V1ihrKJlV0foa6VHORPRroeto81d8JT1TapLRi2XTn0c1lpXMX2cJMdxehAqqqcuEmLtRX0m07vZ6R1JJXZGtqY6fTpm/URrXTp366P5L9CF0m7eu2FT26OKs2GL+6va1EllbLL0Tl71RvBqniZ/oj9OaNPujvQON+oj+j29tsaZNYthe9r9M0qf6J2Jt3bZb3JG3YXvSOXs1nk/BM+1d3JofURy/7ycb9GA/5NXblk5w7Dtw0/XVDzAzeNZs5xZtY7wjc85Mnp8vq+htEsFJSyyq/rUSzK5ZE17NHciexdU58iHvfcLrm9lz6t1HvTizxs90RunocvcKYqxzjWz4PwRQrW3671KUlBTo5GrJK5FRG6u5J39p54vvsI6pthZTaf8AWWm/rNj1jN2dtmyc0ymk5+Wvp0/0ztbuxds5zuH8anRV8typ/wAMn90evPi09gVr0+v+0TBrLZ0ZHZkbP+Lo8DZp2NtqvMtFHcGwNnZL+YPc9rXcTFVOaxu5eg8NDNNTysnp5XxSxuRzHscrXNVOxUVOxTOffJa/ktaDVdf/ANHW/wB4qjBUoz32LdvDbIrcS2zJPC76XMKWpp5W26lvMiNqImQxue7SoVUVyIxqro9V7NEUzObmvvNnyLwZBYW4deXFcok5fwiPDdWa/k2sE6f/AEt19wmJ7+J3oMXwn1dYSLmTvPZE+DkRhBn+Fof63HGlxrvRpJFSoyhwUkcjXt0fdIfgKqcl5O7lM3ONw4nd6+BP/H/aag0xDusdt3FF+uWJLnhPDzqy61ctZOqXyBEWSR6ucqJryTVVOqHdHbZnC5n1Ew1E17fhJ9XY9F9C6JzJ0GrqvNynJUfryXkaz+mo9MvbRvRsn8tMOZaYRysy6qqPD9KlHHM66Q8bmN7FcivamvlXvP11xfvdlXllRl0ifulT/wCuM9OFV+Mv2z7w+lfETxwt26wKdi3e7uTllZl03/CNP/rTg7Eu94e35N8vWL6LjTf6wz44fSviOH0r4jijV9zNfiGTMnFkmLoYYr66+V63SOFUWNlX1h/TI1U5K1H8WmnceaPd5+fLrmN623f3yU8IaGcO7jqdsiCz44TZXtWG6umdU0X1ZW7Txxq2Thl6Lg415ppx66egzFfPvgZWua2zZeRK5NEd1ynXhXyngNx5+lrNj7OtX9HUEoGhmzRGbiPKXfCYsgldcs2sPWWFGOVzKC4QwKiac9FjiVftkWWLMysyMY189TjHHd+vFQ96pI6suMs2q68+TnaGzvck/wCLqr9of96pqzVv92T/ALa/7qlkxdXZ2adpjGmy9i+vxtgWgttTcq6idQq6th6RI2Kuqq3yLqieBnzs4Z97yzaXwNcsa5Y3PB9Rb6S6yUD5rgkUL2zNYyRWNaqL8Hhlbz+kinJp9y182PE3rpVe50hOM9mvC53bMO8u2l8IQYHzRxLgaGzsqmVb6enqGs4pGa8KuVjNV01Xke0wnljvSsD4Ns2AsN4nwBBbcPUcVBSSOcxz5Io2o1vEqs7kREJBtBoS+H9NYDphTe2K7njXACJ9Mf4BUMwhvXnKnSY6wG3i+Npwcv5hnhofRwv2mo/cT5KbzzMnB10wLinNrBluob2xaSqmpmokiU7uT0RzGcSapqi6c9FLSZZbqzaTyGx9bczsu8z8JVl7s3FJSdLBI1nG5qtVFR6KmmiqSugcPzSXGDEuXe9PllWoXOLAjEc7j6FtK1Gt/Wp+Zn67MF7y2SKGR+Y+CoZVVOlZ0bVRPSnwDNADhDUKe88yz2k7Tb8IZhZ+42s13ikqZbRbaS3R8CQrwdI96oiInPhRPKYBkwu+6+R/Ln1ln91cQ9GpMmIqrU2Z90o2U70ZK6ojRjl7EdxJoviTvMyc3hGur9pTC6fRav8A+BBLYf0ct32XD9+htNCjEN2Tm8CVU02lcMp5f+Kv/wCB+BmRg/bzy4y4xRmDW7ReH6qLDNmrLvJBHakR0raeF0qsRVbyVeDT2mbZa/al+bPmx6k3v3KUzxu+6IYV3pu2ArtfxexInk6lD+Cffy07a/8A+vcP8Si/BMRAbE0eyde9srafyaoc3IM/6KzNq6qqpGUb7VG/nDIrOJVRvfoegzz2GM/9pfCttwbm3tC0C0NoqlrYH0tnRVklVqt1eiK3XRFXTmfs7pH5l1h/dm6+8KZlmOE3V1izhbIHaxwRhLDuCMPbRFrW34cpY6SKWezosk8cbUaxr9deSIiJ2li67dKV2J8X4nxzjjO5btcsXrO+4tS1IxGPlej1ex3EvNFRNOSEjZ80Tt0HCGsF8st3xnBl3YJMup9pRl9wErXNjsNysLJ2RI5frHPcvAvP63Q/KwzuysxctMVpinKHatxJhjWRXvokp3SwKirqrOHpEarfQrVM/gOHjLq8rmMc6rJHadqY4Y/ySqsWJqIsjbYiLIvlVEXQ7IckNo9GKlVtGySO7lbQK37jjIgF4xlEdvYclMzcN5a4JzAx3mQuKoaC9zWmBjqdWOp1qYFkc7XVdUXqjUX0ohGQTqb3exJdtja6XHg4lst9tlbr5vFKsGv+W09pBWWTAABRzhidPNHC340jkan0quhLnm7uksmrTkrecY4SvGIKbE9iwtU3COm6Zr4aushpXSI17VTVOJ7dOS95GVs8YQZj/PrLrBUsfHDe8U2uinTTX8xfVRpIv0IziX2GzBWUsNZRz0c8TZI54nRvY5NUc1U0VF8SWaNV4H6uLbI/DWK71hyRFR9quFTRORe1FikcxfvT8ooksyT2wcnrdk/hKxY62qcWWa82u1w0k1uZh+rnjpFjTgSNskaK17eFqaKncp7SDbL2dIlVXbYOJ1//AMXuP4JE+DPGGp3sjbSu0hhOXMDKzaaxJXWOnrpLa+V9rmpuKdjGPc3gn0VyaSN56ac1TuUuMuypWcS1bMzquCuf+eVUFuhjfJ5eLTtPI7rXC0eGtifA0yxcM96luN0m5dqvrZmMX/FRxmWOieQcPH4u1iDmLu1coM2K+iu+N77cX3GiTlWW6GKjmlXyvcxNXfSvM99R7M+MbJTW60WLPPESWu1RJDTU9WvSvY1E0TV6Kiu09JkBonkQchwiLFPyUzMa18Ts7atvSO+GqxuRXJ5PjGHG81xHnds9YGw/DhbaDuMdHieSa21VoboyeWJI9XyNfzcjE5NXRU+MhI5jXE+G8C4ZuuNcY18FFZrNSvraupmVEbFGxFVV/sNeXa+2lL/tRZz3XH9wlnis8L3UdioHu5UlE1y8CadnG74zl8q6dyC+M/8AWLX42T21DnpkHR3Sgymx7U2CG8vbJWpFBFIsr0TRHayNcqLz7UMjshdoreUbSmJGYIyuzMvdYrVRa24vp4I6ehjVfjzTdH8H0Imrl7kU81sP7vfGe1PXsxfieepw5l3RzIk9w6PSe46L8OKl1TTVNNFkVFa3uRy8ibTLbLHAWTGCqDBGWGG6GzWagY1jY4WJrIqdskj+2SRe1XOVVVRJyuQ7W62bMnM+sraCeXObP6qzBrqlvEsElNwRQL5GOX4S/SqJ9BfNH82vZEnE/wCNr3H3jRyI5jV43dmqFDd7ra7HbZbriC5U1upIU4pameVsccaeVXLyQuZ3qP0EkbxoxeHiXsQ+NmR6qjYlXhdw9hh/nfvPNmDKGpmtluxBPjS+wRua1lia2aFj+5Hyq5GJz8iqphtjXed7WO0HcocGbOeX9RZZp/gKttplrq16r38St4I09ntM787WTUvV+xThnC9FNccR3232ymp2q+WWqqGRNY1O9VcqGI2cO9Z2XctqerpcL3mqxpeIFVjaa1wqkKuTyzuRGafRqYUYT3cG3DtF19TiTOzGU2HVfLpK7ElwlqamVF5qrIY1c1E+lzTKnKXc9bPeEaWGozMu13xdc2ua9W9Y6tS6p2pwMTVyL6VG0zPbG3MTfFZ6Y2qKa05MZe2+wTyKrV44nXGolVexGt0RE8FPIVFr3p+07LFaLozHNPQVusidYT6kUiNXyqiM5ejmS6YAyFyUyskcmW2U+GrHUtYjOtUtuiZK5E8sunGviXAZJOmicUa6Jz0TvLx32S/EOeBdzXtAYrqH12aWYNisCuXn0cr7jO/6VThT+cZCYH3LmRtnijkxzmBiXEFSnN7IVjpIVX6ERXafviQvXt4tUXyop2RonCnPX0ieJbrGGzbtLYwslNHEuTVJXvjREWSrraiVz18q6yafaLp4M2Z8gMAwrFhLJ7C1vRU0VfqbHI7T9k9FUucC8YmvyqDC2GrU3hteG7XRonYkFJHGn81EK9tNAz4tNEn0NQ7gMg6nRNVNOhjX0KhxbHoiL1eNFTs5HeC5B0QwokrqlzXNe9OFU15aHeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4Pk4V0Ruq+g5lBJUPSuWGnj4nomr+JdE09Ay30Kl0zVRdE1Vvb3aFLdrnbbTa6m7Xe409vt9LE6apqqiRIo4o2pq5znO5NRE7VUsttMbYuSey3ZXVeP76ysv0sfSUWH6B7X11Tr2KrNdI2fr36JyXTVeRDFtYbd2cW1RdZqK6177Bg6KVXUWHqGVUi4e51Q5NFnf9PwUXsRCb8Gbe17vb7ThtavAGy71e6V7F6KfFVRGklJH5Upo1/PXJ57vgeRHdpFPijFeJcbX6sxRi++1t4u1wlWaprKyZ0ssr1XVVVy/c7E7j8oAVdotF1v8Ac6azWO21NfX1kjYaemponSSyvVdEa1rdVVV9BIZkZubMzMYW204mzkxvSYPpKpGzVFmp6dai4MjX6x7lVI43qn7PTvQyl3XVBss3HKikqcpcMpLjO200SYkuVxodapKtzfhoyVdUSPXXhaxU5dvPUyCzz2uNnvZ7Zw5oY/oae46cTLVSr1mtXlr+cs1c1F8rtEJLvdX0rsmNl/I/IHDlFhfL7L+2R9C1eluVTTxzV1Q/vdJM5vE5V8nJE7ERC7DI2xsSOKNsbW8kRERET2EY2Pt9phKkuFRSZbZN3K40iQvbDWXWuZTO6X613RMR/wAH0K5F+gxSzG3qW15j2HqlvxhQ4Vp9eyyUTY5VT0yP4neGgnXqHtPNKsjWaxtRXeQ85jq13u/4Ov8AYcL3SO23iut01PS10reJlPK9io16p36KupFbsbW/eVZ14rtWOZczcRWjBrJ4paqvvztIa6BHfCbDErdZFVNU4kRE9JLcrJmOREhSRVaiPdrpxenQeP8AvYmZUeeQO5/y4wzVVWJdonEbcdXGabpoqShfLS0bVVdVWRUVHyqqr2aonoUz2wphLB+Xthgw1gnDttsdqpWIyCmoKdkMTe74rUTVfKq81PN5m54ZM5F2xa/NDMKzYdhkRXxw1VSnTS+Xo4k1e/8AeophZmTvlsjcLVEluyvwBfsV8EnOqqXNoaZ6d6t4uKRfaxBMi9pDuGZ3CvDqjk+Fp3lrc/tpXJ3ZswtNiLMnE9NSztjc+itcUqOrK56JyZFEnNdV710anepGTjbew7Sudl7pMvsg8B0eG66+PbRUkcC9erpJXctI3ORrGr6eHke8yT3UmaeY2LYcy9sDHtRVvklbUT2plY6rq6lddeCadVVrG+VrNfJqhOVl6I9LgrAmce8+xhS5k500dxwZkPapHSWXD9PO6Ka9O1VEe56fGRFTnJoifWs73JILl9ljgHKbDtNhHLjCVtsNspI2xxxUcKNV2idr3fGe7yucqqpQX/HWT2ReEoaTEWKcP4SsVlpo4IYZ6mOBsMTU0Y1jNdV5JyRE1Uw2zh3xeQuDHzW7KvD13xtXRO4UqVZ1OiXReej3/mjvQqM0Eye/ZWfUkMPE2rkghWWNFa2V6JxN17dF7inetvmqkklp4JKqlTXpXxpqxF8i9vgQ4YB28MbbUWftssmeWcEeWGXPSvq0o7brFE9zFRY6eWf4y8Xe92icuxCSrH22JsxZX4KXFF2zUslZSU0OlNT0NUlTUVStb8FjGs1VyrppqvLyqOX0y/i9LF6WR1XxuZGic+Jfgq3ymAWeee+ae2DmVX7KuyJcXWzDlukWHGeOYVckEDNdHRQyNVF7Uc34K6vXVEVGori2NDnftS7y/HFVgXKaSty0yjo3pFerjA782kjXmsb5m6K6R7eyJio1EX4SqhIlkhknl3s75e0eXeXVqSloKNOOed6I6ermX40sr9NXPVfBNETkhd59Yen5uzxs6Zb7NWBqfBGX1qTpXIklzusqI6ruFQvxpZZO1eeujexqckLpN4nuXnyQ4Rta1EWLVGuXiXU74111XRPp8pqTEcUiVqLwu5r5Tkxio3R2mpzAHBWO+tVEIq9+Bi6RH5VYBhm+Bpc7xUs8q/mEUK+z838SVchH3yWLI73tUUGHYZeJuHcMUcEjdfiSyySzKn8B8a+0JjA8/Qw9an33EFsscWvHcayGkbp26yPRqfdPzy6+ydhaXGm05lZhuOLpG1WLrW+Zun/MR1LJJV9kbHr7ArZDtVuittro7dAxI4qWCOBjG8ka1rURET2IVaM07zkfSYmIfN9xhNKPNnLrG7WJrdcPVFreqf8AdalZE1/ja+BG0S8b73Dj6jLjLPFzWfBob3WW17tO+eBJGp/7dxEOVXush8aQ5c524Bx7VP4KawYkt1wqF10/MY6hjpE1/YI5DZsRrdE5IarJs8ZOYjXGGUeCcWOk43XnD1ur1drrxLLTMeq/zgPX8KeRD4rfJonsOQJg+Jr3n0AYPmieRAvoPoGD4iLpzXUh733SImb2XOif/LdR704mFIe9918r2XPq3Ue9OKI2y/Gwh88PKX1lpv6yw5fjYQ+eHlL6y039YGxerde9T5wp5VOQAhD3yvztqD1Nt/vFUYJmdm+V+dtQeptv94qjBMDLfdVfPbwT9i3X3CYnw4W+RCA/dVfPcwT9i3X3CYnxA+cLfINE8iH0AcVai9nIIzyqpyAHxE0PoAAAAayWfny65jett398lPCHu8/Pl1zG9bbv75KeEAln3Hf6Ws2Ps61f0dQSgkX247/S1mx9nWr+jqCUECmuX6HVX7Q/71TVmrf7sn/bX/dU2mbl+h1V+0P+9U1Zq3+7J/21/wB1QOkmo3LPzY8TeulV7nSEK5NRuWfmx4m9dKr3OkAkAAAAAAAAAAAEbm+6+R/Ln1ln91cQ9Ewu+6+R/Ln1ln91cQ9AV1h/Ry3fZcP36G00asth/Ry3fZcP36G00ALX7UvzZ82PUm9+5Sl0C1+1L82fNj1JvfuUoGtOAAJ3N0j8y6w/uzdfeFMzDDPdI/MusP7s3X3hTMwAAAAAAAACwW3vg1Md7HWa1j4OJ1Ph+W7tTv4qFzaxNPT/AMHNdQ2fs17J+KXK3GOHODj+qtguFFw+Xpad7NP5xrAgAABk1u2LB+KLbWy0plj420lZVXB3LVE6CkmkRfYrUNg4hS3MWEWXradvuKJ4uKPDuFKl8TtPizzzwRN/yazE1oGt5tmYPnwJtW5rYcnj4EbiqvrYW6aaQVMq1EP+TmYWaMyt7bYfqPto3+4Izh+rlntVfrp8bhp0p9f/AG+nsMNQAB3UNHUXGtp7fSM456mVkMTfOe5URE8VA2PdjSyfie2UcprXwcCphK3TqmnfLC2VV8Xl5T8vCthpcK4Ys+F6FESms9BT0EOiafAijaxvL6GofqAcXORv0nDpVc/gSNeHTVXdyHyo43J0cfJyppxeQwn3l22PBs95ZOyywbdV/F/i6ldHFJC9OO20arwyVDu9rnJxNZ6dV+tJaMS96ttqz5i4pqdnPLm56YXw9Uol9q6eXVtyrWf8zqnbHEuqKne9P1qFsN3zsH3XahxSmNMc0tTQ5a2WfSrmTijfdpk/+FhenYicle9OxOSc15Wd2ZcqcC5t5gyVGcOZFDhLB1ob9Ub5X1c+k9TGi6rDCi6q6V/NNdF0110VeRnli7ez5QZO4Ygyx2WspentFnpuqW6qrl6rTMVOXH0SIr36rzVXKiuVdVM7vUXP1JzarFasJ2Ggw7hu3UdrtFsgZTU9PG1GRwwsREa1qJyRERCy+cW2xswZE9bocY5l26W7U0avWz29y1VS52mqN4WIqNVf1yohEjcNoTb721rlWYfwpW4lutHIqNqLfhyBaWihaq8kke3RET9m8yAyR3M2K7zLBiLaJzDjtsMjUlmtVnd09Uqr9bJUP+C1fLwo76S7b1B1Zu75zHF/lSz5BZbRWlZV6KOpu6JVzuVV0RWQx6N1XyKqlu8O7Mu8K22Lwl9zMvN9stgr1R76q/TPpaRGf/aomKiu/gInpJSsltkDZ32f6WJ2XmXNuZXN7bpWRpU1rl8vSv1c397oXla57vrk5Ly4eQze6aj+yS3O2SGCYUr8579W45uLXo5sUDn0NG3TuVjHK9/tdp6DOHBmXOAsurXBYcBYNtFio4GcMbKKkZEictNVVE1VfSvM9CkSqusztfQdyImnIuQt10rDI/RJJEXh56py5nJIl7XKiqp2gYjrWNyoiI7TQ+Nh0VeTUTu0O0FHX0buzVFTv1OaIiJoiaH0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHxfukeW8b3huKdn6/rktlJRRQYpmomVNdeKmJHpRxSovAkLV5LJy11cionkUkNVCPXeY7CGLdoS6WnNLJm1Q12LaSHqN1on1LIetUzecTmueqNR7VVyc1TVF9BmrEOuJsUYjxnfKvE2LL5W3e7V8iy1NZWTOlllcvernLr/YfmGSrN29tsPkdGmQ10+CuiqtwokRfoVZuZ3pu0tttya/jGVyfTdKH/AFxdiMYz9zAkWFJsaWOLHVRPBh11fB9VZIGqsjaXjTpOFE568OuhkEm7Q221XT8Y6sT/AArQ/wCuPSUe6f20KqmiqJMC2imdJ2xS3um42fsuFyp4Kot6V7vNveU2zAmHp8ntiXA1BgPCUcfQOvbqVEuFU7TR0rGryY5U+vfxP7/gmBt3vF2v9zqb1fbnVXC4Vkiy1FVVTOllleva5z3KquX0qZ3Wfcy7TdfDHLc8UYKtrnpq5j6yaVzF8i8Eengp620bkjNeWRv1czjwvTM+u6tSTyu9nFwmZZBGyEVWqjmroqLqhLXZtyJhGOJq3/O+6zy6fCSltscbdfRxOVT9227lPKGnnR1yzPxJVRIvNrY4o1VPpRDW7+GIyfyU20c2loKGHO3GNPTWuNsNHDT3WWGOFiJoiNaxUTsLqR7zLbCjwCmAm5mu0RFal3WmYty4PN6b/S04vSSO2Lc/7JdA7W7MxNdVTta+6viT+Zop6Gn3UuxTTyo92XV3m07pL9WK1fCRCST4VBfiXFWJsZ3ebEGLsQXG9XOpXWWrr6l88z/pe9VU/LNg6x7uDYzsfOnyYoKjRdf+F1M8/wB89S4Fo2VdmiysSG25G4Mja3s4rRC9fFzVUd/Dprm4JxdirAWKbbjHBVyqLferVOlRR1UCavikTvTkpkNHtw7dl3p54aXMXFk3WOayQUK8Tf2KtZy9hO1aMtMtMPqkdgy/w5QI1NESmtsMeng0/cZZ7U1OH6jUbU8iQt/sFlp01uLxgjaRzcvct+v2F8cYnudU7ikqaqlqJ3uX6XJyP3rdsRbWl2plq6DILF0kSJrqtHwKqehHKiqbGEMFND8GCjijTu4Woh2OcunwU0GVNa79p3f22PeHIlPkHiSFFXTiqmxwIn8NyKXfym3S20tizF1socxrZS4Vw856Prqt9WyaVkSL8JsbGqur1TkmvJO1ScBOLX4y+0P4nIrUVfLyL+mvF5T5X4LyWwVacscvLVHQWizwJEmiJxyu0+FJI7657l5q5T2kbUcjmonI6o/hI5XwdG5e1e9Tuh058DtWlBIV1VVcvNNNDmxiMajU7EOQAAAAa7O8AxU7GW2RmpeHSK9sN7W2sXX62kijpk09kJsRzTRU8L6id6Mjjar3ucuiNaiaqqmr9mXix+PcxcU44k14sQXmtumi9qdPO+TT2cQHmzLjdVWFL3tsYMqXxI9topLnXrqmqJpRyxovsWVPboYjkhG5WwpLc9ovFmLnR8VPY8Jy0/Fp8WepqoODn+wimAmgAAGGO9wwkzEuxreLosfFJhm92y7R+VFWVaZy/wAGqcQSmxXt7YeXE2x1mxb2s4lgw7PcU5dnVVbUKvhEprqADYP3bmNZcc7F2W1dVScVRa6KeyyJrrwtpKmWCJP8UyNfaa+BNZuYsVR3fZlvuGnTIs1gxVUt6PXm2KaCGRq/Qruk8FAz8AAAAAAAAIe9918r2XPq3Ue9OJhCHvfdfK9lz6t1HvTgI2y/Gwh88PKX1lpv6yw5fjYQ+eHlL6y039YGxgAAIQ98r87ag9Tbf7xVGCZnZvlfnbUHqbb/AHiqMEwMt91V89zBP2LdfcJifEgO3VXz3ME/Yt19wmJ8QAAAAAAAAAAA1ks/Pl1zG9bbv75KeEPd5+fLrmN623f3yU8IBLPuO/0tZsfZ1q/o6glBIvtx3+lrNj7OtX9HUEoIFNcv0Oqv2h/3qmrNW/3ZP+2v+6ptM3L9Dqr9of8Aeqas1b/dk/7a/wC6oHSTUbln5seJvXSq9zpCFcmo3LPzY8TeulV7nSASAAAAAAAAAAACNzfdfI/lz6yz+6uIeiYXfdfI/lz6yz+6uIegK6w/o5bvsuH79DaaNWWw/o5bvsuH79DaaAFr9qX5s+bHqTe/cpS6Ba/al+bPmx6k3v3KUDWnAAE7m6R+ZdYf3ZuvvCmZhhnukfmXWH92br7wpmYAAAAAAAAB8c1r2q1yIqKmiovehrB5tYPdl5mpjLALmqn4m7/cLTovPlT1D4k+8Nn011dvqyfif2yM2KDg4elxBJW6fZDGT6+3pdQLAgACVTcd4ed/+7GKnRKjf+Krex6p2r/wh7kT6Pga/ShKoYF7mfCjbLssXXEUkWk2IcWVk7X6fGhihghan0I9kvipnoBD1vuMEut+buXmYbWqkd8w9UWlVTsV9HULJr9Ola3w9BG4TBb7rD61WU2XGKEZqluxDU0Ku07OsU3Hp7erfaIfQBcXZwsaYl2gstbA6PjbXYstULm6a8TVqo9U8C3Rkvu3MLJi3bVyzonx8UVDXVN1eqpyb1akmmYv8NjE+lUA2DJZFjRGoiqq8kODZ15udpo3vTsU5zcSaK1URO/0lNKyNWtgWB7mLq/kvegFvtoTPbCGzzlRd81MYToyKghc2jptfh1dU5F6KFqeVztOfcmq9xrrZuZqYuzrzGvmZuN63rN2vtU6okRuvBE1V+BFGi/FY1ujUTyISrbQ+y9tYbdOclTa8dpBlvlXhKpfFaGzSNqJa7XktSkTHfCe5OxXK1GpyTVddb75A7urZp2fJaa7wYbTFGI2tRqXS+Ik/C/yxxKnRxr6UTX0mdu+hD/kLsN7Ru0PLS1WDcC1NDY6lf0curVpqJGp2q1ypxSfvEUkbyM3PGTOC4aS7Z1YgrMZ3iB6TSUVM9aW3rp9YrU/NHp9Lk18hING1sMbYKWOOGNicLWMTRE9CIh96NFdxOj0cqc3p2lzfZvx+Tg7BWEMCWSCw4FwvbLBbIURGUtBSMgjRP2LUTn6e0/bWFquc5VVeLtRV5HJicLUTXXRDkXB1rEiqiqvYfVjRV115eQ5gDirEUcPwuLX2HIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4Oia7XTVqr2qnJTmAOHRrqi8buR94E8qnIAcUYid6+J84O34S8zmAOKNRE01U+6Jrr3n0AfOFF7j5wt8hyAHHo2dzU5n1GoicPcfQTB8RERNEQcKeRD6CjjwM114U1PvCh9AHzhb5D5wNRFTynIDBxRjU5IOBq9qHIAceBuvFpzPqNanYmh9AAAAAABbjaRxKuD9n3MjE7JeifbcK3SeN+unC9KaThX6ddDWeJ+N6NjB2EdivHDIJejqL7JQWeJde1JaqNZU9sTJU9pAOAJaNx5YehwvmvidY/7rr7XQI/T/oo53qn+WQiXJu9zfg6XD2yhWYjqI9HYoxTXVsLtO2GKOGmRPZJBL4gZ2gADz2YeG6bGWAMTYQrERae+Wettsuqa/AmgfG77TlNXueCSmnkppm8MkT1Y9PIqLoqG1I5Ec1WqnJU0U1jM7cPOwjnNj3Cj2K1bNia6UGi93RVUjP9EDxZJ5uPsVup8XZp4IklVW19utt0iYq8kWCSaN6p9KVDNf2KEYZm3ugcZx4a2vqawzP0bivD9xtkaKvbJG1tUi/waZ6e0Cc0AAAAAAAAh733XyvZc+rdR704mEIe9918r2XPq3Ue9OAjbL8bCHzw8pfWWm/rLDl+NhD54eUvrLTf1gbGAAAhD3yvztqD1Nt/vFUYJmdm+V+dtQeptv8AeKowTAy33VXz3ME/Yt19wmJ8SA7dVfPcwT9i3X3CYnxAAAAAAAAAAADWSz8+XXMb1tu/vkp4Q93n58uuY3rbd/fJTwgEs+47/S1mx9nWr+jqCUEi+3Hf6Ws2Ps61f0dQSggU1y/Q6q/aH/eqas1b/dk/7a/7qm0zcv0Oqv2h/wB6pqzVv92T/tr/ALqgdJNRuWfmx4m9dKr3OkIVyajcs/NjxN66VXudIBIAAAAAAAAAAAI4N9xTzOyXy9qmsVYo8USxud5HOpJFRPBrvAh2NhDeLZJT56bKeK7JarbLXX3DyMxHZ4YWK+R9RTI5XsY1ObnPgfOxGpzVXpoa96oqLoqcwOUUskErJ4Xq2SNyPa5O5UXVFNmHZ+znwzn9lHhzNDC9fTzxXajjdVxRP1Wkq0aiT0707WuY/VNF7U0XsVFNZwuxkFtS537NF4kumU2M5rfT1T2vrbZOxJ6Gs05fmkLuWunLjbwvROxyAbKBjDvGs7bBk1srYyhrqmB13xjb58N2mjdIiSTSVTFjlkanaqRxPe9V7NUai/GQjxm30W0/JSOhiwbl5FOrOFJkt9Wui+cjVqNDEHObPfNfaBxUuMc2cYVd9uDWujp2yI1kFLGq69HDExEZG3s5NTnpquq8wPAgHfQUFZdK6mtluppKirq5WQQQxpq6SR6o1rUTvVVVEQCdrdL0s9PsV4akmYrW1F1usseve3rT26+LVMxy1GyrlFLkVs9YFytq3MdXWa1R/VBzPirWSqstRove1JZHoi+RELrgAAAAAAAACCje6YNXC+2RdL0jOFmLLFbLwnkVWRrSL9uk+36SdciC33tiSDNDLTEyM511gq6FXeXoKhHon/uF8QI1AAiKq6ImqqBsE7tLDcmGtirLiGeLo5LhTVdyVNO1J6uV7F9rFavtMnzwWQWF24JyMy8we2NGLZcLWuhciefHSxtcv0qqKq+lT3oGHW9jwYzFexhiS59HxzYWudtvMKd+vWG0zlT6I6l6/QikDhsebbFg/FNsk5tWtI+NUwrX1aN015wRrMn24zXDAGdW5wsTbntZVV1fHxfUfCtfUNXzXPkhi18JHJ7TBUk63H2FUqcZZqY2fHzt9sttqjeqdvWJZpHon8WZr9KAS2qiL2ofEY1NfScgBwSNE71X6VCRRp9anLs9BzAHHgZ5p90TTQ+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACOvfXYqZbcicFYRbLpLe8TOqVZr8aOmp38S+x00fiQ1my1nRs05IbQ62hc5MBw4l+oPT/U5JaypgSn6bo+l0SGRmvF0Ufbr8Xl3lsvytTYg/UGt/8rXH/aANfI2HN3dY/wAT2xblVQcHB0tokrtNP/qamafX29LqUH5WpsQfqDW/+Vrj/tBkJhHCmHsCYXtOC8J21lustjo4rfb6Rj3ObBTxNRrGI5yq5dGoiaqqqveqgfrgAAa9e8dwtFhPbSzNoqePghrrhBdG8tNXVNNFNIv+Me82FCyOaOxVswZ04wqcfZnZUUd8v9XHFDPWyV9ZC57I2o1iK2KVreTUROwDXFL47DuJHYT2usqLu2Tg4sS0tE5dfralVp3J7UlVPaTOflamxB+oNb/5WuP+0FdY93dsaYbvVvxFY8kqKkuNrqoq2jqGXa4cUM8T0fG9NZ9NUc1F5+QDI0AAAAAAAAh733XyvZc+rdR704mELT5ybKmQG0FdrffM4cuqbEddaqd1JSTS1tVCsUTncStRIZGIvwufNFUDWvL8bCHzw8pfWWm/rJlfytTYg/UGt/8AK1x/2g/cwTsEbI+XOLLXjnBeTdFbL5ZahtXQVjLlXPdDK3scjXzK1faioBf8AAQh75X521B6m2/3iqMEzZDzc2O9m3PfFLMa5sZYUuIL3HSR0LauWvq4VSBjnOazhila3kr3LrprzPE/lamxB+oNb/5WuP8AtAEVO6q+e5gn7FuvuExPiWOyy2JdlzJvGVHmBlrlNSWTEFvbKymrY7hWSujSRisenDJM5q6tc5Oad5fEAAAAAAAAAAANZLPz5dcxvW27++SnhDYbvm7u2NMSXq4YiveSFDVXG61UtbVzrdK9qyzyvV73qjZ0RNXOVdERE5lD+VqbEH6g1v8A5WuP+0AYs7jv9LWbH2dav6OoJQS22TOzlkts9090pMnMDQYbivT4pK5sVVUT9M6NHIxV6aR+miOd2adpckCmuX6HVX7Q/wC9U1Zq3+7J/wBtf91TagkjZLG6KRurXorXJ5UXtMaH7tfYike6R+Q9vVzlVVX6rXHmq/8A5AGvgTUbln5seJvXSq9zpC8v5WpsQfqDW/8Ala4/7QXhyfyOyryDw5U4RyjwjDh60VdY6vmpoqiaZH1DmNYr9ZnvdzaxiaIunLsA90AAAAAAAAAABDbvIN3bfsvcQ3XPfJDD8tfg65SurL1aaNivms07l1klYxNVdTOcqu+D+dqqpojERUmSAGquCfrPbdm7LOeVRUXp2FZ8G36qldPNc8NSNplme5dVWSBzXQu1VVVVRiOVV+MY51W49wY6Zy0W0Beo4tfgtlscT3InpVJWovgBEiCWn8o7wt/2hbr/AOn4/wDXlTb9x/gOOpa665936op0VOJlPZoYXqnoc6R6J4KBEfHHJNI2KJjnveqNa1qaq5V7ERO9SVfdmbvC92S82zaOz2sT6Gak4arC1grItJWydra2oY5NY3N5LGxdHIuj100brl/kDu+9mPZ4norzhbBP1axHQu6SK/X6RKusZJ3PjTRIolTuWONq+lTJAAAAAAAAAAAABGnvvMILWZY5a49az9Cb7V2h7k/73TpKmv8AE18SSw8Rm7krlhnxhaLBebWE4cQ2WGsjr46WWeaJG1DGva16Oie12qNkenbp8JQNY49Bl5YJ8V4/wzhelZxTXi8UVBG3TXV0s7GIni4nt/K1NiD9Qa3/AMrXH/aD9TC2762PMFYmtOMcMZKUFFeLHWwXG31KXKukWCphekkUiNfOrVVrmtXRUVOXYBkFS08dJSw0kKaRwxtjYnkRE0T7h2gAfnYjsdDifD10w1c2cdHdqKehqG6dsUrFY5PBymrjdLdUWi51dpq28M9FPJTyp5HscrV+2htPmN923dOxffbrW3u65G0E9bcKiSqqZfqpXt6SWRyue7Rs6Imqqq6IiIBrzkyG5Ow+lFkdjrEix6PumJmU3Fp2sgpmKiexZXeJf38rU2IP1Brf/K1x/wBoLy5SZMZZZE4WfgrKfCkOH7LJVSVzqWKeWVFnejUc/ile52qoxqaa6cgPbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEcubO+Ow1lrmZifL2jyPuV0Zhu6VFqdWPvLIFnfDIsb3pH0TuFFc1dPhKumnZ2ASNAi4/LxbJ/2eK7/wBRM/1BIvk/jm6ZmZX4ZzCvGF5cOVWIrbDcltc0/TPpmSpxMa53C3VVYrVXkmmuncB7AAAAR5bSe99wPlHj65Ze5YZf/i3nss7qSvuktzSlo+sMXR8cPCx7pUaqK1XfBTVF01Tmvksq99fh2+4torNmvlA7DdmrJmQyXa3XVazqiOXTpJIXRNVzE7VVqq5E10a5eShJ0Cnt1wobtb6a62yqjqaOthZUU88TuJksb2o5rmr3oqKiovpKgADGnbY217Lsa2bC1fccDVeJ6rFVRVQ08ENY2mbEynbGsjnPVrueszERETy8+XPE78vFsn/Z4rv/AFEz/UASjgi4/LxbJ/2eK7/1Ez/UGUmxTtrXDbGTEtwpMpKnC1mw50EK1810bUpUVMmq9CxqRt+KxvE5deXE3lzAyjAAAAAAD8zEuJLFg7D1yxXie6QW20Wellra6rndwxwQRtVz3uXyIiKoH6YIuMwN9zbLfiSroMtckHXazU8ro4a+6XdaWSpai6I9IWRP4EXtRFcq6KmqIvIubsrb2XA2e2YFvyxzBwI/BN3vUqU1qq46/rlHU1Ll0ZA5VjY6Jzuxqqioq8tUVU1DPoAAAfFVGoqquiIRxZ975PBWX2OLjg3KXLdMa09qndTT3qe6pS0k8jV0d1dGRvWRiLqiPVWouiqiKmiqEjwIzcoN9RhXEuL6GwZt5Tuwtaq6dsC3igui1jKTiXRHyxOjY7gRfjOaqqic+FdCS6nnhqoI6qmlZLDMxJI3sXVrmqmqKi96KgHYAAAI+Npre7YGybx/csucs8ArjissszqS43GS5dUo46hi6SRRKkb3Sq1UVqu+CmqLpqianhstd9pY7tiijteaWTLrDZaqZkU10tl1dVupGquiyOhdE1XtTtXhdxaa6Iq8lCUAFFZbza8RWehv9kroq23XKnjq6SpidxMmhkajmPaveitVFT6StAAGGW2RvLsvtlnE6ZcWPC8mNMYRxNmrqSOtSmprc17UdG2aThe7pHNcjkYjfiqiqqapqGZoIpLBvw6111gbinZ8hjtrnIk77ff1fOxuvNWtfCjXLp3K5v0oSXZUZpYNzpy+suZ2ALktdYr7T9PTSuZwPaqOVr43tX4r2Pa5rk7lavaB60AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXTx9he14426b5gy99L9Tr9mhNbavoX8L+hmuaxv4Xc9F4XLopsWGvVW/wDKLP8A/F9P87oBKpYt1HsZ2K8Ud4bgm8VzqKZk7aetvU8kEjmrqiPZqnE3VObV5L2LyMwGtaxqMY1GtamiIiaIiH0AD8PHVRNSYIxDV00ixzQ2qrkjena1yQuVFT2ofuFBf7W2+WK42Vz+BtwpJqVXeakjFbr9sDXm2E8tsI5zbV+CMEZiWz6q2S41VTUVtI+RzW1KxU8srWvVqoqtV7G6przTVO8ulvW8kctMkdoSzUGVuGKXD1uv+GoLpU0FG3gp2VPWaiJzo2djEVsTNWpy1RV05qWy2ZsUP2WttHDVXj2lfRMwpiaayXpsnbTMeslLNIvlRnGr+XajeXaTm452Y8i818y7NnFmDgahxLfbJbm2+3Or3OnpI4UkfI13V1XopHcUrlRz2u05aaKiKB5zYPTEDdj3KhuJmVLK1MOwI1KhFR/V+J3V+3u6HotP1uhfk4QwxU8TIIImRxRtRjGMaiNa1E0REROxEOYFndojZPya2o6exU2b1or65mHH1D6DqlfJTKxZ0jSTXgX4WvRM7ezT0kUu9E2SsmNlqTLVuUNouFCmJm3hbh1uvkqePq/U+i4eNfg6dPJrp26p5Cbwio35v59kr+xxF923Ad+wTu89mnPrZlw5mjmRYr1W367VVwZPJBdpYI0bDVyRMRrGaInwY017V1VSRTJfI/LPZ+wTDl9lVhxlns8Uz6l7OlfLLPM/Tikkkeque5dETVV5IiImiIiFgd1L8yDBP2Zd/wDOM5l0AAAAAADGzeOsuEmxVmg22MmdKlup1ekSKq9ElXCsirp3cHFr6NTJM6ayjpLhSTUFfSw1NNURuimhmYj2SMcmitc1eSoqLoqKBCbuk8hsqM7M0MaTZq4NocSwYes8EtDR17Vkp2yzSuY57o/ivVGt0TiRUTXXTVEVMfNo/Btmya2uMX4Ry/bNbbdhvFX/ABU1squfStSRskbWuXn8BVREVVVdETVVXmT05ZbMmSmTWOsRZhZX4JpsOXLFMEUFyhoXujpHoxyuRWQa9HEurl14Eai+TXUgt2xaqHEm29mMtlf1rpsZyUkfBz45WSNic1PL8NqoBsSAADx+clRPSZRY4qqWV0U0OHLlJG9q6Oa5KaRUVF8qKQPbunKXAmdO1ZhjBeZFlZd7ElNW181DI9zY6h8EDnxtk4VRXM4kRVbro7TRdUVUWd3Oz5GseerN091kIU90r89PDn7kXX3VwHRvTMk8ucjdpimseWGHobHar7hqkvktvpk4aeCofUVML0iZ2MYqU7XcKckVXaaJoiTC7HFXU1uyhlDU1c75pXYMtKOe9dXLpTMRNV7+SIRab6f51WG/UKh9/uBKLsXfNJyg9TbV7uwC9B0VySLRVCRa8fRP4dO3XRdDvAGunsmZcWLNPbEwdl5mZa5K63XO/wBQy7Ucz3MdMsbJZHRvVFRyavj0doqLpqhd7ew5I5Y5KZ84fpMrcKUeHKC/Ybjr6qgomdHTpUNqJolexicmatYzVE0TVFXTVVJdLrsq5D3XN2yZ6pgChoMbWGeSphulv1pXVD3sexy1DI9GTLo93wnoruznpyIvN9bcaSo2isHW6GZHz0eDolman1nHWVKtRfTomv0KgEi+75rauv2McqZ6yofNI2ydCjnrqqMjnkYxv0I1rUT0IhkMY8bvamnpNjDKmKoidG91lWVEVO1r55XNX2tci+0yHAGu/tL2yLF23fjaw3iaaSC6ZhSW+dyO+H0L6tItEXu0auieTRDYgNevO/8A5QrEn/ignv7QMot7Fsr5GZI5Y5fYrynwLR4Zq/qm6xVCUSuRtXB1d8jXzcSqskqLH+eKvEqOXiVdE0vpuWa2rqdlzEtNUVEkkVJjmsjgY52qRsWhoXq1vkTic5dPK5V7z8Xfa/ITgL1tX3Oc/U3KXzYsW+vlX/m+gAkEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1vs6cWXHAe2DjTG9ohp5a7D+YNddKaOoaronSwV7pGI9Gqiq1VamqIqLp3obIJrt4ssVpxRt83PDV+omVdtuuaslFWU71VGzQSXTgexdNF0VqqnLygZc5Ab0va5z0zlwllTZ8BZePfiC5xU9Q+G2VvFBSovFUTarVKiIyJsj9VRfi9i9hLWW6y72dMiMpbpJfMtMpMLYbuUsawvrLfbY4p1jXtb0iJxIi96IuilxQAAAh83xOzIuFMcW7aQwrbZPqXilW2/EKxt1ZBcWN0ilXyJLG3hXu4olVV1fzyy3Wu05+PpkLDgfEdxfPi3LxkVsq3TO1fU0OipSz69rl4WrG5V1XWPVfjIZL545R4bz2yoxJlTiqPWhxBRPp0lRPhU8yfCimb+uZI1j0/Y6LyVSCnZxzLxhsI7XbYsZw1NJFZrlLh3FdHGvKaic9Gve1Ox6N0ZOxfruFui6OA2EAdNHV01wpIK+inZNT1MbZYpGLq17HJq1yL5FRUU7gBFRvzfz7JX9jiL7tuJVyKjfm/n2Sv7HEX3bcBk7upfmQYJ+zLv8A5xnMujEXdS/MgwT9mXf/ADjOZdAAAAAAAAAW+z/zbtuROTOLs2rpHHLHhu2yVUML38LZ6hdGQQqvdxyvjZ++ISt3vlJfNpPbBtOIb67rFJYK9+M79UPTXpHxzJJG3TsVZKh0eqebxr3GT++lz9je/CuzjY62Xjj0xHfmsdoxUXiZSQu07V/PpFavJNYl+i++6Y2fVym2d25iX21dWxFmNK25OdI3SRltZqlIz0I5HPmTypM3Xs0QM4AAB4vOz5GseerN091kIU90r89PDn7kXX3VxNZnZ8jWPPVm6e6yEKe6V+enhz9yLr7q4D2e+n+dVhv1Coff7gSi7F3zScoPU21e7sIut9P86rDfqFQ+/wBwJRdi75pOUHqbavd2AXoAAHGSSOGN0sr2sYxquc5y6I1E7VVe416s88WYk24ttKtbhVI1di2/Q4fsGuqxxUMbkhhmd36dG1Zn6diufoStb0PPyDJXZgvFioKiRmIMweLDtu6J3C6OF7daqZe/hSHiZqnPilZ3amIW5j2fnX3GuItofEFpa+hw7EtmsMsrdda6VutRIz0xwqjNf+8LpzRdAlay/wAGWvLrAuHsA2Ti+p+HLZTWumVyaKscMbY2qvpVG6qegAAGvXnf/wAoViT/AMUE9/abChr153/8oViT/wAUE9/aBn5vtfkJwF62r7nOfqblL5sWLfXyr/zfQH5e+1+QnAXravuc5+puUvmxYt9fKv8AzfQASCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa9Vb/yiz/8AxfT/ADuhsKmufmNiygwFtw4hxxdKeeejw9mbUXSoigRFkfHBclkc1iKqJxKjVRNVRNe8DYwBHr+XW7OP6m+Yn8Wov9oMu9nLPmy7SeWFHmvhvC19sdouNRPBRR3iOJk1QyJ3A6ZqRvenAr0e1FVddWLy00VQueUd5ucFktFdeaprnQ0FNLVSI3tVrGq5dPTohWH4uNaOouODb9b6ONZJ6q2VUMTE7XPdE5ETxVAIydmveO54bTW2Xg7BM31Pw3garqLgv1GooGvkqI20szousTvRXuc1WtX4HA1VT4vZpZXfJYNtWHNqq3X+2xpHLijCtHX1qInxqiOaen4vbHDEn0ovlLB7HWbGHtnbafwnmJmDTVsdqsNXU09ybTw8c0CSQyQq7g5KvC5+qp26Iumq8i4e8r2lsuNpzPO04oysqKyrsdjw9DaErKmmfTrUzJUTzOcyN6I9GokzW/CRFVUdy001CX7YXxRcsY7IeVN8u8iyVX4nYKNz1XVXtpldTtcq96q2JqqvlVS+pYHYIsV1w3sdZVWu80j6aq+oLKpYnpo5I55HzRqqd2rJGrp6S/wAio35v59kr+xxF923Eq5FRvzfz7JX9jiL7tuAyd3UvzIME/Zl3/zjOZdGIu6l+ZBgn7Mu/wDnGcy6AAGEW8C298dbH+L8J4cwjgixX2LEFtnrppLjJM10bmSoxGt6NyctOfMDN0FqtlrOK55/ZCYRzdvNnprVW4hpZJp6Sme58Ub2TPjXhV3PReDXn2a6aqXVAH5+Ib9acK2C5Ynv1bHR2y0Uk1dW1Mi6MhgiYr5Hu9CNaqr9B+gY5bxK9Vdi2Ls0qyierJJrTHROVF0/M56iKF6e1kjk9oEO1hpcSbfW25G27z1asx1iF01S5ifDobPCiu4G9zejpYkY1V+uRNdVXnsGWq12+yWujstppWU1FQQR01NBGmjYomNRrGonkRERPYQDbvzajyy2TcyMQ5gZhYVvd6mr7Olrt/1LZC58HFM18qu6V7NNUjYnLVe0z0/LsNnr9TDMH/FUX+vAkPB5PKfMGnzXy3w7mTR2K42amxJQR3GnorijEqI4ZE1jV6Mc5qK5vC7RFXk5NdF1Q9YB4vOz5GseerN091kIU90r89PDn7kXX3VxNpm1bq275V4ytNup3T1dbh+4U8ETE1dJI+ne1rU9KqqIQD7B+eODtnPaYw9mRmG2tZYqaCsoaySlhWWSnSaF0aSdGnNyNcqaonPTXRFVNFC+++n+dVhv1Coff7gSi7F3zScoPU21e7sIZd49tHYA2nNoSDGuWb6uew2jD9LY4aupgdCtW6OeomdK2NyI5rdajhRHIi/A10TUmn2RrPc8P7LmU9mvNHLSV1Lg+1MnglarXxP6sxVa5F5o5NdFReaLyAu2AdNZK6Cknmb2xxucn0omoEFu9Gzsr889qmowFhmpnuNpwMqYbt1LTor0luKvTrasanNXrNww8u3oG6emYDZXyQtuzvkLhHKuhj/4TbaFs1zlVdVmuE35pUv18nSOcjfI1rU7iAzJ3NjD2Btp6xZ05iWyuu1BacTPxBW09IjHTyzNkfKxW8bmtVUl4Hc1TsUk8/LsNnr9TDMH/FUX+vAkPBZbZX2osM7WGB7hmBg7CN/slqobi62Mdd2wtdUSNYx71jSN79WpxtRVXTnqncpekAa9ed//AChWJP8AxQT39psKGvBtVVdTgDbrx1fLxb5kW1Y7ddnQacLpIesNnZpr5zFaqL5HIoEg++1+QnAXravuc5+puUvmxYt9fKv/ADfQGM+8024Mldp3L3A+DspKm6V0tDcX3m4y1dE+mSkXoXRtgVH/AB3/AJo5VVqq1OFNHLryym3MVjutq2Vb3cLhQywU94xpW1dC97dEnhbSUkKvb5W9JFI3XysXyAZ7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYLY+3Q2z9mHji/wCPLtjrHUFbiK5VN0qIqeppUiZJNI6RzWIsCrworl01VV0M6QBH3BuWNmqOZj5seZgyxtciuZ1ykbxJ3pqlPyM6MD4Kwzlxg+z4DwbbGW+yWKjjoaGma5XJHCxNGornKquXvVVVVVVVV5qfuAAAAMMNordYZB5+Y3rMxaS7XnBl7usiz3P6kpE+mrJ3KqumdFIi8MjtfhK1URy81TiVVXyuV25y2fMDYsosTYtxXiLGcFBKk0drrmwwUkr05p0yRpxPbrz4eJEXTRdU1Rc+QB1wQQ0sEdNTRMihiajI42NRGsaiaIiInYiIdgAAx/2sdi3LHa/p8NR5hXm/WybCr6taKa0zxsVzahIula9JGPRU1gjVFTRU0XymQAAtvs85F4Y2b8qLTlFg65XKvtVokqZYp7i9jp3LNO+Z3ErGtbyc9UTROxELkAADHXar2GcpNrqvsN3zBuuIbZX4ehmpqee01MUfSRSOa5WvbJG9F0c3VFTRea668tMigB4XJDKHDuQ2VthymwpW19XasPQvhp5657HTvR8jpFV6sa1uur17ETloe6AAHgs9MncPZ/ZVX7KPFdfX0VqxBHFHUT0LmNnYkczJU4Ve1zfjRp2ovLU96AI9vylTZx/VEzA/jNJ/qD9HD25m2YrPe6K6XTFGN7zS0szZZKCpradkNSjV16N6xwtfwr2LwuaunYqGfAA6KGipLbRU9ut9NHT0tLE2GCGNqNZHG1ERrWonJERERET0HeAAMJc/d0/kBnXjiuzBtN6veCrndpXVNxhtaRSUlRO5VV8qRSJ+Zvcq6rwqjVXnw6qqrm0AMEcnt0Ds95a4xosYYoxHfsbrbpUnprdcWxQ0ayN5tdKyNOKREXnwq5Grp8JFTkZ2tRGojWoiIiaIiH0ADrnibPDJA9VRsjVYunboqaHYAI+p9y1s5TzyTrmHmAiyPV6olTSctV1/6A4N3KuzgjkV2YeYKprzRKmkTX/IEhAA8Zk/lFgbIvLu0ZYZdWtaGx2aJWQte/jlle5yuklkevxnvcquVfKuiIiIiJ7MAAYz7VmwFkntYV1LiTFC3CwYopIkp0vVpViSzwp8WOdj0VsiN7l5OTs4tORkwAI57BuT8j6C7U9Zf81cYXWiiejpaNkVPTdMiL8VZEa5URexdNF8ioZ/YLwXhbLvCtswTgqyU1osdnp201DRU7dI4Y07k15quuqqqqqqqqqqqqftAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoFcCh67L5rPBR12XzWeCgVwKHrsvms8FHXZfNZ4KBXAoeuy+azwUddl81ngoH//Z', 3, 1, 0, '', '', 0, '', 'ya29.a0Ae4lvC2_Ifzaor-wGVJ3lTe9wi9Zi7dZkw2kkZgQ7hU2t6lxKIJ2ST7IspeX3QdkVnf5EYAqQ9A9XGwJjM4twzL-HnWGcQgdPwy9kacz5S74hPiIoO7VNwq2r5RhNDc8e_w9ZWxLqhhAQJTCALO98OECyUiblpVDFJg', '1//03iC0tACZRIe9CgYIARAAGAMSNwF-L9IrukR6DlLzcs0J1RHkl45UK7jrmXcHbagL4h70vm06Yxu_kQDjfRge0nsHSHFCQMGttcE', 1, '1586384415', 0, '2020-01-22 00:59:42', '2020-04-08 21:20:16');
INSERT INTO `users` (`userId`, `email`, `password`, `firstName`, `lastName`, `phone`, `gender`, `image`, `typeId`, `isActive`, `isDeleted`, `resetToken`, `activationToken`, `createdUserId`, `fcm-tokenid`, `google_event_access_token`, `google_event_refresh_token`, `google_event_active`, `expiry_google_event_token`, `modifiedUserId`, `createdAt`, `modifiedAt`) VALUES
(29, 'nabeelzafar@zeikh.com', 'd5eabfae3b9f7c0aef723321d06635d5f6b7a5867b87f6c1c88b432ad17ce5fd2adf15d69baf87871febaa83e4663ed544d48f3c0850cfb21cd1118354ba5fa79ZKQT3iN--3ity6fvaiM--18lOZj2RjE76--1lfWQmbuQjAuA--2', 'Nabeel', 'Zafar', '', 0, '', 3, 1, 0, '', 'fbPxd5hoCDI:APA91bHXWUB_TSKmRNZBemaY9-TXIgp34F9uJsLrrG9tX046JCP7qJnwNF8Y0yCfgxWKtBQi5FByf13lCnEJnuBBbMN1njyPAxa9Rokh0IjipVC9Y7c62Zi-VIc841mr0ug1fQMjIKuZ', 0, '', '', '', 0, '', 0, '2020-03-10 16:38:54', '2020-03-22 16:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_abundance_list`
--

CREATE TABLE `user_abundance_list` (
  `id` int(11) NOT NULL,
  `abundance_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_abundance_list`
--

INSERT INTO `user_abundance_list` (`id`, `abundance_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(1, 5, 8, 0, '2020-04-07 04:37:42'),
(2, 6, 8, 0, '2020-04-07 04:37:48'),
(3, 10, 8, 0, '2020-04-08 04:26:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_bukcet_list`
--

CREATE TABLE `user_bukcet_list` (
  `id` int(11) NOT NULL,
  `bucketlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_bukcet_list`
--

INSERT INTO `user_bukcet_list` (`id`, `bucketlist_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(5, 3, 8, 0, '2020-04-07 19:44:42'),
(6, 2, 8, 0, '2020-04-07 19:44:42'),
(10, 9, 8, 0, '2020-04-07 19:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_healthandbeauty_list`
--

CREATE TABLE `user_healthandbeauty_list` (
  `id` int(11) NOT NULL,
  `healthandbeauty_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_healthandbeauty_list`
--

INSERT INTO `user_healthandbeauty_list` (`id`, `healthandbeauty_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(1, 11, 8, 0, '2020-04-08 04:34:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_love_list`
--

CREATE TABLE `user_love_list` (
  `id` int(11) NOT NULL,
  `love_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_love_list`
--

INSERT INTO `user_love_list` (`id`, `love_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(1, 4, 8, 0, '2020-04-05 19:48:16'),
(3, 2, 8, 0, '2020-04-05 19:48:30'),
(4, 16, 8, 0, '2020-04-08 21:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_personalaffirmation_list`
--

CREATE TABLE `user_personalaffirmation_list` (
  `id` int(11) NOT NULL,
  `personalaffirmation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_personalaffirmation_list`
--

INSERT INTO `user_personalaffirmation_list` (`id`, `personalaffirmation_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(1, 4, 8, 0, '2020-04-07 03:57:59'),
(2, 5, 8, 0, '2020-04-07 03:58:07'),
(3, 6, 8, 0, '2020-04-07 03:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_selflovecare_list`
--

CREATE TABLE `user_selflovecare_list` (
  `id` int(11) NOT NULL,
  `selflovecare_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_selflovecare_list`
--

INSERT INTO `user_selflovecare_list` (`id`, `selflovecare_id`, `user_id`, `isDeleted`, `createdAt`) VALUES
(22, 3, 1, 0, '2020-04-05 19:17:20'),
(23, 8, 1, 0, '2020-04-05 19:18:02'),
(40, 6, 8, 0, '2020-04-09 02:22:40');

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE `vacancies` (
  `vacancyId` int(11) NOT NULL,
  `vacancyName` varchar(255) NOT NULL,
  `vacancyExp` varchar(255) NOT NULL,
  `vacancyQual` varchar(255) NOT NULL,
  `vacancyApplyEmail` varchar(255) NOT NULL,
  `vacancyArea` varchar(255) NOT NULL,
  `vacancySkill1` varchar(255) NOT NULL,
  `vacancySkill2` varchar(255) NOT NULL,
  `vacancySkill3` varchar(255) NOT NULL,
  `vacancySkill4` varchar(255) NOT NULL,
  `vacancySkill5` varchar(255) NOT NULL,
  `vacancyIsActive` int(11) NOT NULL,
  `vacancyCreatedUserId` int(11) NOT NULL,
  `vacancyIsDeleted` int(11) NOT NULL,
  `vacancyOrderId` int(11) NOT NULL,
  `vacancyCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vacancyModifiedUserId` int(11) NOT NULL,
  `vacancyModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`vacancyId`, `vacancyName`, `vacancyExp`, `vacancyQual`, `vacancyApplyEmail`, `vacancyArea`, `vacancySkill1`, `vacancySkill2`, `vacancySkill3`, `vacancySkill4`, `vacancySkill5`, `vacancyIsActive`, `vacancyCreatedUserId`, `vacancyIsDeleted`, `vacancyOrderId`, `vacancyCreatedAt`, `vacancyModifiedUserId`, `vacancyModifiedAt`) VALUES
(1, 'mel', '2 years', 'BS', 'test@test.com', 'melbourne', 'skill 1', 'skill 2', 'skill 3', 'skill 4', 'skill 5', 1, 0, 0, 1, '2019-03-21 05:49:35', 0, '2019-03-21 05:49:35'),
(2, 'darwin', 'exp', 'qual', 'abc@xyz.com', 'darwin', '', '', '', '', '', 1, 0, 0, 2, '2019-03-21 08:29:26', 0, '2019-03-21 08:29:26'),
(3, 'tiwi', 'exp', 'MS', 'abc@xyz.com', 'tiwiIsland', '', '', '', '', '', 1, 0, 0, 0, '2019-03-21 09:58:31', 0, '2019-03-21 09:58:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abundance_list`
--
ALTER TABLE `abundance_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blogId`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`blogCategoryId`);

--
-- Indexes for table `bucket_list`
--
ALTER TABLE `bucket_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calendar_dailyaffirmation`
--
ALTER TABLE `calendar_dailyaffirmation`
  ADD PRIMARY KEY (`month`);

--
-- Indexes for table `career_page`
--
ALTER TABLE `career_page`
  ADD PRIMARY KEY (`careerPageId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `category_pages`
--
ALTER TABLE `category_pages`
  ADD PRIMARY KEY (`categoryPageId`);

--
-- Indexes for table `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`directorId`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`fileId`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`footerId`);

--
-- Indexes for table `footer_menus`
--
ALTER TABLE `footer_menus`
  ADD PRIMARY KEY (`footerMenuId`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`galleryId`);

--
-- Indexes for table `gratitude_journal`
--
ALTER TABLE `gratitude_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`headerId`);

--
-- Indexes for table `header_menus`
--
ALTER TABLE `header_menus`
  ADD PRIMARY KEY (`footerMenuId`);

--
-- Indexes for table `healthandbeauty`
--
ALTER TABLE `healthandbeauty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highest_good_of_all`
--
ALTER TABLE `highest_good_of_all`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_data`
--
ALTER TABLE `home_data`
  ADD PRIMARY KEY (`homeDataId`);

--
-- Indexes for table `home_sliders`
--
ALTER TABLE `home_sliders`
  ADD PRIMARY KEY (`homeSliderId`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`loginId`);

--
-- Indexes for table `love`
--
ALTER TABLE `love`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`mediaId`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`menuId`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`);

--
-- Indexes for table `page_sections`
--
ALTER TABLE `page_sections`
  ADD PRIMARY KEY (`pageSectionId`);

--
-- Indexes for table `page_section_categories`
--
ALTER TABLE `page_section_categories`
  ADD PRIMARY KEY (`pageSectionCategoryId`);

--
-- Indexes for table `personalaffirmation`
--
ALTER TABLE `personalaffirmation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positiveAffirmationDashboard`
--
ALTER TABLE `positiveAffirmationDashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positive_affirmations_list`
--
ALTER TABLE `positive_affirmations_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positive_affirmations_user_list`
--
ALTER TABLE `positive_affirmations_user_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selflovecare`
--
ALTER TABLE `selflovecare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selflovecare_dashboard`
--
ALTER TABLE `selflovecare_dashboard`
  ADD PRIMARY KEY (`month`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`subscriberId`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonialId`);

--
-- Indexes for table `thirdsection`
--
ALTER TABLE `thirdsection`
  ADD PRIMARY KEY (`testimonialId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `user_abundance_list`
--
ALTER TABLE `user_abundance_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_bukcet_list`
--
ALTER TABLE `user_bukcet_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_healthandbeauty_list`
--
ALTER TABLE `user_healthandbeauty_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_love_list`
--
ALTER TABLE `user_love_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_personalaffirmation_list`
--
ALTER TABLE `user_personalaffirmation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_selflovecare_list`
--
ALTER TABLE `user_selflovecare_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`vacancyId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abundance_list`
--
ALTER TABLE `abundance_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `blogCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bucket_list`
--
ALTER TABLE `bucket_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `career_page`
--
ALTER TABLE `career_page`
  MODIFY `careerPageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `category_pages`
--
ALTER TABLE `category_pages`
  MODIFY `categoryPageId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `directorId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `fileId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `footerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `footer_menus`
--
ALTER TABLE `footer_menus`
  MODIFY `footerMenuId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `galleryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gratitude_journal`
--
ALTER TABLE `gratitude_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `header`
--
ALTER TABLE `header`
  MODIFY `headerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `header_menus`
--
ALTER TABLE `header_menus`
  MODIFY `footerMenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `healthandbeauty`
--
ALTER TABLE `healthandbeauty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `highest_good_of_all`
--
ALTER TABLE `highest_good_of_all`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `home_data`
--
ALTER TABLE `home_data`
  MODIFY `homeDataId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_sliders`
--
ALTER TABLE `home_sliders`
  MODIFY `homeSliderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `loginId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `love`
--
ALTER TABLE `love`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `mediaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `menuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_sections`
--
ALTER TABLE `page_sections`
  MODIFY `pageSectionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `page_section_categories`
--
ALTER TABLE `page_section_categories`
  MODIFY `pageSectionCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `personalaffirmation`
--
ALTER TABLE `personalaffirmation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `positiveAffirmationDashboard`
--
ALTER TABLE `positiveAffirmationDashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `positive_affirmations_list`
--
ALTER TABLE `positive_affirmations_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `positive_affirmations_user_list`
--
ALTER TABLE `positive_affirmations_user_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `selflovecare`
--
ALTER TABLE `selflovecare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `subscriberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonialId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `thirdsection`
--
ALTER TABLE `thirdsection`
  MODIFY `testimonialId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_abundance_list`
--
ALTER TABLE `user_abundance_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_bukcet_list`
--
ALTER TABLE `user_bukcet_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_healthandbeauty_list`
--
ALTER TABLE `user_healthandbeauty_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_love_list`
--
ALTER TABLE `user_love_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_personalaffirmation_list`
--
ALTER TABLE `user_personalaffirmation_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_selflovecare_list`
--
ALTER TABLE `user_selflovecare_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `vacancies`
--
ALTER TABLE `vacancies`
  MODIFY `vacancyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
